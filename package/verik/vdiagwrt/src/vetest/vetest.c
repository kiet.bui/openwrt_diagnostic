#include "common.h"
#include "diag_ddr.h"

void usage()
{
	printf("veDiag program, should follow format:\n");
	printf("Command: vetest <command> [option-1] ... [option-n]\n");
	printf("\tcommand: ddr i2c led pci\n");
	printf("\toption:\n");
	printf("\t\tddr-start-address\n");
	printf("\t\tddr-end-address\n");
	printf("\t\tddr-execution-counter\n");
	// tobe continue 
}

int run_ddr_diag(int argc, char *argv[])
{
	int status = 0;
	if (ddr_diag_start(argc, argv) != RET_OK) {
		printf("[diag] diag-ddr test failed or invalid command.\n");
		ddr_diag_usage();
		return RET_GEN_ERR;
	}

	return RET_OK;
}

int run_led_diag(int argc, char *argv[])
{
	return RET_OK;
}

int run_i2c_diag(int argc, char *argv[])
{
	return RET_OK;
}

int run_pci_diag(int argc, char *argv[])
{
	return RET_OK;
}

int main(int argc, char *argv[])
{
	int ret = 0;
	printf("[diag] execute vetest testing !\n");
	if (argc < 2) {
		goto __error;
	}
	if (strcmp(argv[1], "ddr") == 0) {
		ret = run_ddr_diag(argc, argv);
		if (ret != RET_OK)
			goto __error;

	} else if (strcmp(argv[1], "led") == 0) {
		ret = run_led_diag(argc,argv);
		if (ret != RET_OK)
			goto __error;

	} else if (strcmp(argv[1], "i2c") == 0) {
		ret = run_i2c_diag(argc,argv);
		if (ret != RET_OK)
			goto __error;

	} else if (strcmp(argv[1], "pci") == 0) {
		ret = run_pci_diag(argc,argv);
		if (ret != RET_OK)
			goto __error;

	} else {
		goto __error;
	}
	return RET_OK;

__error:
	usage();
	return RET_GEN_ERR;
}
