
#include "diag_ddr.h"

#if 0
#if defined(CONFIG_VEDIAG_MEMTESTER)
#include "../apps/memtester-4.3.0/types.h"
#include "../apps/memtester-4.3.0/tests.h"

struct test mem_tests[] = {
    { "Random Value", test_random_value },
    { "Compare XOR", test_xor_comparison },
    { "Compare SUB", test_sub_comparison },
    { "Compare MUL", test_mul_comparison },
    { "Compare DIV",test_div_comparison },
    { "Compare OR", test_or_comparison },
    { "Compare AND", test_and_comparison },
    { "Sequential Increment", test_seqinc_comparison },
    { "Solid Bits", test_solidbits_comparison },
    { "Block Sequential", test_blockseq_comparison },
    { "Checkerboard", test_checkerboard_comparison },
    { "Bit Spread", test_bitspread_comparison },
    { "Bit Flip", test_bitflip_comparison },
    { "Walking Ones", test_walkbits1_comparison },
    { "Walking Zeroes", test_walkbits0_comparison },
#ifdef TEST_NARROW_WRITES
    { "8-bit Writes", test_8bit_wide_random },
    { "16-bit Writes", test_16bit_wide_random },
#endif
    { NULL, NULL }
};

#endif

#define DDR_DEFAULT_CPU_NUM				0		/* Use 6 core for DDR stress test */
#define CONFIG_KERNEL_MEMORY_USED 		80000000ULL
#define MEMORY_REMAIN_MINIMUM			0x06400000	/* minimum remain 100MB for the others */

#define MEMORY_THREAD_NUMBER_KEY		"threads"
#define MEMORY_REMAIN_SIZE_KEY			"free"
#define MEMORY_TEST_SIZE_KEY			"size"
#define MEMORY_CHUNK_SIZE_KEY			"chunk_size"
#define MEMORY_CORE_NUMBER_KEY			"core_number"
#define MEMORY_RUNNING_TIME_KEY			"time"

typedef struct memory_test {
	pthread_mutex_t mutex;
	void volatile *mem_start;
	void volatile *aligned;
	size_t mem_size;
    u32 check_flag;
	struct list_head memory_list;
} memory_test_t;

struct vediag_memory_test_info {
	pthread_mutex_t mutex;
	struct list_head mem_reserved;
	struct list_head *current_entry;
};

/*Specific defines*/
#if defined(CONFIG_VEDIAG_MEMTESTER)
#define CONFIG_DDR_TEST_MODEL_MAX 		1
#else
#define CONFIG_DDR_TEST_MODEL_MAX 		8
#endif
#define CONFIG_MEM_MAX_ERROR			1
#define CONFIG_DDR_MAX_WARNING			1
#define CONFIG_DDR_STEP_SIZE			0x80000	/* 1 MB*/
#define CONFIG_MEMORY_TEST_DISTANCE		0x1400000	/* Step 20MB */
//#define CONFIG_DDR_STEP_SIZE		0x400000	/* 4 MB*/
//#define CONFIG_DDR_STEP_SIZE		0x2000000	/* 32 MB*/

/*#define CONFIG_DDR_TEST_DEFAULT_SIZE		0x2000000*/
#define DDR_DEFAULT_TEST_SIZE			~0ULL
#if defined(CONFIG_VEDIAG_MEMTESTER)
#define DDR_DEFAULT_TEST_MODE			2			/*All tests*/
#else
#define DDR_DEFAULT_TEST_MODE			9			/*All tests*/
#endif
#define CONFIG_DDR_TEST_DEFAULT_LOOP	1

#define CONFIG_DDR_TEST_FULL_SIZE		0x80000000	/*4GB*/
#if defined(CONFIG_VEDIAG_MEMTESTER)
#define CONFIG_DDR_TEST_FULL_NUMBER		2			/*All tests*/
#else
#define CONFIG_DDR_TEST_FULL_NUMBER		9			/*All tests*/
#endif
#define CONFIG_DDR_TEST_FULL_LOOP		1

/*If CPU still idle, increase to make test become more effective*/
#ifndef DDR_DEFAULT_THREAD_NUMBER
#define DDR_DEFAULT_THREAD_NUMBER 		6
#endif

#ifndef DDR_DEFAULT_CORE_NUMBER
#define DDR_DEFAULT_CORE_NUMBER 		32
#endif

#ifdef CONFIG_VEDIAG_SLT
#define DDR_DEFAULT_RUNNING_TIME		180
#else
#define DDR_DEFAULT_RUNNING_TIME		0
#endif

#define printhex(variable) xinfo(" %s = 0x%X\n", #variable, (u32) (variable))
#define printdec(variable) xinfo(" %s = %d\n", #variable, (u32) (variable))
#define cmd_ddrc_print(fmt,args...)
#define cmd_ddrc_printhex(variable)
#define cmd_ddrc_printdec(variable)
#define mem_dbg(fmt,args...)
#define mem_print(fmt,args...)

#define DDR_MAX_TEST_CHUNK				0x400000	 	//4MB
#define DDR_DEFAULT_TEST_CHUNK			0x100000		//1MB

enum {
	MEM_MEMTESTER = 0,
	MEM_STRESSAPP = 1,
};

/*#define CONFIG_MEMORY_LOCK*/
static int memory_lock_enable = 1;

struct mem_test_argument {
    u64 remain_size;
    int thread_number;
    int time;
};

int ddr_diag_help(void)
{
	printf("\nDDR test:\n");
	printf("  diag ddr [opt ...]");
	printf("  Options: \n");
	printf("    -h : print help\n");
	printf("    -s <size>   : test size in hex, will be recounted, default is full\n");
	printf("    -c <chunk size> : Chunk/Packet-size\n");
	printf("    -t <thread> : number thread use to test\n");
	printf("    -f          : test full\n");
	printf("    -cpu <core numbers>  : number of cores used to run test\n");

	return 0;

}

static int mem_max_error = CONFIG_MEM_MAX_ERROR;
static int mem_error = 0;
static int mem_max_warning = CONFIG_DDR_MAX_WARNING;
static int mem_warning = 0;

#ifdef CONFIG_RAS
int diag_mem_set_mc_error ( struct ras_mc_event *ras_mc_event )
{
    xdebug("vediag_mem_set_mc_error:\n");
    xdebug("    type    : %s\n", ras_mc_event->error_type);
    xdebug("    message : %s\n", ras_mc_event->msg);
    xdebug("    label   : %s\n", ras_mc_event->label);
    xdebug("    count   : %d\n", ras_mc_event->error_count);
    xdebug("    driver  : %s\n", ras_mc_event->driver_detail);

    if (!strncasecmp(ras_mc_event->error_type, "corrected", 9)
        || !strncasecmp(ras_mc_event->error_type, "info", 4)) {
        mem_warning += ras_mc_event->error_count;
        xinfo("Mem Warning: %d\n", mem_warning);
    }
    else if (!strncasecmp(ras_mc_event->error_type, "uncorrected", 11)
             || !strncasecmp(ras_mc_event->error_type, "fatal", 5)) {
        mem_error += ras_mc_event->error_count;
        xerror("Mem Error: %d\n", mem_error);
    } else {
        xerror("Unknown Error Type %s\n", ras_mc_event->error_type);
        return -1;
    }
    return 0;
}

int diag_mem_set_aer_error ( struct ras_aer_event *ras_aer_event )
{
    xdebug("vediag_mem_set_aer_error:\n");
    xdebug("    type    : %s\n", ras_aer_event->error_type);
    xdebug("    message : %s\n", ras_aer_event->msg);
    xdebug("    device  : %s\n", ras_aer_event->dev_name);

    return 0;
}

#else

int diag_set_mem_error ( int error )
{
    mem_error += error;
    xinfo("DDR Error[%d] \n", mem_error);
    return 0;
}

int diag_set_mem_warning ( int warning )
{
    mem_warning += warning;
    xinfo("DDR Warning[%d] \n", mem_warning);
    return 0;
}

#endif

static memory_test_t * get_data_test_exec(struct list_head *head)
{
	struct list_head *list;
	memory_test_t *test;

	for(list = head; list->next != head; list = list->next) {
		test = container_of(list, struct memory_test, memory_list);
		if(!IS_ERR_OR_NULL((void *)test)) {
			return test;
		}
	}
	return NULL;
}

static int mem_test_detail (memory_test_t *mem_entry)
{
#if defined(CONFIG_VEDIAG_MEMTESTER)
	int i;
	size_t halflen, count;
	ulv *bufa, *bufb;
	void volatile *buf, *aligned;
	ul testmask = 0;
#endif

	int ret = 0;

	if(IS_ERR_OR_NULL((void *)mem_entry) || IS_ERR_OR_NULL((void *)mem_entry->mem_start)) {
		ret = 1;
		goto exit;
	}
	pthread_mutex_lock(&mem_entry->mutex);

	mem_dbg("Testing:@%p, 0x%p -> 0x%p\n", mem_entry, mem_entry->mem_start, (ulong*)((ulong)mem_entry->mem_start + mem_entry->mem_size));

#if 0
	if (memory_lock_enable) {
		/* Try mlock */
		if (mlock((void *) mem_entry->aligned, mem_entry->mem_size) < 0) {
			mem_dbg("Mem test: try mlock 0x%p size 0x%llx ... failed (%d)\n", (void *) mem_entry->aligned, mem_entry->mem_size, errno);
			ret = 1;
			goto exit;
		}
		else {
			mem_dbg("Mem test: try mlock 0x%p size 0x%llx ... locked\n", (void *) mem_entry->aligned, mem_entry->mem_size);
		}
	}
#endif

#if defined(CONFIG_VEDIAG_MEMTESTER)
	buf = (void volatile *) mem_entry->mem_start;
	aligned = (void volatile *) mem_entry->aligned;

    halflen = mem_entry->mem_size / 2;
    count = halflen / sizeof(ul);
    bufa = (ulv *) aligned;
    bufb = (ulv *) ((size_t) aligned + halflen);

	if(test_stuck_address(aligned, mem_entry->mem_size / sizeof(ul))) {
		ret = 1;
		goto exit;
	}
    for (i = 0; ; i++) {
        if (!mem_tests[i].name) break;
        /* If using a custom testmask, only run this test if the
           bit corresponding to this test was set by the user.
         */
        if (testmask && (!((1 << i) & testmask))) {
            continue;
        }
        mem_dbg("%s\n", mem_tests[i].name);
        if (mem_tests[i].fp(bufa, bufb, count)) {
    		ret = 100 + i;
    		vediag_err(VEDIAG_MEM, NULL, "%s failed.\n", mem_tests[i].name);
    		goto exit;
        }
    }
#endif

	/*Check continue testing condition*/
	if ((mem_max_error > 0) && (mem_error >= mem_max_error)) {
		/*ERROR: not allow to test*/
	    xinfo("< DDR > Over maximum errors[%d] !\n", mem_error);
		ret = 1;
		goto exit;
	}
	/*Check continue testing condition*/
	if ((mem_max_warning > 0) && (mem_warning >= mem_max_warning)) {
		/* Max Warning: not allow to test*/
	    xinfo("< DDR > Over maximum warning[%d], check log\n", mem_warning);
		ret = 1;
		goto exit;
	}

exit:
#if 0
	if (memory_lock_enable) {
		munlock((void *) mem_entry->aligned, mem_entry->mem_size);
		mem_dbg("Mem test: munlock 0x%p size 0x%llx\n", (void *) mem_entry->aligned, mem_entry->mem_size);
	}
#endif
	pthread_mutex_unlock(&mem_entry->mutex);
	return ret;
}

#define TEXT_LINE_SIZE					1024
#define Megabyte						1024*1024
#define Megabyte_shift					20
int stressapptest_execute(void * __data) 
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	char *cmd = NULL, *result = NULL, *result_done = NULL;
    int test_done = 0, ret = 0;
    char *apppath = STRESSAPPTEST_LOCAL;
    FILE *devf;
    u32 size_test = info->chunk_size >> 20;

    cmd = malloc(TEXT_LINE_SIZE);
    if (!cmd) {
        vediag_err(VEDIAG_MEM, NULL, "Alloc 'cmd' fail\n");
        ret = -1;
        goto exit;
    }

    result_done = malloc(TEXT_LINE_SIZE);
    if (!result_done) {
        vediag_err(VEDIAG_MEM, NULL, "Alloc 'result_done' fail\n");
        ret = -1;
        goto exit;
    }
    memset(cmd, 0, sizeof(TEXT_LINE_SIZE));

    if (apppath && strlen(apppath)) {
        sprintf(cmd, "%s -s 30 -M %d --stop_on_errors",
            apppath, size_test); /**/
    } else {
        vediag_err(VEDIAG_MEM, NULL, "%s Not found stressapptest for test\n", apppath);
        ret = -1;
        goto exit;
    }

    vediag_debug(VEDIAG_MEM, NULL, "\n%s stressapptest cmd: %s\n", apppath, cmd);

    memset(result_done, 0, sizeof(TEXT_LINE_SIZE));
	sprintf(result_done, "Stats: Completed:");

    devf = popen(cmd, "r");
    if (devf != NULL) {
        while (1) {
            char *line;
            char buf[TEXT_LINE_SIZE];
            memset(buf, 0, sizeof(buf));
            line = fgets(buf, sizeof(buf), devf);

            if (line == NULL) break;
            vediag_debug(VEDIAG_MEM, NULL, "%s \n", line);

            if (test_done == 0 && strncmp(line + 25, result_done, strlen(result_done)) == 0) {
                test_done = 1;
                result = malloc(strlen(line) + 256);
                if (!result) {
                    vediag_err(VEDIAG_MEM, NULL, "Alloc 'result' fail\n");
                    ret = -1;
                    goto exit;
                }else {
                    memset(result, 0, strlen(line) + 256);
                    strcpy(result, line);
                }
            }
        }
        pclose(devf);
    } else {
        vediag_err(VEDIAG_MEM, NULL, "Cannot run test with %s\n", cmd);
        ret = -1;
        goto exit;
    }

    if (test_done) {
        int i = 0;
        char *ptr = NULL, *spd_s = NULL;
        char speed[TEXT_LINE_SIZE];
        
        vediag_info(VEDIAG_MEM, NULL, "%s\n", result);
        for(i = 0; i < strlen(result); i++) {
            ptr = result + i;
            if (strncmp(ptr, "Completed: ", 11) == 0) {
            	spd_s = ptr + 11;
            }
            if ((spd_s != NULL) && (strncmp(ptr, ".00M ", 5) == 0)) {
            	strncpy(speed, spd_s, ptr - spd_s);
            	test_ctrl->tested_size += strtoul(speed, NULL, 0) << 20;
            	vediag_info(VEDIAG_MEM, NULL, "size %s %lld\n", speed, test_ctrl->tested_size);
            }
            if ((strncmp(ptr, "incidents, ", 11) == 0) || (strncmp(ptr, ", with ", 7) == 0)) {
                vediag_debug(VEDIAG_MEM, NULL, "%s\n", ptr);
                if ((strncmp(ptr + 11, "0", 1) == 0) || (strncmp(ptr + 7, "0", 1) == 0)) {
                    vediag_debug(VEDIAG_MEM, NULL, "TEST PASS\n");
                    goto exit;
                }else {
                    vediag_err(VEDIAG_MEM, NULL, "TEST FAIL\n");
                    ret = -1;
                    goto exit;
                }
            }
        }
    } else {
        vediag_err(VEDIAG_MEM, NULL, "Test run ERROR\n");
        ret = -1;
        goto exit;
    }

exit:
    if (cmd)
        free(cmd);

    if (result_done)
        free(result_done);

    if (result)
        free(result);

    return ret;
}

static void *ddr_diag_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct vediag_memory_test_info *mem_list = (struct vediag_memory_test_info *) info->data;
    int test_done = 0;
    int ret;
    u32 flag = 0;
    memory_test_t *mem_entry;

    // Clear previos error
    mem_warning = 0;
    mem_error = 0;
    while (!test_done) {
        if(test_ctrl->data_stop) {
        	test_done = 1;
        	continue;
        }

        if(info->test_app == MEM_MEMTESTER) {
	    	/* Get memory entry */
	        pthread_mutex_lock(&mem_list->mutex);
	    	if(mem_list->current_entry == &mem_list->mem_reserved) {
	    		mem_list->current_entry = mem_list->current_entry->next;
	    	}
	    	mem_entry = get_data_test_exec(mem_list->current_entry);
            if(mem_entry == NULL) {
                vediag_err(VEDIAG_MEM, NULL, "Mtest: '%s():mem' can't get memory list !", __func__);
            }
	    	mem_list->current_entry = mem_list->current_entry->next;
            flag = mem_entry->check_flag;
	    	pthread_mutex_unlock(&mem_list->mutex);
            if(flag) {
                continue;
            }

            mem_entry->check_flag = 1;
	        /* Call test function */
	    	ret = mem_test_detail(mem_entry);

	    	if (ret) {
	    		test_ctrl->error = ret;
	    	}
            mem_entry->check_flag = 0;

	        /* Update tested size, offset and decrease size test */
	    	test_ctrl->tested_size += (mem_entry->mem_size);

	        if(test_ctrl->error) {
				while(!test_ctrl->test_restart) {
					sleep(1);
				}
				test_ctrl->test_restart = 0;
			} else {
	        	sleep(0);
	        }
	    } else if(info->test_app == MEM_STRESSAPP) {
	    	ret = stressapptest_execute(__data);
	    	if (ret) {
	    		test_ctrl->error = ret;
	    	}
	    	test_ctrl->tested_size += (info->chunk_size);
	        if(test_ctrl->error) {
				while(!test_ctrl->test_restart) {
					sleep(1);
				}
				test_ctrl->test_restart = 0;
			} else {
	        	sleep(0);
	        }
	    }
    }
    pthread_mutex_lock(&test_ctrl->mutex);
	test_ctrl->data_running --;
    pthread_mutex_unlock(&test_ctrl->mutex);
	free(info);

	pthread_exit(NULL);
}

static void memory_add_chunk(struct vediag_memory_test_info *mem_list, memory_test_t *entry)
{
	pthread_mutex_lock(&mem_list->mutex);
	list_add_tail(&entry->memory_list, &mem_list->mem_reserved);
	pthread_mutex_unlock(&mem_list->mutex);
}

extern int memtester_pagesize(void);

static unsigned long long get_free_mem(void)
{
	char buf[TEXT_LINE_SIZE], cmd[60];
	FILE *devf;
	int line_num = 0;
	char *line, *word;
	long ps;
	long avpages;
	unsigned long long availMem = 0;

	ps = sysconf(_SC_PAGESIZE);
	if(ps < 0) {
		xerror("%s(): get pagesize error (%ld)!\n", __FUNCTION__, ps);
		goto error;
	}
	avpages = sysconf(_SC_AVPHYS_PAGES);
	if(avpages < 0) {
		xerror("%s(): get available pages error (%ld)!\n", __FUNCTION__, avpages);
		goto error;
	}
	xinfo("%s(): pagesize=0x%lx, pagenumber=0x%lx\n", __FUNCTION__, ps, avpages);

	availMem = ps * avpages;

	return availMem;

error:

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "free");
	//vediag_debug(VEDIAG_EVENT, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			/*
			 * 				total         used         free       shared      buffers
			 * Mem:        513028        91584       421444            0        46876
			 *
			 */
			word = get_word_in_string(line, " ", 0);
			if ((word == NULL) || (strcmp(word, "Mem:") != 0)) {
				free(word);
				continue;
			}
			free(word);

			/* Get 'free' */
			word = get_word_in_string(line, " ", 3);
			/* If no value */
			if (word == NULL) {
				availMem = 0;
				break;
			}
			availMem = strtoul(word, NULL, 0) * 1024;

			xinfo("%s(): free='%s'\n", __FUNCTION__, word);

			free(word);
		}
		pclose(devf);
	}
	return availMem;
}

#define DDR_LOOP_FIND_POINTER 128
static u64 memory_reserve(struct vediag_memory_test_info *mem_list, u64 chunk_size, u64 size)
{
	memory_test_t *mem;
	u64 size_tmp, chunk_size_tmp;
	u64 real_size = 0;
	size_t pagesize, wantbytes;
	ptrdiff_t pagesizemask;
	int done_mem;
	int reserve_done = 0;

	size_tmp = size;
	chunk_size_tmp = chunk_size;
	cmd_ddrc_printhex(chunk_size);

    pagesize = memtester_pagesize();
    pagesizemask = (ptrdiff_t) ~(pagesize - 1);
    mem_dbg("pagesizemask is 0x%tx\n", pagesizemask);

    vediag_info(VEDIAG_MEM, NULL, "Reserve memory size: 0x%llx (chunk size 0x%llx)\n", size, chunk_size);

	while( !reserve_done ) {

		if(chunk_size_tmp > size_tmp) {
			chunk_size_tmp = size_tmp;
		}

		/* Alloc struct mem */
		mem = malloc(sizeof(memory_test_t));
		if(IS_ERR_OR_NULL( (void *)mem)) {
		    xinfo("Mtest: '%s():mem' out of memory !", __func__);
			real_size = 0;
			goto stop;
		}

		memset(mem, 0, sizeof(memory_test_t));
		pthread_mutex_init(&mem->mutex, NULL);

		done_mem = 0;
		wantbytes = chunk_size_tmp;

	    while (!done_mem) {
			mem_dbg("got  %lluKB (%llu bytes)", (ull) wantbytes >> 10, (ull) wantbytes);
	        while (!mem->mem_start && wantbytes) {
	        	mem->mem_start = (void volatile *) malloc(wantbytes);
	            if (!mem->mem_start)
	            	wantbytes -= pagesize;
	        }
	        mem->mem_size = wantbytes;
			if ((size_t) mem->mem_start % pagesize) {
				/* xinfo("aligning to page -- was 0x%tx\n", buf); */
				mem->aligned = (void volatile *) ((size_t) mem->mem_start & pagesizemask) + pagesize;
				/* xinfo("  now 0x%tx -- lost %d bytes\n", aligned,
				 *      (size_t) aligned - (size_t) buf);
				 */
				mem->mem_size -= ((size_t) mem->aligned - (size_t) mem->mem_start);
			} else {
				mem->aligned = mem->mem_start;
				mem->mem_size = wantbytes;
			}
			if (memory_lock_enable) {
				/* Try mlock */
				if (mlock((void *) mem->aligned, mem->mem_size) < 0) {
					xdebug("Mem test: try mlock 0x%p size 0x%x ... failed (%d)\n", (void *) mem->aligned, mem->mem_size, errno);
					if (mem->mem_start) {
						free((void *)mem->mem_start);
						mem->mem_start = NULL;
					}
					if (wantbytes < pagesize) {
						reserve_done = 1;
						break;
					}
					wantbytes -= pagesize;
					continue;
				}
				else {
					mem_dbg("Mem test: try mlock 0x%p size 0x%llx ... locked\n", (void *) mem->aligned, mem->mem_size);
				}
			}
	        done_mem = 1;
	    }

		size_tmp -= wantbytes;
		if(mem->mem_start) {
			if (!memory_lock_enable)
				memset((void *)mem->mem_start, 0, mem->mem_size);
			mem_dbg("Mtest:@%p, 0x%p -> 0x%p (remain 0x%llx)\n", mem, mem->mem_start, (ulong*)((ulong)mem->mem_start + mem->mem_size), size_tmp);
			memory_add_chunk(mem_list, mem);
		}
		if ((size_tmp <= 0) || (wantbytes <= 0)) {
			reserve_done = 1;
		}
	}
	cmd_ddrc_print("Alloc OK");
	real_size = size - size_tmp;

stop:
	return real_size;
}

static int mem_test_complete(void *data)
{
	struct vediag_memory_test_info *mem_list;
	memory_test_t *mem;

	struct vediag_test_info *info = (struct vediag_test_info *) data;
	mem_list = (struct vediag_memory_test_info *) (info->data);

	if (!mem_list)
		return 0;

	while(!list_empty(&mem_list->mem_reserved)) {
		mem = list_entry(mem_list->mem_reserved.next, struct memory_test, memory_list);
		cmd_ddrc_print("Mtest: %s():'mem'=@%p \n", __func__, mem);
		if(mem != NULL) {
			list_del_init(&mem->memory_list);
			if (memory_lock_enable) {
				munlock((void *) mem->aligned, mem->mem_size);
				mem_dbg("Mem test: munlock 0x%p size 0x%llx\n", (void *) mem->aligned, mem->mem_size);
			}
			free((void *)mem->mem_start);
			free(mem);
			mem = NULL;/*Add this to allow free many times*/
		}
	}
	return 0;
}

static int memory_check(struct vediag_test_info *info, u64 remain_size)
{
	u64 remain_used_size = 0;
	u64 total_mem_free;
	struct vediag_memory_test_info *mem_list;

	mem_list = malloc(sizeof(struct vediag_memory_test_info));
	if(IS_ERR_OR_NULL((void *)mem_list)) {
        vediag_err(VEDIAG_MEM, NULL, "Can't malloc mem_list !\n");
		return 1;
	}
	INIT_LIST_HEAD(&mem_list->mem_reserved);
	pthread_mutex_init(&mem_list->mutex, NULL);
	info->data = mem_list;

	vediag_info(VEDIAG_MEM, NULL, "Testing memory size 0x%llx, keep free size 0x%llx\n", info->total_size, remain_size);

//	info->total_size = 0;
	remain_used_size = remain_size;
	total_mem_free = (u64) get_free_mem();
	vediag_info(VEDIAG_MEM, NULL, "Free size: 0x%llx\n", total_mem_free);
	//total_mem_free /= 4;
	vediag_info(VEDIAG_MEM, NULL, "Available memory can be used for memtester: 0x%llx\n", total_mem_free);
	if(total_mem_free > remain_used_size) {
		/* Aligned 1MB */
		total_mem_free = (total_mem_free - remain_used_size) & (~(0x100000 - 1));
	}
	else {
		if (total_mem_free < MEMORY_REMAIN_MINIMUM) {
			vediag_err(VEDIAG_MEM, NULL, "Free memory (0x%llx) is smaller than 0x%llx.\n", total_mem_free, MEMORY_REMAIN_MINIMUM);
			return 1;
		}
		total_mem_free = (total_mem_free - MEMORY_REMAIN_MINIMUM) & (~(0x100000 - 1));
	}

	/*Count memmory for alloc*/
	if (total_mem_free > info->total_size) {
		total_mem_free = info->total_size;
	}

	/* Malloc a max memory, with distance between: 2 memory 8MB */
	info->total_size = memory_reserve(mem_list, info->chunk_size, total_mem_free);
	vediag_info(VEDIAG_MEM, NULL, "Real reserved memory size: 0x%llx\n", info->total_size);
	if(info->total_size == 0) {
		return 1;
	}
	mem_list->current_entry = &mem_list->mem_reserved;
	return 0;

}

static int mem_create_arg(struct list_head *arg_list)
{
	vediag_set_argument(MEMORY_THREAD_NUMBER_KEY, "1", arg_list);
	vediag_set_argument(MEMORY_REMAIN_SIZE_KEY, "0x09600000", arg_list);
	vediag_set_argument(MEMORY_CHUNK_SIZE_KEY, "0x20000", arg_list);
	/*chunk_size for memtester: 0x100000; for stressapptest shoule: 0x6400000*/
	vediag_set_argument(MEMORY_CORE_NUMBER_KEY, "1", arg_list);
	vediag_set_argument(MEMORY_TEST_SIZE_KEY, "0x06400000", arg_list);
	vediag_set_argument(MEMORY_RUNNING_TIME_KEY, "", arg_list);
	return 0;
}

static int mem_get_app(char *app)
{
	if (app && !strcmp(app, "memtester"))
		return MEM_MEMTESTER;
	else if (app && !strcmp(app, "stressapptest"))
		return MEM_STRESSAPP;
	else
		return MEM_MEMTESTER;
}

static int mem_check_dev(struct vediag_test_info *info, struct vediag_test_info *dev_info, char **error)
{
	char *val;
	u64 remain_size;

    if(IS_ERR_OR_NULL((void *)info)) {
    	vediag_err(VEDIAG_MEM, NULL, "%s(): info NULL\n", __func__);
    	return 0;
    }

    if(IS_ERR_OR_NULL((void *)dev_info)) {
    	vediag_err(VEDIAG_MEM, NULL, "%s(): dev_info NULL\n", __func__);
    	return 0;
    }

    val = vediag_get_argument(dev_info, MEMORY_REMAIN_SIZE_KEY);
    if (val) {
    	remain_size = strtoull(val, NULL, 0);
    	free(val);
    } else
    	remain_size = CONFIG_KERNEL_MEMORY_USED;

	if (info->total_size != 0)
		dev_info->total_size = info->total_size;
	else {
	    val = vediag_get_argument(dev_info, MEMORY_TEST_SIZE_KEY);
		if (val) {
			dev_info->total_size = strtoull(val, NULL, 0);
			free(val);
		}
		else
			dev_info->total_size = DDR_DEFAULT_TEST_SIZE;
	}

	if (info->chunk_size != 0)
		dev_info->chunk_size = info->chunk_size;
	else {
	    val = vediag_get_argument(dev_info, MEMORY_CHUNK_SIZE_KEY);
		if (val) {
			dev_info->chunk_size = strtoull(val, NULL, 0);
			free(val);
		}
		else
			dev_info->chunk_size = DDR_DEFAULT_TEST_CHUNK;
	}

    /* Scan memory size */
	if (memory_check(dev_info, remain_size)) {
		*error = strdup("Reserve memory failed");
		vediag_err(VEDIAG_MEM, NULL, "< DDR > Failed to reserve memory\n");
		return 0;
	}

	/* Check number of threads */
	if (info->threads > 0)
		dev_info->threads = info->threads;
	else {
	    val = vediag_get_argument(dev_info, MEMORY_THREAD_NUMBER_KEY);
		if (val) {
			dev_info->threads = strtoul(val, NULL, 0);
			free(val);
		}
		else
			dev_info->threads = DDR_DEFAULT_THREAD_NUMBER;
	}

	/* Check running time */
	if (info->runtime > 0)
		dev_info->runtime = info->runtime;
	else {
	    val = vediag_get_argument(dev_info, MEMORY_RUNNING_TIME_KEY);
		if (val) {
			dev_info->runtime = strtoul(val, NULL, 0);
			free(val);
		}
		else
			dev_info->runtime = DDR_DEFAULT_RUNNING_TIME;
	}

	if (info->core_nums > 0)
		dev_info->core_nums = info->core_nums;
	else {
	    val = vediag_get_argument(dev_info, "Core number");
		if (val) {
			dev_info->core_nums = strtoul(val, NULL, 0);
			free(val);
		}
		else
			dev_info->core_nums = DDR_DEFAULT_CORE_NUMBER;
	}

    return 1;
}

static int mem_test_start (int argc, char *argv[], void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
    int ret = 0, arp;

    if ((arp = vediag_getopt(argc, argv, "-h")) > 0) {
        mem_test_help();
        return 0;
    }

    if(IS_ERR_OR_NULL((void *)info)) {
    	return 0;
    }

    /* Default arguments */
    info->test_mode = DDR_DEFAULT_TEST_MODE;

    /* Get test mode */
    info->dev_mask = 0xFFFFFFFF;
    if ((arp = vediag_getopt(argc, argv, "-m")) > 0) {
    	info->test_mode = strtoul(argv[arp + 1], NULL, 16);
    	mem_dbg("Test mode: %d", info->test_mode);
    }

    /* Get size option */
    if ((arp = vediag_getopt(argc, argv, "-s")) > 0) {
    	info->total_size = strtoull(argv[arp + 1], NULL, 0);
        if (info->total_size <= 0)
        	info->total_size = DDR_DEFAULT_TEST_SIZE;
        mem_dbg("Size: %d", info->total_size);
    }

    /* Get chunk size */
    if ((arp = vediag_getopt(argc, argv, "-c")) > 0) {
    	info->chunk_size = strtoull(argv[arp + 1], NULL, 0);
        if(info->chunk_size > DDR_MAX_TEST_CHUNK){
        	info->chunk_size = DDR_MAX_TEST_CHUNK;
        }
		if(info->chunk_size <= 0) {
			info->chunk_size = DDR_DEFAULT_TEST_CHUNK;
		}
		mem_dbg("Chunk size: %d", info->chunk_size);
    }

    /* Get core number */
	if ((arp = vediag_getopt(argc, argv, "-core")) > 0){
		info->core_nums = strtoul(argv[arp + 1], NULL, 0);
		if(info->core_nums <= 0) {
			info->core_nums = DDR_DEFAULT_CPU_NUM;
		}
	}

    ret = sizeof(struct vediag_test_info);
    return ret;
}

struct vediag_test_device vediag_mem = {
    .type           = VEDIAG_MEM,
    .name           = "MEM",
    .cmd            = "mem",
    .desc           = "MEMORY TEST",
	.default_app	= "memtester" /*"stressapptest"*/,
    .test_start     = mem_test_start,
	.get_test_app	= mem_get_app,
	.create_argument= mem_create_arg,
    .check_dev      = mem_check_dev,
    .test_execute   = ddr_diag_execute,
    .test_complete  = mem_test_complete,
};

late_initcall(diag_ddr_test_register)
{
	/* Check memory lock enable */
	char *mlock_en = vediag_parse_system("Memory lock");
	if (mlock_en != NULL) {
		memory_lock_enable = strtoul(mlock_en, NULL, 0);
        free(mlock_en);
	}
	return vediag_test_register(&vediag_mem);
}



#else
int use_phys = 0;
ul physaddrbase;
char progress[] = "-\\|/";
int test_stuck_address(ulv *bufa, size_t count) {
    ulv *p1 = bufa;
    unsigned int j;
    size_t i;
    ul physaddr;

    printf("           ");
    fflush(stdout);
    for (j = 0; j < 16; j++) {
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        p1 = (ulv *) bufa;
        printf("setting %3u", j);
        fflush(stdout);
        for (i = 0; i < count; i++) {
            *p1 = ((j + i) % 2) == 0 ? (ul) p1 : ~((ul) p1);
            *p1++;
        }
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        printf("testing %3u", j);
        fflush(stdout);
        p1 = (ulv *) bufa;
        for (i = 0; i < count; i++, p1++) {
            if (*p1 != (((j + i) % 2) == 0 ? (ul) p1 : ~((ul) p1))) {
                if (use_phys) {
                    physaddr = physaddrbase + (i * sizeof(ul));
                    printf(
                            "FAILURE: possible bad address line at physical "
                            "address 0x%08lx.\n", 
                            physaddr);
                } else {
                    printf(
                            "FAILURE: possible bad address line at offset "
                            "0x%08lx.\n", 
                            (ul) (i * sizeof(ul)));
                }
                printf("Skipping to next test...\n");
                fflush(stdout);
                return -1;
            }
        }
    }
    printf("\b\b\b\b\b\b\b\b\b\b\b           \b\b\b\b\b\b\b\b\b\b\b");
    fflush(stdout);
    return 0;
}

int test_random_value(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    size_t i, j;

    putchar(' ');
    fflush(stdout);
    for (i = 0; i < count; i++) {
        *p1++ = *p2++ = rand_ul();
        if (!(i % PROGRESSOFTEN)) {
            putchar('\b');
            putchar(progress[++j % PROGRESSLEN]);
            fflush(stdout);
        }
    }
    printf("\b \b");
    fflush(stdout);
    return compare_regions(bufa, bufb, count);
}

int test_xor_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    size_t i;
    ul q = rand_ul();

    for (i = 0; i < count; i++) {
        *p1++ ^= q;
        *p2++ ^= q;
    }
    return compare_regions(bufa, bufb, count);
}

int test_sub_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    size_t i;
    ul q = rand_ul();

    for (i = 0; i < count; i++) {
        *p1++ -= q;
        *p2++ -= q;
    }
    return compare_regions(bufa, bufb, count);
}

int test_mul_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    size_t i;
    ul q = rand_ul();

    for (i = 0; i < count; i++) {
        *p1++ *= q;
        *p2++ *= q;
    }
    return compare_regions(bufa, bufb, count);
}

int test_div_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    size_t i;
    ul q = rand_ul();

    for (i = 0; i < count; i++) {
        if (!q) {
            q++;
        }
        *p1++ /= q;
        *p2++ /= q;
    }
    return compare_regions(bufa, bufb, count);
}

int test_or_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    size_t i;
    ul q = rand_ul();

    for (i = 0; i < count; i++) {
        *p1++ |= q;
        *p2++ |= q;
    }
    return compare_regions(bufa, bufb, count);
}

int test_and_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    size_t i;
    ul q = rand_ul();

    for (i = 0; i < count; i++) {
        *p1++ &= q;
        *p2++ &= q;
    }
    return compare_regions(bufa, bufb, count);
}

int test_seqinc_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    size_t i;
    ul q = rand_ul();

    for (i = 0; i < count; i++) {
        *p1++ = *p2++ = (i + q);
    }
    return compare_regions(bufa, bufb, count);
}

int test_solidbits_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    unsigned int j;
    ul q;
    size_t i;

    printf("           ");
    fflush(stdout);
    for (j = 0; j < 64; j++) {
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        q = (j % 2) == 0 ? UL_ONEBITS : 0;
        printf("setting %3u", j);
        fflush(stdout);
        p1 = (ulv *) bufa;
        p2 = (ulv *) bufb;
        for (i = 0; i < count; i++) {
            *p1++ = *p2++ = (i % 2) == 0 ? q : ~q;
        }
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        printf("testing %3u", j);
        fflush(stdout);
        if (compare_regions(bufa, bufb, count)) {
            return -1;
        }
    }
    printf("\b\b\b\b\b\b\b\b\b\b\b           \b\b\b\b\b\b\b\b\b\b\b");
    fflush(stdout);
    return 0;
}

int test_checkerboard_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    unsigned int j;
    ul q;
    size_t i;

    printf("           ");
    fflush(stdout);
    for (j = 0; j < 64; j++) {
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        q = (j % 2) == 0 ? CHECKERBOARD1 : CHECKERBOARD2;
        printf("setting %3u", j);
        fflush(stdout);
        p1 = (ulv *) bufa;
        p2 = (ulv *) bufb;
        for (i = 0; i < count; i++) {
            *p1++ = *p2++ = (i % 2) == 0 ? q : ~q;
        }
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        printf("testing %3u", j);
        fflush(stdout);
        if (compare_regions(bufa, bufb, count)) {
            return -1;
        }
    }
    printf("\b\b\b\b\b\b\b\b\b\b\b           \b\b\b\b\b\b\b\b\b\b\b");
    fflush(stdout);
    return 0;
}

int test_blockseq_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    unsigned int j;
    size_t i;

    printf("           ");
    fflush(stdout);
    for (j = 0; j < 256; j++) {
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        p1 = (ulv *) bufa;
        p2 = (ulv *) bufb;
        printf("setting %3u", j);
        fflush(stdout);
        for (i = 0; i < count; i++) {
            *p1++ = *p2++ = (ul) UL_BYTE(j);
        }
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        printf("testing %3u", j);
        fflush(stdout);
        if (compare_regions(bufa, bufb, count)) {
            return -1;
        }
    }
    printf("\b\b\b\b\b\b\b\b\b\b\b           \b\b\b\b\b\b\b\b\b\b\b");
    fflush(stdout);
    return 0;
}

int test_walkbits0_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    unsigned int j;
    size_t i;

    printf("           ");
    fflush(stdout);
    for (j = 0; j < UL_LEN * 2; j++) {
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        p1 = (ulv *) bufa;
        p2 = (ulv *) bufb;
        printf("setting %3u", j);
        fflush(stdout);
        for (i = 0; i < count; i++) {
            if (j < UL_LEN) { /* Walk it up. */
                *p1++ = *p2++ = ONE << j;
            } else { /* Walk it back down. */
                *p1++ = *p2++ = ONE << (UL_LEN * 2 - j - 1);
            }
        }
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        printf("testing %3u", j);
        fflush(stdout);
        if (compare_regions(bufa, bufb, count)) {
            return -1;
        }
    }
    printf("\b\b\b\b\b\b\b\b\b\b\b           \b\b\b\b\b\b\b\b\b\b\b");
    fflush(stdout);
    return 0;
}

int test_walkbits1_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    unsigned int j;
    size_t i;

    printf("           ");
    fflush(stdout);
    for (j = 0; j < UL_LEN * 2; j++) {
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        p1 = (ulv *) bufa;
        p2 = (ulv *) bufb;
        printf("setting %3u", j);
        fflush(stdout);
        for (i = 0; i < count; i++) {
            if (j < UL_LEN) { /* Walk it up. */
                *p1++ = *p2++ = UL_ONEBITS ^ (ONE << j);
            } else { /* Walk it back down. */
                *p1++ = *p2++ = UL_ONEBITS ^ (ONE << (UL_LEN * 2 - j - 1));
            }
        }
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        printf("testing %3u", j);
        fflush(stdout);
        if (compare_regions(bufa, bufb, count)) {
            return -1;
        }
    }
    printf("\b\b\b\b\b\b\b\b\b\b\b           \b\b\b\b\b\b\b\b\b\b\b");
    fflush(stdout);
    return 0;
}

int test_bitspread_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    unsigned int j;
    size_t i;

    printf("           ");
    fflush(stdout);
    for (j = 0; j < UL_LEN * 2; j++) {
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        p1 = (ulv *) bufa;
        p2 = (ulv *) bufb;
        printf("setting %3u", j);
        fflush(stdout);
        for (i = 0; i < count; i++) {
            if (j < UL_LEN) { /* Walk it up. */
                *p1++ = *p2++ = (i % 2 == 0)
                    ? (ONE << j) | (ONE << (j + 2))
                    : UL_ONEBITS ^ ((ONE << j)
                                    | (ONE << (j + 2)));
            } else { /* Walk it back down. */
                *p1++ = *p2++ = (i % 2 == 0)
                    ? (ONE << (UL_LEN * 2 - 1 - j)) | (ONE << (UL_LEN * 2 + 1 - j))
                    : UL_ONEBITS ^ (ONE << (UL_LEN * 2 - 1 - j)
                                    | (ONE << (UL_LEN * 2 + 1 - j)));
            }
        }
        printf("\b\b\b\b\b\b\b\b\b\b\b");
        printf("testing %3u", j);
        fflush(stdout);
        if (compare_regions(bufa, bufb, count)) {
            return -1;
        }
    }
    printf("\b\b\b\b\b\b\b\b\b\b\b           \b\b\b\b\b\b\b\b\b\b\b");
    fflush(stdout);
    return 0;
}

int test_bitflip_comparison(ulv *bufa, ulv *bufb, size_t count) {
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    unsigned int j, k;
    ul q;
    size_t i;

    printf("           ");
    fflush(stdout);
    for (k = 0; k < UL_LEN; k++) {
        q = ONE << k;
        for (j = 0; j < 8; j++) {
            printf("\b\b\b\b\b\b\b\b\b\b\b");
            q = ~q;
            printf("setting %3u", k * 8 + j);
            fflush(stdout);
            p1 = (ulv *) bufa;
            p2 = (ulv *) bufb;
            for (i = 0; i < count; i++) {
                *p1++ = *p2++ = (i % 2) == 0 ? q : ~q;
            }
            printf("\b\b\b\b\b\b\b\b\b\b\b");
            printf("testing %3u", k * 8 + j);
            fflush(stdout);
            if (compare_regions(bufa, bufb, count)) {
                return -1;
            }
        }
    }
    printf("\b\b\b\b\b\b\b\b\b\b\b           \b\b\b\b\b\b\b\b\b\b\b");
    fflush(stdout);
    return 0;
}

struct test mem_tests[] = {
    { "Random Value", test_random_value },
    { "Compare XOR", test_xor_comparison },
    { "Compare SUB", test_sub_comparison },
    { "Compare MUL", test_mul_comparison },
    { "Compare DIV",test_div_comparison },
    { "Compare OR", test_or_comparison },
    { "Compare AND", test_and_comparison },
    { "Sequential Increment", test_seqinc_comparison },
    { "Solid Bits", test_solidbits_comparison },
    { "Block Sequential", test_blockseq_comparison },
    { "Checkerboard", test_checkerboard_comparison },
    { "Bit Spread", test_bitspread_comparison },
    { "Bit Flip", test_bitflip_comparison },
    { "Walking Ones", test_walkbits1_comparison },
    { "Walking Zeroes", test_walkbits0_comparison },
    { NULL, NULL }
};

int ddr_diag_usage(void)
{
	printf("\nDDR test:\n");
	printf("  diag ddr [opt ...]");
	printf("  Options: \n");
	printf("    -h : print help\n");
	printf("    -a : start address\n");
	printf("    -s <size>   : test size in hex, will be recounted, default is full\n");
	printf("    -c <chunk size> : Chunk/Packet-size\n");
//	printf("    -t <thread> : number thread use to test\n");
//	printf("    -f          : test full\n");
//	printf("    -cpu <core numbers>  : number of cores used to run test\n");

	return 0;

}

int ddr_diag_execute(u64 start_addr, u32 size, u32 chunk)
{
	int i;
	size_t halflen, count;
	ulv *bufa, *bufb;
	void volatile *buf, *aligned;
	ul testmask = 0;

	buf = (void volatile *) start_addr;
	aligned = (void volatile *) chunk;

    halflen = size / 2;
    count = halflen / sizeof(ul);
    bufa = (ulv *) aligned;
    bufb = (ulv *) ((size_t) aligned + halflen);

	if(test_stuck_address(aligned, size / sizeof(ul))) {
		goto _exit;
	}
    for (i = 0; ; i++) {
        // if (!mem_tests[i].name) break;
        /* If using a custom testmask, only run this test if the
           bit corresponding to this test was set by the user.
         */
        if (testmask && (!((1 << i) & testmask))) {
            continue;
        }

        if (mem_tests[i].fp(bufa, bufb, count)) {

    		printf("[diag-ddr] %s failed.\n", mem_tests[i].name);
    		goto _exit;
        }
    }
	return RET_OK;
_exit:
	return RET_MEM_ERR;
}

int ddr_diag_start (int argc, char *argv[])
{
    int arp;
	u32 size = 0, chunk = 0;
	u64 s_addr = 0;

    if ((arp = diag_ddr_getopt(argc, argv, "-h")) > 0) {
        ddr_diag_usage();
        return 0;
    }

	/* Get start address option */
    if ((arp = diag_ddr_getopt(argc, argv, "-a")) > 0) {
    	s_addr = strtoull(argv[arp + 1], NULL, 0);
        mem_dbg("Start address: %lu", s_addr);
    }

    /* Get size option */
    if ((arp = diag_ddr_getopt(argc, argv, "-s")) > 0) {
    	size = strtoull(argv[arp + 1], NULL, 0);
        if (size <= 0)
        	size = DDR_DEFAULT_SIZE;
        mem_dbg("Size: %u", size);
    }

    /* Get chunk size */
    if ((arp = diag_ddr_getopt(argc, argv, "-c")) > 0) {
    	chunk = strtoull(argv[arp + 1], NULL, 0);
        if(chunk > DDR_MAX_CHUNK){
        	chunk = DDR_MAX_CHUNK;
        }
		if(chunk <= 0) {
			chunk = DDR_DEFAULT_CHUNK;
		}
		mem_dbg("Chunk size: %d", chunk);
    }

	if (ddr_diag_execute(s_addr, size, chunk) != RET_OK) {
		printf("\n[diag-ddr] Failed to execute diag test for ddr device\n");
		return RET_GEN_ERR;
	}
    return RET_OK;
}

#endif