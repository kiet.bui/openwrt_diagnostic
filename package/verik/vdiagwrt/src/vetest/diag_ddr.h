
#include "common.h"

typedef unsigned long ul;
typedef unsigned long long ull;
typedef unsigned long volatile ulv;
typedef unsigned char volatile u8v;
typedef unsigned short volatile u16v;

#define UL_BYTE(x) (((ul)x | (ul)x<<8 | (ul)x<<16 | (ul)x<<24 | (ul)x<<32 | (ul)x<<40 | (ul)x<<48 | (ul)x<<56))
#define rand32() ((unsigned int) rand() | ( (unsigned int) rand() << 16))
#define rand64() (((ul) rand32()) << 32 | ((ul) rand32()))
#define rand_ul() rand64()
#define mem_dbg		printf
#define DDR_DEFAULT_SIZE 		0x100000
#define DDR_DEFAULT_CHUNK		0x400
#define DDR_MAX_CHUNK			0x4000
#define ONE 					0x00000001L
#define UL_LEN					64
#define PROGRESSLEN 			4
#define PROGRESSOFTEN 			2500
#define UL_ONEBITS 				0xffffffffffffffffUL
#define CHECKERBOARD1 			0x5555555555555555
#define CHECKERBOARD2 			0xaaaaaaaaaaaaaaaa

struct test {
    char *name;
    int (*fp)(ulv*, ulv*, size_t);
};

static inline int diag_ddr_getopt(int nargc, char * const *nargv, const char *ostr)
{
    int i, point;
    char *p;

    for (i = 0, point = -1; i < nargc; i++) {
        p = nargv[i];
        if (strcmp(p, ostr) == 0) { 
            point = i; 
            break;
        }
    }    

    return (point); /* return pointer of next argurement */
}

static inline int compare_regions(ulv *bufa, ulv *bufb, size_t count) {
    int r = 0;
    size_t i;
    ulv *p1 = bufa;
    ulv *p2 = bufb;
    ul physaddr;
    int use_phys = 0;
    ul physaddrbase;

    for (i = 0; i < count; i++, p1++, p2++) {
        if (*p1 != *p2) {
            if (use_phys) {
                physaddr = physaddrbase + (i * sizeof(ul));
                printf(
                        "FAILURE: 0x%08lx != 0x%08lx at physical address "
                        "0x%08lx.\n", 
                        (ul) *p1, (ul) *p2, physaddr);
            } else {
                printf(
                        "FAILURE: 0x%08lx != 0x%08lx at offset 0x%08lx.\n", 
                        (ul) *p1, (ul) *p2, (ul) (i * sizeof(ul)));
            }
            /* printf("Skipping to next test..."); */
            r = -1; 
        }
    }   
    return r;
}

/* prototype */
int ddr_diag_start (int argc, char *argv[]);
int ddr_diag_usage(void);