/* for definition of common functions */
#include "common.h"

char the_char[2];
char * chartostr(uchar c)
{
    char *s; 
    s = the_char;

    memset(s, 0, 2); 
    s[0] = c;
    s[1] = '\0';
    return s;
}

#define MAX_LINE_LENGTH_BYTES (64)
#define DEFAULT_LINE_LENGTH_BYTES (16)
int print_buffer(ulong addr, void* data, uint width, uint count, uint linelen)
{
    uint8_t linebuf[MAX_LINE_LENGTH_BYTES];
    uint32_t *uip = (void*) linebuf;
    uint16_t *usp = (void*) linebuf;
    uint8_t *ucp = (void*) linebuf;
    int i;
    int count_bk = count;

    if (linelen * width > MAX_LINE_LENGTH_BYTES)
        linelen = MAX_LINE_LENGTH_BYTES / width;
    if (linelen < 1)
        linelen = DEFAULT_LINE_LENGTH_BYTES / width;

    while (count_bk) {
        printf("%08lx:", addr);

        /* check for overflow condition */
        if (count_bk < linelen)
            linelen = count_bk;

        /* Copy from memory into linebuf and print hex values */
        for (i = 0; i < linelen; i++) {
            if (width == 4) {
                uip[i] = *(volatile uint32_t *) data;
                printf(" %08x", uip[i]);
            } else if (width == 2) {
                usp[i] = *(volatile uint16_t *) data;
                printf(" %04x", usp[i]);
            } else {
                ucp[i] = *(volatile uint8_t *) data;
                printf(" %02x", ucp[i]);
            }
            data += width;
        }

        /* Print data in ASCII characters */
        printf("    ");
        for (i = 0; i < linelen * width; i++) {
            printf("%s", isprint(ucp[i]) && (ucp[i] < 0x80) ? chartostr(ucp[i]) : ".");
        }
        printf("\n");

        /* update references */
        addr += linelen * width;
        count_bk -= linelen;
    }   

    return 0;
}