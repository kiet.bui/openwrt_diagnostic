#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gpio.h"

int do_gpio (int argc, char *argv[])
 {
	/*gpio_direction_t direction;*/
	gpio_value_t value;
	char *gpio_pin;
	char *cmd;
	int rc = RET_OK;

	if (argc < 3) {
		GPIO_ERROR("Wrong command\n");
		return RET_GEN_ERR;
	}

	cmd = argv[1];
	gpio_pin = argv[2];

	if (!strcmp(cmd, "set")) {

		if (argc < 4) {
			GPIO_ERROR("Miss output value in command\n");
			return RET_GEN_ERR;

		} else {
			value = strtoul(argv[3], NULL, 0);

			if(gpio_set_output_value(gpio_pin, value) < 0) {
				GPIO_ERROR("Set output value failed!!!\n");
				return RET_GEN_ERR;
			}

			if (gpio_get_direction(gpio_pin) != GPIO_OUT)
				rc = gpio_set_direction(gpio_pin, GPIO_OUT);

			if (rc < 0) {
				return RET_GEN_ERR;
			} else {
				xinfo("%s : Set output value is %d\n", gpio_pin, value);
				return RET_OK;
			}
		}

	} else if (!strcmp(cmd, "get")) {

		if (gpio_get_direction(gpio_pin) != GPIO_IN) {

			if (gpio_set_direction(gpio_pin, GPIO_IN) < 0) {

				GPIO_ERROR("Config : %s to input failed!!!\n", gpio_pin);
				return RET_GEN_ERR;
			}
		}

		xinfo("%s : Input value %d\n", gpio_pin, gpio_get_input_value(gpio_pin));
	}

	return rc;
}

void usage(void)
{
	printf(" gpio command\n");
	printf(" gpio status <board|pin>            - Board GPIO table status or one pin status\n");
	printf(" gpio irq    <pin> [<type> <active>]- Config gpio as external irq or check irq status\n");
	printf(" gpio get    <pin>                  - Get gpi|gpio input value\n");
	printf(" gpio set    <pin> <value>          - Set gpio output value\n");
	printf("     pin = [gpi0...gpi15]\n");
	printf("     pin = [gpio0...gpio15]\n");
	printf("     type   = level|edge\n");
	printf("     active = low|high\n");
	printf("     value = 0|1\n");
	printf(" Ex :\n");
	printf(" gpio status board         - Show all gpio status in board gpio table\n");
	printf(" gpio status gpio0         - Show gpio0 status\n");
	printf(" gpio irq gpio7            - Check interrupt status gpio7\n");
	printf(" gpio irq gpio9 level high - Config gpio9 was ext irq type level active high\n");
	printf(" gpio get gpi2             - Get input value of gpi2\n");
	printf(" gpio set gpio10 0         - Set output value for gpio10 is 0\n");
}

int main(int argc, char* argv[])
{
    printf("[diag] GPIO testing\n");
    usage();
    do_gpio(argc, argv);
    return 0;
}

