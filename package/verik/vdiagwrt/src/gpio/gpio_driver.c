
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gpio.h"

static gpio_ops_t *gpio_ops = NULL;

static int gpio_init(const char *gpio_pin)
 {
	char str_num[2];
	int gpio_num = 0;

	if (!strncmp(gpio_pin, "gpio", 4) || !strncmp(gpio_pin, "GPIO", 4)) {

#if 0
		strcpy(&str_num[0], &gpio_pin[4]);
		gpio_num = strtoul(&str_num[0], NULL, 0);
		if (gpio_num >= XGENE_GPIO_MAX_PIN) {
			GPIO_ERROR("No Support GPIO_%d\n", gpio_num);
			return -1;
		}

		xgene_gpio_init(&gpio_ops);
#else
		xinfo("Not suppot %s!!!\n", gpio_pin);
		return -1;
#endif
	} else if (!strncmp(gpio_pin, "gpi", 3) || !strncmp(gpio_pin, "GPI", 3)) {

		strcpy(&str_num[0], &gpio_pin[3]);
		gpio_num = strtoul(&str_num[0], NULL, 0);
		if (gpio_num >= XGENE_GPI_MAX_PIN) {
			GPIO_ERROR("No Support GPI_%d\n", gpio_num);
			return -1;
		}

		xgene_gpi_init(&gpio_ops);

	} else {
		GPIO_ERROR("No Support pin %s\n", gpio_pin);
		return -1;
	}

	return gpio_num;
}

/* gpio_set_direction(const char *gpio_pin, gpio_direction_t direction) : Set gpio direction
 * gpio_pin :
 * 		Xgene gpio : GPI0->GPI7, GPIO0->GPIO15
 * 		Expander gpio : Not support yet
 * direction :
 * 		GPIO_IN 			: Input
 * 		GPIO_OUT			: Output
 * 		GPIO_IRQ_LEVEL_LOW	: IRQ type Level active Low
 * 		GPIO_IRQ_LEVEL_HIGH	: IRQ type Level active High
 * 		GPIO_IRQ_EDGE_LOW	: IRQ type Edge active Low
 * 		GPIO_IRQ_EDGE_HIGH	: IRQ type Edge active High*/
int gpio_set_direction(const char *gpio_pin, gpio_direction_t direction)
 {
	int rc = 0;
	int gpio_num;

	if ((gpio_num = gpio_init(gpio_pin)) < 0) {
		return -1;
	}

	if (gpio_ops && gpio_ops->set_direction) {
		rc = gpio_ops->set_direction(gpio_num, direction);
	} else {
		rc = -1;
	}

	return rc;
}

/* gpio_get_direction(gpio_pin_t gpio_pin) : Get gpio direction
 * gpio_pin :
 * 		Xgene gpio : GPI0->GPI7, GPIO0->GPIO15
 * 		Expander gpio : Not support yet
 */
int gpio_get_direction(const char *gpio_pin)
 {
	int rc = 0;
	int gpio_num;

	if ((gpio_num = gpio_init(gpio_pin)) < 0) {
		return -1;
	}

	if (gpio_ops && gpio_ops->get_direction) {
		rc = gpio_ops->get_direction(gpio_num);
	}  else {
		rc = -1;
	}

	return rc;
}

/* gpio_get_input_value(gpio_pin_t gpio_pin) : Get gpio input value
 * gpio_pin :
 * 		Xgene gpio : GPI0->GPI7, GPIO0->GPIO15
 * 		Expander gpio : Not support yet
 */
int gpio_get_input_value(const char *gpio_pin)
 {
	int rc = 0;
	int gpio_num;

	if ((gpio_num = gpio_init(gpio_pin)) < 0) {
		return -1;
	}

	if (gpio_ops && gpio_ops->get_input_value) {
		rc = gpio_ops->get_input_value(gpio_num);
	}  else {
		rc = -1;
	}

	return rc;
}

/* gpio_set_output_value(gpio_pin_t gpio_pin, gpio_value_t value) : Set gpio output value
 * gpio_pin :
 * 		Xgene gpio : GPI0->GPI7, GPIO0->GPIO15
 * 		Expander gpio : Not support yet
 * value :
 * 		GPIO_HIGH :
 * 		GPIO_LOW  :
 */
int gpio_set_output_value(const char *gpio_pin, gpio_value_t value)
 {
	int rc = 0;
	int gpio_num;

	if ((gpio_num = gpio_init(gpio_pin)) < 0) {
		return -1;
	}

	if (value != GPIO_HIGH && value != GPIO_LOW) {
		GPIO_ERROR("No Support Output value %d\n", value);
		return -1;
	}

	if (gpio_ops && gpio_ops->set_output_value) {
		rc = gpio_ops->set_output_value(gpio_num, value);
	}  else {
		rc = -1;
	}

	return rc;
}

/* gpio_get_output_value(gpio_pin_t gpio_pin) : Get gpio output value
 * gpio_pin :
 * 		Xgene gpio : GPI0->GPI7, GPIO0->GPIO15
 * 		Expander gpio : Not support yet
 */
int gpio_get_output_value(const char *gpio_pin)
 {
	int rc = 0;
	int gpio_num;

	if ((gpio_num = gpio_init(gpio_pin)) < 0) {
		return -1;
	}

	if (gpio_ops && gpio_ops->get_output_value) {
		rc = gpio_ops->get_output_value(gpio_num);
	}  else {
		rc = -1;
	}

	return rc;
}

/* int gpio_get_irq_status(const char *gpio_pin) : Get gpio irq status
 * gpio_pin :
 * 		Xgene gpio : GPI0->GPI7, GPIO0->GPIO15
 * 		Expander gpio : Not support yet
 */
int gpio_get_irq_status(const char *gpio_pin)
 {
	int rc = 0;
	int gpio_num;

	if ((gpio_num = gpio_init(gpio_pin)) < 0) {
		return -1;
	}

	if (gpio_ops && gpio_ops->get_irq_status) {
		rc = gpio_ops->get_irq_status(gpio_num);
	}  else {
		rc = -1;
	}

	return rc;
}

#if 0
static void gpio_print_direction(gpio_direction_t direction)
 {
	switch (direction) {

	case GPIO_IN:
		printf("GPIO_INPUT - Irq Disable");
		break;

	case GPIO_OUT:
		printf("GPIO_OUTPUT - ");
		break;

	case GPIO_IRQ_LEVEL_LOW:
		printf("GPIO_INPUT - Irq type Level active Low");
		break;

	case GPIO_IRQ_LEVEL_HIGH:
		printf("GPIO_INPUT - Irq type Level active High");
		break;

	case GPIO_IRQ_EDGE_LOW:
		printf("GPIO_INPUT - Irq type Edge active Low");
		break;

	case GPIO_IRQ_EDGE_HIGH:
		printf("GPIO_INPUT - Irq type Edge active High");
		break;

	default:
		printf("Unknow Direction %d\n", direction);
	}
}

static void gpio_print(board_gpio_t *gpio)
 {
	printf("%20s : %7s - ", gpio->gpio_name, gpio->gpio_pin);
	gpio_print_direction(gpio->gpio_dir);

	if (gpio->gpio_dir == GPIO_OUT) {
		printf("Value %d\n", gpio->gpio_val);
	} else {
		printf("\n");
	}
}

void gpio_pin_status(char * gpio_name, char *gpio_pin)
{
	board_gpio_t gpio;

	if(!gpio_name)
		gpio.gpio_name = gpio_pin;
	else
		gpio.gpio_name = gpio_name;

	gpio.gpio_pin = gpio_pin;
	gpio.gpio_dir = gpio_get_direction(gpio_pin);
	if (gpio.gpio_dir == GPIO_OUT)
		gpio.gpio_val = gpio_get_output_value(gpio_pin);
	else
		gpio.gpio_val = 0;

	gpio_print(&gpio);
}

void gpio_board_status(void)
 {
#if defined(CONFIG_BOARD_GPIO_TBL)
	int i;

	for (i = 0; i < ARRAY_SIZE(board_gpio_tbl); i++)
		gpio_pin_status(board_gpio_tbl[i].gpio_name, board_gpio_tbl[i].gpio_pin);

#else
	printf("GPIO >> No Board GPIO Table\n");
#endif
}
#endif
