
#include <common.h>
#include <string.h>
#include <time.h>
#include <err.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <memory.h>
#include <log.h>

#define DBG(format, args...)	\
	do { \
		if (dbg) \
			xinfo(format, ##args); \
	} while(0)

int dbg = 0;

void csr_wrl(void volatile *base, off_t offset, unsigned int val)
{
	unsigned int volatile * csr = base + offset;

	DBG("CSR WR %p value 0x%08X\n", csr, val);
	*csr = val;
}
void csr_wrw(void volatile *base, off_t offset, unsigned short val)
{
	unsigned short volatile * csr = base + offset;

	DBG("CSR WR %p value 0x%08X\n", csr, val);
	*csr = val;
}
void csr_wrb(void volatile *base, off_t offset, unsigned char val)
{
	unsigned char volatile * csr = base + offset;

	DBG("CSR WR %p value 0x%08X\n", csr, val);
	*csr = val;
}

unsigned int csr_rdl(void volatile *base, off_t offset)
{
	unsigned int volatile * csr = base + offset;
	unsigned int val;

	val = *csr;
	DBG("CSR RD %p value 0x%08X\n", csr, val);
	return val;
}

unsigned short csr_rdw(void volatile *base, off_t offset)
{
	unsigned short volatile * csr = base + offset;
	unsigned short val;

	val = *csr;
	DBG("CSR RD %p value 0x%08X\n", csr, val);
	return val;
}

unsigned char csr_rdb(void volatile *base, off_t offset)
{
	unsigned char volatile * csr = base + offset;
	unsigned char val;

	val = *csr;
	DBG("CSR RD %p value 0x%08X\n", csr, val);
	return val;
}

unsigned int reg_read(unsigned long addr, u32 len)
{
	off_t          dev_base = addr;
	size_t         mask_size = 64 * 1024;
	unsigned long  mask = mask_size - 1;
	void          *mapped_base;
	int            memfd;
	unsigned int   val;

	memfd = open("/dev/mem", O_RDWR | O_SYNC);
	if (memfd < 0) {
		/*try with vediagmem node*/
		memfd = open("/dev/vediagmem", O_RDWR | O_SYNC);
		if (memfd < 0) {
			xerror("Error: '/dev/mem/ and /dev/vediagmem/ are not exist\n");
			exit(1);
		}
	}
	mapped_base = mmap(0, mask_size, PROT_READ | PROT_WRITE, MAP_SHARED,
			   memfd, dev_base & ~mask);
	if (mapped_base == MAP_FAILED) {
	    xinfo("mmap addr 0x%lu failure\n", addr);
	    exit(1);
	}

	if(len == 4)
		val = csr_rdl(mapped_base, dev_base & mask);
	else if(len == 2)
		val = csr_rdw(mapped_base, dev_base & mask);
	else
		val = csr_rdb(mapped_base, dev_base & mask);

	munmap(mapped_base, mask_size);
	close(memfd);
	return val;
}

void reg_write(unsigned long addr, unsigned int val, u32 len)
{
	off_t          dev_base = addr;
	size_t         mask_size = 64 * 1024;
	unsigned long  mask = mask_size - 1;
	void          *mapped_base;
	int            memfd;

	memfd = open("/dev/mem", O_RDWR | O_SYNC);
	if (memfd < 0) {
		/*try with vediagmem node*/
		memfd = open("/dev/vediagmem", O_RDWR | O_SYNC);
		if (memfd < 0) {
			xerror("Error: '/dev/mem/ and /dev/vediagmem/ are not exist\n");
			exit(1);
		}
	}
	mapped_base = mmap(0, mask_size, PROT_READ | PROT_WRITE, MAP_SHARED,
				memfd, dev_base & ~mask);
	if (mapped_base == MAP_FAILED) {
		xinfo("mmap addr 0x%lu failure", addr);
	    exit(1);
	}

	if(len == 4)
		csr_wrl(mapped_base, dev_base & mask, val);
	else if(len == 2)
		csr_wrw(mapped_base, dev_base & mask, val);
	else
		csr_wrb(mapped_base, dev_base & mask, val);

	munmap(mapped_base, mask_size);
	close(memfd);
}

#if 0

extern char * chartostr(uchar c);
static uint	dp_last_addr, dp_last_size;
static uint	dp_last_length = 0x40;
static ulong base_address = 0;
#define DISP_LINE_LEN				16

#define MAX_LINE_LENGTH_BYTES (64)
#define DEFAULT_LINE_LENGTH_BYTES (16)
int print_real_buffer(ulong addr, void* data, uint width, uint count, uint linelen)
{
	uint8_t linebuf[MAX_LINE_LENGTH_BYTES];
	uint32_t *uip = (void*) linebuf;
	uint16_t *usp = (void*) linebuf;
	uint8_t *ucp = (void*) linebuf;
	int i;
	int count_bk = count;

	if (linelen * width > MAX_LINE_LENGTH_BYTES)
		linelen = MAX_LINE_LENGTH_BYTES / width;
	if (linelen < 1)
		linelen = DEFAULT_LINE_LENGTH_BYTES / width;

	while (count_bk) {
		xinfo("%08lx:", addr);

		/* check for overflow condition */
		if (count_bk < linelen)
			linelen = count_bk;

		/* Copy from memory into linebuf and print hex values */
		for (i = 0; i < linelen; i++) {
			if (width == 4) {
				// uip[i] = *(volatile uint32_t *) data;
				uip[i] = readl(data);
				xinfo(" %08x", uip[i]);
			} else if (width == 2) {
				// usp[i] = *(volatile uint16_t *) data;
				uip[i] = readw(data);
				xinfo(" %04x", usp[i]);
			} else {
				// ucp[i] = *(volatile uint8_t *) data;
				uip[i] = readb(data);
				xinfo(" %02x", ucp[i]);
			}
			data += width;
		}

		/* Print data in ASCII characters */
		xinfo("    ");
		for (i = 0; i < linelen * width; i++) {
		    xinfo("%s", isprint(ucp[i]) && (ucp[i] < 0x80) ? chartostr(ucp[i]) : ".");
		}
		xinfo("\n");

		/* update references */
		addr += linelen * width;
		count_bk -= linelen;
	}

	return 0;
}

int do_md(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	ulong addr, length;
	int	size;
	int rc = 0;

	/* We use the last specified parameters, unless new ones are
	 * entered.
	 */
	addr = dp_last_addr;
	size = dp_last_size;
	length = dp_last_length;

	if (argc < 2) {
		xprint("Usage:\n%s\n", cmdtp->usage);
		return -1;
	}

	/* New command specified.  Check for a size specification.
	 * Defaults to long if no or incorrect specification.
	 */
	if ((size = cmd_get_data_size(argv[0], 4)) < 0)
		return -1;

	/* Address is specified since argc > 1
	 */
	addr = strtoul(argv[1], NULL, 16);
	addr += base_address;

	/* If another parameter, it is the length to display.
	 * Length is the number of objects, not number of bytes.
	 */
	if (argc > 2)
		length = strtoul(argv[2], NULL, 16);

	/* Print the lines. */
	print_real_buffer(addr, (void*) addr, size, length, DISP_LINE_LEN / size);
	addr += size * length;


	dp_last_addr = addr;
	dp_last_length = length;
	dp_last_size = size;
	return (rc);
}

int do_mw(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	ulong	addr;
	u32 writeval, count;
	int	size;

	if ((argc < 3) || (argc > 4)) {
	    xinfo("Usage:\n%s\n", cmdtp->usage);
		return -1;
	}

	/* Check for size specification.
	*/
	if ((size = cmd_get_data_size(argv[0], 4)) < 1)
		return -1;

	/* Address is specified since argc > 1
	*/
	addr = strtoul(argv[1], NULL, 16);
	addr += base_address;

	/* Get the value to write.
	*/
	writeval = (u32)strtoul(argv[2], NULL, 16);

	/* Count ? */
	if (argc == 4) {
		count = strtoul(argv[3], NULL, 16);
	} else {
		count = 1;
	}

	while (count-- > 0) {
		if (size == 4)
			writel(writeval, (void*) addr);
		else if (size == 2)
			writew((u16)writeval, (void*) addr);
		else
			writeb((u8)writeval, (void*) addr);
		addr += size;
	}
	return 0;
}

VEDIAG_CMD(md, 32, 0, do_md,
		"memory display",
		" md[.b, .w, .l] address [# of objects] - memory display\r\n");


VEDIAG_CMD(mw, 32, 0, do_mw,
	"memory write (fill)",
	" mw[.b, .w, .l] address value [count]  - write memory\r\n"
);

#endif
