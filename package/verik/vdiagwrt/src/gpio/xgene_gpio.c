
#include <stdio.h>
#include <string.h>
#include "gpio.h"
#include "memory.h"

#if 0
int gpio_base_array[2] = {GPIO0_7_REG_BASE, GPIO8_15_REG_BASE};

static int xgene_gpio_set_direction(gpio_pin_t gpio_pin, gpio_direction_t direction)
{
	uint32_t dir_value;
	uint32_t irq_en_value;
	uint32_t irq_type_value;
	uint32_t irq_pol_value;
	uint32_t irq_mask_value;

	uintptr_t gpio_base = 0;

	if (gpio_pin > XGENE_GPIO_MAX_PIN) {
		GPIO_ERROR("Not support GPIO_%d!!!\n", gpio_pin);
		return -1;
	}

	gpio_base = gpio_base_array[gpio_pin / 8];

	GPIODBG("%s : GPIO_%d - base 0x%lx - direction %d\n", __func__, gpio_pin,
			gpio_base, direction);

	gpio_pin %= 8;

	dir_value = mmio_read_32(gpio_base + GPIO_SWPORTA_DDR);
	irq_en_value = mmio_read_32(gpio_base + GPIO_INTEN);
	irq_type_value = mmio_read_32(gpio_base + GPIO_INTTYPE_LEVEL);
	irq_pol_value = mmio_read_32(gpio_base + GPIO_INT_POLARITY);
	irq_mask_value = mmio_read_32(gpio_base + GPIO_INTMASK);

	GPIODBG("GPIO_SWPORTA_DDR   read [0x%lx]=0x%x\n", gpio_base + GPIO_SWPORTA_DDR,
			dir_value);
	GPIODBG("GPIO_INTEN         read [0x%lx]=0x%x\n", gpio_base + GPIO_INTEN,
			irq_en_value);
	GPIODBG("GPIO_INTTYPE_LEVEL read [0x%lx]=0x%x\n",
			gpio_base + GPIO_INTTYPE_LEVEL, irq_type_value);
	GPIODBG("GPIO_INT_POLARITY  read [0x%lx]=0x%x\n", gpio_base + GPIO_INT_POLARITY,
			irq_pol_value);
	GPIODBG("GPIO_INTMASK       read [0x%lx]=0x%x\n", gpio_base + GPIO_INTMASK,
			irq_mask_value);

	switch (direction) {

	case GPIO_IN:
		dir_value &= ~(1 << gpio_pin); /*Set input direction*/
		irq_mask_value &= ~(1 << gpio_pin); /*Unmask IRQ*/
		irq_en_value &= ~(1 << gpio_pin); /*Disable IRQ*/
		break;

	case GPIO_OUT:
		dir_value |= (1 << gpio_pin); /*Set Output direction*/
		irq_mask_value &= ~(1 << gpio_pin);/*Unmask IRQ*/
		irq_en_value &= ~(1 << gpio_pin);/*Disable IRQ*/
		break;

	case GPIO_IRQ_LEVEL_LOW:
		dir_value &= ~(1 << gpio_pin);/*Set input direction*/
		irq_mask_value |= (1 << gpio_pin); /*Mask IRQ*/
		irq_en_value |= (1 << gpio_pin); /*Enable IRQ*/
		irq_type_value &= ~(1 << gpio_pin);/*Select Level type (0)*/
		irq_pol_value &= ~(1 << gpio_pin);/*Active Low (0)*/
		break;

	case GPIO_IRQ_LEVEL_HIGH:
		dir_value &= ~(1 << gpio_pin);/*Set input direction*/
		irq_mask_value |= (1 << gpio_pin); /*Mask IRQ*/
		irq_en_value |= (1 << gpio_pin); /*Enable IRQ*/
		irq_type_value &= ~(1 << gpio_pin);/*Select Level type (0)*/
		irq_pol_value |= (1 << gpio_pin); /*Active High (1)*/
		break;

	case GPIO_IRQ_EDGE_LOW:
	case GPIO_IRQ_EDGE_HIGH:
		printf("Warning : Not support IRQ EDGE type now!!!\n");
		return -1;

	default:
		GPIO_ERROR("No Support direction %d\n", direction);
		return -1;
	}

	GPIODBG("GPIO_SWPORTA_DDR   write 0x%x\n", dir_value);
	GPIODBG("GPIO_INTEN         write 0x%x\n", irq_en_value);
	GPIODBG("GPIO_INTTYPE_LEVEL write 0x%x\n", irq_type_value);
	GPIODBG("GPIO_INT_POLARITY  write 0x%x\n", irq_pol_value);
	GPIODBG("GPIO_INTMASK       write 0x%x\n", irq_mask_value);

	mmio_write_32(gpio_base + GPIO_SWPORTA_DDR, dir_value);
	mmio_write_32(gpio_base + GPIO_INTTYPE_LEVEL, irq_type_value);
	mmio_write_32(gpio_base + GPIO_INT_POLARITY, irq_pol_value);
	mmio_write_32(gpio_base + GPIO_INTMASK, irq_mask_value);
	mmio_write_32(gpio_base + GPIO_INTEN, irq_en_value);

	return 0;
}

static int xgene_gpio_get_direction(gpio_pin_t gpio_pin)
{
	uintptr_t gpio_base = 0;
	gpio_direction_t direction = 0;
	uint32_t reg_value = 0;

	if (gpio_pin > XGENE_GPIO_MAX_PIN) {
		GPIO_ERROR("Not support GPIO_%d!!!\n", gpio_pin);
		return -1;
	}

	gpio_base = gpio_base_array[gpio_pin / 8];

	GPIODBG("%s : GPIO_%d - base 0x%lx\n", __func__, gpio_pin, gpio_base);

	gpio_pin %= 8;

	reg_value = mmio_read_32(gpio_base + GPIO_SWPORTA_DDR);
	GPIODBG("GPIO_SWPORTA_DDR   read [0x%lx]=0x%x\n", gpio_base + GPIO_SWPORTA_DDR,
			reg_value);

	if ((reg_value >> gpio_pin) & 1) {
		direction = GPIO_OUT;

	} else {
		reg_value = mmio_read_32(gpio_base + GPIO_INTEN);
		GPIODBG("GPIO_INTEN   read [0x%lx]=0x%x\n", gpio_base + GPIO_INTEN,
				reg_value);
		direction = ((reg_value >> gpio_pin) & 1); /*0 mean INPUT, 1 mean IRQ*/

		if (direction) {
			reg_value = mmio_read_32(gpio_base + GPIO_INTTYPE_LEVEL);
			GPIODBG("GPIO_INTTYPE_LEVEL   read [0x%lx]=0x%x\n",
					gpio_base + GPIO_INTTYPE_LEVEL, reg_value);
			direction += (((reg_value >> gpio_pin) & 1) * 2); /*0 mean type level, 1 mean type edge*/

			reg_value = mmio_read_32(gpio_base + GPIO_INT_POLARITY);
			GPIODBG("GPIO_INT_POLARITY   read [0x%lx]=0x%x\n",
					gpio_base + GPIO_INT_POLARITY, reg_value);
			direction += ((reg_value >> gpio_pin) & 1); /* 0 mean active low, 1 mean active high*/
		}
	}

	return direction;
}

static int xgene_gpio_get_input_value(gpio_pin_t gpio_pin)
{
	uintptr_t gpio_base = 0;

	if (gpio_pin > XGENE_GPIO_MAX_PIN) {
		GPIO_ERROR("Not support GPIO_%d!!!\n", gpio_pin);
		return -1;
	}

	gpio_base = gpio_base_array[gpio_pin / 8] + GPIO_EXT_PORTA;

	GPIODBG("%s : GPIO_%d - Register 0x%lx\n", __func__, gpio_pin, gpio_base);

	gpio_pin %= 8;

	return ((mmio_read_32(gpio_base) >> gpio_pin) & 1);
}

static int xgene_gpio_set_output_value(gpio_pin_t gpio_pin, gpio_value_t value)
 {
	uint32_t reg_value;
	uintptr_t gpio_base = 0;

	if (gpio_pin > XGENE_GPIO_MAX_PIN) {
		GPIO_ERROR("Not support GPIO_%d!!!\n", gpio_pin);
		return -1;
	}

	gpio_base = gpio_base_array[gpio_pin / 8] + GPIO_SWPORTA_DR;

	GPIODBG("%s : GPIO_%d - Register 0x%lx - Output value %d\n", __func__, gpio_pin,
			gpio_base, value);

	gpio_pin %= 8;

	reg_value = mmio_read_32(gpio_base);

	GPIODBG("reg_value read 0x%x\n", reg_value);

	if (value)
		reg_value |= (1 << gpio_pin);
	else
		reg_value &= ~(1 << gpio_pin);

	GPIODBG("reg_value write 0x%x\n", reg_value);

	mmio_write_32(gpio_base, reg_value);

	return 0;
}

static int xgene_gpio_get_output_value(gpio_pin_t gpio_pin)
 {
	uintptr_t gpio_base = 0;

	if (gpio_pin > XGENE_GPIO_MAX_PIN) {
		GPIO_ERROR("Not support GPIO_%d!!!\n", gpio_pin);
		return -1;
	}

	gpio_base = gpio_base_array[gpio_pin / 8] + GPIO_SWPORTA_DR;

	GPIODBG("%s : GPIO_%d - Register 0x%lx\n", __func__, gpio_pin, gpio_base);

	gpio_pin %= 8;

	return ((mmio_read_32(gpio_base) >> gpio_pin) & 1);
}

static int xgene_gpio_get_irq_status(gpio_pin_t gpio_pin)
 {
	uintptr_t gpio_base = 0;

	if (gpio_pin > XGENE_GPIO_MAX_PIN) {
		GPIO_ERROR("Not support GPIO_%d!!!\n", gpio_pin);
		return -1;
	}

	gpio_base = gpio_base_array[gpio_pin / 8] + GPIO_INTSTATUS;

	GPIODBG("%s : GPIO_%d - Register 0x%lx\n", __func__, gpio_pin, gpio_base);

	gpio_pin %= 8;

	return ((mmio_read_32(gpio_base) >> gpio_pin) & 1);
}

static gpio_ops_t xgene_gpio_ops = {
	.get_direction	= xgene_gpio_get_direction,
	.set_direction	= xgene_gpio_set_direction,
	.get_input_value	= xgene_gpio_get_input_value,
	.set_output_value	= xgene_gpio_set_output_value,
	.get_output_value	= xgene_gpio_get_output_value,
	.get_irq_status		= xgene_gpio_get_irq_status,
};

void xgene_gpio_init(gpio_ops_t **gpio_ops)
{

	GPIODBG("%s\n", __func__);
	*gpio_ops = &xgene_gpio_ops;
}

#else
void xgene_gpio_init(gpio_ops_t **gpio_ops)
{
	*gpio_ops = NULL;
}
#endif

static int xgene_gpi_set_direction(gpio_pin_t gpio_pin, gpio_direction_t direction)
 {
	xinfo("Warning GPI_%d Was Input Only!!!!\n", gpio_pin);
	return -1;
}

static int xgene_gpi_get_direction(gpio_pin_t gpio_pin)
{
	return GPIO_IN;
}

static int xgene_gpi_set_output_value(gpio_pin_t gpio_pin, gpio_value_t value)
{
	xinfo("Warning GPI_%d Was Input Only!!!!\n", gpio_pin);
	return -1;
}

static int xgene_gpi_get_output_value(gpio_pin_t gpio_pin)
{
	xinfo("Warning GPI_%d Was Input Only!!!!\n", gpio_pin);
	return -1;
}

static int xgene_gpi_get_irq_status(gpio_pin_t gpio_pin)
{
	xinfo("Warning GPI_%d Was Input Only!!!!\n", gpio_pin);
	return -1;
}

static int xgene_gpi_get_input_value(gpio_pin_t gpio_pin)
{
	uintptr_t gpio_base = GPI_REG_BASE + GPI_EXT_PORTA;

	GPIODBG("%s : GPI_%d - Register 0x%lx\n", __func__, gpio_pin, gpio_base);

	return ((readl(gpio_base) >> gpio_pin) & 1);
}

static gpio_ops_t xgene_gpi_ops = {
	.get_direction	= xgene_gpi_get_direction,
	.set_direction	= xgene_gpi_set_direction,
	.get_input_value	= xgene_gpi_get_input_value,
	.set_output_value	= xgene_gpi_set_output_value,
	.get_output_value	= xgene_gpi_get_output_value,
	.get_irq_status		= xgene_gpi_get_irq_status,
};

void xgene_gpi_init(gpio_ops_t **gpio_ops)
{
	GPIODBG("%s\n", __func__);
	*gpio_ops = &xgene_gpi_ops;
}
