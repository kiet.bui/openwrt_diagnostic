/*
 * kmsg-reader.h
 *
 *  Created on: Apr 28, 2017
 *      Author: ddh
 */

#ifndef _RAS_KMSG_READER_H_
#define _RAS_KMSG_READER_H_

/* Close the log.  Currently a NOP. */
#define SYSLOG_ACTION_CLOSE          0
/* Open the log. Currently a NOP. */
#define SYSLOG_ACTION_OPEN           1
/* Read from the log. */
#define SYSLOG_ACTION_READ           2
/* Read all messages remaining in the ring buffer. (allowed for non-root) */
#define SYSLOG_ACTION_READ_ALL       3
/* Read and clear all messages remaining in the ring buffer */
#define SYSLOG_ACTION_READ_CLEAR     4
/* Clear ring buffer. */
#define SYSLOG_ACTION_CLEAR          5
/* Disable printk's to console */
#define SYSLOG_ACTION_CONSOLE_OFF    6
/* Enable printk's to console */
#define SYSLOG_ACTION_CONSOLE_ON     7
/* Set level of messages printed to console */
#define SYSLOG_ACTION_CONSOLE_LEVEL  8
/* Return number of unread characters in the log buffer */
#define SYSLOG_ACTION_SIZE_UNREAD    9
/* Return size of the log buffer */
#define SYSLOG_ACTION_SIZE_BUFFER   10

#define KMSG_READ_BEGIN 1
#define KMSG_READ_END   2

enum {
	DMESG_TIMEFTM_NONE = 0,
	DMESG_TIMEFTM_CTIME,		/* [ctime] */
	DMESG_TIMEFTM_CTIME_DELTA,	/* [ctime <delta>] */
	DMESG_TIMEFTM_DELTA,		/* [<delta>] */
	DMESG_TIMEFTM_RELTIME,		/* [relative] */
	DMESG_TIMEFTM_TIME,		/* [time] */
	DMESG_TIMEFTM_TIME_DELTA,	/* [time <delta>] */
	DMESG_TIMEFTM_ISO8601		/* 2013-06-13T22:11:00,123456+0100 */
};
#define is_timefmt(c, f) ((c)->time_fmt == (DMESG_TIMEFTM_ ##f))

struct dmesg_record {
    char *mesg;
    size_t mesg_size;

    int level;
    int facility;
    struct timeval  tv;

    const char *next; /* buffer with next unparsed record */
    size_t next_size; /* size of the next buffer */
};

typedef void (*new_kmsg_handler) (struct dmesg_record *rec);

struct dmesg_control {
    int     action;
    int     kmsg;
    char    kmsg_buf[BUFSIZ];
    int     read_pos;
	unsigned int	time_fmt;	/* time format */
    new_kmsg_handler kmsg_handler;
};


int init_kmsg ( struct dmesg_control *ctl );
int read_buffer( struct dmesg_control *ctl );


#endif /* _RAS_KMSG_READER_H_ */
