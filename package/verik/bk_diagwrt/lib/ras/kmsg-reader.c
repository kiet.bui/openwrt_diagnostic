/*
 * ras-apei.c
 *
 *  Created on: Apr 25, 2017
 *      Author: ddh
 */
/**************************************************************************
 *                            HEADER FILES
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/klog.h>

#include <log.h>
#include <ras/kmsg-reader.h>

/**************************************************************************
 *                       PREPROCESSORS & MACROS
 **************************************************************************/
#define RAS_KMSG_PFX "[RAS_KMSG]: "
#define ras_debug(fmt, ...)  xdebug(RAS_KMSG_PFX fmt, ##__VA_ARGS__)
#define ras_error(fmt, ...)  xerror(RAS_KMSG_PFX fmt, ##__VA_ARGS__)
#define ras_info(fmt, ...)   xinfo(RAS_KMSG_PFX fmt, ##__VA_ARGS__)
#define ras_print(fmt, ...)  xprint(RAS_KMSG_PFX fmt, ##__VA_ARGS__)

/* seek stuff */
#ifndef SEEK_DATA
# define SEEK_DATA  3
#endif
#ifndef SEEK_HOLE
# define SEEK_HOLE  4
#endif

#define from_hex(c)		(isdigit(c) ? c - '0' : tolower(c) - 'a' + 10)

#define INIT_DMESG_RECORD(_r)  \
    do { \
        (_r)->mesg = NULL; \
        (_r)->mesg_size = 0; \
        (_r)->facility = -1; \
        (_r)->level = -1; \
        (_r)->tv.tv_sec = 0; \
        (_r)->tv.tv_usec = 0; \
    } while (0)

#define LAST_KMSG_FIELD(s)  (!s || !*s || *(s - 1) == ';')

/**************************************************************************
 *                    PRIVATE FUNCTION DECLARATIONS
 **************************************************************************/
static const char *skip_item ( const char *begin,
                               const char *end,
                               const char *sep );

static void unhexmangle_to_buffer ( const char *s,
                                    char *buf,
                                    size_t len );

static int parse_kmsg_record ( struct dmesg_control *ctl,
                               struct dmesg_record *rec,
                               char* buf,
                               size_t sz );

static ssize_t read_kmsg_one ( struct dmesg_control *ctl );

static void notify_kmsg_record ( struct dmesg_control *ctl,
                                 struct dmesg_record *rec );

static int read_kmsg ( struct dmesg_control *ctl );

/**************************************************************************
 *                    PRIVATE FUNCTION DEFINITIONS
 **************************************************************************/
static const char *skip_item ( const char *begin,
                               const char *end,
                               const char *sep )
{
    while (begin < end) {
        int c = *begin++;
        if (c == '\0' || strchr(sep, c))
            break;
    }
    return begin;
}

/*
 * Parses timestamp from /dev/kmsg, expected formats:
 *
 *	microseconds,
 *	microseconds;
 *
 * the ',' is fields separators and ';' items terminator (for the last item)
 */
static const char *parse_kmsg_timestamp(const char *str0, struct timeval *tv)
{
	const char *str = str0;
	char *end = NULL;
	uint64_t usec;

	if (!str0)
		return str0;

	errno = 0;
	usec = strtoumax(str, &end, 10);

	if (!errno && end && (*end == ';' || *end == ',')) {
		tv->tv_usec = usec % 1000000;
		tv->tv_sec = usec / 1000000;
	} else
		return str0;

	return end + 1;	/* skip separator */
}

static void unhexmangle_to_buffer ( const char *s,
                                    char *buf,
                                    size_t len )
{
	size_t sz = 0;

	if (!s)
		return;

	while(*s && sz < len - 1) {
        if (*s == '\\' && sz + 3 < len - 1 && s[1] == 'x'
            && isxdigit(s[2]) && isxdigit(s[3])) {

			*buf++ = from_hex(s[2]) << 4 | from_hex(s[3]);
			s += 4;
			sz += 4;
        } else {
			*buf++ = *s++;
			sz++;
		}
	}
	*buf = '\0';
}

static int parse_kmsg_record ( struct dmesg_control *ctl,
                               struct dmesg_record *rec,
                               char* buf,
                               size_t sz )
{
    const char *p = buf, *end;

    if (sz == 0 || !buf || !*buf)
        return -1;

    end = buf + (sz - 1);
    INIT_DMESG_RECORD(rec);

    /* skip left space of buffer */
    while (p < end && isspace(*p))
        p++;

    /* A) skip priority & facility */
    p = skip_item(p, end, ",");
    if (LAST_KMSG_FIELD(p))
        goto _mesg;

    /* B) skip sequence number*/
    p = skip_item(p, end, ",;");
    if (LAST_KMSG_FIELD(p))
        goto _mesg;

    /* C) skip timestamp */
	if (is_timefmt(ctl, NONE))
		p = skip_item(p, end, ",;");
	else
		p = parse_kmsg_timestamp(p, &rec->tv);
    if (LAST_KMSG_FIELD(p))
        goto _mesg;

    /* D) option fields (ignore) */
    p = skip_item(p, end, ";");

_mesg:
    /* E) message text */
    rec->mesg = (char *) p;
    p = skip_item(p, end, "\n");
    if (!p)
        return -1;
    rec->mesg_size = p - rec->mesg;

	/*
	 * Kernel escapes non-printable characters, unfortunately kernel
	 * definition of "non-printable" is too strict. On UTF8 console we can
	 * print many chars, so let's decode from kernel.
	 */
	unhexmangle_to_buffer(rec->mesg, (char *) rec->mesg, rec->mesg_size + 1);

    /* F) message tags (ignore) */

    return 0;
}

static ssize_t read_kmsg_one ( struct dmesg_control *ctl )
{
    ssize_t size;

    do {
        size = read(ctl->kmsg, ctl->kmsg_buf, sizeof(ctl->kmsg_buf) - 1);
    } while (size < 0 && errno == EPIPE);

    return size;
}

static void notify_kmsg_record ( struct dmesg_control *ctl,
                                 struct dmesg_record *rec )
{
    if (ctl->kmsg_handler)
        ctl->kmsg_handler(rec);
}

static int read_kmsg ( struct dmesg_control *ctl )
{
    struct dmesg_record rec;
    ssize_t sz;

    sz = read_kmsg_one(ctl);
    while (sz > 0) {
        *(ctl->kmsg_buf + sz) = '\0';   /* for debug messages */

        if (parse_kmsg_record(ctl, &rec, ctl->kmsg_buf, (size_t) sz) == 0)
            notify_kmsg_record(ctl, &rec);

        sz = read_kmsg_one(ctl);
    }

    return 0;
}

/**************************************************************************
 *                    PUBLIC FUNCTION DEFINITIONS
 **************************************************************************/

int init_kmsg ( struct dmesg_control *ctl )
{
	ctl->kmsg = open("/dev/kmsg", O_RDONLY | O_NONBLOCK);
    if (ctl->kmsg < 0)
        return -1;

    if (ctl->read_pos == KMSG_READ_BEGIN)
        lseek(ctl->kmsg, 0, SEEK_DATA);
    else
        lseek(ctl->kmsg, 0, SEEK_END);

    return 0;
}

int read_buffer( struct dmesg_control *ctl )
{
    ssize_t n = -1;

    n = read_kmsg(ctl);
    if (n == 0 && ctl->action == SYSLOG_ACTION_READ_CLEAR)
        n = klogctl(SYSLOG_ACTION_CLEAR, NULL, 0);

    return n;
}

#if 0
int main ( int argc,
           char **argv )
{
    int n;
	static struct dmesg_control ctl = {
		.action = SYSLOG_ACTION_READ_ALL,
		.kmsg = -1,
		.kmsg_buf = { 0 },
		.read_pos = KMSG_READ_END,
	};

    if (!init_kmsg(&ctl)) {
        n = read_buffer(&ctl);
    }

    return 0;
}
#endif
