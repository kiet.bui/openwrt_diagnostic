/*
 * ras-apei.c
 *
 *  Created on: Apr 25, 2017
 *      Author: ddh
 */

/**************************************************************************
 *                            HEADER FILES
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include <log.h>

#include <ras/ras-apei.h>
#include <ras/kmsg-reader.h>


/**************************************************************************
 *                       PREPROCESSORS & MACROS
 **************************************************************************/
#define GHES_SEC_NO  "{%u}"
#define GHES_PREFIX  "[Hardware Error]: "
#define GHES_ERR_PFX "[Firmware Warn]: "

#ifdef ARRAY_SIZE
#undef ARRAY_SIZE
#endif
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define RAS_APEI_PFX "[RAS_APEI]: "
#define ras_debug(fmt, ...)  xdebug(RAS_APEI_PFX fmt, ##__VA_ARGS__)
#define ras_error(fmt, ...)  xerror(RAS_APEI_PFX fmt, ##__VA_ARGS__)
#define ras_info(fmt, ...)   xinfo(RAS_APEI_PFX fmt, ##__VA_ARGS__)
#define ras_print(fmt, ...)  xprint(RAS_APEI_PFX fmt, ##__VA_ARGS__)

#define RAS_HDR_SOURCE_ID    (1 << 0)
#define RAS_HDR_EVENT_SEV    (1 << 1)
#define RAS_HDR_SECTION_N    (1 << 2)
#define RAS_HDR_SECTION_T    (1 << 3)
#define RAS_HDR_FRU_ID       (1 << 4)
#define RAS_HDR_FRU_TEXT     (1 << 5)
#define RAS_HEADER_OK        (0x0000003F)

#define RAS_PROC_TYPE        (1 << 6)
#define RAS_PROC_ISA         (1 << 7)
#define RAS_PROC_ERR_TYPE    (1 << 8)
#define RAS_PROC_OPERATION   (1 << 9)
#define RAS_PROC_FLAGS       (1 << 10)
#define RAS_PROC_LEVEL       (1 << 11)
#define RAS_PROC_VERSION     (1 << 12)
#define RAS_PROC_ID          (1 << 13)
#define RAS_PROC_TARGET_ADDR (1 << 14)
#define RAS_PROC_REQ_ID      (1 << 15)
#define RAS_PROC_RES_ID      (1 << 16)
#define RAS_PROC_IP          (1 << 17)
#define RAS_PROCESSOR_OK     (0x0003FFC0)

#define RAS_MEM_ERR_STAT     (1 << 18)
#define RAS_MEM_PHY_ADDR     (1 << 29)
#define RAS_MEM_PHY_ADDR_M   (1 << 20)
#define RAS_MEM_DEVICE_ID    (1 << 21)
#define RAS_MEM_ERR_TYPE     (1 << 22)
#define RAS_MEM_DIM_LOC      (1 << 23)
#define RAS_MEMORY_OK        (0x00FC0000)

#define RAS_PCIE_PORT_T      (1 << 24)
#define RAS_PCIE_VERSION     (1 << 25)
#define RAS_PCIE_CMD_STT     (1 << 26)
#define RAS_PCIE_DEVICE_ID   (1 << 27)
#define RAS_PCIE_SERIAL_NUM  (1 << 28)
#define RAS_PCIE_BRIDGE      (1 << 29)
#define RAS_PCIE_OK          (0x3F000000)

#define GHES_DECODE_TIMEOUT  (1) // 2 second


/**************************************************************************
 *                      LOCAL DATA TYPES
 **************************************************************************/
typedef int (*section_decoder) ( struct ghes_generic *ghes,
                                 struct dmesg_record *rec );

typedef struct section_handler {
    int section_type;
    section_decoder decode;
} section_handler;

/**************************************************************************
 *                   PRIVATE FUNCTION DECLARATIONS
 **************************************************************************/
static uint32_t _get_section_type ( const char *str );
static uint32_t _get_severity_type ( const char *str );
static uint32_t _get_mem_err_type ( const char *str );
static uint32_t _get_proc_type ( const char *str );
static uint32_t _get_proc_isa_type ( const char *str );
static uint32_t _get_proc_op_type ( const char *str );
static uint32_t _get_flag_type ( const char *str );
static uint32_t _get_pcie_port_type ( const char *str );

static int _decode_hest_generic_info ( struct ghes_generic *hest_generic,
                                       struct dmesg_record *rec );
static int _decode_sec_proc_generic ( struct ghes_generic *ghes,
                                      struct dmesg_record *rec );
static int _decode_sec_memory ( struct ghes_generic *ghes,
                                struct dmesg_record *rec );
static int _decode_sec_pcie ( struct ghes_generic *ghes,
                              struct dmesg_record *rec );
static int _decode_ghes_header ( struct ghes_generic *ghes,
                                 struct dmesg_record *rec );
static int _is_ghes_rec ( struct dmesg_record *rec,
                          uint32_t *seqno );

static struct ghes_generic * _new_ghes ( void );
static void _delete_ghes ( struct ghes_generic **ghes );
static void _do_callback ( struct ghes_generic *ghes );
static void _decode_ghes ( struct dmesg_record *rec );

/**************************************************************************
 *                      PRIVATE VARIABLES
 **************************************************************************/
static const char * const ghes_fmt_strs[] = {
    /* GHES_SEC_NO GHES_PREFIX */ "Hardware error from APEI Generic Hardware Error Source: %d\n",
    /* GHES_SEC_NO GHES_PREFIX */ "event severity: %s\n",
    /* GHES_SEC_NO GHES_PREFIX */ " Error %d, type: %s\n",
    /* GHES_SEC_NO GHES_PREFIX */ " fru_id: %pUl\n",
    /* GHES_SEC_NO GHES_PREFIX */ " fru_text: %s\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  section_type: %s\n",
};

static const char * const ghes_error_fmt_str[] = {
    GHES_ERR_PFX "error section length is too small\n",
};

static const char * const sec_proc_generic_fmt_str[] = {
    /* GHES_SEC_NO GHES_PREFIX */ "  processor_type: %d, %s\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  processor_isa: %d, %s\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  error_type: 0x%02x\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  operation: %d, %s\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  flags: 0x%02x\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  level: %d\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  version_info: 0x%016llx\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  processor_id: 0x%016llx\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  target_address: 0x%016llx\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  requestor_id: 0x%016llx\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  responder_id: 0x%016llx\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  IP: 0x%016llx\n",
};

static const char * const sec_mem_err_fmt_str[] = {
    /* GHES_SEC_NO GHES_PREFIX */ "  error_status: 0x%016llx\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  physical_address: 0x%016llx\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  physical_address_mask: 0x%016llx\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  node: %d card: %d module: %d rank: %d bank: %d device: %d row: %d column: %d bit_position: %d requestor_id: 0x%016llx responder_id: 0x%016llx target_id: 0x%016llx \n",
    /* GHES_SEC_NO GHES_PREFIX */ "  error_type: %d, %s\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  DIMM location: %s %s \n",
    /* GHES_SEC_NO GHES_PREFIX */ "  DIMM location: %s. DMI handle: 0x%.4x \n",
};

static const char * const sec_pcie_err_fmt_str[] = {
    /* GHES_SEC_NO GHES_PREFIX */ "  port_type: %d, %s\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  version: %d.%d\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  command: 0x%04x, status: 0x%04x\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  device_id: %04x:%02x:%02x.%x\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  slot: %d\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  secondary_bus: 0x%02x\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  vendor_id: 0x%04x, device_id: 0x%04x\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  class_code: %02x%02x%02x\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  serial number: 0x%04x, 0x%04x\n",
    /* GHES_SEC_NO GHES_PREFIX */ "  bridge: secondary_status: 0x%04x, control: 0x%04x\n",
};

static const char * const sec_unknown_fmt_str[] = {
    /* GHES_SEC_NO GHES_PREFIX */ "  section type: unknown, %pUl\n",
};

static const char * const severity_strs[] = {
    "recoverable",
    "fatal",
    "corrected",
    "info",
    "unknown",
};

static const char * const mem_err_type_strs[] = {
    "unknown",
    "no error",
    "single-bit ECC",
    "multi-bit ECC",
    "single-symbol chipkill ECC",
    "multi-symbol chipkill ECC",
    "master abort",
    "target abort",
    "parity error",
    "watchdog timeout",
    "invalid address",
    "mirror Broken",
    "memory sparing",
    "scrub corrected error",
    "scrub uncorrected error",
    "physical memory map-out event",
};

static const char * const proc_type_strs[] = {
    "IA32/X64",
    "IA64",
};

static const char * const proc_isa_strs[] = {
    "IA32",
    "IA64",
    "X64",
};

static const char * const proc_error_type_strs[] = {
    "cache error",
    "TLB error",
    "bus error",
    "micro-architectural error",
};

static const char * const proc_op_strs[] = {
    "unknown or generic",
    "data read",
    "data write",
    "instruction execution",
};

static const char * const proc_flag_strs[] = {
    "restartable",
    "precise IP",
    "overflow",
    "corrected",
};

static const char * const pcie_port_type_strs[] = {
    "PCIe end point",
    "legacy PCI end point",
    "unknown",
    "unknown",
    "root port",
    "upstream switch port",
    "downstream switch port",
    "PCIe to PCI/PCI-X bridge",
    "PCI/PCI-X to PCIe bridge",
    "root complex integrated endpoint device",
    "root complex event collector",
};

static const char * const section_type_strs[] = {
    "general processor error",
    "memory error",
    "PCIe error",
    "unknown",
};

static section_handler section_tbl[] = {
    { SEC_PROC_GENERIC, _decode_sec_proc_generic },
    { SEC_PLATFORM_MEM, _decode_sec_memory },
    { SEC_PCIE, _decode_sec_pcie },
};

static ghes_event_handler cpu_event_handler;
static ghes_event_handler mem_event_handler;
static ghes_event_handler pcie_event_handler;

/**************************************************************************
 *                    PRIVATE FUNCTION DEFINITIONS
 **************************************************************************/
static uint32_t _get_section_type ( const char *str )
{
    uint32_t i = 0;
    for (i = 0; i < ARRAY_SIZE(section_type_strs); ++i)
        if (strstr(section_type_strs[i], str))
            return i;
    return -1;
}

static uint32_t _get_severity_type ( const char *str )
{
    uint32_t i = 0;
    for (i = 0; i < ARRAY_SIZE(severity_strs); ++i)
        if (!strcmp(str, severity_strs[i]))
            return i;
    return -1;
}

static uint32_t __attribute__((unused))
_get_mem_err_type ( const char *str )
{
    uint32_t i = 0;
    for (i = 0; i < ARRAY_SIZE(mem_err_type_strs); ++i)
        if (!strcmp(str, mem_err_type_strs[i]))
            return i;
    return -1;
}

static uint32_t _get_proc_type ( const char *str )
{
    uint32_t i = 0;
    for (i = 0; i < ARRAY_SIZE(proc_type_strs); ++i)
        if (!strcmp(str, proc_type_strs[i]))
            return i;
    return -1;
}

static uint32_t _get_proc_isa_type ( const char *str )
{
    uint32_t i = 0;
    for (i = 0; i < ARRAY_SIZE(proc_isa_strs); ++i)
        if (!strcmp(str, proc_isa_strs[i]))
            return i;
    return -1;
}

static uint32_t _get_proc_op_type ( const char *str )
{
    uint32_t i = 0;
    for (i = 0; i < ARRAY_SIZE(proc_op_strs); ++i)
        if (!strcmp(str, proc_op_strs[i]))
            return i;
    return -1;
}

static uint32_t __attribute__((unused))
_get_flag_type ( const char *str )
{
    uint32_t i = 0;
    for (i = 0; i < ARRAY_SIZE(proc_flag_strs); ++i)
        if (!strcmp(str, proc_flag_strs[i]))
            return i;
    return -1;
}

static uint32_t _get_pcie_port_type ( const char *str )
{
    uint32_t i = 0;
    for (i = 0; i < ARRAY_SIZE(pcie_port_type_strs); ++i)
        if (!strcmp(str, pcie_port_type_strs[i]))
            return i;
    return -1;
}

static int __attribute__((unused))
_decode_hest_generic_info ( struct ghes_generic *hest_generic,
                            struct dmesg_record *rec )
{
    return 0;
}

static int _decode_sec_proc_generic ( struct ghes_generic *ghes,
                                      struct dmesg_record *rec )
{
    uint32_t ret = 0;
    int i = 0, c = 0;
    char proctype_str[256] = { 0 };
    char procisa_str[256] = { 0 };
    char operation_str[256] = { 0 };

    struct sec_proc_generic *proc = &ghes->section.proc;

    /* decode generic header */
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->proc_type, proctype_str);
    if (c == 2) {
        ret |= RAS_PROC_TYPE;
        if (proc->proc_type != _get_proc_type(proctype_str))
            ras_error("Proc Type not consistent: %d %s\n",
                      proc->proc_type,
                      proctype_str);
    }
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->proc_isa, procisa_str);
    if (c == 2) {
        ret |= RAS_PROC_ISA;
        if (proc->proc_isa != _get_proc_isa_type(procisa_str))
            ras_error("Proc ISA not consistent: %d %s\n",
                      proc->proc_isa,
                      procisa_str);
    }
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->proc_error_type);
    if (c == 1)
        ret |= RAS_PROC_ERR_TYPE;
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->operation, operation_str);
    if (c == 2) {
        ret |= RAS_PROC_OPERATION;
        if (proc->operation != _get_proc_op_type(operation_str))
            ras_error("Operation not consistent: %d %s\n",
                      proc->operation,
                      operation_str);
    }
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->flags);
    if (c == 1)
        ret |= RAS_PROC_FLAGS;
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->level);
    if (c == 1)
        ret |= RAS_PROC_LEVEL;
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->cpu_version);
    if (c == 1)
        ret |= RAS_PROC_VERSION;
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->proc_id);
    if (c == 1)
        ret |= RAS_PROC_ID;
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->target_addr);
    if (c == 1)
        ret |= RAS_PROC_TARGET_ADDR;
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->requestor_id);
    if (c == 1)
        ret |= RAS_PROC_REQ_ID;
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->responder_id);
    if (c == 1)
        ret |= RAS_PROC_RES_ID;
    c = sscanf(rec->mesg, sec_proc_generic_fmt_str[i++], &proc->ip);
    if (c == 1)
        ret |= RAS_PROC_IP;

    ghes->section.proc.validation_bits |= ret;
    return ret;
}

static int _decode_sec_memory ( struct ghes_generic *ghes,
                                struct dmesg_record *rec )
{
    uint32_t ret = 0;
    int i = 0, c = 0;
    char errtype_str[256] = { 0 };

    struct sec_memory *mem = &ghes->section.mem;

    /* decode generic header */
    c = sscanf(rec->mesg, sec_mem_err_fmt_str[i++], &mem->error_status);
    if (c == 1)
        ret |= RAS_MEM_ERR_STAT;
    c = sscanf(rec->mesg, sec_mem_err_fmt_str[i++], &mem->phy_addr);
    if (c == 1)
        ret |= RAS_MEM_PHY_ADDR;
    c = sscanf(rec->mesg, sec_mem_err_fmt_str[i++], &mem->phy_addr_mask);
    if (c == 1)
        ret |= RAS_MEM_PHY_ADDR_M;
    c = sscanf(rec->mesg, sec_mem_err_fmt_str[i++],
               &mem->node, &mem->card, &mem->module, &mem->rank, &mem->bank,
               &mem->device, &mem->row, &mem->col, &mem->bit_pos,
               &mem->requestor_id, &mem->responder_id, &mem->target_id);
    if (c == 12)
        ret |= RAS_MEM_DEVICE_ID;
    c = sscanf(rec->mesg, sec_mem_err_fmt_str[i++], &mem->error_type, errtype_str);
    if (c == 2)
        ret |= RAS_MEM_ERR_TYPE;

    c = sscanf(rec->mesg, sec_mem_err_fmt_str[i++], mem->dim_location[0], mem->dim_location[1]);
    if (c == 2)
        ret |= RAS_MEM_DIM_LOC;
    else {
        c = sscanf(rec->mesg, sec_mem_err_fmt_str[i++], mem->dim_location[0], &mem->mem_dev_handle);
        if (c == 2)
            ret |= RAS_MEM_DIM_LOC;
    }

    ghes->section.mem.validation_bits |= ret;
    return ret;
}

static int _decode_sec_pcie ( struct ghes_generic *ghes,
                              struct dmesg_record *rec )
{
    uint32_t ret = 0;
    int i = 0, c = 0;
    char porttype_str[256] = { 0 };

    struct sec_pcie *pcie = &ghes->section.pcie;

    memset(pcie, 0, sizeof(struct sec_pcie));

    /* decode generic header */
    c = sscanf(rec->mesg, sec_pcie_err_fmt_str[i++], &pcie->port_type, porttype_str);
    if (c == 2) {
        ret |= RAS_PCIE_PORT_T;
        if (pcie->port_type != _get_pcie_port_type(porttype_str))
            ras_error("PCIe Port inconsistent: %d %s", pcie->port_type, porttype_str);
    }
    c = sscanf(rec->mesg, sec_pcie_err_fmt_str[i++],
               &pcie->version.major, &pcie->version.minor);
    if (c == 2)
        ret |= RAS_PCIE_VERSION;
    c = sscanf(rec->mesg, sec_pcie_err_fmt_str[i++], &pcie->command, &pcie->status);
    if (c == 2)
        ret |= RAS_PCIE_CMD_STT;
    c = sscanf(rec->mesg, sec_pcie_err_fmt_str[i++],
               &pcie->device_id.segment, &pcie->device_id.bus,
               &pcie->device_id.device, &pcie->device_id.function);
    c += sscanf(rec->mesg, sec_pcie_err_fmt_str[i++], &pcie->device_id.slot);
    c += sscanf(rec->mesg, sec_pcie_err_fmt_str[i++], &pcie->device_id.secondary_bus);
    c += sscanf(rec->mesg, sec_pcie_err_fmt_str[i++],
                &pcie->device_id.vendor_id, &pcie->device_id.device_id);
    c += sscanf(rec->mesg, sec_pcie_err_fmt_str[i++],
                &pcie->device_id.class_code[0],
                &pcie->device_id.class_code[1],
                &pcie->device_id.class_code[2]);
    if (c == 11)
        ret |= RAS_PCIE_DEVICE_ID;
    c = sscanf(rec->mesg, sec_pcie_err_fmt_str[i++],
               &pcie->serial_number.lower, &pcie->serial_number.upper);
    if (c == 2)
        ret |= RAS_PCIE_SERIAL_NUM;
    c = sscanf(rec->mesg, sec_pcie_err_fmt_str[i++],
               &pcie->bridge.secondary_status, &pcie->bridge.control);
    if (c == 2)
        ret |= RAS_PCIE_BRIDGE;

    ghes->section.pcie.validation_bits |= ret;
    return ret;
}

static int _decode_ghes_header ( struct ghes_generic *ghes,
                                 struct dmesg_record *rec )
{
    uint32_t ret = 0;
    int i = 0, c = 0;
    char event_sev_str[256] = { 0 };
    char section_type_str[256] = { 0 };

    /* decode generic header */
    c = sscanf(rec->mesg, ghes_fmt_strs[i++], &ghes->source_id);
    if (c == 1)
        ret |= RAS_HDR_SOURCE_ID;
    c = sscanf(rec->mesg, ghes_fmt_strs[i++], event_sev_str);
    if (c == 1)
        ret |= RAS_HDR_EVENT_SEV;
    c = sscanf(rec->mesg, ghes_fmt_strs[i++], &ghes->section_no, event_sev_str);
    if (c == 2) {
        ret |= RAS_HDR_SECTION_N;
        ghes->event_severity = _get_severity_type(event_sev_str);
    }
    c = sscanf(rec->mesg, ghes_fmt_strs[i++], &ghes->fru_id);
    if (c == 1)
        ret |= RAS_HDR_FRU_ID;
    c = sscanf(rec->mesg, ghes_fmt_strs[i++], &ghes->fru_text);
    if (c == 1)
        ret |= RAS_HDR_FRU_TEXT;
    c = sscanf(rec->mesg, ghes_fmt_strs[i++], section_type_str);
    if (c == 1) {
        ret |= RAS_HDR_SECTION_T;
        ghes->section_type = _get_section_type(section_type_str);
    }

    ghes->validation_bits |= ret;
    return ret;
}

static struct ghes_generic * _new_ghes ( void )
{
    struct ghes_generic *ghes = NULL;
    ghes = malloc(sizeof(struct ghes_generic));
    if (ghes) {
        memset(ghes, 0xFF, sizeof(struct ghes_generic));
        memset(ghes->fru_text, 0, sizeof(ghes->fru_text));
    }
    return ghes;
}

static void _delete_ghes ( struct ghes_generic **ghes )
{
    if (*ghes)
        free(*ghes);
    *ghes = NULL;
}

static int _is_ghes_rec ( struct dmesg_record *rec,
                          uint32_t *seqno )
{
    char *str = NULL, *old_mesg = NULL;

    if (1 == sscanf(rec->mesg, GHES_SEC_NO GHES_PREFIX, seqno)) {
        str = strstr(rec->mesg, GHES_PREFIX);
        if (str) {
            old_mesg = rec->mesg;
            rec->mesg = str + strlen(GHES_PREFIX);
            rec->mesg_size = rec->mesg_size - (rec->mesg - old_mesg);
            return 1;
        }
    }
    return 0;
}

static void _do_callback ( struct ghes_generic *ghes )
{
    // call the relevant handler when decoding completed
    if (ghes->section_type == SEC_PROC_GENERIC) {
        if (cpu_event_handler) {
            cpu_event_handler(ghes);
        }
    }
    else if (ghes->section_type == SEC_PLATFORM_MEM) {
        if (mem_event_handler) {
            mem_event_handler(ghes);
        }
    }
    else if (ghes->section_type == SEC_PCIE) {
        if (pcie_event_handler) {
            pcie_event_handler(ghes);
        }
    }
    else { // decode wrong or bad record
        ras_error("Bad record  0x%x  0x%x  0x%x  0x%x\n",
                  ghes->validation_bits,
                  ghes->section.proc.validation_bits,
                  ghes->section.mem.validation_bits,
                  ghes->section.pcie.validation_bits);
    }
}

static void _decode_ghes ( struct dmesg_record *rec )
{
    static uint32_t old_seqno = 0, cur_seqno = 0;
    static struct ghes_generic *ghes = NULL;
    static time_t start_time, cur_time;
    int i;

    // detect a GHES record
    if (_is_ghes_rec(rec, &cur_seqno)) {
        // this is absolutely a new ghes record.
        if (cur_seqno != old_seqno) {
            // detect new ghes record while processing the current one
            if (ghes)
                _do_callback(ghes); // force decode_done
            // continue with new record
            _delete_ghes(&ghes);
            ghes = _new_ghes();
            ghes->seqno = old_seqno = cur_seqno;
            ras_print("%d\n", cur_seqno);
        }

        // log the time when we start decoding
        time(&start_time);
        // decode ghes
        if (_decode_ghes_header(ghes, rec) == 0) {
            for (i = 0; i < ARRAY_SIZE(section_tbl); ++i) {
                if (section_tbl[i].decode(ghes, rec))
                    break;
            }
        }

    } else { // normal kernel message

    }

    /* FIXME hdang: I have no other way to determine whether
     *       the record is completed or not
     */
    time(&cur_time);
    if ((cur_time - start_time) > GHES_DECODE_TIMEOUT) {
        /* timed out while waiting next ghes information => decoding done */
        _do_callback(ghes);
        _delete_ghes(&ghes);
        start_time = cur_time = 0;
    }

}

/**************************************************************************
 *                     PUBLIC FUNCTION DEFINITIONS
 **************************************************************************/
void dump_ghes ( struct ghes_generic *ghes )
{
    ras_debug(GHES_SEC_NO GHES_PREFIX
              "    source_id       : %d\n"
              "    event_sev       : %d\n"
              "    sec_no          : %d\n"
              "    fru_id          : %ld\n"
              "    fru_text        : %s\n"
              "    sec_type        : %d\n"
              "    validation_bits : 0x%x\n",
              ghes->seqno, ghes->source_id, ghes->event_severity,
              ghes->section_no, ghes->fru_id, ghes->fru_text, ghes->section_type,
              ghes->validation_bits);

    if (ghes->section_type == SEC_PROC_GENERIC) {
    }
    else if (ghes->section_type == SEC_PLATFORM_MEM) {
    }
    else if (ghes->section_type == SEC_PCIE) {
    }

}

int ras_monitor_ghes ( void )
{
    static struct dmesg_control ctl = {
        .action = SYSLOG_ACTION_READ_ALL,
        .kmsg = -1,
        .kmsg_buf = { 0 },
        .read_pos = KMSG_READ_END,
        .kmsg_handler = _decode_ghes,
    };

    if (!init_kmsg(&ctl)) {
        if (!read_buffer(&ctl))
            ras_error("Something wrong while read DMESG buffer\n");
    }

    return 0;
}

void register_cpu_event_handler ( ghes_event_handler handler )
{
    cpu_event_handler = handler;
}

void register_mem_event_handler ( ghes_event_handler handler )
{
    mem_event_handler = handler;
}

void register_pcie_event_handler ( ghes_event_handler handler )
{
    pcie_event_handler = handler;
}
