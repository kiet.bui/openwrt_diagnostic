/*
 * ras-apei.h
 *
 *  Created on: Apr 25, 2017
 *      Author: ddh
 */

#ifndef _RAS_APEI_H_
#define _RAS_APEI_H_

#include <stdint.h>

enum section_type {
    SEC_PROC_GENERIC,
    SEC_PLATFORM_MEM,
    SEC_PCIE,
    SEC_UNKNOWN,
};

enum event_severity {
    SEV_RECOVERABLE,
    SEV_FATAL,
    SEV_CORRECTED,
    SEV_INFO,
    SEV_UNKNOWN,
};

enum mem_error_type {
    MEM_ERR_UNKNOWN,
    MEM_ERR_OK,
    MEM_ERR_SINGLE_BIT_ECC,
    MEM_ERR_MULTI_BIT_ECC,
    MEM_ERR_SINGLE_SYMBOL_CHIPKILL_ECC,
    MEM_ERR_MULTI_SYMBOL_CHIPKILL_ECC,
    MEM_ERR_MASTER_ABORT,
    MEM_ERR_TARGET_ABORT,
    MEM_ERR_PARITY_ABORT,
    MEM_ERR_WATCHDOG_TIMEOUT,
    MEM_ERR_INVALID_ADDRESS,
    MEM_ERR_MEMORY_SPARING,
    MEM_ERR_SCRUB_CORRECTED,
    MEM_ERR_SCRUB_UNCORRECTED,
    MEM_ERR_MEMORY_MAP_OUT_EVENT,
};

enum proc_type {
    PROC_IA32_X64,
    PROC_IA64,
};

enum proc_isa_type {
    ISA_IA32,
    ISA_IA64,
    ISA_X64,
};

enum proc_error_type {
    PROC_ERR_CACHE,
    PROC_ERR_TLB,
    PROC_ERR_BUS,
    PROC_ERR_MICRO_ARCH
};

enum proc_oper_type {
    PROC_OPER_GENERIC,
    PROC_OPER_DATA_READ,
    PROC_OPER_DATA_WRITE,
    PROC_OPER_INS_EXEC,
};

enum proc_flags {
    PROC_FLAG_RESTARTABLE,
    PROC_FLAG_PRECISE_IP,
    PROC_FLAG_OVERFLOW,
    PROC_FLAG_CORRECED,
};

enum pcie_port_type {
    PCIE_PORT_ENDPOINT,
    PCIE_PORT_LEGACY_EP,
    PCIE_PORT_UNKNOWN1,
    PCIE_PORT_UNKNOWN2,
    PCIE_PORT_ROOT,
    PCIE_PORT_UPSTREAM_SWITCH,
    PCIE_PORT_DOWNSTREAM_SWITCH,
    PCIE_PORT_PCIE_TO_PCIX_BRIGDE,
    PCIE_PORT_PCIX_TO_PCIE_BRIDGE,
    PCIE_PORT_ROOT_COMPLEX_INTEGRATED_EP,
    PCIE_PORT_ROOT_COMPLEX_EVENT_COLLECTOR,
};

struct sec_proc_generic {
    uint32_t  validation_bits;
    uint8_t  proc_type;
    uint8_t  proc_isa;
    uint8_t  proc_error_type;
    uint8_t  operation;
    uint8_t  flags;
    uint8_t  level;
    uint64_t cpu_version;
    uint64_t proc_id;
    uint64_t target_addr;
    uint64_t requestor_id;
    uint64_t responder_id;
    uint64_t ip;
};

struct sec_memory {
    uint32_t  validation_bits;
    uint64_t error_status;
    uint64_t phy_addr;
    uint64_t phy_addr_mask;
    uint16_t node;
    uint16_t card;
    uint16_t module;
    uint16_t rank;
    uint16_t bank;
    uint16_t device;
    uint16_t row;
    uint16_t col;
    uint16_t bit_pos;
    uint64_t requestor_id;
    uint64_t responder_id;
    uint64_t target_id;
    uint8_t  error_type;
    uint8_t  dim_location[2][64];
    uint16_t mem_dev_handle;
};

struct sec_pcie {
    uint32_t  validation_bits;
    uint32_t port_type;
    struct {
        uint8_t    minor;
        uint8_t    major;
    } version;
    uint16_t command;
    uint16_t status;
    struct {
        uint16_t   vendor_id;
        uint16_t   device_id;
        uint8_t    class_code[3];
        uint8_t    function;
        uint8_t    device;
        uint16_t   segment;
        uint8_t    bus;
        uint8_t    secondary_bus;
        uint16_t   slot;
    } device_id;
    struct {
        uint32_t   lower;
        uint32_t   upper;
    } serial_number;
    struct {
        uint16_t   secondary_status;
        uint16_t   control;
    } bridge;
};


struct ghes_generic {
    uint32_t  validation_bits;
    uint32_t  seqno;
    uint16_t  source_id;
    uint16_t  event_severity;
    uint32_t  section_no;
    uint64_t  fru_id;
    uint8_t   fru_text[64];
    uint8_t   section_type;
    union {
        struct sec_proc_generic proc;
        struct sec_memory mem;
        struct sec_pcie pcie;
    } section;
};

typedef void (*ghes_event_handler) ( struct ghes_generic *ghes );

int ras_monitor_ghes ( void );
void register_cpu_event_handler ( ghes_event_handler handler );
void register_mem_event_handler ( ghes_event_handler handler );
void register_pcie_event_handler ( ghes_event_handler handler );
void dump_ghes ( struct ghes_generic *ghes );

#endif /* _RAS_APEI_H_ */
