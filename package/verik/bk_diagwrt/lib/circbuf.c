/*
 * circ_buf.c
 */
#include <stdio.h>
#include <common.h>
#include <string.h>
#include <malloc.h>
#include <circbuf.h>
#include <pthread.h>
#include <log.h>

int cbInit(CircularBuffer *cb, int size)
{
	cb->size = size + 1; /* include empty elem */
	cb->tail = 0;
	cb->head = 0;
	cb->elems = (ElemType *) calloc(cb->size, sizeof(ElemType));
	if (!cb->elems) {
		xerror("Error: Out of memory\n");
		return -1;
	}
	memset(&cb->_mutex, 0, sizeof (cb->_mutex));
	return 0;
}

void cbFree(CircularBuffer *cb)
{
	pthread_mutex_lock(&cb->_mutex);
	free(cb->elems); /* OK if null */
	cb->elems = NULL;
	pthread_mutex_unlock(&cb->_mutex);
}

int cbIsFull(CircularBuffer *cb)
{
	int ret;
	pthread_mutex_lock(&cb->_mutex);
	ret = (cb->head + 1) % cb->size == cb->tail;
	pthread_mutex_unlock(&cb->_mutex);
	return ret;
}

int cbIsEmpty(CircularBuffer *cb)
{
	int ret;
	pthread_mutex_lock(&cb->_mutex);
	ret = cb->head == cb->tail;
	pthread_mutex_unlock(&cb->_mutex);
	return ret;
}

/* Write an element, overwriting oldest element if buffer is full. App can
 choose to avoid the overwrite by checking cbIsFull(). */
void cbWrite(CircularBuffer *cb, ElemType *elem)
{
	debug("cbWrite, head=%d, tail=%d\n", cb->head, cb->tail);

	if (!cb->elems || !elem)
		return;

	pthread_mutex_lock(&cb->_mutex);
	memcpy(cb->elems[cb->head].buf, elem->buf, sizeof(ElemType));
	cb->head = (cb->head + 1) % cb->size;
	if (cb->head == cb->tail) {
		debug("full, overwrite\n");
		cb->tail = (cb->tail + 1) % cb->size; /* full, overwrite */
	}
	pthread_mutex_unlock(&cb->_mutex);
	debug("cbWrite, head=%d, tail=%d\n", cb->head, cb->tail);
}

/* Read oldest element. App must ensure !cbIsEmpty() first. */
void cbRead(CircularBuffer *cb, ElemType *elem)
{
	debug("head=%d, tail=%d\n", cb->head, cb->tail);

	if (!cb->elems || !elem)
		return;

	pthread_mutex_lock(&cb->_mutex);
	memcpy(elem->buf, cb->elems[cb->tail].buf, sizeof(ElemType));
	cb->tail = (cb->tail + 1) % cb->size;
	pthread_mutex_unlock(&cb->_mutex);
}

#define CONFIG_TEST_CIRBUF
#ifdef CONFIG_TEST_CIRBUF
#include <cmd.h>
int do_cirbuf(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]) {
	CircularBuffer cb;
	ElemType elem = { "1. The 1st buffer." };

	int testBufferSize = 10; /* arbitrary size */
	if (argc >= 2) {
		testBufferSize = strtoul(argv[1], NULL, 10);
	}
	cbInit(&cb, testBufferSize);

	xinfo("count in buffer = %d\n", CIRC_CNT(cb.head, cb.tail, cb.size));
	xinfo("circular space = %d\n", CIRC_SPACE(cb.head, cb.tail, cb.size));
	xinfo("circular count to end = %d\n",
			CIRC_CNT_TO_END(cb.head, cb.tail, cb.size));
	xinfo("circular space to end = %d\n",
			CIRC_SPACE_TO_END(cb.head, cb.tail, cb.size));


	/* Fill buffer with test elements 3 times */
	int i;
	char buf[128] = { 0 };
	for (i = 1; i <= 6; i++) {
		sprintf(buf, "%d. The %d buffer", i, i);
		memcpy(elem.buf, buf, sizeof(ElemType));
		cbWrite(&cb, &elem);
	}

	xinfo("count in buffer = %d\n", CIRC_CNT(cb.head, cb.tail, cb.size));
	xinfo("circular space = %d\n", CIRC_SPACE(cb.head, cb.tail, cb.size));
	xinfo("circular count to end = %d\n",
			CIRC_CNT_TO_END(cb.head, cb.tail, cb.size));
	xinfo("circular space to end = %d\n",
			CIRC_SPACE_TO_END(cb.head, cb.tail, cb.size));

	/* Remove and print all elements */
	while (!cbIsEmpty(&cb)) {
		cbRead(&cb, &elem);
		xinfo("%s\n", elem.buf);
	}

	cbFree(&cb);
	return 0;
}


VEDIAG_CMD(cirbuf, 32, 0, do_cirbuf,
		"Testing circular buffer",
		"Usage: \n" \
		"\tcirbuf [size]"
		)

#endif
