/*
 * cmd_binarytree.c
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <binary_tree.h>
#include <cmd.h>
#include <log.h>

static int do_binarytree(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	// @test malloc a node
	struct node *root = bt_newNode(120);
	if (!root) {
		xinfo("malloc a node is failure\n");
		return -1;
	}
	// @test insert a node to tree
	bt_insertion(&root, 345);
	bt_insertion(&root, 123);
	bt_insertion(&root, 2);

	bt_insertion(&root, 5);
	bt_insertion(&root, 22);

	// @test delete a node
	bt_deletion(&root, 123);

	// @test order
	xinfo("\nInorder\n");
	bt_inorder(root);
	xinfo("\nPreorder\n");
	bt_preorder(root);
	xinfo("\nPostorder\n");
	bt_postorder(root);
	xinfo("\n");

	// @test search a node with key is 5
	if (bt_search(root, 5) == true) {
	    xinfo("found the node with key is 5\n");
	} else {
	    xinfo("not found the node with key is 5\n");
	}

	// @test get a node
	struct node *find_node = bt_getNode(root, 123);
	if (find_node) {
	    xinfo("get a node with key is %d\n", find_node->key);
	} else {
	    xinfo("not found the node\n");
	}
	// @test destroy the tree
	bt_destroy(&root);
	return 0;
}

VEDIAG_CMD(bt, 32, 0, do_binarytree,
		"Testing binary tree",
		"")

