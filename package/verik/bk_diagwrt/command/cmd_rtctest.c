
/* Stripped-down & simplified ntpclient by tofu
 *
 * Copyright (C) 1997-2010  Larry Doolittle <larry@doolittle.boa.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License (Version 2,
 *  June 1991) as published by the Free Software Foundation.  At the
 *  time of writing, that license was published by the FSF with the URL
 *  http://www.gnu.org/copyleft/gpl.html, and is incorporated herein by
 *  reference.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <poll.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <sys/timex.h>
#include <fcntl.h>
#include <common.h>
#include <cmd.h>
#include "iniparser/iniparser.h"

static const char* ntp_server[] = {
		"0.centos.pool.ntp.org",
		"1.centos.pool.ntp.org",
		"2.centos.pool.ntp.org",
		"3.centos.pool.ntp.org"
};

static struct tm test_date_time = {
		.tm_sec = 1,    /* Seconds.	[0-60] (1 leap second) */
		.tm_min = 0,    /* Minutes.	[0-59] */
		.tm_hour = 0,   /* Hours.	[0-23] */
		.tm_mday = 1,   /* Day.		[1-31] */
		.tm_mon = 0,    /* Month.	[0-11] */
		.tm_year = 117, /* Year	- 1900.  */
		.tm_isdst = -1
};

#define JAN_1970  0x83aa7e80      /* 2208988800 1970 - 1900 in seconds */
#define NTP_PORT  123

/* How to multiply by 4294.967296 quickly (and not quite exactly)
 * without using floating point or greater than 32-bit integers.
 * If you want to fix the last 12 microseconds of error, add in
 * (2911*(x))>>28)
 */
#define NTPFRAC(x) ( 4294*(x) + ( (1981*(x))>>11 ) )

/* The reverse of the above, needed if we want to set our microsecond
 * clock (via settimeofday) based on the incoming time in NTP format.
 * Basically exact.
 */
#define USEC(x) ( ( (x) >> 12 ) - 759 * ( ( ( (x) >> 10 ) + 32768 ) >> 16 ) )

/* Converts NTP delay and dispersion, apparently in seconds scaled
 * by 65536, to microseconds.  RFC1305 states this time is in seconds,
 * doesn't mention the scaling.
 * Should somehow be the same as 1000000 * x / 65536
 */
#define sec2u(x) ( (x) * 15.2587890625 )

#define LI 0
#define VN 3
#define MODE 3
#define STRATUM 0
#define POLL 4
#define PREC -6

struct ntptime {
	unsigned int coarse;
	unsigned int fine;
};

#define VEDIAG_RTCTEST_FILE	"/etc/diagwrt/vediag_rtctest.ini"
static int rtctest_set_test_done(void);
static int query_server(char *srv, uint32_t rx_pkt[12]);

int rtctest_check(void)
{
	__time_t ntp_sec_old = 0, rtc_sec_old = 0, ntp_sec_new;
	char *s;
	int ret = 0;
	int fail = 1;

	dictionary * dic = iniparser_load(VEDIAG_RTCTEST_FILE);
	if (dic == NULL) {
		return -1;
	}
	int ena = iniparser_getboolean(dic, "RTCTEST:Enable", 0);
	if (!ena) {
		ret = 0;
		goto exit;
	} else {
		xinfo(" Found run rtc-test from previous power cycle\n");
	}

	s = iniparser_getstring(dic, "NTP:tv_sec", NULL);
	if (!s) {
		xerror(" 'NTP:tv_sec' key not found\n");
		ret = -1;
		goto exit;
	} else {
		xdebug(" NTP:tv_sec = %s\n", s);
		ntp_sec_old = strtol(s, NULL, 10);
	}

	s = iniparser_getstring(dic, "rtc:tv_sec", NULL);
	if (!s) {
		xerror(" 'RTC:tv_sec' key not found\n");
		ret = -1;
		goto exit;
	} else {
		xdebug(" RTC:tv_sec = %s\n", s);
		rtc_sec_old = strtol(s, NULL, 10);
	}

	/* Get current time (RTC time) */
	struct timeval now;
	gettimeofday(&now, NULL);
	if (now.tv_sec > rtc_sec_old) {
		fail = 0;
	} else {
		xerror(" Error: RTC does not count up from last power cycle!\n");
		xinfo("         now.tv_sec = %ld\n", now.tv_sec);
		xinfo("    previous.tv_sec = %ld\n", rtc_sec_old);
		ret = -1;
		goto exit;
	}

	/* Query timestamp from NTP server */
	uint32_t rx_pkt[12] = {0};
	int i;

	for (i = 0; i < ARRAY_SIZE(ntp_server); i++) {
		char *srv = (char*) ntp_server[i];
		int rc = query_server(srv, rx_pkt);

		/* Get time ok! */
		if (0 == rc) {
			break;
		}
	}

	if (i == ARRAY_SIZE(ntp_server)){
		xinfo(" Info: Cannot get timestamp from server\n");
		ret = 0;
		/* Test still pass because RTC count up*/
		xinfo("\n Info: RTC counts up, test PASS \n");
		goto exit;
	}

	struct tm* tm_info;
	char buffer[64] = { 0 };

	ntp_sec_new = ntohl(((uint32_t *) rx_pkt)[10]) - JAN_1970;

	xinfo(" NTP:\n");
	tm_info = localtime(&ntp_sec_old);
	strftime(buffer, 63, "%F %H:%M:%S", tm_info);
	xinfo("    Previous: %s\n", buffer);
	tm_info = localtime(&ntp_sec_new);
	strftime(buffer, 63, "%F %H:%M:%S", tm_info);
	xinfo("    Current : %s\n", buffer);

	tm_info = localtime(&rtc_sec_old);
	strftime(buffer, 63, "%F %H:%M:%S", tm_info);
	xinfo(" RTC:\n");
	xinfo("    Previous: %s\n", buffer);
	tm_info = localtime(&now.tv_sec);
	strftime(buffer, 63, "%F %H:%M:%S", tm_info);
	xinfo("    Current : %s\n", buffer);

	long diff_ntp = ntp_sec_new - ntp_sec_old;
	long diff_rtc = now.tv_sec - rtc_sec_old;
	xdebug("  RTC time diff: %ld (sec)\n", diff_rtc);
	xdebug("  NTP time diff: %ld (sec)\n", diff_ntp);

	if (abs(diff_rtc - diff_ntp) < 5) {
		xinfo(" RTC test PASS\n");
	} else {
		xinfo(" RTC test FAIL, time_diff between NTP and RTC is too big\n");
	}

	// restore current time
	struct timeval cur_time;
	cur_time.tv_sec = ntp_sec_new;
	cur_time.tv_usec = 0;
	if (settimeofday(&cur_time, NULL) < 0) {
		perror("settimeofday"); /* Ouch, this should not happen :-( */
	} else {
		xinfo(" Info: Restore to new NTP time...\n");
		/* Set to hardware RTC */
		system("hwclock --systohc");
	}

exit:
	rtctest_set_test_done();
	iniparser_freedict(dic);
	return ret;
}

static int rtctest_set_test_done(void)
{
	char* file_path = VEDIAG_RTCTEST_FILE;

	FILE* file = fopen(file_path, "w");
	if (!file){
		xerror("Error: Can't create '%s'\n", file_path);
		return -1;
	}

	dictionary * dic = dictionary_new(0);
	if (dic == NULL) {
		fclose(file);
		xerror("Can't parse file [%s]", file_path);
		return -1;
	}

	iniparser_set(dic, "RTCTEST", NULL);
	iniparser_set(dic, "RTCTEST:Enable", "N");

	iniparser_dump_ini(dic, file);
	fclose(file);
	iniparser_freedict(dic);
	return 0;
}

/**
 * Write test data to file
 * @param ntp_tv: Timestamp get from NTP server
 * @param rtc_tv: Timestamp write to RTC
 * @return
 */
static int rtctest_add_new_test(struct timeval* ntp_tv, struct timeval* rtc_tv)
{
	char* file_path = VEDIAG_RTCTEST_FILE;
	char value[64] = {0};

	FILE* file = fopen(file_path, "w");
	if (!file){
		xerror("Error: Can't create '%s'\n", file_path);
		return -1;
	}
	dictionary * dic = dictionary_new(0);
	if (dic == NULL) {
		fclose(file);
		xerror("Can't parse file [%s]", file_path);
		return -1;
	}

	iniparser_set(dic, "RTCTEST", NULL);
	iniparser_set(dic, "RTCTEST:Enable", "Y");

	iniparser_set(dic, "NTP", NULL);
	snprintf(value, 63, "%lu", ntp_tv->tv_sec);
	iniparser_set(dic, "NTP:tv_sec", value);

	iniparser_set(dic, "RTC", NULL);
	snprintf(value, 63, "%lu", rtc_tv->tv_sec);
	iniparser_set(dic, "RTC:tv_sec", value);

	iniparser_dump_ini(dic, file);
	fclose(file);
	iniparser_freedict(dic);

	if (settimeofday(rtc_tv, NULL) < 0) {
		perror("settimeofday");
		return 1; /* Ouch, this should not happen :-( */
	}

	return 0;
}

static void send_packet(int sd)
{
	uint32_t data[12];
	struct timeval now;

	memset(data, 0, sizeof(data));
	data[0] = htonl (
		( LI << 30 ) | ( VN << 27 ) | ( MODE << 24 ) |
		( STRATUM << 16) | ( POLL << 8 ) | ( PREC & 0xff ) );
	data[1] = htonl(1<<16);  /* Root Delay (seconds) */
	data[2] = htonl(1<<16);  /* Root Dispersion (seconds) */
	gettimeofday(&now,NULL);
	data[10] = htonl(now.tv_sec + JAN_1970); /* Transmit Timestamp coarse */
	data[11] = htonl(NTPFRAC(now.tv_usec));  /* Transmit Timestamp fine   */
	send(sd,data,48,0);
}

static int set_time(char *srv, uint32_t *data)
{
	struct timeval ntp_tv;
	struct timeval rtc_tv;
	struct tm* tm_info;
	char buffer[80] = { 0 };

	ntp_tv.tv_sec = ntohl(((uint32_t *) data)[10]) - JAN_1970;
	ntp_tv.tv_usec = USEC(ntohl(((uint32_t * )data)[11]));

	tm_info = localtime(&ntp_tv.tv_sec);
	strftime(buffer, 63, "%F %H:%M:%S", tm_info);
	xinfo(" NTP time: %s\n", buffer);


	time_t ret = mktime((struct tm *)&test_date_time);
	if (ret == (time_t)-1) {
		printf("Error: unable to make time using mktime\n");
		return -1;
	} else {
		rtc_tv.tv_sec = ret;
		rtc_tv.tv_usec = 0;
		tm_info = localtime(&rtc_tv.tv_sec);
		strftime(buffer, 63, "%F %H:%M:%S", tm_info);
		xinfo(" RTC time: %s\n", buffer);
		return rtctest_add_new_test(&ntp_tv, &rtc_tv);
	}

	return -1;
}

/**
 * Get timestamp from NTP sever
 * @param srv
 * @param rx_pkt
 * @return 0 if success
 */
static int query_server(char *srv, uint32_t rx_pkt[12])
{
	int sd, rc;
	struct pollfd pfd;
	struct hostent *he;
	struct sockaddr_in sa;

	sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sd == -1) {
		xinfo("socket error");
		return -1;	/* Fatal error, cannot even create a socket? */
	}

	he = gethostbyname(srv);
	if (!he) {
		perror("gethostbyname");
		close(sd);

		return 1;	/* Failure in name resolution. */
	}

	memset(&sa, 0, sizeof(sa));
	memcpy(&sa.sin_addr, he->h_addr_list[0], sizeof(sa.sin_addr));
	sa.sin_port = htons(NTP_PORT);
	sa.sin_family = AF_INET;

	xinfo(" Connecting to %s [%s]...\n", srv, inet_ntoa(sa.sin_addr));
	if (connect(sd, (struct sockaddr*)&sa, sizeof(sa)) == -1) {
		perror("connect");
		close(sd);
		return 1;      /* Cannot connect to server, try next. */
	}

	/* Send NTP query to server ... */
	send_packet(sd);

	/* Wait for reply from server ... */
	pfd.fd = sd;
	pfd.events = POLLIN;
	rc = poll(&pfd, 1, 3000);
	if (rc == 1) {
		int len;
		uint32_t packet[12];

		len = recv(sd, packet, sizeof(packet), 0);
		if (len == sizeof(packet)) {
			close(sd);
			/* Looks good, try setting time on host ... */
			memcpy(rx_pkt, packet, 12 * 4);

			return 0;          /* All done! :) */
		}
	} else if (rc == 0) {
		xinfo("Timed out waiting for %s [%s].", srv, inet_ntoa(sa.sin_addr));
	}

	close(sd);

	return 1;			   /* No luck, try next server. */
}

/* Connects to each server listed on the command line and sets the time */
int rtctest_main(int argc, char *argv[])
{
	int i;
	char buf[128];

	for (i = 0; i < ARRAY_SIZE(ntp_server); i++) {
		char *srv = (char*) ntp_server[i];
		uint32_t rx_pkt[12] = { 0 };
		int rc = query_server(srv, rx_pkt);

		/* Done, time set! */
		if (0 == rc) {
			if (set_time(srv, rx_pkt) == 0){
				/* Disable Chrony NTP service */
				system ("systemctl disable chronyd.service");
				sleep(1);
				/* Set to hardware RTC */
				system("hwclock --systohc");
				sleep(1);
				system("sync");
				sleep(1);
				xinfo(" Info: Set new date/time to RTC done\n");
				xinfo(" Info: Call power cycle script...\n");

				/* Power cycle */
				snprintf(buf, 127, "./%s\n", argv[1]);
				run_command(buf);
			}
			return 0;
		}
	}

	return 1;
}

/**
 * Local Variables:
 *  compile-command: "make mini-ntpclient"
 *  indent-tabs-mode: t
 *  c-file-style: "linux"
 * End:
 */


static int do_rtctest(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if (argc != 2) {
		goto exit_usage;
	}

	return rtctest_main(argc, argv);

exit_usage:
	xinfo("Usage:\n");
	xinfo("%s", cmdtp->help);
	return COMMAND_ERROR;
}

VEDIAG_CMD(rtctest, 1, 1, do_rtctest,
		"Set rtc to hardware and power cycle",
		" rtctest <power cycle script>\n")
