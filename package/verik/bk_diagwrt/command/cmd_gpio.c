#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmd.h>
#include <vediag/gpio.h>

int do_gpio (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
 {
	/*gpio_direction_t direction;*/
	gpio_value_t value;
	char *gpio_pin;
	char *cmd;
	int rc = COMMAND_OK;

	if (argc < 3) {
		GPIO_ERROR("Wrong command\n");
		return COMMAND_ERROR;
	}

	cmd = argv[1];
	gpio_pin = argv[2];

	if (!strcmp(cmd, "set")) {

		if (argc < 4) {
			GPIO_ERROR("Miss output value in command\n");
			return COMMAND_ERROR;

		} else {
			value = strtoul(argv[3], NULL, 0);

			if(gpio_set_output_value(gpio_pin, value) < 0) {
				GPIO_ERROR("Set output value failed!!!\n");
				return COMMAND_ERROR;
			}

			if (gpio_get_direction(gpio_pin) != GPIO_OUT)
				rc = gpio_set_direction(gpio_pin, GPIO_OUT);

			if (rc < 0) {
				return COMMAND_ERROR;
			} else {
				xinfo("%s : Set output value is %d\n", gpio_pin, value);
				return COMMAND_OK;
			}
		}

	} else if (!strcmp(cmd, "get")) {

		if (gpio_get_direction(gpio_pin) != GPIO_IN) {

			if (gpio_set_direction(gpio_pin, GPIO_IN) < 0) {

				GPIO_ERROR("Config : %s to input failed!!!\n", gpio_pin);
				return COMMAND_ERROR;
			}
		}

		xinfo("%s : Input value %d\n", gpio_pin, gpio_get_input_value(gpio_pin));
	}

	return rc;
}

VEDIAG_CMD(gpio, 5, 0, do_gpio,
		" gpio command",
		" gpio status <board|pin>            - Board GPIO table status or one pin status\n"
		" gpio irq    <pin> [<type> <active>]- Config gpio as external irq or check irq status\n"
		" gpio get    <pin>                  - Get gpi|gpio input value\n"
		" gpio set    <pin> <value>          - Set gpio output value\n"
		"     pin = [gpi0...gpi15]\n"
		"     pin = [gpio0...gpio15]\n"
		"     type   = level|edge\n"
		"     active = low|high\n"
		"     value = 0|1\n"
		" Ex :\n"
		" gpio status board         - Show all gpio status in board gpio table"
		" gpio status gpio0         - Show gpio0 status\n"
		" gpio irq gpio7            - Check interrupt status gpio7\n"
		" gpio irq gpio9 level high - Config gpio9 was ext irq type level active high\n"
		" gpio get gpi2             - Get input value of gpi2\n"
		" gpio set gpio10 0         - Set output value for gpio10 is 0");
