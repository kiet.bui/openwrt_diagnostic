
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <string_plus.h>
#include <sys/time.h>
#include <unistd.h>
#include <common.h>
#include <stdio.h>
#include <unistd.h>
#include <cmd.h>
#include <memory.h>
#include <time.h>
#include <time_util.h>
#include <vediag_common.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/kernel.h>
#include <log.h>
#include <vediag/event.h>

int vediag_list_event_device(void)
{
	int total_event = 0;
	FILE *devf;
	int line_num = 0;
	char *line, *word, *type;
	char buf[TEXT_LINE_SIZE], cmd[TEXT_LINE_SIZE];

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "cat /proc/interrupts");
	vediag_debug(VEDIAG_EVENT, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			/* Check if first word is not format 'dddd:' */
			word = get_word_in_string(line, " ", 0);
			vediag_debug(VEDIAG_EVENT, "vediag_list_event_device", "word#0: '%s'\n", word);
			if ((word == NULL) || (check_string_isdigit(word, strlen(word) - 1) == 0)) {
				free(word);
				continue;
			}
			free(word);

			/* Check type */
			type = get_word_in_string(line, " ", 2);
			vediag_debug(VEDIAG_EVENT, "vediag_list_event_device", "type#2: '%s'\n", type);
			/* If event no name */
			if (type == NULL) {
				continue;
			}

			/* Get event name */
			word = get_word_in_string(line, " ", 4);
			vediag_debug(VEDIAG_EVENT, "vediag_list_event_device", "word#4: '%s'\n", word);
			/* If event no name */
			if(word == NULL)
				continue;

			/* Remove  LF, CR, CRLF, LFCR */
			word[strcspn(word, "\r\n")] = 0;

			vediag_info(VEDIAG_EVENT, "", "%s\t%s\n", type, word);

			free(type);
			free(word);
			total_event++;
		}
		pclose(devf);
		vediag_info(VEDIAG_EVENT, "", "Found : %d\n", total_event);
	}
	return total_event;
}

int event_count(struct event_test *test)
{
	u32 event_count = 0;
	FILE *devf;
	int line_num = 0;
	char *line, *word, *type, *exttype;
	char buf[TEXT_LINE_SIZE], cmd[60l];

	vediag_info(VEDIAG_EVENT, test->name, "Please press button/trigger!\n");

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "cat /proc/interrupts");
	//vediag_debug(VEDIAG_EVENT, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			/* Check if first word is not format 'dddd:' */
			word = get_word_in_string(line, " ", 0);
			//vediag_debug(VEDIAG_EVENT, "event_count", "word#0: '%s'\n", word);
			if ((word == NULL) || (check_string_isdigit(word, strlen(word) - 1) == 0)) {
				free(word);
				continue;
			}
			free(word);

			/* Check type GPIO */
			type = get_word_in_string(line, " ", 2);
			//vediag_debug(VEDIAG_EVENT, "event_count", "type#2: '%s'\n", type);
			/* If event no name */
			if (type == NULL)
				continue;

			if (strcmp(type, "GPIO") != 0) {
				/* Kernel 3.18.20:
				 *  29:          0  44e07000.gpio   6  mmc0
				 *  42:         22  44e07000.gpio  19  Motion_sensor
				 *  91:          0  481ac000.gpio   2  Restore_button
				 *  94:          0  481ac000.gpio   5  WPS_button
				 */
				exttype = get_word_in_string(type, ".", 1);
				if (exttype == NULL)
					continue;

				if (strcmp(exttype, "gpio") != 0) {
					free(exttype);
					continue;
				}
				free(exttype);
			}
			free(type);

			/* Check event name */
			word = get_word_in_string(line, " ", 4);
			//vediag_debug(VEDIAG_EVENT, test->name, "event_count() word#4: '%s'\n", word);
			/* If event no name */
			if ((word == NULL) || (strncmp(word, test->name, min(strlen(word), strlen(test->name))) != 0)) {
				free(word);
				continue;
			}
			free(word);

			/* Get event count */
			word = get_word_in_string(line, " ", 1);
			//vediag_debug(VEDIAG_EVENT, "event_count", "word#1: '%s'\n", word);
			/* If event no name */
			if (word == NULL) {
				continue;
			}

			/* Remove  LF, CR, CRLF, LFCR */
			//word[strcspn(word, "\r\n")] = 0;

			vediag_debug(VEDIAG_EVENT, test->name, "Count: '%s'\n", word);
			event_count = strtoul(word, NULL, 0);

			free(word);
		}
		pclose(devf);
		//vediag_debug(VEDIAG_EVENT, "Count:", "%d\n", event_count);
	}
	sleep(5);
	return event_count;
}

int event_test(struct event_test *test)
{
	u32 count = 0, time = 0;

	vediag_info(VEDIAG_EVENT, "count:", "%d\n", count);
	test->count1 = event_count(test);
	while (1) {
		test->count2 = event_count(test);
		if (test->count2 > test->count1) {
			count += test->count2 - test->count1;
			vediag_info(VEDIAG_EVENT, "count:", "%d\n", count);
			test->count1 = test->count2;
		}

		/* Check to complete test */
		if (test->cycle && (count > test->cycle))
			break;

		/* Check hotkey */
		if (ctrlc())
			return 1;
		else
			sleep(1);

		/* Check timeout */
		if (test->timeout && (++time > test->timeout))
			return 1;
	}
	return 0;
}

int do_event(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	struct event_test test;
	int ret = 0;

	if (argc > 1) {
		if (strncmp(argv[1], "ls", 2) == 0) {
			vediag_list_event_device();
		} else if (strcmp(argv[1], "test") == 0) {
			if(argc < 3)
				goto usage;

			memset(&test, 0, sizeof(struct event_test));
			test.name = argv[2];
			if(argc >= 4)
				test.timeout = strtoul(argv[3], NULL, 0);
			if(argc >= 5)
				test.cycle = strtoul(argv[4], NULL, 0);
			ret = event_test(&test);
		} else {
			goto usage;
		}
	} else {
		goto usage;
	}

	return ret;

usage:
    xinfo("Usage: \n");
    xinfo("%s\n", cmdtp->help);

	return -1;
}

VEDIAG_CMD(event, 5, 0, do_event,
	"Event Monitoring Command Utilities",
	" event ls 			      		- List all Events\r\n"
	" event mon <name> [timeout] [cnt]	- Monitoring Event in nsec\r\n"
	"\n Argument:\n"
	"    - <name> 	: Name of Event\r\n"
	"    - [timeout]: Max Time to monitor\r\n"
	"    - [cnt]	: Max event monitored\r\n");
