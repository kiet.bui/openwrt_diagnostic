
#include <stdio.h>
#include <vediag_common.h>
#include <cmd.h>

int do_run(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int i;

	if (argc < 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return COMMAND_ERROR;
	}

	for (i = 1; i < argc; ++i) {
		char *arg;

		if ((arg = xgetenv(argv[i])) == NULL) {
			printf("## Error: \"%s\" not defined\n", argv[i]);
			return COMMAND_ERROR;
		}
		if (run_command(arg) == COMMAND_ERROR)
			return COMMAND_ERROR;
	}

	return COMMAND_OK;
}

VEDIAG_CMD(run, 1, 1, do_run,
		"Run commands in an environment variable",
		"run var [...]\n"
		"    - run the commands in the environment variable(s) 'var'\r\n")
