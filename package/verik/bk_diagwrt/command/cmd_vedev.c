
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <cmd.h>
#include <string.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include "iniparser.h"
#include <log.h>
#include <fcntl.h>

extern char file_system[128];
extern char file_testconfig[128];
extern void vediag_create_default_system(void);

static void __vediag_print_identified_devices(int type)
{
	char section[64];
	char *device, *physical_dev;
	char *val;
	int cnt = 0;
	int dev;

	/* Parse device */
	val = vediag_parse_system(get_section_type(type));
	if (val)
		cnt = strtoul(val, NULL, 0);

	xinfo("[%s - %d device(s)]\n", get_section_type(type), cnt);
	for (dev = 1; dev <= cnt; dev++) {
		memset(section, 0 , sizeof(section));
	    sprintf(section, "%s%d", get_section_type(type), dev);
		device = vediag_parse_system_section(section, "Device");
		physical_dev = vediag_parse_system_section(section, "Physical");
		xinfo("%d. %-16s ==> %s\n", dev, (device ? device : "Unknown"), (physical_dev ? physical_dev : "Unknown"));
	}
}

static int vediag_print_identified_devices(void)
{
	/* Parse storage device */
	__vediag_print_identified_devices(VEDIAG_MEM);
#ifdef CONFIG_VEDIAG_STORAGE_TEST
	__vediag_print_identified_devices(VEDIAG_STORAGE);
#endif
#ifdef CONFIG_VEDIAG_NET_TEST
	__vediag_print_identified_devices(VEDIAG_NET);
#endif
#ifdef CONFIG_VEDIAG_PCI_TEST
	__vediag_print_identified_devices(VEDIAG_PCIE);
#endif
#ifdef CONFIG_VEDIAG_LED_TEST
	__vediag_print_identified_devices(VEDIAG_LED);
#endif
#ifdef CONFIG_VEDIAG_EVENT_TEST
	__vediag_print_identified_devices(VEDIAG_EVENT);
#endif
#ifdef CONFIG_VEDIAG_I2C_TEST
	__vediag_print_identified_devices(VEDIAG_I2C);
#endif
#ifdef CONFIG_VEDIAG_AUDIO_TEST
	__vediag_print_identified_devices(VEDIAG_AUDIO);
#endif
#ifdef CONFIG_VEDIAG_MICRO_TEST
	__vediag_print_identified_devices(VEDIAG_MICRO);
#endif
#ifdef CONFIG_VEDIAG_BLUETOOTH_TEST
	__vediag_print_identified_devices(VEDIAG_BLUETOOTH);
#endif
#ifdef CONFIG_VEDIAG_FIRMWARE
	__vediag_print_identified_devices(VEDIAG_FIRMWARE);
#endif
#ifdef CONFIG_VEDIAG_ZWAVE_TEST
	__vediag_print_identified_devices(VEDIAG_ZWAVE);
#endif
#ifdef CONFIG_VEDIAG_ZIGBEE_TEST
	__vediag_print_identified_devices(VEDIAG_ZIGBEE);
#endif
	return 0;
}

static int __vediag_identify_device_type(int type, char *device, char *physical_dev)
{
	char section[64];
	char *val;
	int cnt = 0;
	int dev;
	int section_found = 0;

	/* Find storage device */
	val = vediag_parse_system(get_section_type(type));
	if (val)
		cnt = strtoul(val, NULL, 0);

	for (dev = 1; dev <= cnt; dev++) {
		memset(section, 0 , sizeof(section));
	    sprintf(section, "%s%d", get_section_type(type), dev);
	    val = vediag_parse_system_section(section, "Device");
	    if (strcmp(val, device) == 0) {
	    	section_found = 1;
	    	goto update;
	    }
	}

update:
	if (section_found) {
	    xinfo("[%s]\n%d. %-16s ==> %s\n", section, dev, device, physical_dev);
		return vediag_update_system_config(section, "Physical", physical_dev);
	} else
		return 1;
}

static int __vediag_identify_device(char *device, char *physical_dev)
{
	/* Find storage device */
	if (__vediag_identify_device_type(VEDIAG_STORAGE, device, physical_dev) == 0)
		return 0;

	/* Find network device */
	if (__vediag_identify_device_type(VEDIAG_NET, device, physical_dev) == 0)
		return 0;

	/* Find PCI device */
	if (__vediag_identify_device_type(VEDIAG_PCIE, device, physical_dev) == 0)
		return 0;

	/* Find LED device */
	if (__vediag_identify_device_type(VEDIAG_LED, device, physical_dev) == 0)
		return 0;

	xinfo("Don't find any device name '%s'\n", device);
	return 1;
}

static int vediag_set_sensor(char *sensor)
{
	vediag_update_system_config("COMMON", "Sensor", sensor);

	return 0;
}

int vediag_identify_device (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int type = 0;

	if (argc < 2)
		goto help;

	else if(strcmp(argv[1], "info") == 0)
		return vediag_print_identified_devices();

	else if(strcmp(argv[1], "set") == 0) {
		if (argc != 4)
			goto help;

		return __vediag_identify_device(argv[2], argv[3]);
	}

	else if(strcmp(argv[1], "add") == 0) {
		if (argc < 4)
			goto help;

		if (strcmp(argv[2], "stor") == 0)
			type = VEDIAG_STORAGE;
		else if(strcmp(argv[2], "net") == 0)
			type = VEDIAG_NET;
		else if(strcmp(argv[2], "pci") == 0)
			type = VEDIAG_PCIE;
		else if(strcmp(argv[2], "led") == 0)
			type = VEDIAG_LED;
		else if(strcmp(argv[2], "sensor") == 0)
			return vediag_set_sensor(argv[3]);

		if (argc == 5)
			return vediag_add_identified_device(type, argv[3], argv[4]);
		else
			return vediag_add_identified_device(type, argv[3], "Unknown");
	}

	else if(strcmp(argv[1], "scan") == 0) {
		int force = 0;
		int fd;

		memset(file_system, 0, sizeof(file_system));
		sprintf(file_system, "%s", VEDIAG_SYSTEM_CONFIG_FILE);
		/* Check file exist */
		if (access(VEDIAG_SYSTEM_CONFIG_FILE, 0) != 0) {
			fd = open(VEDIAG_SYSTEM_CONFIG_FILE, O_RDWR|O_CREAT, 0777);
			if (fd != -1){
				close(fd);
				vediag_create_default_system();
			}
			else {
				xerror("Can't create '%s'\n", VEDIAG_SYSTEM_CONFIG_FILE);
				return -1;
			}
		}

		if ((argc == 3) && strcmp(argv[2], "-f") == 0)
			force = 1;

		xinfo(" Scan memory device...\n");
		vediag_scan_memory_device(force);
#ifdef CONFIG_VEDIAG_STORAGE_TEST
		xinfo(" Scan storage device...\n");
		vediag_scan_storage_device(force);
#endif
#ifdef CONFIG_VEDIAG_NET_TEST
		xinfo(" Scan network device...\n");
		vediag_scan_network_device(force);
#endif
#ifdef CONFIG_VEDIAG_PCI_TEST
		xinfo(" Scan pci device...\n");
		vediag_scan_pci_device(force);
#endif
#ifdef CONFIG_VEDIAG_LED_TEST
		xinfo(" Scan led device...\n");
		vediag_scan_led_device(force);
#endif
#ifdef CONFIG_VEDIAG_EVENT_TEST
		xinfo(" Scan event device...\n");
		vediag_scan_event_device(force);
#endif
#ifdef CONFIG_VEDIAG_I2C_TEST
		xinfo(" Scan i2c device...\n");
		vediag_scan_i2c_device(force);
#endif
#ifdef CONFIG_VEDIAG_AUDIO_TEST
		xinfo(" Scan audio device...\n");
		vediag_scan_audio_device(force);
#endif
#ifdef CONFIG_VEDIAG_MICRO_TEST
		xinfo(" Scan micro device...\n");
		vediag_scan_micro_device(force);
#endif
#ifdef CONFIG_VEDIAG_BLUETOOTH_TEST
		xinfo(" Scan bluetooth device...\n");
		vediag_scan_bluetooth_device(force);
#endif
#ifdef CONFIG_VEDIAG_FIRMWARE
		vediag_scan_firmware_device(force);
#endif
#ifdef CONFIG_VEDIAG_ZWAVE_TEST
		xinfo(" Scan zwave device...\n");
		vediag_scan_zwave_device(force);
#endif
#ifdef CONFIG_VEDIAG_ZIGBEE_TEST
		xinfo(" Scan zigbee device...\n");
		vediag_scan_zigbee_device(force);
#endif
		return 0;
	}

help:
    xprint ("Usage:\n%s\n", cmdtp->usage);
#ifdef	CONFIG_LONGHELP
    xprint ("%s\n", cmdtp->help);
#endif
	return 1;
}

VEDIAG_CMD(
	vedev,	5,	0,	vediag_identify_device,
    "Identify hardware device",
	"vedev info                            - show identified devices\n"
	"vedev set <device> <physical>         - update identified device\n"
	"vedev add <type> <device> [physical]  - add new identified device\n"
	"                           <type>    : stor | net | pci | sensor\n"
	"vedev scan [-f]                       - Scan identified devices\n"
	"                           -f        : force remove all identified device before\n"
	"                                       keep identified device before without this opt\n"
)
