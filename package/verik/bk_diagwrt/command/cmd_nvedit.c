
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <types.h>
#include <configs/config.h>
#include <vediag/environment.h>
#include <cmd.h>
#include <vediag_common.h>
#include <common.h>
#include <log.h>


#define XMK_STR(x)	#x
#define MK_STR(x)	XMK_STR(x)

extern int envmatch (uchar *s1, int i2);

/*
 * This variable is incremented on each do_setenv (), so it can
 * be used via get_env_id() as an indication, if the environment
 * has changed or not. So it is possible to reread an environment
 * variable only if the environment was changed ... done so for
 * example in NetInitLoop()
 */
static int env_id = 1;

int get_env_id (void)
{
	return env_id;
}

int set_env_id (void)
{
	env_id++;
	if (env_id > 256)
		env_id = 1;

	return 0;
}

/*
 * Set an environment variable to an integer value
 *
 * @param varname	Environmet variable to set
 * @param value		Value to set it to
 * @return 0 if ok, 1 on error
 */
int setenv_ulong(const char *varname, ulong value)
{
	char str[32] = {0};
	snprintf(str, 31, "%lu", value);
	return xsetenv(varname, str);
}

/*
 * Set an environment variable to an value in hex
 *
 * @param varname	Environmet variable to set
 * @param value		Value to set it to
 * @return 0 if ok, 1 on error
 */
int setenv_hex(const char *varname, ulong value)
{
	char str[32] = {0};

	snprintf(str, 31, "%lx", value);
	return xsetenv(varname, str);
}

/*
 * Set a new environment variable,
 * or replace or delete an existing one.
 *
 * This function will ONLY work with a in-RAM copy of the environment
 */
int _do_setenv (int argc, char * const argv[])
{
	int   i, len, oldval;
	uchar *env, *nxt = NULL;
	char *name = NULL;
	char *name_s = NULL;
	int rc = 0;

	unsigned char *env_data = (unsigned char *)env_get_addr(0);

	if (!env_data)	/* need copy in RAM */
		return 1;

	name_s = malloc(strlen(argv[1]));
	strcpy(name_s, argv[1]);
	/*name = argv[1];*/

	name = name_s;

	if (strchr(name, '=')) {
		xerror ("## Error: illegal character '=' in variable name \"%s\"\n", name);
		rc = 1;
		goto exit;
	}

oldval_loop:
	/*
	 * search if variable with this name already exists
	 */
	oldval = -1;
	for (env = env_data; *env; env = nxt + 1) {
		for (nxt = env; *nxt; ++nxt)
			;
		if ((oldval = envmatch((uchar *)name, env - env_data)) >= 0) {
			break;
		}
	}

	/*
	 * Delete any existing definition
	 */
	if (oldval >= 0) {

		if (*++nxt == '\0') {
			if (env > env_data) {
				env--;
			} else {
				*env = '\0';
			}
		} else {
			for (;;) {
				*env = *nxt++;
				if ((*env == '\0') && (*nxt == '\0'))
					break;
				++env;
			}
		}
		*++env = '\0';

		/* Delete next env, if have more than one 'name' */
		goto oldval_loop;
	}

	/* Delete only ? */
	if ((argc < 3) || argv[2] == NULL) {
		env_crc_update ();
		rc = 0;
		goto exit;
	}

	/*
	 * Append new definition at the end
	 */
	for (env = env_data; *env || *(env+1); ++env)
		;
	if (env > env_data)
		++env;
	/*
	 * Overflow when:
	 * "name" + "=" + "val" +"\0\0"  > ENV_SIZE - (env-env_data)
	 */
	len = strlen(name) + 2;
	/* add '=' for first arg, ' ' for all others */
	for ( i = 2; i < argc; ++i) {
		len += strlen(argv[i]) + 1;
	}
	if (len > (&env_data[ENV_SIZE]-env)) {
		xerror ("## Error: environment overflow, \"%s\" deleted\n", name);
		rc = 1;
		goto exit;
	}
	while ((*env = *name++) != '\0')
		env++;
	for (i = 2; i < argc; ++i) {
		char *val = argv[i];

		*env = (i==2) ? '=' : ' ';
		while ((*++env = *val++) != '\0')
			;
	}

	/* end is marked with double '\0' */
	*++env = '\0';

	/* Update CRC */
	env_crc_update ();

	set_env_id();

	/*
	 * Some variables should be updated when the corresponding
	 * entry in the environment is changed
	 */

exit:
	if(name_s) {
		free(name_s);
		name_s = NULL;
	}
	return rc;
}

int xsetenv (const char * varname, const char *varvalue)
{
	char *name = NULL;
	char *value = NULL;
	int rc = 0;

	name = malloc(strlen(varname));
	value = malloc(strlen(varvalue));

	strcpy(name, varname);
	strcpy(value, varvalue);

	char *argv[4] = {"xsetenv", name, value, NULL };
	if (varvalue == NULL)
		rc = _do_setenv (2, argv);
	else
		rc = _do_setenv (3, argv);

	free(name);
	free(value);
	return rc;
}

static int do_setenv(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if (argc < 2) {
		xerror ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	return _do_setenv (argc, argv);
}

extern int saveenv(void);


#if defined(CONFIG_CMD_SAVEENV) && !defined(CONFIG_ENV_IS_NOWHERE)
static int do_saveenv (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	xinfo ("Saving Environment...\n");
	return (saveenv() ? 1 : 0);
}
#endif
extern int cleanenv(void);
#if defined(CONFIG_CMD_CLEANENV) && !defined(CONFIG_ENV_IS_NOWHERE)
static int do_cleanenv (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	xinfo ("Clean Environment...\n");
	return (cleanenv() ? 1 : 0);
}
#endif

static int do_env_print(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{

	int i, j, k, nxt;
	int rcode = 0;

	if (argc == 1) {		/* Print all env variables	*/
		for (i=0; env_get_char(i) != '\0'; i=nxt+1) {
			for (nxt=i; env_get_char(nxt) != '\0'; ++nxt)
				;
			for (k=i; k < nxt; ++k)
				xinfo("%c",env_get_char(k));
			xinfo("\n");

			if (ctrlc()) {
				xinfo ("\n ** Abort\n");
				return 1;
			}
		}

		xinfo("\nEnvironment size: %d/%ld bytes\n",
			i, (ulong)ENV_SIZE);

		return 0;
	}

	for (i=1; i<argc; ++i) {	/* print single env variables	*/
		/*char *name = argv[i];*/

		k = -1;

		for (j=0; env_get_char(j) != '\0'; j=nxt+1) {

			for (nxt=j; env_get_char(nxt) != '\0'; ++nxt)
				;
			k = envmatch((uchar *)argv[i], j);
			if (k < 0) {
				continue;
			}
			xinfo ("%s",argv[i]);
			xinfo ("=");
			while (k < nxt)
				xinfo("%c",env_get_char(k++));
			xinfo ("\n");
			break;
		}
		if (k < 0) {
			xerror ("## Error: \"%s\" not defined\n", argv[i]);
			rcode ++;
		}
	}
	return rcode;
}


/**************************************************/

VEDIAG_CMD(
	printenv, 32, 0, do_env_print,
	"print environment variables",
	"printenv [name] - print all or specific environment variable"
);

VEDIAG_CMD(
	setenv, 32, 0, do_setenv,
	"set environment variables",
	"name value ...\n"
	"    - set environment variable 'name' to 'value ...'\n"
	"xsetenv name\n"
	"    - delete environment variable 'name'\n"
);

#if defined(CONFIG_CMD_SAVEENV) && !defined(CONFIG_ENV_IS_NOWHERE)
VEDIAG_CMD(
	saveenv, 32, 0, do_saveenv,
	"save environment variables to persistent storage",
	"saveenv - save environment variables to persistent storage\n"
);

#endif

#if defined(CONFIG_CMD_CLEANENV) && !defined(CONFIG_ENV_IS_NOWHERE)
VEDIAG_CMD(
	cleanenv, 32, 0, do_cleanenv,
	"save environment variables to persistent storage",
	"saveenv - save environment variables to persistent storage\n"
);

#endif
