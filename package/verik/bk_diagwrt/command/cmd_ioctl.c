
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <cmd.h>

#include <vediagmod.h>
#include <log.h>

static int do_vediag_ioctl(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	char *dev = "/dev/"VEDIAG_DEV_NAME;

	if (argc >= 2) {
		dev = argv[1];
	}

	int file = open(dev, 0);
	if (file < 0) {
	    xinfo("Could not open file \"%s\"(error code %d)", dev, file);
		return -1;
	}

	ioctl(file, 0, 0);
	ioctl(file, 1, 0);

	xinfo("Open file \"%s\" successfully\n", dev);
	close(file);
	return 0;
}

VEDIAG_CMD(ioctl, 16, 0, do_vediag_ioctl,
		"vediag ioctl utilities",
		"")
