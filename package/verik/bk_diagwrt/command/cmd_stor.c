
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cmd.h>
#include <log.h>
#include <time_util.h>
#include <types.h>
#include <linux/list.h>
#include <dirent.h>
#include <unistd.h>
#include <vediag_storage.h>
#include <pthread.h>
#include <vediag_common.h>
#include <dirent.h>
#include <sys/stat.h>
#include <linux/fs.h>
#include <sys/utsname.h>

//#define VEDIAG_CMD_STOR_DEBUG
#if defined(VEDIAG_XPATH_DEBUG)
#define xtor_cmd_debug(fmt,args...)	xinfo (fmt ,##args)
#else
#define xtor_cmd_debug(fmt,args...)
#endif

extern int storage_raw_read(char *diskpath, void *buffer,
		unsigned long long offset,
		unsigned long long length);

int do_storage(cmd_tbl_t* cmdtp, int flag, int argc, char* argv[])
{
	struct stat fileStat;
	char diskpath[16];
	char *phypath = NULL;
	char *devpath = NULL;
	int total_dev = 0;
	int dev = 0;
	unsigned long long blkoff, blkcnt;
	char *memrd = NULL;

	if (argc < 2)
		goto usage;

	if (strcmp(argv[1], "-a") == 0) {
		for (dev = 0; dev < 32; dev++) {
			memset(diskpath, 0, sizeof(diskpath));
			sprintf(diskpath, "/dev/sd%c", 'a' + dev);
			if (stat(diskpath,&fileStat) >= 0) {
				phypath = vepath_alloc_n_get_phypath(diskpath);
			    devpath = vepath_alloc_n_get_devpath(diskpath);
			    total_dev++;

			    xinfo("%s:\n", diskpath);
			    xinfo("  - physical: %s\n", phypath);
			    xinfo("  - logical: %s\n", devpath);
			    xinfo("\n");

		    	if(phypath)
		    		free(phypath);
		    	if(devpath)
		    		free(devpath);
			}
		}

		xinfo("Total devices: %d\n\n", total_dev);
	}

	if (strcmp(argv[1], "dump") == 0) {
		if (argc < 5)
			goto usage;

		memset(diskpath, 0, sizeof(diskpath));
		strncpy(diskpath, argv[2], sizeof(diskpath)>strlen(argv[2])?strlen(argv[2]):sizeof(diskpath));

		blkoff = strtoul(argv[3], NULL, 0);
		blkcnt = strtoul(argv[4], NULL, 0);

		unsigned long long len = blkcnt*STORAGE_SECTOR_SIZE;
		memrd = malloc(len);
	    if (!memrd) {
	    	xerror("Alloc (size 0x%llx) fail\n", len);
	        return -1;
	    }
	    memset(memrd, 0, len);

	    int ret = storage_raw_read(diskpath, memrd, blkoff*STORAGE_SECTOR_SIZE, len);

	    if (ret) {
	    	xerror("Read device %s error !!\n", diskpath);

	    	if (memrd)
	    		free(memrd);

	    	return -1;
	    }

	    int i;
	    for (i = 0; i < len; i++) {
		    if (i%16)
		    	xinfo("0x%02x ", memrd[i]);
		    else
		    	xinfo("\n0x%08x: ", i);
	    }
	    xinfo("\n");
	}

	return 0;

usage:
    xinfo("Usage:\n");
    xinfo("%s\n", cmdtp->help);
	return 1;
}

VEDIAG_CMD(stor, 32, 0, do_storage,
	"Storage Commands Utilities",
	" stor -a                               - List all devices\r\n"
	" stor dump </dev/sdX> <block_offset> <block_count> - Dump storage devices\r\n"
);
