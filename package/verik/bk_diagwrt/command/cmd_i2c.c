
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <common.h>
#include <stdio.h>
#include <unistd.h>
#include <cmd.h>
#include <memory.h>
#include <time.h>
#include <time_util.h>
#include <vediag_common.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/kernel.h>
#include <log.h>
#include <vediag/i2c.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>

/* *****************************************************************************
 * DEFINE
 ******************************************************************************/
#define PAGE_BYTES  64

/* *****************************************************************************
 * PRIVATE FUNCTIONS PROTOTYPE
 ******************************************************************************/

/* *****************************************************************************
 * PRIVATE VARIABLE
 ******************************************************************************/
static char i2c_dev[30] = "";

/* *****************************************************************************
 * PUPLIC VARIABLE
 ******************************************************************************/

/* *****************************************************************************
 * FUNCTIONS IMPLEMENT
 ******************************************************************************/
int i2c_write(int fd, unsigned char chip, unsigned int addr, int alen,
		unsigned char * data, int len)
{
	struct i2c_rdwr_ioctl_data i2c_data; // structure pass to i2c driver
	struct i2c_msg i2cmsg[2]; // message count in the structure
	int i;

	if (len > PAGE_BYTES) {
		xerror("%s() data length too large\n", __FUNCTION__);
		return -1;
	}

	i2c_data.nmsgs = 1; // message count in the structure
	i2c_data.msgs = i2cmsg;
	i2c_data.msgs[0].len = (alen + len); // the offset address word is 2 bytes long
	i2c_data.msgs[0].addr = chip;
	i2c_data.msgs[0].flags = 0; // 1 for read; 0 for write
	i2c_data.msgs[0].buf = (unsigned char *) malloc(i2c_data.msgs[0].len);

	for (i = 0; i < alen; i++) {
		i2c_data.msgs[0].buf[i]	= (unsigned char) (addr >> (8 * (alen - i - 1)));
	}
	for (i = 0; i < len; i++) {
		i2c_data.msgs[0].buf[i + alen] = (unsigned char) data[i];
	}

	if (ioctl(fd, I2C_RDWR, &i2c_data) < 0) {
		xerror("Failed to write data from i2c device\n");
		free(i2c_data.msgs[0].buf);
		return -1;
	}
	free(i2c_data.msgs[0].buf);

	return 0;
}

int i2c_read(int fd, unsigned char chip, unsigned int addr, int alen,
		unsigned char * buf, int len)
{
	struct i2c_rdwr_ioctl_data i2c_data; // structure pass to i2c driver
	struct i2c_msg i2cmsg[2]; // message count in the structure
	char dummy_write_buf[alen];
	int i;

	// A dummy write operation should be done according to the AT24C256 i2c protocol
	i2c_data.nmsgs = 2; // message count in the structure
	i2c_data.msgs = i2cmsg;
	i2c_data.msgs[0].len = alen; // the offset address word is 2 bytes long
	i2c_data.msgs[0].addr = chip;
	i2c_data.msgs[0].flags = 0; // 0 for write;
	i2c_data.msgs[0].buf = (unsigned char*) dummy_write_buf;
	for (i = 0; i < alen; i++) {
		i2c_data.msgs[0].buf[i] = (unsigned char) (addr >> (8 * (alen - i - 1)));
	}

	// read operation
	i2c_data.msgs[1].len = len;
	i2c_data.msgs[1].addr = chip;
	i2c_data.msgs[1].flags = 1; // 1 for read; 0 for write
	i2c_data.msgs[1].buf = buf;

	if (ioctl(fd, I2C_RDWR, &i2c_data) < 0) {
		//		printf("%s() ioctl() failed\n",__FUNCTION__);
		return -1;
	}
	return 0;
}

int i2c_probe(char *i2cdev, char *result)
{
	int fd = 0, i;
	int result_offset = 0;
	int found = 0;
	int alen = 1;
	int nbytes = 1;
	int offset = 0;
	unsigned char *buf;

	/* Sanity check */
	if (i2cdev == NULL) {
		return -1;
	}

	fd = open(i2cdev, O_RDWR);
	if (fd < 0) {
		xerror("%s() Failed to open I2C device\n", __FUNCTION__);
		return -1;
	}

	// i2c timeout, 1 for 10ms ; if too small, i2c may lose response
	if (ioctl(fd, I2C_TIMEOUT, 3) < 0) {
		xerror("%s() ioctl() set timeout failed\n", __FUNCTION__);
		return -1;
	}

	// i2c timeout, 1 for 10ms ; if too small, i2c may lose response
	if (ioctl(fd, I2C_RETRIES, 3) < 0) {
		xerror("%s() ioctl() set retries failed\n", __FUNCTION__);
		return -1;
	}

	if (result == NULL)
		xinfo("Valid chip addresses:");

	buf = (unsigned char*) malloc(1);
	for (i = 0; i < 0x80; i++) {
		if (i2c_read(fd, i, offset, alen, buf, nbytes) == 0) {
			/* Found new i2c device */
			found++;
			if ((result+result_offset) == NULL) {
				xinfo("%02X ", i);
			} else {
				result_offset += sprintf( result + result_offset, "%02X ", i);
			}
		}
	}
	if ((result+result_offset) == NULL) {
		xinfo("\n");
	} else {
		result_offset += sprintf( result + result_offset, "\n");
	}

	free(buf);
	close(fd);

	return found;
}

/*
 * i2c_probe_test()
 *
 * @i2ct: input struct all i2c necessary
 * @offset: probe offset
 * @return: 0 if pass, < 0 if fail.
 */
int i2c_probe_test(struct i2c_test *i2ct, unsigned int offset)
{
	int fd;
	unsigned char tmp;

	/* Sanity check */
	if (i2ct->i2cdev == NULL) {
		xerror("ERROR: i2cdev NULL\n");
		return -1;
	}

	/* Test open i2cdev */
	fd = open(i2ct->i2cdev, O_RDWR);
	if (fd < 0) {
		xerror("Failed to open I2C device!");
		close(fd);
		return -1;
	}

	/* i2c timeout, 1 for 10ms ; if too small, i2c may lose response */
	if (ioctl(fd, I2C_TIMEOUT, 100) < 0) {
		xerror("%s() ioctl() failed!\n", __FUNCTION__);
		close(fd);
		return -1;
	}

	/* i2c timeout, 1 for 10ms ; if too small, i2c may lose response */
	if (ioctl(fd, I2C_RETRIES, 100) < 0) {
		xerror("%s() ioctl() failed!\n", __FUNCTION__);
		close(fd);
		return -1;
	}

	/* Test if device available */
	if (i2c_read(fd, i2ct->i2caddr, offset, i2ct->addrcycle, &tmp, 1) < 0) {
		/* Mark device not available */
		xerror("%s(): Read ID not respond\n", i2ct->i2cdev);
		close(fd);
		return -1;
	}

	/* Close device */
	close(fd);

	return 0;
}

/*
 * i2c_readid_test()
 *
 * @i2ct: input struct all i2c necessary
 * @idoffset: register ID
 * @identify: ID value
 * @return: 0 if pass, < 0 if fail.
 */
int i2c_readid_test(struct i2c_test *i2ct, unsigned int idoffset, unsigned char identify)
{
	int fd;
	unsigned char tmp;

	/* Sanity check */
	if (i2ct->i2cdev == NULL) {
		xerror("ERROR: i2cdev NULL\n");
		return -1;
	}

	/* Test open i2cdev */
	fd = open(i2ct->i2cdev, O_RDWR);
	if (fd < 0) {
		xerror("Failed to open I2C device!");
		close(fd);
		return -1;
	}

	/* i2c timeout, 1 for 10ms ; if too small, i2c may lose response */
	if (ioctl(fd, I2C_TIMEOUT, 100) < 0) {
		xerror("%s() ioctl() failed!\n", __FUNCTION__);
		close(fd);
		return -1;
	}

	/* i2c timeout, 1 for 10ms ; if too small, i2c may lose response */
	if (ioctl(fd, I2C_RETRIES, 100) < 0) {
		xerror("%s() ioctl() failed!\n", __FUNCTION__);
		close(fd);
		return -1;
	}

	/* Store for later using */
	i2ct->fd = fd;

	/* Test if device available */
	if (i2c_read(fd, i2ct->i2caddr, idoffset, i2ct->addrcycle, &tmp, 1) < 0) {
		/* Mark device not available */
		i2ct->fd = 0;
		xerror("%s(): Read ID not respond\n", i2ct->i2cdev);
		close(fd);
		return -1;
	}

	if(tmp != identify) {
		xerror("%s(): ID not match [0x%x] != Reg[0x%x] ID[0x%x]\n", i2ct->i2cdev, identify, idoffset, tmp);
		close(fd);
		return 1;
	}

	/* Close device */
	close(fd);

	return 0;
}

int do_i2c(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int fd = 0, ndev = 0;
	int len = 1;
	int size = 1;
	int alen = 1;
	int nbytes;
	unsigned char chip;
	unsigned int addr;
	unsigned char j;
	unsigned char *buf;
	unsigned int writeval;

	if (i2c_dev[0] == 0x0) {
		memset(i2c_dev, 0, sizeof(i2c_dev));
		memcpy(i2c_dev, DEFAULT_I2C_DEV, sizeof(DEFAULT_I2C_DEV));
	}

	if (argc == 1) {
		goto usage;
	} else if (strncmp(argv[1], "dev", 3) == 0) {
		if (argc == 2) {
			xinfo("I2C dev : %s\n", (char*) i2c_dev);
			return 0;
		} else if (argc == 3) {
			memset(i2c_dev, 0, sizeof(i2c_dev));
			memcpy(i2c_dev, argv[2], min(strlen((const char*) argv[2]), sizeof(i2c_dev)));
			xinfo("I2C dev : %s\n", (char*) i2c_dev);
			return 0;
		} else {
			goto usage;
		}
	} else if ((strncmp(argv[1], "probe", 5) == 0) && (argc == 2)) {
		ndev = i2c_probe( i2c_dev, NULL);
		if (ndev > 0)
			xinfo("Found: %d\n", ndev);
		return 0;
	} else if ((strncmp(argv[1], "md", 2) == 0) && (argc <= 5)) {

		if (argc == 5) {
			len = (unsigned int) strtoul(argv[4], NULL, 16);
		}
		if (argc >= 4) {
			size = cmd_get_data_size((char*) argv[1], (int) 1);
			chip = (unsigned char) strtoul(argv[2], NULL, 16);
			addr = (unsigned int) strtoul(argv[3], NULL, 16);
			if (size == 1) {
			} else if (size == 2) {
				if (addr % 2 != 0) {
					xerror("ERROR: offset not align to size\n");
					return -1;
				}
			} else if (size == 4) {
				if (addr % 4 != 0) {
					xerror("ERROR: offset not align to size\n");
					return -1;
				}
			} else {
				xerror("ERROR: Get size failed.\n");
				return -1;
			}
			for (j = 0; j < 8; j++) {
				if (argv[3][j] == '.') {
					alen = argv[3][j + 1] - '0';
					if (alen > 4) {
						goto usage;
					}
					break;
				} else if (argv[3][j] == '\0')
					break;
			}
			fd = open(i2c_dev, O_RDWR);
			if (fd < 0) {
				xerror("%s() Failed to open I2C device\n", __FUNCTION__);
				return -1;
			}
			nbytes = len * (int) size;
			buf = (unsigned char*) malloc(nbytes + 1);
			memset(buf, 0, nbytes + 1);
			if (i2c_read(fd, chip, addr, alen, buf, nbytes) < 0) {
				xerror("%s() i2c_read(): Failed to read data from i2c device\n", __FUNCTION__);
				free(buf);
				close(fd);
				return -1;
			}
			print_buffer((unsigned int) addr, (void*) buf, (unsigned int) size,
					len, (unsigned int) (DISP_LINE_LEN / size));
			free(buf);
			close(fd);
		} else {
			goto usage;
		}

	} else if ((strncmp(argv[1], "mw", 2) == 0) && (argc <= 6)) {

		if (argc == 6) {
			len = (unsigned int) strtoul(argv[5], NULL, 16);
		}
		if (argc >= 5) {
			size = cmd_get_data_size((char*) argv[1], (int) 1);
			chip = (unsigned char) strtoul(argv[2], NULL, 16);
			addr = (unsigned int) strtoul(argv[3], NULL, 16);
			writeval = (unsigned int) strtoul(argv[4], NULL, 16);
			if (size == 1) {
			} else if (size == 2) {
				if (addr % 2 != 0) {
					xerror("ERROR: offset not align to size\n");
					return -1;
				}
			} else if (size == 4) {
				if (addr % 4 != 0) {
					xerror("ERROR: offset not align to size\n");
					return -1;
				}
			} else {
				xerror("ERROR: Get size failed.\n");
				return -1;
			}
			for (j = 0; j < 8; j++) {
				if (argv[3][j] == '.') {
					alen = argv[3][j + 1] - '0';
					if (alen > 4) {
						goto usage;
					}
					break;
				} else if (argv[3][j] == '\0')
					break;
			}
			fd = open(i2c_dev, O_RDWR);
			if (fd < 0) {
				xerror("%s() Failed to open I2C device\n", __FUNCTION__);
				return -1;
			}
			nbytes = len * (int) size;
			while (len-- > 0) {
				if (i2c_write(fd, chip, addr, alen,
						(unsigned char *) &writeval, size) < 0) {
					xerror("%s() i2c_write(): Failed to write data to i2c device\n", __FUNCTION__);
					close(fd);
					return -1;
				}
				addr += size;
				usleep(10000);
			}
			close(fd);

		} else {
			goto usage;
		}
	} else {
		goto usage;
	}
	return 0;

usage:
	xinfo("Usage: \n");
	xinfo("%s\n", cmdtp->help);

	return -1;
}

VEDIAG_CMD(i2c, 6, 0, do_i2c,
	"I2C Command Utilities",
	" i2c dev [dev]                                                     - Show/Set current I2C bus\r\n"
	" i2c probe                                                         - Show devices on the I2C bus\r\n"
	" i2c md[.b, .w, .l] <chip> <address>[.0, .1, .2] [count]           - Read from I2C device\r\n"
	" i2c mw[.b, .w, .l] <chip> <address>[.0, .1, .2] <value> [count]   - Write/fill to I2C device\r\n"
	"\n Argument:\n"
	"    - [dev] : I2C device Ex: /dev/i2c-0\r\n"
	"    - <chip> : Hex 7-bit I2C address\r\n"
	"    - <address> : Hex device offset\r\n"
	"    - [count] : Hex number of Byte being read/write\r\n"
	"    - <value> : Hex value to be written\r\n");
