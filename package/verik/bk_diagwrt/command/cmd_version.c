#include <stdio.h>
#include <string.h>
#include <cmd.h>
#include <log.h>

int do_xversion (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
 {
	int rc = COMMAND_OK;

	xinfo("diagwrt-%d.%d.%d %s@%s %s %s\n",
			CONFIG_VEDIAG_VERSION, CONFIG_VEDIAG_PATCHLEVEL, CONFIG_VEDIAG_SUBLEVEL,
			CONFIG_VEDIAG_USER, CONFIG_SYS_BOARD, __DATE__, __TIME__);

	return rc;
}

VEDIAG_CMD(ver, 5, 0, do_xversion,
		" diagwrt App version display",
		"");
