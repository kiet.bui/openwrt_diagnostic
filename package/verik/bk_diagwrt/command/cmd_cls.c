
#include <stdio.h>
#include <cmd.h>

static int do_cls(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	/* reset device*/
	printf("\033c");

	/* all atributes are OFF*/
	printf("\033[0m");

	/* clear screen */
	printf("\033[2J");

	/* set position to 0,0*/
	printf("\33[0;0H");

	fflush(stdout);

	return 0;
}

VEDIAG_CMD(cls, 1, 1, do_cls,
		"Clear screen",
		" cls - Clear screen\r\n"
		"")
