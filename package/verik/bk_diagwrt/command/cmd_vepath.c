
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cmd.h>
#include <log.h>
#include <time_util.h>
#include <types.h>
#include <linux/list.h>
#include <dirent.h>
#include <unistd.h>
#include <vediag_storage.h>
#include <ctype.h>
#include <linux/err.h>

/*#define VEDIAG_XPATH_DEBUG*/
#if defined(VEDIAG_XPATH_DEBUG)
#define vepath_debug(fmt,args...)	xinfo (fmt ,##args)
#else
#define vepath_debug(fmt,args...)	xdebug (fmt ,##args)
#endif



/************************************************ LIB for vepath_device ********************************************/
static int char_loc_from_right(char *str, char c)
{
	int i;

	for(i = strlen(str) - 1; i >= 0; i--) {
		if (str[i] == c)
			return i;
	}

	return -1;
}

static void vepath_device_free(struct vepath_device *dev)
{
	struct vepath_device *dev_t = dev;

	if (dev_t->name)
		free(dev_t->name);

	if (dev_t->path)
		free(dev_t->path);

	if (dev_t->drivers)
		free(dev_t->drivers);

	if (dev_t->drivers_path)
		free(dev_t->drivers_path);

	if (dev_t->subsystems)
		free(dev_t->subsystems);

	if (dev_t->subsystems_path)
		free(dev_t->subsystems_path);

	if (dev_t->firmware_node)
		free(dev_t->firmware_node);

	if (dev_t->firmware_node_path)
		free(dev_t->firmware_node_path);
}

static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;	/* only process access to find devpath */
char *vepath_alloc_n_get_devpath(char *diskpath)
{
	FILE *pp;
	int linenum = 0;
	char *dev = NULL;
	char *devpath = NULL;
	char *cmd = NULL;
	char *strbuf = NULL;
	char *line = NULL;

	pthread_mutex_lock(&_mutex);

	/* malloc (max string length 1024) */
	strbuf = malloc(50 * VEDIAG_MAX_STR_ARG_SIZE);
	if (!strbuf) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto err_exit;
	}
	memset(strbuf, 0, 50 * VEDIAG_MAX_STR_ARG_SIZE);

	cmd = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!cmd) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto err_exit;
	}
	memset(cmd, 0, VEDIAG_MAX_STR_ARG_SIZE);

	devpath = malloc(VEDIAG_MAX_STR_ARG_SIZE);
    if (!devpath) {
    	xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
        goto err_exit;
    }
    memset(devpath, 0, VEDIAG_MAX_STR_ARG_SIZE);

    /* parse device name */
	if (strncmp(diskpath, "/dev/", 5) == 0) {
		dev = diskpath + 5;
		vepath_debug("Find devpath of device '%s'\n", dev);
	}else {
		/*xerror("Device name must formatted /dev/XXX !!\n");
		goto err_exit;*/
		dev = diskpath;
		vepath_debug("Find devpath of device '%s'\n", dev);
	}

	/* find path */
	if((strlen(dev) == 4) && (strncmp(dev, "sd", 2) == 0)) {
		char dev_t[16];
		memset(dev_t, 0, sizeof(dev_t));
		sprintf(dev_t, "%c%c%c/%s", dev[0], dev[1], dev[2], dev);
		sprintf(cmd, "find /sys/devices -name '%s' | grep '%s'", dev, dev_t);
	} else
		sprintf(cmd, "find /sys/devices -name '%s'", dev);

	vepath_debug("%s \n", cmd);

	pp = popen(cmd, "r");
	if (!pp) {
		xerror("%s %d Open command fail, error code %s !!\n", __FUNCTION__, __LINE__, strerror(errno));
		goto err_exit;
	}

	char *ptr = strbuf;
	while (1) {
		//memset(strbuf, 0, VEDIAG_MAX_STR_ARG_SIZE);
		line = fgets(ptr, VEDIAG_MAX_STR_ARG_SIZE, pp);

		if (line == NULL) break;
		//printf("%s", line); /* line includes '\n' */
		linenum++;

		if (linenum == 1)
			strcpy(devpath, line);

		if (linenum < 50)
			ptr += VEDIAG_MAX_STR_ARG_SIZE;
	}

	if (pclose(pp) == -1)
		xerror("%s %d Close command fail !!\n", __FUNCTION__, __LINE__);

	if (linenum > 1) {
		xerror("%s %d Found many devpath, cannot identify device %s line %d\n", __FUNCTION__, __LINE__, dev, linenum);
		xerror("%s %d %s\n", __FUNCTION__, __LINE__, devpath);

//		int i;
//		ptr = strbuf;
//		for (i = 0; i < 50; i++) {
//			if (strlen(ptr))
//				xerror("%s %d %s\n", __FUNCTION__, __LINE__, ptr);
//		}
		goto err_exit;
	} else if (linenum == 0) {
		xerror("%s %d No found any devpath, cannot identify device %s\n", __FUNCTION__, __LINE__, dev);
		goto err_exit;
	}

	/* remove end of line */
	if (devpath[strlen(devpath)-1] == '\n')
		devpath[strlen(devpath)-1] = 0;
	vepath_debug("Found devpath %s\n", devpath);
	goto exit;

err_exit:
	if (devpath)
		free(devpath);
	devpath = NULL;

exit:
	if (strbuf)
		free(strbuf);

	if (cmd)
		free(cmd);

	pthread_mutex_unlock(&_mutex);
	return devpath;
}

static int vepath_get_rel_drivers(struct vepath_device *rel)
{
	int ret = 0, loc, len;
	char *target_path = NULL;
	char *link_path = NULL;

	/* Malloc */
	target_path = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!target_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(target_path, 0, VEDIAG_MAX_STR_ARG_SIZE);

	link_path = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!link_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(link_path, 0, VEDIAG_MAX_STR_ARG_SIZE);

	/* Attempt to read the target of the symbolic link. */
	sprintf(link_path, "%s/driver", rel->path);
	len = readlink (link_path, target_path, VEDIAG_MAX_STR_ARG_SIZE);
	if (len == -1) {
		vepath_debug ("%s is not a symbolic link\n", link_path);				/* not error because this path is not driver */
		ret = -1;
		goto err_exit;
	}
	vepath_debug ("%s\n", target_path);

	rel->drivers_path = malloc(strlen(target_path) + 1);
	if (!rel->drivers_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(rel->drivers_path, 0, strlen(target_path) + 1);
	strcpy(rel->drivers_path, target_path);

	loc = char_loc_from_right(target_path, '/');
	if (loc == -1) {
		vepath_debug("%s %d Found '/' error !!\n", __FUNCTION__, __LINE__);	/* not error because this path is not driver */
		ret = -1;
		goto err_exit;
	}

	rel->drivers = malloc(strlen(target_path) - loc + 1);
	if (!rel->drivers) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(rel->drivers, 0, strlen(target_path) - loc + 1);
	strcpy(rel->drivers, &target_path[loc + 1]);

	goto exit;

err_exit:
	if (rel->drivers_path)
		free(rel->drivers_path);
	rel->drivers_path = NULL;

	if (rel->drivers)
		free(rel->drivers);
	rel->drivers = NULL;

exit:
	if (target_path)
		free(target_path);

	if (link_path)
		free(link_path);

    return ret;
}

static int vepath_get_rel_subsystems(struct vepath_device *rel)
{
	int ret = 0, loc, len;
	char *target_path = NULL;
	char *link_path = NULL;

	/* Malloc */
	target_path = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!target_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(target_path, 0, VEDIAG_MAX_STR_ARG_SIZE);

	link_path = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!link_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(link_path, 0, VEDIAG_MAX_STR_ARG_SIZE);

	/* Attempt to read the target of the symbolic link. */
	sprintf(link_path, "%s/subsystem", rel->path);
	len = readlink (link_path, target_path, VEDIAG_MAX_STR_ARG_SIZE);
	if (len == -1) {
		vepath_debug ("%s is not a symbolic link\n", link_path);				/* not error because this path is not driver */
		ret = -1;
		goto err_exit;
	}
	vepath_debug ("%s\n", target_path);

	rel->subsystems_path = malloc(strlen(target_path) + 1);
	if (!rel->subsystems_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(rel->subsystems_path, 0, strlen(target_path) + 1);
	strcpy(rel->subsystems_path, target_path);

	loc = char_loc_from_right(target_path, '/');
	if (loc == -1) {
		vepath_debug("%s %d Found '/' error !!\n", __FUNCTION__, __LINE__);	/* not error because this path is not driver */
		ret = -1;
		goto err_exit;
	}

	rel->subsystems = malloc(strlen(target_path) - loc + 1);
	if (!rel->subsystems) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}

	memset(rel->subsystems, 0, strlen(target_path) - loc + 1);
	strcpy(rel->subsystems, &target_path[loc + 1]);

	goto exit;

err_exit:
	if (rel->subsystems_path)
		free(rel->subsystems_path);
	rel->subsystems_path = NULL;

	if (rel->subsystems)
		free(rel->subsystems);
	rel->subsystems = NULL;

exit:
	if (target_path)
		free(target_path);

	if (link_path)
		free(link_path);

	return ret;
}

static int vepath_get_rel_firmware_node(struct vepath_device *rel)
{
	int ret = 0, loc, len;
	char *target_path = NULL;
	char *link_path = NULL;

	/* Malloc */
	target_path = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!target_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(target_path, 0, VEDIAG_MAX_STR_ARG_SIZE);

	link_path = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!link_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(link_path, 0, VEDIAG_MAX_STR_ARG_SIZE);

	/* Attempt to read the target of the symbolic link. */
	sprintf(link_path, "%s/firmware_node", rel->path);
	len = readlink (link_path, target_path, VEDIAG_MAX_STR_ARG_SIZE);
	if (len == -1) {
		vepath_debug ("%s is not a symbolic link\n", link_path);				/* not error because this path is not driver */
		ret = -1;
		goto err_exit;
	}
	vepath_debug ("%s\n", target_path);

	rel->firmware_node_path = malloc(strlen(target_path) + 1);
	if (!rel->firmware_node_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(rel->firmware_node_path, 0, strlen(target_path) + 1);
	strcpy(rel->firmware_node_path, target_path);

	loc = char_loc_from_right(target_path, '/');
	if (loc == -1) {
		vepath_debug("%s %d Found '/' error !!\n", __FUNCTION__, __LINE__);	/* not error because this path is not driver */
		ret = -1;
		goto err_exit;
	}

	rel->firmware_node = malloc(strlen(target_path) - loc + 1);
	if (!rel->firmware_node) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(rel->firmware_node, 0, strlen(target_path) - loc + 1);
	strcpy(rel->firmware_node, &target_path[loc + 1]);

	goto exit;

err_exit:
	if (rel->firmware_node_path)
		free(rel->firmware_node_path);
	rel->firmware_node_path = NULL;

	if (rel->firmware_node)
		free(rel->firmware_node);
	rel->firmware_node = NULL;

exit:
	if (target_path)
		free(target_path);

	if (link_path)
		free(link_path);

    return ret;
}

static int vepath_get_rel_path(char *syspath, struct vepath_device *rel)
{
	char *ptr;
	int i;

	ptr = syspath;
	for(i = 0; i < strlen(syspath); i++) {
		if (strncmp(ptr + i, rel->name, strlen(rel->name)) == 0) {
			if (i > 0 && ptr[i-1] == '/') {
				if ((i + strlen(rel->name)) >= strlen(syspath) ||
					((i + strlen(rel->name)) < strlen(syspath) && ptr[i+strlen(rel->name)] == '/')) {

					rel->path = malloc(i + strlen(rel->name) + 1);
					if (!rel->path) {
						xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
						return -1;
					}
					memset(rel->path, 0, i + strlen(rel->name) + 1);
					strncpy(rel->path, syspath, i + strlen(rel->name));
					return 0;
				}
			}
		}
	}

	return -1;
}

static struct vepath_device *vepath_alloc_n_get_relinfo(char *devpath, char *relname)
{
    int ret = 0;
    struct vepath_device *rel;

    /* find name */
	rel = (struct vepath_device *)malloc(sizeof(struct vepath_device));
	if (!rel) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(rel, 0, sizeof(struct vepath_device));

	rel->name = malloc(strlen(relname) + 1);
	if (!rel->name) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto err_exit;
	}
	memset(rel->name, 0, strlen(relname) + 1);
	strcpy(rel->name, relname);

    /* find path */
	ret = vepath_get_rel_path(devpath, rel);
    if (ret)
    	goto err_exit;

	vepath_debug("Get info for %s (%s)\n", rel->name, rel->path);

	/* find firmware_node */
	ret = vepath_get_rel_firmware_node(rel);

	/* find subsystem */
	ret = vepath_get_rel_subsystems(rel);

	/* find driver */
	ret = vepath_get_rel_drivers(rel);

    vepath_debug("\n\n");
	return rel;

err_exit:
	if (rel) {
		vepath_device_free(rel);
		free(rel);
	}

	return NULL;
}

int vepath_scan_device(struct vepath_device *dev, char *diskpath)
{
	char *devpath = NULL, *path = NULL, *pch = NULL;
	struct vepath_device *dev_t;
	int ret = 0, index = 0;

	/* Get real path for search */
	devpath = vepath_alloc_n_get_devpath(diskpath);
	if (!devpath || strncmp(devpath, "/sys/devices/", 13) != 0) {
	    xerror("path invalid !!\n");
	    ret = -1;
		goto exit;
	}

	/* Copy path because strtok will change original str */
	path = malloc(strlen(devpath) + 1);
	if (!path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
	    ret = -1;
	    goto exit;
	}
	memset(path, 0, strlen(devpath) + 1);
	strcpy(path, devpath + 13);
	vepath_debug ("Real path to check %s \n", path);

	/* Search and assign parent of device */
	INIT_LIST_HEAD(&(dev->relations));
	pch = strtok(path, "/");
	while (pch != NULL)	{
		dev_t = vepath_alloc_n_get_relinfo(devpath, pch);
		dev_t->index = index++;

		list_add(&(dev_t->relations), &(dev->relations));

		pch = strtok (NULL, "/");
	}

exit:
	if (devpath)
		free(devpath);

	if (path)
		free(path);

	return ret;
}



/************************************************ LIB for PHY path ********************************************/
static void mptxsas_get_slot(char *result, struct vepath_device *curr_dev,
		struct vepath_device *dev)
{
	struct vepath_device *dev_t;
    struct list_head *pos;

    int found = 0;
    char *ret_str = NULL, *line = NULL, *fpath = NULL;

    ret_str = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!ret_str) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(ret_str, 0, VEDIAG_MAX_STR_ARG_SIZE);

    line = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!line) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(line, 0, VEDIAG_MAX_STR_ARG_SIZE);

    fpath = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!fpath) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(fpath, 0, VEDIAG_MAX_STR_ARG_SIZE);

	list_for_each_prev(pos, &(dev->relations)) {	/* from first parent */
		dev_t = list_entry(pos, struct vepath_device, relations);

		if (found == 0 && curr_dev == dev_t)
			found = 1;

		if (found) {
			if (strncmp(dev_t->name, "end_device", 10) == 0) {
				FILE* fd = NULL;

			    sprintf(fpath, "%s/sas_device/%s/bay_identifier", dev_t->path, dev_t->name);
			    vepath_debug("mptxsas_get_slot path %s\n", fpath);

			    fd = fopen(fpath,"rb");
			    if (fd) {
			    	if (fgets(line, VEDIAG_MAX_STR_ARG_SIZE, fd) != NULL) {
			    		if (line[strlen(line) - 1] == '\n')
			    			line[strlen(line) - 1] = 0;
			    		vepath_debug("bay_identifier %s\n", line);
			    		sprintf(ret_str, "%s", line);
			    	}
			    	if (fclose(fd) != 0)
			    		xerror("%s %d Close file fail !!\n", __FUNCTION__, __LINE__);
			    } else {
			    	vepath_debug("%s %d fopen fail !!\n", __FUNCTION__, __LINE__);	/* not error because this not support */
			    }
			    break;
			}
		}
	}

	if (strlen(ret_str))
		strcpy(result, ret_str);

exit:
	if (ret_str)
		free(ret_str);

	if (line)
		free(line);

	if (fpath)
		free(fpath);
}

static void usbhub_get_slot(char *result, struct vepath_device *curr_dev,
		struct vepath_device *dev)
{
	FILE* fd = NULL;
    char *ret_str = NULL, *line = NULL, *fpath = NULL;

    ret_str = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!ret_str) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(ret_str, 0, VEDIAG_MAX_STR_ARG_SIZE);

    line = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!line) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(line, 0, VEDIAG_MAX_STR_ARG_SIZE);

    fpath = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!fpath) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(fpath, 0, VEDIAG_MAX_STR_ARG_SIZE);

	sprintf(fpath, "%s/devpath", curr_dev->path);
	vepath_debug("usbhub_get_slot path %s\n", fpath);

	fd = fopen(fpath,"rb");
	if (fd) {
		if (fgets(line, VEDIAG_MAX_STR_ARG_SIZE, fd) != NULL) {
			if (line[strlen(line) - 1] == '\n')
				line[strlen(line) - 1] = 0;
			vepath_debug("devpath %s\n", line);

			char *p;
			p = strtok (line, ".");
			while (p != NULL) {		/* get until reach final '.' */
				memset(ret_str, 0, VEDIAG_MAX_STR_ARG_SIZE);
				sprintf(ret_str, "%s", p);
				p = strtok (NULL, ".");
			}
		}
    	if (fclose(fd) != 0)
    		xerror("%s %d Close file fail !!\n", __FUNCTION__, __LINE__);
	} else {
    	vepath_debug("%s %d fopen fail !!\n", __FUNCTION__, __LINE__);	/* not error because this not support */
    }

	if (strlen(ret_str))
		strcpy(result, ret_str);

exit:
	if (ret_str)
		free(ret_str);

	if (line)
		free(line);

	if (fpath)
		free(fpath);
}

static void ahci_get_slot(char *result, struct vepath_device *curr_dev,
		struct vepath_device *dev)
{
	if (curr_dev->drivers)
		strcpy(result, curr_dev->drivers);
}

static void xhci_hcd_get_slot(char *result, struct vepath_device *curr_dev,
		struct vepath_device *dev)
{
	if (curr_dev->drivers)
		strcpy(result, curr_dev->drivers);
}

static void megaraid_sas_get_slot(char *result, struct vepath_device *curr_dev,
		struct vepath_device *dev)
{
	if (curr_dev->drivers)
		strcpy(result, curr_dev->drivers);
}

/* return IP name if having, otherwise return original acpi name */
static void vepath_search_acpi_ipname(char *result, char *acpi_name)
{
	int i;
	struct acpi_ip acpi_ip_tbl[] = ACPI_IP_TABLE;

	/* Find name of ACPI node, otherwise use ACPI name */
	for(i = 0; i < sizeof(acpi_ip_tbl)/sizeof(struct acpi_ip); i++) {
		if (strncmp(acpi_name, acpi_ip_tbl[i].acpi_name, strlen(acpi_ip_tbl[i].acpi_name)) == 0) {
			vepath_debug("acpi %s\n", acpi_ip_tbl[i].acpi_name);
			sprintf(result, "%s", acpi_ip_tbl[i].ip_name);
			break;
		}
	}

	if (i >= sizeof(acpi_ip_tbl)/sizeof(struct acpi_ip)) {
		char tmp[VEDIAG_MAX_STR_ARG_SIZE], *p;
		strcpy(tmp, acpi_name);
		p = strtok (tmp, ":");
		if (p != NULL)
			sprintf(result, "%s", p);
		else
			sprintf(result, "%s", acpi_name);
	}
}

/* return uid if having, otherwise return NULL */
static void vepath_find_acpi_uid(char *result, char *path)
{
    FILE* fd = NULL;
    char *line = NULL, *fpath = NULL;

    line = malloc(VEDIAG_MAX_STR_ARG_SIZE);
 	if (!line) {
 		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
 		goto exit;
 	}
    memset(line, 0, VEDIAG_MAX_STR_ARG_SIZE);

    fpath = malloc(VEDIAG_MAX_STR_ARG_SIZE);
 	if (!fpath) {
 		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
 		goto exit;
 	}
    memset(fpath, 0, VEDIAG_MAX_STR_ARG_SIZE);

	/* Find name of uid */
    sprintf(fpath, "%s/uid", path);
    fd = fopen(fpath,"rb");
    if (fd) {
    	if (fgets(line, VEDIAG_MAX_STR_ARG_SIZE, fd) != NULL) {
    		if (line[strlen(line) - 1] == '\n')
    			line[strlen(line) - 1] = 0;
    		vepath_debug("uid %s\n", line);

    		/* just get number in uid */
    		int i;
    		for(i = strlen(line) - 1; i >= 0; i--) {
    			if (line[i] > '9' || line[i] < '0')
    				break;
    		}

    		sprintf(result, "_%s", &line[i+1]);
    	}
    	if (fclose(fd) != 0)
    		xerror("%s %d Close file fail !!\n", __FUNCTION__, __LINE__);
    }else {
    	vepath_debug("%s %d fopen fail !!\n", __FUNCTION__, __LINE__);	/* not error because this not support */
    }

exit:
	if (line)
		free(line);

	if (fpath)
		free(fpath);
}

/* return IP name if having, otherwise return original acpi name */
static int vepath_search_omap_hsmmc_ipname(char *result, char *mmc_name)
{
	int i;
	struct omap_hsmmc_ip omap_hsmmc_tbl[] = OMAP_HSMMC_TABLE;

	/* Find name of omap_hsmmc & portnumber */
	for(i = 0; i < sizeof(omap_hsmmc_tbl)/sizeof(struct omap_hsmmc_ip); i++) {
		if (strncmp(mmc_name, omap_hsmmc_tbl[i].omap_hsmmc_name, strlen(omap_hsmmc_tbl[i].omap_hsmmc_name)) == 0) {
			vepath_debug("omap_hsmmc: %s\n", omap_hsmmc_tbl[i].omap_hsmmc_name);
			sprintf(result, "%s%s", "omap_hsmmc", omap_hsmmc_tbl[i].omap_hsmmc_port);
			return true;
		}
	}
	return false;
}

void str_rem(char *str, const char *to_remove)
{
	if (NULL == (str = strstr(str, to_remove))) {
		return;
	}

	const size_t remLen = strlen(to_remove);
	char *copy_end;
	char *copy_from = str + remLen;
	while (NULL != (copy_end = strstr(copy_from, to_remove))) {
		memmove(str, copy_from, copy_end - copy_from);
		str += copy_end - copy_from;
		copy_from = copy_end + remLen;
	}
	memmove(str, copy_from, 1 + strlen(copy_from));
}

static int nvme_parse_path(char* path, char* phypath, int len)
{
	char *p, *q;
	char* pci_root;
	char domain_remove[6] = { 0 };

	/* path = "/sys/devices/LNXSYSTM:00/LNXSYBUS:00/PNP0A08:00/pci0002:00/0002:00:00.0/0002:01:00.0/0002:02:04.0/0002:03:00.0"*/
	p = strstr(path, "pci");
	if (p) {
		/* ==> p = "pci0002:00/0002:00:00.0/0002:01:00.0/0002:02:04.0/0002:03:00.0" */
		char* t1 = strdup(p);
		int ii = 0;

		pci_root = strtok(t1, ":");
		if (!pci_root)
			goto exit_err;
		/* ==> pci_root = "pci0002" */

		str_rem(pci_root, "000"); // remove '000' in pci_root because Skylark has only 8 Hosts
		/* pci_root = "pci2" */

		q = strchr(p, '/'); // get next '/'
		if (!q)
			goto exit_err;
		q++;
		/* ==> q = "0002:00:00.0/0002:01:00.0/0002:02:04.0/0002:03:00.0" */

		q = strchr(q, '/'); // get next '/' to remove d.b.d.f of Host
		if (!q)
			goto exit_err;
		q++;
		/* ==> q = "0002:01:00.0/0002:02:04.0/0002:03:00.0" */

		char* t = q; // remove domain number
		for (ii = 0; ii < 5; ii++) {
			domain_remove[ii] = *t;
			t++;
		}

		char* t2 = strdup(q);
		str_rem(t2, domain_remove);
		/* ==> t2 = "01:00.0/02:04.0/03:00.0" */

		char* res = malloc(len + 1);
		if (!res) {
			free(t1);
			free(t2);
			goto exit_err;
		}

		snprintf(res, len, "%s/%s", pci_root, t2);
		q = res;
		while ((*q = toupper(*q))) {
			q++;
		}

		strncpy(phypath, res, len);
		/* ==> res = "PCI2/01:00.0/02:04.0/03:00.0" */

		free(t1);
		free(t2);
		free(res);
	} else {
		goto exit_err;
	}

	return true;

exit_err: 
	printf("Error: parsing NVMe path failed\n");
	return false;
}

void vepath_scan_phypath(char *result, struct vepath_device *dev)
{
	struct vepath_device *dev_t;
    struct list_head *pos;

	int found_acpi = 0;
	int i, usb_level = 0;
	char *phypath = NULL, *target_path = NULL, *link_path = NULL;
	char *subsystems = NULL, *driver_path = NULL;
	char *ptr;

	phypath = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!phypath) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(phypath, 0, VEDIAG_MAX_STR_ARG_SIZE);

    target_path = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!target_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(target_path, 0, VEDIAG_MAX_STR_ARG_SIZE);

    link_path = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!link_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(link_path, 0, VEDIAG_MAX_STR_ARG_SIZE);

    subsystems = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!subsystems) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(subsystems, 0, VEDIAG_MAX_STR_ARG_SIZE);

    driver_path = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!driver_path) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
    memset(driver_path, 0, VEDIAG_MAX_STR_ARG_SIZE);

    ptr = phypath;
	list_for_each_prev(pos, &(dev->relations)) {	/* from first parent */
		dev_t = list_entry(pos, struct vepath_device, relations);

#ifdef VEDIAG_XPATH_DEBUG
		int detail = 1;
		vepath_debug("Device : %s\n", dev_t->name);
		vepath_debug(" - Path : %s\n", dev_t->path);
		if (dev_t->subsystems)
			vepath_debug(" - Subsystems : %s\n", detail?dev_t->subsystems_path:dev_t->subsystems);
		if (dev_t->drivers)
			vepath_debug(" - Drivers : %s\n", detail?dev_t->drivers_path:dev_t->drivers);
		if (dev_t->firmware_node)
			vepath_debug(" - Firmware_node : %s\n", detail?dev_t->firmware_node_path:dev_t->firmware_node);
#endif

		/* found acpi until meet final acpi node */
		if (dev_t->subsystems && strcmp(dev_t->subsystems, "acpi") == 0) {
			/* Find name of ACPI node, otherwise use ACPI name */
			ptr = phypath;			/* ignore previous acpi node */

			vepath_search_acpi_ipname(ptr, dev_t->name);
			ptr += strlen(ptr);

			/* Find name of uid */
			vepath_find_acpi_uid(ptr, dev_t->path);
			ptr += strlen(ptr);

			found_acpi = 1;
			vepath_debug("First location %s\n", phypath);
		}
		/* find acpi if path not contain it */
		else if (found_acpi == 0) {
			if (dev_t->firmware_node) {
				int len;

				memset(target_path, 0, VEDIAG_MAX_STR_ARG_SIZE);
				memset(link_path, 0, VEDIAG_MAX_STR_ARG_SIZE);
				memset(subsystems, 0, VEDIAG_MAX_STR_ARG_SIZE);
				sprintf(link_path, "%s/%s/subsystem", dev_t->path, dev_t->firmware_node_path);
				vepath_debug("link_path %s\n", link_path);

				/* Attempt to read the target of the symbolic link. */
				len = readlink (link_path, target_path, VEDIAG_MAX_STR_ARG_SIZE);

				if (len != -1) {
					vepath_debug("subsystems %s\n", target_path);

					for(i = strlen(target_path) - 1; i >= 0; i--) {
						if (target_path[i] == '/') {
							memset(subsystems, 0, strlen(target_path) - i + 1);
							strcpy(subsystems, &target_path[i+1]);
							memset(&target_path[i], 0, strlen(target_path) - i);

							if (strcmp(subsystems, "acpi") == 0) {
								char fpath[VEDIAG_MAX_STR_ARG_SIZE];

								/* Find name of ACPI node, otherwise use ACPI name */
								ptr = phypath;			/* ignore previous acpi node */

								vepath_search_acpi_ipname(ptr, dev_t->firmware_node);
								ptr += strlen(ptr);

								/* Find name of uid */
								sprintf(fpath, "%s/%s", dev_t->path, dev_t->firmware_node_path);
								vepath_debug("uid path %s\n", fpath);

								vepath_find_acpi_uid(ptr, fpath);
								ptr += strlen(ptr);

							    found_acpi = 1;
							    vepath_debug("First location %s\n", phypath);
							    break;
							}
						}
					}
				}
			}
		}

		/* Found driver path */
		if (dev_t->drivers) {
			if (strcmp(dev_t->drivers, "mpt3sas") == 0 ||
				strcmp(dev_t->drivers, "mpt2sas") == 0) {
				memset(driver_path, 0, VEDIAG_MAX_STR_ARG_SIZE);
				mptxsas_get_slot(driver_path, dev_t, dev);
				sprintf(ptr, "/%s_%s", dev_t->drivers, driver_path);
				ptr += strlen(ptr);
			}else if (strcmp(dev_t->drivers, "megaraid_sas") == 0) {
				memset(driver_path, 0, VEDIAG_MAX_STR_ARG_SIZE);
				megaraid_sas_get_slot(driver_path, dev_t, dev);
				sprintf(ptr, "/%s", driver_path);
				ptr += strlen(ptr);
			}else if (strcmp(dev_t->drivers, "sd") == 0) {
#if 0			/* ignored */
				struct vepath_device *dev_tmp;
			    struct list_head *pos_tmp;

				list_for_each(pos_tmp, &(dev->relations)) {	/* from first parent */
					dev_tmp = list_entry(pos_tmp, struct vepath_device, relations);

					sprintf(ptr, "/%s", dev_tmp->name);
					ptr += strlen(ptr);
					break;
				}
#endif
			}else if (strcmp(dev_t->drivers, "ahci") == 0) {
				memset(driver_path, 0, VEDIAG_MAX_STR_ARG_SIZE);
				ahci_get_slot(driver_path, dev_t, dev);
				sprintf(ptr, "/%s", driver_path);
				ptr += strlen(ptr);
			}else if (strcmp(dev_t->drivers, "xgene-ahci") == 0 ||
					  strcmp(dev_t->drivers, "ahci") == 0) {
				if (!found_acpi) {
					sprintf(ptr, "/%s", dev_t->drivers);
					ptr += strlen(ptr);
				}
			}else if (strcmp(dev_t->drivers, "pcieport") == 0) {
				if (!found_acpi) {
					sprintf(ptr, "/%s", dev_t->drivers);
					ptr += strlen(ptr);
				}
			}else if (strcmp(dev_t->drivers, "xhci_hcd") == 0) {
				memset(driver_path, 0, VEDIAG_MAX_STR_ARG_SIZE);
				xhci_hcd_get_slot(driver_path, dev_t, dev);
				sprintf(ptr, "/%s", driver_path);
				ptr += strlen(ptr);
			}else if (strcmp(dev_t->drivers, "usb") == 0) {
				if (usb_level == 0) {
					sprintf(ptr, "/%s", dev_t->drivers);
					ptr += strlen(ptr);
					usb_level++;
				} else if (usb_level == 1) {	/* USB host always hub controller */
					usb_level++;
				} else {
					memset(driver_path, 0, VEDIAG_MAX_STR_ARG_SIZE);
					usbhub_get_slot(driver_path, dev_t, dev);
					sprintf(ptr, ".%s", driver_path);
					ptr += strlen(ptr);
					usb_level++;
				}
			}else if (strcmp(dev_t->drivers, "usb-storage") == 0) {
				/* ignored */
			}else if (strcmp(dev_t->drivers, "nvme") == 0) {
				if (nvme_parse_path(dev_t->path, phypath, VEDIAG_MAX_STR_ARG_SIZE) == true){
					goto exit;
				}
			} else if (strcmp(dev_t->drivers, "omap_hsmmc") == 0) {
				if (vepath_search_omap_hsmmc_ipname(phypath, dev_t->name) == true) {
					goto exit;
				}
			} else if (strcmp(dev_t->drivers, "leds-gpio") == 0) {
				sprintf(phypath, "%s", "leds-gpio");
				goto exit;
			} else {
				sprintf(phypath, "%s", dev_t->drivers);
				goto exit;
			}
		}
	}

exit:
	if (strlen(phypath))
		strcpy(result, phypath);

	if (phypath)
		free(phypath);

	if (target_path)
		free(target_path);

	if (link_path)
		free(link_path);

	if (subsystems)
		free(subsystems);

	if (driver_path)
		free(driver_path);
}

char *vepath_alloc_n_get_phypath(char *diskpath)
{
	struct vepath_device *dev = NULL;
	struct list_head *pos, *q;
	struct vepath_device *dev_t;
	char *phypath = NULL;

	dev = malloc(sizeof(struct vepath_device));
	if (!dev) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto err_exit;
	}
	memset(dev, 0, sizeof(struct vepath_device));

	phypath = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!phypath) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto exit;
	}
	memset(phypath, 0, VEDIAG_MAX_STR_ARG_SIZE);

	if (vepath_scan_device(dev, diskpath) == 0) {
		vepath_debug("Found Device Path of %s successful\n", diskpath);

		/* find phypath */
		vepath_scan_phypath(phypath, dev);
		vepath_debug("PHY path '%s'\n", phypath);

		/* free */
		list_for_each_safe(pos, q, &(dev->relations)) {
			 dev_t = list_entry(pos, struct vepath_device, relations);
			 list_del(pos);

			 if (dev_t) {
				 vepath_device_free(dev_t);
				 free(dev_t);
			 }
		}
	}else {
		vepath_debug("\nCannot Found Device Path of %s !!\n\n", diskpath);
		goto err_exit;
	}

	if (strlen(phypath))
		goto exit;
	else
		goto err_exit;

err_exit:
	if (phypath)
		free(phypath);
	phypath = NULL;

exit:
	if (dev)
		free(dev);

	return phypath;
}

static int do_vepath(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int ret = 0;
	int detail = 0;

	if (argc < 2 || strncmp(argv[1], "/dev/", 5) != 0) {
	    xprint ("Usage:\n%s\n", cmdtp->usage);
		return -1;
	}

	if (argc > 2 && strncmp(argv[2], "detail", 2) == 0)
		detail = 1;

	/* Search */
	struct vepath_device *dev;
	struct list_head *pos, *q;
	struct vepath_device *dev_t;
	char *phypath;

	dev = malloc(sizeof(struct vepath_device));
	if (!dev) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		return -1;
	}
	memset(dev, 0, sizeof(struct vepath_device));

	phypath = malloc(VEDIAG_MAX_STR_ARG_SIZE);
	if (!phypath) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		return -1;
	}
	memset(phypath, 0, VEDIAG_MAX_STR_ARG_SIZE);

	if (vepath_scan_device(dev, argv[1]) == 0) {
	    xinfo("Found Device Path of %s successful\n", argv[1]);

		/* Display list */
		list_for_each(pos, &(dev->relations)) {	/* from end device */
			dev_t = list_entry(pos, struct vepath_device, relations);

			if(dev_t == NULL)
				continue;

			if (strcmp(dev_t->name, argv[1] + 5) == 0) {
				xinfo("Device : %s\n", dev_t->name);
				xinfo(" - Path : %s\n", dev_t->path);
				if (dev_t->subsystems)
					xinfo(" - Subsystems : %s\n", detail?dev_t->subsystems_path:dev_t->subsystems);
				if (dev_t->drivers)
					xinfo(" - Drivers : %s\n", detail?dev_t->drivers_path:dev_t->drivers);
				if (dev_t->firmware_node)
					xinfo(" - Firmware_node : %s\n", detail?dev_t->firmware_node_path:dev_t->firmware_node);
			} else /*if (dev_t->drivers)*/ {
				xinfo("Parent : %s\n", dev_t->name);
				xinfo(" - Path : %s\n", dev_t->path);
				if (dev_t->subsystems)
					xinfo(" - Subsystems : %s\n", detail?dev_t->subsystems_path:dev_t->subsystems);
				if (dev_t->drivers)
					xinfo(" - Drivers : %s\n", detail?dev_t->drivers_path:dev_t->drivers);
				if (dev_t->firmware_node)
					xinfo(" - Firmware_node : %s\n", detail?dev_t->firmware_node_path:dev_t->firmware_node);
			}
			xinfo("\n");
		}

		/* Find PHY path */
		vepath_scan_phypath(phypath, dev);
		xinfo("PHY path '%s'\n", phypath);

		/* free */
		list_for_each_safe(pos, q, &(dev->relations)) {
			 dev_t= list_entry(pos, struct vepath_device, relations);
			 list_del(pos);

			 if (dev_t) {
				 vepath_device_free(dev_t);
				 free(dev_t);
			 }
		}
	}else {
	    xerror("\nCannot find devpath for this device \n");
		ret = -1;
	}

	if (dev)
		free(dev);

	if (phypath)
		free(phypath);

	return ret;
}

VEDIAG_CMD(vepath, 32, 1, do_vepath,
		"Display storage device path",
		" vepath <device_name: /dev/XXX> - Display device location\r\n"
		"")
