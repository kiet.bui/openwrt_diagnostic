
#include <common.h>
#include <setjmp.h>
#include <sys/param.h>
#include <signal.h>
#ifdef	__USE_READLINE__
#include <readline/readline.h>
#include <readline/history.h>
#endif
#include <log.h>

static int ctrlc_was_pressed = 0;
int ctrlc (void)
{
	int ret = ctrlc_was_pressed;
	ctrlc_was_pressed = 0;

	return ret;
}

/* Signal Handler for SIGINT */
void sigintHandler(int sig_num)
{
    /* Reset handler to catch SIGINT next time.
       Refer http://en.cppreference.com/w/c/program/signal */
    signal(SIGINT, sigintHandler);
    fflush(stdout);
#ifdef	__USE_READLINE__
    xinfo("\n");
    rl_on_new_line();
    rl_replace_line("", 0);
    rl_redisplay();
#else
    xinfo("\n%s", CONFIG_PROMPT);
#endif

    ctrlc_was_pressed = 1;
}

device_initcall (console_init)
{
	/* Set the SIGINT (Ctrl-C) signal handler to sigintHandler
	   Refer http://en.cppreference.com/w/c/program/signal */
	signal(SIGINT, sigintHandler);

	return 0;
}
