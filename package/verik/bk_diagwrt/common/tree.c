/*
 * tree.c
 * CopyRight(C) 2013 by Nguyen
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/list.h>
#include <types.h>
#include <tree.h>
#include <log.h>
#include <stdarg.h>
#include <linux/bug.h>

/*#define TREE_DEBUG*/
#ifdef TREE_DEBUG
#define treedbg(x, fmt ...) xinfo(x, ##fmt)
#else
#define treedbg(x, fmg ...) do {} while(0)
#endif

#define TREE_LOG
#ifdef TREE_LOG
#define treelog	xinfo
#else
#define treelog	xinfo
#endif

/**
 * Allocate a new node with the specified name
 * @name	- represent a name of this node
 */
struct tree_node *tree_alloc(const char *name)
{
	struct tree_node *new;
	new = (struct tree_node *) malloc(sizeof(struct tree_node));
	if (!new) {
		treelog("%s, Out of memory\n", __func__);
		BUG();
		return NULL;
	}
	memset(new, 0, sizeof(struct tree_node));
	INIT_LIST_HEAD(&new->sibling);

	if (name) {
		int len = strlen(name);
		strncpy(new->fname, name, (len > FILENAME_MAX) ? FILENAME_MAX : len);
	}

	treedbg("Allocate a new tree at addr=%p, fname = %s\n", new, new->fname);

	return new;
}

struct tree_node *tree_alloc_fmt(const char *fmt, ...)
{
	va_list args;
	int size;
	struct tree_node *new;
	char *buf;
	new = (struct tree_node *) malloc(sizeof(struct tree_node));
	if (!new) {
		treelog("%s, Out of memory\n", __func__);
		BUG();
		return NULL;
	}
	memset(new, 0, sizeof(struct tree_node));
	INIT_LIST_HEAD(&new->sibling);


	buf = (char *) malloc(FILENAME_MAX + 1);
	if (!buf) {
		treelog("%s, Out of memory\n", __func__);
		BUG();
		return NULL;
	}
	memset(buf, 0, FILENAME_MAX + 1);

	va_start(args, fmt);
	size = vsprintf(buf, fmt, args);
	if (size > 0 && size <= FILENAME_MAX) {
		strncpy(new->fname, buf, (size > FILENAME_MAX) ? FILENAME_MAX : size);
	}

	va_end(args);
	free(buf);

	treedbg("Allocate a new tree at addr=%p, fname = %s\n", new, new->fname);

	return new;
}

/**
 * Destroy a total tree
 */
void _tree_destroy(struct tree_node **root)
{
	struct tree_node *tmp = *root;
	struct tree_node *sib, *sib_free;
	if (!tmp)
		return;
	for (sib = list_entry((&tmp->sibling)->next, typeof(*sib), sibling);
			&sib->sibling != (&tmp->sibling);) {
		if (sib->child) {
			_tree_destroy(&sib->child);
		}
		sib_free = sib;
		sib = list_entry(sib->sibling.next, typeof(*sib), sibling);
		treedbg("%s, free sib addr %p, name = %s, sib->child=%p\n",
				__func__, sib_free, sib_free->fname, sib_free->child);
		free(sib_free);
		sib_free = NULL;
	}

	if (tmp->child)
		_tree_destroy(&tmp->child);

	treedbg("%s, free node addr %p, name = %s\n", __func__, tmp, tmp->fname);
	free(tmp);
	tmp = NULL;
	*root = tmp;
}

void tree_destroy(struct tree_node **node)
{
	struct tree_node *root = tree_get_root(*node);
	_tree_destroy(&root);
	root = NULL;
	*node = root;
}

/**
 * Add a sibling to a node
 */
int tree_add_sibling(struct tree_node *newNode, struct tree_node *node)
{
	if (!newNode || !node)
		return -1;
	list_add_tail(&newNode->sibling, &node->sibling);
	return 0;
}

/**
 * Add a child to a node
 * param1: parent
 * param2: the new node which want to add to the parent
 */
struct tree_node *tree_add_node(struct tree_node *parent,
		struct tree_node *node)
{
	if (!parent || !node) {
		return NULL;
	}

	node->parent = parent;
	if (parent->child) {
		tree_add_sibling(node, parent->child);
	} else {
		parent->child = node;
	}

	return node;
}

/**
 * Add a child to a node
 * param1: parent
 * param2: the name of new node which want to add to the parent
 */
struct tree_node *tree_add_node_name(struct tree_node *parent, const char *name)
{
	struct tree_node *new_node = tree_alloc(name);
	return tree_add_node(parent, new_node);
}

/**
 * Truncate node name
 */
struct tree_node *tree_trunc_node_name(struct tree_node *node, const char *name)
{
	if (!node) {
		return node;
	}

	if (snprintf(node->fname, FILENAME_MAX, "%s%s", node->fname,
			name) > FILENAME_MAX) {
		treelog("Warning: new name is too long\n");
	}

	return node;
}

struct tree_node *tree_trunc_node_name_fmt(struct tree_node *node,
		const char *fmt, ...)
{
	va_list args;
	int size;
	char *buf;
	if (!node)
		return node;

	buf = (char *) malloc(FILENAME_MAX);
	if (!buf) {
		treelog("%s, Out of memory\n", __func__);
		BUG();
		return NULL;
	}
	memset(buf, 0, FILENAME_MAX);
	va_start(args, fmt);
	size = vsprintf(buf, fmt, args);
	if (size > 0 && size <= FILENAME_MAX) {
		if (snprintf(node->fname, FILENAME_MAX, "%s%s", node->fname,
				buf) > FILENAME_MAX) {
			treelog("Warning: new name is too long\n");
		}
	}
	va_end(args);
	free(buf);
	return node;
}

struct tree_node *tree_add_node_fmt(struct tree_node *p, const char *fmt, ...)
{
	struct tree_node *new_node = NULL;
	va_list args;
	int size;
	char *buf = (char *) malloc(FILENAME_MAX);
	if (!buf) {
		treelog("%s, Out of memory\n", __func__);
		BUG();
		return NULL;
	}
	memset(buf, 0, FILENAME_MAX);

	va_start(args, fmt);
	size = vsprintf(buf, fmt, args);
	if (size > 0 && size <= FILENAME_MAX) {
		new_node = tree_add_node_name(p, buf);
	}

	va_end(args);
	free(buf);
	return new_node;
}

/**
 * Get root of the tree
 */
struct tree_node *tree_get_root(struct tree_node *node)
{
	if (node->parent == NULL) {
		return node;
	} else {
		return tree_get_root(node->parent);
	}
}

/**
 * Get node's level
 */
int tree_get_node_level(struct tree_node *node)
{
	int level = 0;
	struct tree_node *tmp = node->parent;
	while (tmp) {
		tmp = tmp->parent;
		level++;
	}
	return level;
}

/**
 * Search a node from the tree
 * walk through its descendant if not found, walk
 * all its its ascendant
 */
#if 0
struct tree_node *tree_search_node(const char *fname, struct tree_node *node) {
	return NULL; //todo
}
#endif

/**
 * Iterate over a tree
 * Recursively do the following: If the tree is not empty, visit the root and then traverse all
 * subtrees in ascending order.
 */
void _tree_traversal(struct tree_node *node, int indent)
{
	char c;
	int i = 0;
	char *hyphen;
	char *space;
	struct tree_node *sib;
	if (!node) {
		return;
	}

	space = (char *) malloc(indent + 1);
	if (!space) {
		return;
	}
	memset(space, 0, indent + 1);
	hyphen = (char *) malloc(CONFIG_TREE_INDENT + 1);
	if (!hyphen) {
		free(space);
		return;
	}
	memset(hyphen, '-', CONFIG_TREE_INDENT - 1);
	hyphen[CONFIG_TREE_INDENT - 1] = '\0';
	for (i = 0; i < indent; i++) {
		if ((i % CONFIG_TREE_INDENT) == 0) {
			space[i] = '|';
		} else {
			space[i] = 0x20;
		}
	}
	c = (node->stat.st_mode & S_IFDIR) ? 'o' : '|';
	treelog("%s%c%s%s\n", space, c, hyphen, node->fname);
	if (node->child) {
		_tree_traversal(node->child, indent + 3);
	}

	list_for_each_entry(sib, &node->sibling, sibling) {
		if (sib->stat.st_mode & S_IFDIR) {
			c = 'o';
		} else {
			if (sib->sibling.next == (&node->sibling)) {
				c = '`';
			} else {
				c = '|';
			}
		}
		treelog("%s%c%s%s\n", space, c, hyphen, sib->fname);

		if (sib->child) {
			_tree_traversal(sib->child, indent + CONFIG_TREE_INDENT);
		}
	}
	free(space);
	free(hyphen);
}

void tree_traversal(struct tree_node *node)
{
	struct tree_node *root = tree_get_root(node);
	_tree_traversal(root, 0);
}

