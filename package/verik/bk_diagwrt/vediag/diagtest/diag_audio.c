
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmd.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include <time.h>
#include <vediag/audio.h>
#include <time_util.h>
#include <log.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>
#include <string_plus.h>

static LIST_HEAD(testlist);
static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
static uint32_t test_count;

struct audio_test_list {
	struct list_head l;
	char* devname;
};

struct audio_test_info {
	char audio_file1[60];		/* File1 name */
	char audio_format1[60];		/* File1 format */
	u32 audio_delay1;					/* n sec */
	char audio_file2[60];		/* File1 name */
	char audio_format2[60];		/* File1 format */
	u32 audio_delay2;					/* n sec */
};

static int audio_get_app(char *app)
{
	xinfo("app='%s'\n", app);

	if (app && !strcmp(app, "aplay"))
		return AUDIO_APLAY_TEST;
	else
		return AUDIO_MPG123_TEST;
}

static int audio_create_arg(struct list_head *arg_list)
{
	vediag_set_argument("audio_file1", "audio.wav", arg_list);
	vediag_set_argument("audio_format1", "dat", arg_list);
	vediag_set_argument("audio_delay1", "10", arg_list);

	vediag_set_argument("audio_file2", "micro.wav", arg_list);
	vediag_set_argument("audio_format2", "dat", arg_list);
	vediag_set_argument("audio_delay2", "10", arg_list);

	return 0;
}

static int audio_check_dev(struct vediag_test_info *info,
		struct vediag_test_info *dev_info, char **error)
{
	char name[8];
	struct audio_test_info *test_arg;
	int dev;
	char *tmp;

	if (IS_ERR_OR_NULL((void *) info)) {
		vediag_err(VEDIAG_AUDIO, NULL, "%s(): info NULL\n", __func__);
		return 0;
	}

	if (IS_ERR_OR_NULL((void *) dev_info)) {
		vediag_err(VEDIAG_AUDIO, NULL, "%s(): dev_info NULL\n", __func__);
		return 0;
	}

	/*xmanager will change 'dev_mask' to device index (count from 1)*/
	dev = dev_info->dev_mask;

	memset(name, 0, sizeof(name));

	test_arg = (struct audio_test_info *) malloc(sizeof(struct audio_test_info));
	if (IS_ERR_OR_NULL(test_arg)) {
		vediag_err(VEDIAG_AUDIO, dev_info->dbg_info, "Allocate memory failed\n");
		*error = strdup("Out of memory");
		return 0;
	}
	memset(test_arg, 0, sizeof(struct audio_test_info));

	if (!dev_info->physical_dev) {
		char section[64] = { 0 };
		snprintf(section, 63, "%s%d", get_section_type(dev_info->test_type),
				dev_info->dev_mask);
		*error = strdup("Parse physical failed");
		vediag_err(VEDIAG_AUDIO, dev_info->dbg_info,
				"Parse physical failed, section '%s'\n", section);
		return 0;
	}

	dev_info->total_size = 0;
	/* Check file audio.wav */
	tmp = vediag_get_argument(dev_info, "audio_file1");
	if (tmp != NULL) {
		strncpy(test_arg->audio_file1, tmp, min(sizeof(test_arg->audio_file1), strlen(tmp)));
		free(tmp);
		dev_info->total_size++;
	}
	tmp = vediag_get_argument(dev_info, "audio_format1");
	if (tmp != NULL) {
		strncpy(test_arg->audio_format1, tmp, min(sizeof(test_arg->audio_format1), strlen(tmp)));
		free(tmp);
	}
	test_arg->audio_delay1 = 0;
	tmp = vediag_get_argument(dev_info, "audio_delay1");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->audio_delay1 = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}

	/* Check file audio.wav */
	tmp = vediag_get_argument(dev_info, "audio_file2");
	if (tmp != NULL) {
		strncpy(test_arg->audio_file2, tmp, min(sizeof(test_arg->audio_file2), strlen(tmp)));
		free(tmp);
		dev_info->total_size++;
	}
	tmp = vediag_get_argument(dev_info, "audio_format2");
	if (tmp != NULL) {
		strncpy(test_arg->audio_format2, tmp, min(sizeof(test_arg->audio_format2), strlen(tmp)));
		free(tmp);
	}
	test_arg->audio_delay2 = 0;
	tmp = vediag_get_argument(dev_info, "audio_delay2");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->audio_delay2 = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}

	dev_info->data = (void *) test_arg;
	dev_info->threads = info->threads;
	//dev_info->total_size = 1;
	dev_info->dev_mask = 0;

	xinfo("[%s] Test arguments:\n", dev_info->device);
	xinfo("    test_app     		= %d\n", dev_info->test_app);
	xinfo("    audio_file1     		= %s\n", test_arg->audio_file1);
	xinfo("    audio_format1   		= %s\n", test_arg->audio_format1);
	xinfo("    audio_delay1    		= %d\n", test_arg->audio_delay1);
	xinfo("    audio_file2     		= %s\n", test_arg->audio_file2);
	xinfo("    audio_format2   		= %s\n", test_arg->audio_format2);
	xinfo("    audio_delay2    		= %d\n", test_arg->audio_delay2);
	xinfo("    play_list   			= %lld\n", dev_info->total_size);

	struct audio_test_list *new = malloc(sizeof(struct audio_test_list));
	if (!new){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	new->devname = malloc(32);
	if (!new->devname){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	list_add_tail(&new->l, &testlist);

	pthread_mutex_lock(&_mutex);
	test_count++;
	pthread_mutex_unlock(&_mutex);

    return 1;
}

static int audio_test_start(int argc, char *argv[], void * _data)
{
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;
	//struct audio_test_info *test_arg = (struct audio_test_info *)info->data;

	xdebug("%s Entry\n", __func__);

	//info->total_size = 0xffffffff;

	/* Select test device name */
	info->dev_mask = 0xffffffff;	//no input
	if ((arp = vediag_getopt(argc, argv, "-d")) > 0) {
		if (isdigit(argv[arp + 1][0]))
			info->dev_mask =  strtoul(argv[arp + 1], NULL, 0);
	}

	if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
		info->cfg_file = malloc(strlen(argv[arp + 1]));
		strcpy(info->cfg_file, argv[arp + 1]);
	}

	info->threads = 1;

	pthread_mutex_lock(&_mutex);
	test_count = 0;
	pthread_mutex_unlock(&_mutex);

	return sizeof(struct vediag_test_info);
}

static int audio_test_playback(char *device, struct audio_test_info *test, u32 *size)
{
	int ret;
	char cmd[60];

	/* aplay -f dat -D hw:0,0 audio.wav */
	if(test->audio_file1 != NULL) {
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "aplay -f %s -D %s %s", test->audio_format1, device, test->audio_file1);
		vediag_info(VEDIAG_AUDIO, device, "cmd='%s'\n", cmd);
		ret = system(cmd);
		if (ret != 0) {
			vediag_err(VEDIAG_AUDIO, device, "exec cmd '%s' failed (%d)\n", cmd, ret);
			return -1;
		}
		sleep(test->audio_delay1);
		*size += 1;
	}

	/* aplay -f dat -D hw:0,0 micro.wav */
	if(test->audio_file2 != NULL) {
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "aplay -f %s -D %s %s", test->audio_format2, device, test->audio_file2);
		vediag_info(VEDIAG_AUDIO, device, "cmd='%s'\n", cmd);
		ret = system(cmd);
		if (ret != 0) {
			vediag_err(VEDIAG_AUDIO, device, "exec cmd '%s' failed (%d)\n", cmd, ret);
			return -1;
		}
		sleep(test->audio_delay2);
		*size += 1;
	}

	return 0;
}

static void *audio_test_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct audio_test_info *test_arg = (struct audio_test_info *)info->data;
	int test_done = 0, ret = 0;
	extern int keyinput_yes;
	extern int keyinput_no;
	u32 size = 0;
	
	keyinput_yes = 0;
	keyinput_no = 0;
	

	while (!test_done) {
		if (test_ctrl->data_stop) {
			test_done = 1;
			continue;
		}

		if(info->test_app == AUDIO_APLAY_TEST) {
			vediag_info(VEDIAG_AUDIO, info->dbg_info, "Audio aplay start\n");
			test_ctrl->error = audio_test_playback(info->device, test_arg, &size);
			test_done = 1;
		}

		if(test_ctrl->error == 0) {
			if(info->test_app == AUDIO_APLAY_TEST) {
				test_ctrl->tested_size += size;
			}

			ret = check_key_result(VEDIAG_AUDIO, info->device, 5, 3);
			if(ret == 1) {
				info->total_size = test_ctrl->tested_size;
				test_done = 1;
				break;
			} else if(ret == 2) {
				test_done = 1;
				test_ctrl->error = 1;
				break;
			}
		}
		
		if (test_ctrl->error) {
			vediag_err(VEDIAG_AUDIO, info->dbg_info, "Audio failed\n");
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		}

		/* Check to complete test */
		if(test_ctrl->tested_size >= info->total_size) {
			test_done = 1;
			test_ctrl->error = 1;
		}

		sleep(0);
	}

	test_ctrl->data_running--;
	pthread_exit(NULL);
}

static int audio_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct audio_test_info *test_arg = (struct audio_test_info *) (info->data);
	struct audio_test_list *e;
	struct list_head *pos, *q;

	pthread_mutex_lock(&_mutex);
	test_count--;
	pthread_mutex_unlock(&_mutex);

	if (test_count == 0) {
		list_for_each_entry(e, &testlist, l) {

			if (e->devname) {
				free(e->devname);
			}
		}

		list_for_each_safe(pos, q, &testlist) {
			e = list_entry(pos, struct audio_test_list, l);
			list_del(pos);
			free(e);
		}
	}

	if (test_arg)
		free(test_arg);

	return 0;
}

struct vediag_test_device vediag_audio = {
    .type           = VEDIAG_AUDIO,
	.flags			= VEDIAG_TEST_MANUAL,
    .name           = "AUDIO",
    .cmd            = "audio",
    .desc           = "AUDIO TEST",
	.default_app	= "aplay",
	.get_test_app	= audio_get_app,
    .test_start     = audio_test_start,
	.create_argument= audio_create_arg,
    .check_dev      = audio_check_dev,
    .test_execute   = audio_test_execute,
    .test_complete  = audio_test_complete,
};

late_initcall(diag_audio_test_register)
{
    return vediag_test_register(&vediag_audio);
}
