
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmd.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include <time.h>
#include <vediag/zigbee.h>
#include <time_util.h>
#include <log.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>
#include <string_plus.h>

static LIST_HEAD(testlist);
static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
static uint32_t test_count;

struct zigbee_test_list {
	struct list_head l;
	char* devname;
};

#define ZIGBEE_CMD_SET	2
struct zigbee_test_info {
	char zigbee_fwver[20];					/* Firmware File */
	char zigbee_cmd_fwver[20];				/* Command to Read Firmware Version */

	char zigbee_fwpath[100];				/* Firmware File */
	int zigbee_program;						/* Flag fw program */
	char zigbee_cmd_prog[20];				/* Command to Program Firmware */

	int zigbee_reset; 						/* Reset Controller */
	char zigbee_cmd_reset[20];				/* Command to Reset Controller */

	int zigbee_add; 						/* Add a new device */
	char zigbee_cmd_add[20];				/* Command to Add New Device */

	int zigbee_cmd_retry;					/* Number of retry for command Set(work around for release 5.0/6.0) */
	char zigbee_cmd_set1[20];				/* Basic command set range['-d' off, '-e' on] */
	int zigbee_interval;					/* Delay interval second */
	char zigbee_cmd_set2[20];				/* Basic command set range['-d' off, '-e' on] */
	int zigbee_cmd_loop;					/* Number of loop command */

	int zigbee_rssi_min;					/* From -94 dBm to -32 dBm */
	int zigbee_rssi_max;

	int zigbee_remove; 						/* Remove a new device */
	char zigbee_cmd_remove[20];				/* Command to Remove Device */
};

static int zigbee_get_app(char *app)
{
	xinfo("app='%s'\n", app);

	if (app && !strcmp(app, "zigbee_tools"))
		return ZIGBEE_TOOLS_TEST;
	else
		return ZIGBEE_TOOLS_TEST;
}

static int zigbee_create_arg(struct list_head *arg_list)
{
	vediag_set_argument("zigbee_fwpath", "", arg_list);
	vediag_set_argument("zigbee_cmd_prog", "", arg_list);
	vediag_set_argument("zigbee_fwver", "5.10.1", arg_list);
	vediag_set_argument("zigbee_cmd_fwver", "-v", arg_list);

	vediag_set_argument("zigbee_reset", "1", arg_list);
	vediag_set_argument("zigbee_cmd_reset", "-n", arg_list);

	vediag_set_argument("zigbee_add", "1", arg_list);
	vediag_set_argument("zigbee_cmd_add", "-o", arg_list);

	vediag_set_argument("zigbee_cmd_retry", "0", arg_list);
	vediag_set_argument("zigbee_cmd_set1", "-d", arg_list);
	vediag_set_argument("zigbee_interval", "5", arg_list);
	vediag_set_argument("zigbee_cmd_set2", "-e", arg_list);

	vediag_set_argument("zigbee_rssi_min", "-94", arg_list);
	vediag_set_argument("zigbee_rssi_max", "-32", arg_list);

	vediag_set_argument("zigbee_cmd_loop", "3", arg_list);

	vediag_set_argument("zigbee_remove", "1", arg_list);
	vediag_set_argument("zigbee_cmd_remove", "-a", arg_list);

	return 0;
}

static int zigbee_check_dev(struct vediag_test_info *info,
		struct vediag_test_info *dev_info, char **error)
{
	char name[8];
	struct zigbee_test_info *test_arg;
	int dev;
	char *tmp;

	if (IS_ERR_OR_NULL((void *) info)) {
		vediag_err(VEDIAG_ZIGBEE, NULL, "%s(): info NULL\n", __func__);
		return 0;
	}

	if (IS_ERR_OR_NULL((void *) dev_info)) {
		vediag_err(VEDIAG_ZIGBEE, NULL, "%s(): dev_info NULL\n", __func__);
		return 0;
	}

	/*xmanager will change 'dev_mask' to device index (count from 1)*/
	dev = dev_info->dev_mask;

	memset(name, 0, sizeof(name));

	test_arg = (struct zigbee_test_info *) malloc(sizeof(struct zigbee_test_info));
	if (IS_ERR_OR_NULL(test_arg)) {
		vediag_err(VEDIAG_ZIGBEE, dev_info->dbg_info, "Allocate memory failed\n");
		*error = strdup("Out of memory");
		return 0;
	}
	memset(test_arg, 0, sizeof(struct zigbee_test_info));

	if (!dev_info->physical_dev) {
		char section[64] = { 0 };
		snprintf(section, 63, "%s%d", get_section_type(dev_info->test_type),
				dev_info->dev_mask);
		*error = strdup("Parse physical failed");
		vediag_err(VEDIAG_ZIGBEE, dev_info->dbg_info,
				"Parse physical failed, section '%s'\n", section);
		return 0;
	}

	dev_info->total_size = 0;

	/* Get Firmware Path */
	test_arg->zigbee_program =  0;
	tmp = vediag_get_argument(dev_info, "zigbee_fwpath");
	if (tmp != NULL) {
		strncpy(test_arg->zigbee_fwpath, tmp, min(sizeof(test_arg->zigbee_fwpath), strlen(tmp)));
		free(tmp);
		dev_info->total_size++;
		/* Set flag program */
		test_arg->zigbee_program =  1;
	}
	/* Get Program Command Option */
	tmp = vediag_get_argument(dev_info, "zigbee_cmd_prog");
	if (tmp != NULL) {
		strncpy(test_arg->zigbee_cmd_prog, tmp, min(sizeof(test_arg->zigbee_cmd_prog), strlen(tmp)));
		free(tmp);
	}
	/* Get Firmware Version */
	tmp = vediag_get_argument(dev_info, "zigbee_fwver");
	if (tmp != NULL) {
		strncpy(test_arg->zigbee_fwver, tmp, min(sizeof(test_arg->zigbee_fwver), strlen(tmp)));
		free(tmp);
	}
	/* Get Firmware Version Command Option */
	tmp = vediag_get_argument(dev_info, "zigbee_cmd_fwver");
	if (tmp != NULL) {
		strncpy(test_arg->zigbee_cmd_fwver, tmp, min(sizeof(test_arg->zigbee_cmd_fwver), strlen(tmp)));
		free(tmp);
	}

	/* Default Reset Controller */
	test_arg->zigbee_reset = 1;
	tmp = vediag_get_argument(dev_info, "zigbee_reset");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zigbee_reset = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	test_arg->zigbee_reset = !!test_arg->zigbee_reset;
	/* Get Reset Command Option */
	tmp = vediag_get_argument(dev_info, "zigbee_cmd_reset");
	if (tmp != NULL) {
		strncpy(test_arg->zigbee_cmd_reset, tmp, min(sizeof(test_arg->zigbee_cmd_reset), strlen(tmp)));
		free(tmp);
	}

	/* Default Add new device */
	test_arg->zigbee_add = 1;
	tmp = vediag_get_argument(dev_info, "zigbee_add");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zigbee_add = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	test_arg->zigbee_add = !!test_arg->zigbee_add;
	dev_info->total_size += test_arg->zigbee_add;
	/* Get Add Command Option */
	tmp = vediag_get_argument(dev_info, "zigbee_cmd_add");
	if (tmp != NULL) {
		strncpy(test_arg->zigbee_cmd_add, tmp, min(sizeof(test_arg->zigbee_cmd_add), strlen(tmp)));
		free(tmp);
	}

	/* Number of retry for command Set(work around for release 5.0/6.0) */
	test_arg->zigbee_cmd_retry = 0;
	tmp = vediag_get_argument(dev_info, "zigbee_cmd_retry");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zigbee_cmd_retry = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}

	/* Default cmd Off-On Light Bulb */
	tmp = vediag_get_argument(dev_info, "zigbee_cmd_set1");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			strncpy(test_arg->zigbee_cmd_set1, tmp, min(strlen(tmp), sizeof(test_arg->zigbee_cmd_set1)));
		}
		free(tmp);
	}
	test_arg->zigbee_interval = 5;
	tmp = vediag_get_argument(dev_info, "zigbee_interval");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zigbee_interval = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	tmp = vediag_get_argument(dev_info, "zigbee_cmd_set2");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			strncpy(test_arg->zigbee_cmd_set2, tmp, min(strlen(tmp), sizeof(test_arg->zigbee_cmd_set2)));
		}
		free(tmp);
	}
	test_arg->zigbee_rssi_min = -94; /* dBm */
	tmp = vediag_get_argument(dev_info, "zigbee_rssi_min");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zigbee_rssi_min = strtol(tmp, NULL, 0);
		}
		free(tmp);
	}
	test_arg->zigbee_rssi_max = -32; /* dBm */
	tmp = vediag_get_argument(dev_info, "zigbee_rssi_max");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zigbee_rssi_max = strtol(tmp, NULL, 0);
		}
		free(tmp);
	}
	/* Default Cmd Loop */
	test_arg->zigbee_cmd_loop = 1;
	tmp = vediag_get_argument(dev_info, "zigbee_cmd_loop");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zigbee_cmd_loop = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	dev_info->total_size += test_arg->zigbee_cmd_loop*2;
	/* Default Remove new device after test completed */
	test_arg->zigbee_remove = 1;
	tmp = vediag_get_argument(dev_info, "zigbee_remove");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zigbee_remove = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	test_arg->zigbee_remove = !!test_arg->zigbee_remove;
	dev_info->total_size += test_arg->zigbee_remove;
	/* Get Remove Command Option */
	tmp = vediag_get_argument(dev_info, "zigbee_cmd_remove");
	if (tmp != NULL) {
		strncpy(test_arg->zigbee_cmd_remove, tmp, min(sizeof(test_arg->zigbee_cmd_remove), strlen(tmp)));
		free(tmp);
	}

	dev_info->data = (void *) test_arg;
	dev_info->threads = info->threads;
	dev_info->dev_mask = 0;

	xinfo("[%s] Test arguments:\n", dev_info->device);
	xinfo("    test_app     		= %d\n", dev_info->test_app);

	xinfo("    zigbee_program		= %d\n", test_arg->zigbee_program);
	xinfo("    zigbee_fwpath		= %s\n", test_arg->zigbee_fwpath);
	xinfo("    zigbee_cmd_prog		= %s\n", test_arg->zigbee_cmd_prog);
	xinfo("    zigbee_fwver			= %s\n", test_arg->zigbee_fwver);
	xinfo("    zigbee_cmd_fwver		= %s\n", test_arg->zigbee_cmd_fwver);

	xinfo("    zigbee_reset			= %d\n", test_arg->zigbee_reset);
	xinfo("    zigbee_cmd_reset		= %s\n", test_arg->zigbee_cmd_reset);

	xinfo("    zigbee_add			= %d\n", test_arg->zigbee_add);
	xinfo("    zigbee_cmd_add		= %s\n", test_arg->zigbee_cmd_add);

	xinfo("    zigbee_cmd_retry		= %d\n", test_arg->zigbee_cmd_retry);
	xinfo("    zigbee_cmd_set1		= %s\n", test_arg->zigbee_cmd_set1);
	xinfo("    zigbee_interval		= %d sec\n", test_arg->zigbee_interval);
	xinfo("    zigbee_cmd_set2		= %s\n", test_arg->zigbee_cmd_set2);

	xinfo("    zigbee_rssi_min		= %d\n", test_arg->zigbee_rssi_min);
	xinfo("    zigbee_rssi_max		= %d\n", test_arg->zigbee_rssi_max);

	xinfo("    zigbee_cmd_loop		= %d\n", test_arg->zigbee_cmd_loop);

	xinfo("    zigbee_remove		= %d\n", test_arg->zigbee_remove);
	xinfo("    zigbee_cmd_remove	= %s\n", test_arg->zigbee_cmd_remove);

	xinfo("    Total events			= %lld\n", dev_info->total_size);

	struct zigbee_test_list *new = malloc(sizeof(struct zigbee_test_list));
	if (!new){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	new->devname = malloc(32);
	if (!new->devname) {
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	list_add_tail(&new->l, &testlist);

	pthread_mutex_lock(&_mutex);
	test_count++;
	pthread_mutex_unlock(&_mutex);

    return 1;
}

static int zigbee_test_start(int argc, char *argv[], void * _data)
{
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;
	//struct zigbee_test_info *test_arg = (struct zigbee_test_info *)info->data;

	xdebug("%s Entry\n", __func__);

	//info->total_size = 0xffffffff;

	/* Select test device name */
	info->dev_mask = 0xffffffff;	//no input
	if ((arp = vediag_getopt(argc, argv, "-d")) > 0) {
		if (isdigit(argv[arp + 1][0]))
			info->dev_mask =  strtoul(argv[arp + 1], NULL, 0);
	}

	if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
		info->cfg_file = malloc(strlen(argv[arp + 1]));
		strcpy(info->cfg_file, argv[arp + 1]);
	}

	info->threads = 1;

	pthread_mutex_lock(&_mutex);
	test_count = 0;
	pthread_mutex_unlock(&_mutex);

	return sizeof(struct vediag_test_info);
}

static int zigbee_tools_fw_program(char *device, struct zigbee_test_info *test_arg)
{
	FILE *devf;
	int line_num = 0, ret = -1, retval = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60], version[60];
	char *fwpath = test_arg->zigbee_fwpath;
	char *fwver = test_arg->zigbee_fwver;

	if(fwver != NULL) {
		/*
		 * zigbee_tools -v
		 * Reset info: 11 (SOFTWARE)
		 * ezsp ver 0x05 stack type 0x02 stack ver. [5.10.1 GA build 80]
		 * Ezsp Config: set source route table size to 0x00FA:Success: set
		 * Ezsp Config: set security level to 0x0005:Success: set
		 * Ezsp Config: set address table size to 0x0002:Success: set
		 * Ezsp Config: set TC addr cache to 0x0002:Success: set
		 * Ezsp Config: set stack profile to 0x0002:Success: set
		 * Ezsp Config: set MAC indirect TX timeout to 0x1E00:Success: set
		 * Ezsp Config: set max hops to 0x001E:Success: set
		 * Ezsp Config: set tx power mode to 0x8000:Success: set
		 * Ezsp Config: set supported networks to 0x0001:Success: set
		 * Ezsp Policy: set binding modify to "allow for valid endpoints & clusters only":Success: set
		 * Ezsp Policy: set message content in msgSent to "return":Success: set
		 * Ezsp Value : set maximum incoming transfer size to 0x00000052:Success: set
		 * Ezsp Value : set maximum outgoing transfer size to 0x00000052:Success: set
		 * Ezsp Config: set binding table size to 0x0010:Success: set
		 * Ezsp Config: set key table size to 0x0000:Success: set
		 * Ezsp Config: set max end device children to 0x0020:Success: set
		 * NCP supports maxing out packet buffers
		 * Ezsp Config: set packet buffers to 255
		 * Ezsp Config: set end device poll timeout to 0x0005:Success: set
		 * Ezsp Config: set end device poll timeout shift to 0x0006:Success: set
		 * Ezsp Config: set zll group addresses to 0x0000:Success: set
		 * Ezsp Config: set zll rssi threshold to 0xFF80:Success: set
		 * Ezsp Config: set transient key timeout to 0x0078:Success: set
		 * Ezsp Endpoint 1 added, profile 0x0104, in clusters: 3, out clusters 10
		 * Ezsp Endpoint 242 added, profile 0xA1E0, in clusters: 1, out clusters 1
		 * ......................................................
		 * ...........waiting for program do something...........
		 * EMBER_NETWORK_UP 0x0000
		 * ezsp ver 0x05 stack type 0x02 stack ver. [5.10.1 GA build 80]
		 * -----return: 0x0005
		 */
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "zigbee_tools %s", test_arg->zigbee_cmd_fwver);
		vediag_debug(VEDIAG_ZIGBEE, "command:", "%s\n", cmd);
		devf = popen(cmd, "r");
		if (devf != NULL) {
			while (1) {
				memset(buf, 0, sizeof(buf));
				line = fgets(buf, sizeof(buf), devf);

				if (line == NULL) break;
				line_num++;
				memset(version, 0, sizeof(version));
				ret = sscanf(line, "ezsp ver 0x%*[a-z0-9] stack type 0x%*[a-z0-9] stack ver. [%s GA build 80]", version);
				if(ret < 1) {
					ret = -1;
					//vediag_debug(VEDIAG_ZIGBEE, device, "%d: '%s' ret(%d)\n", bt_num, addr, ret);
					continue;
				}
				if(strcmp(fwver, version) == 0) {
					vediag_info(VEDIAG_ZIGBEE, device, "Firmware version updated '%s'\n", version);
					pclose(devf);
					return 0;
				} else {
					vediag_info(VEDIAG_ZIGBEE, device, "Firmware version current '%s'\n", version);
					break;
				}
			}
			pclose(devf);
		}
	}
	/*
	 * zigbee_tools -p <firmware>
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zigbee_tools %s %s", test_arg->zigbee_cmd_prog, fwpath);
	vediag_debug(VEDIAG_ZIGBEE, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			ret = sscanf(line, "------return: 0x%x", &retval);
			if(ret < 1) {
				ret = -1;
				//vediag_debug(VEDIAG_ZIGBEE, device, "%d: '%s' ret(%d)\n", bt_num, addr, ret);
				continue;
			}
			ret = retval!=0 ? -1:0;
			if (ret < 0) {
				vediag_err(VEDIAG_ZIGBEE, device, "program failed (%d)\n", ret);
			}
			break;
		}
		pclose(devf);
	}

	return ret;
}

#define ZIGBEE_ADD_DEVICE_RETRY	3
static int zigbee_tools_add_device(char *device, struct zigbee_test_info *test_arg, int *nodeid_add)
{
	FILE *devf;
	int line_num = 0, ret = -1, retval = 0, nodeid;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60];

	/* Check if enable Reset command */
	if(test_arg->zigbee_reset) {
		/*
		 * zigbee_tools -n
		 * Reset info: 11 (SOFTWARE)
		 * ezsp ver 0x05 stack type 0x02 stack ver. [5.10.1 GA build 80]
		 * Ezsp Config: set source route table size to 0x00FA:Success: set
		 * Ezsp Config: set security level to 0x0005:Success: set
		 * Ezsp Config: set address table size to 0x0002:Success: set
		 * Ezsp Config: set TC addr cache to 0x0002:Success: set
		 * Ezsp Config: set stack profile to 0x0002:Success: set
		 * Ezsp Config: set MAC indirect TX timeout to 0x1E00:Success: set
		 * Ezsp Config: set max hops to 0x001E:Success: set
		 * Ezsp Config: set tx power mode to 0x8000:Success: set
		 * Ezsp Config: set supported networks to 0x0001:Success: set
		 * Ezsp Policy: set binding modify to "allow for valid endpoints & clusters only":Success: set
		 * Ezsp Policy: set message content in msgSent to "return":Success: set
		 * Ezsp Value : set maximum incoming transfer size to 0x00000052:Success: set
		 * Ezsp Value : set maximum outgoing transfer size to 0x00000052:Success: set
		 * Ezsp Config: set binding table size to 0x0010:Success: set
		 * Ezsp Config: set key table size to 0x0000:Success: set
		 * Ezsp Config: set max end device children to 0x0020:Success: set
		 * NCP supports maxing out packet buffers
		 * Ezsp Config: set packet buffers to 255
		 * Ezsp Config: set end device poll timeout to 0x0005:Success: set
		 * Ezsp Config: set end device poll timeout shift to 0x0006:Success: set
		 * Ezsp Config: set zll group addresses to 0x0000:Success: set
		 * Ezsp Config: set zll rssi threshold to 0xFF80:Success: set
		 * Ezsp Config: set transient key timeout to 0x0078:Success: set
		 * Ezsp Endpoint 1 added, profile 0x0104, in clusters: 3, out clusters 10
		 * Ezsp Endpoint 242 added, profile 0xA1E0, in clusters: 1, out clusters 1
		 * ......................................................
		 * ...........waiting for program do something...........
		 * EMBER_NETWORK_UP 0x0000
		 * ------return: 0x0000
		 */
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "zigbee_tools %s", test_arg->zigbee_cmd_reset);
		vediag_debug(VEDIAG_ZIGBEE, "command:", "%s\n", cmd);
		devf = popen(cmd, "r");
		if (devf != NULL) {
			while (1) {
				memset(buf, 0, sizeof(buf));
				line = fgets(buf, sizeof(buf), devf);

				if (line == NULL) break;
				line_num++;

				ret = sscanf(line, "------return: 0x%x", &retval);
				if(ret < 1) {
					ret = -1;
					//vediag_debug(VEDIAG_ZIGBEE, device, "%d: '%s' ret(%d)\n", bt_num, addr, ret);
					continue;
				}
				vediag_debug(VEDIAG_ZIGBEE, device, "Reset Controller %s\n", retval==0?"successful":"fail");
				ret = retval!=0?-1:0;
				break;
			}
			pclose(devf);
		}
		if (ret < 0) {
			return -1;
		}
	}

	/*
	 * zigbee_tools -o
     * Reset info: 11 (SOFTWARE)
     * ezsp ver 0x05 stack type 0x02 stack ver. [5.10.1 GA build 80]
     * Ezsp Config: set source route table size to 0x00FA:Success: set
     * Ezsp Config: set security level to 0x0005:Success: set
     * Ezsp Config: set address table size to 0x0002:Success: set
     * Ezsp Config: set TC addr cache to 0x0002:Success: set
     * Ezsp Config: set stack profile to 0x0002:Success: set
     * Ezsp Config: set MAC indirect TX timeout to 0x1E00:Success: set
     * Ezsp Config: set max hops to 0x001E:Success: set
     * Ezsp Config: set tx power mode to 0x8000:Success: set
     * Ezsp Config: set supported networks to 0x0001:Success: set
     * Ezsp Policy: set binding modify to "allow for valid endpoints & clusters only":Success: set
     * Ezsp Policy: set message content in msgSent to "return":Success: set
     * Ezsp Value : set maximum incoming transfer size to 0x00000052:Success: set
     * Ezsp Value : set maximum outgoing transfer size to 0x00000052:Success: set
     * Ezsp Config: set binding table size to 0x0010:Success: set
     * Ezsp Config: set key table size to 0x0000:Success: set
     * Ezsp Config: set max end device children to 0x0020:Success: set
     * NCP supports maxing out packet buffers
     * Ezsp Config: set packet buffers to 255
     * Ezsp Config: set end device poll timeout to 0x0005:Success: set
     * Ezsp Config: set end device poll timeout shift to 0x0006:Success: set
     * Ezsp Config: set zll group addresses to 0x0000:Success: set
     * Ezsp Config: set zll rssi threshold to 0xFF80:Success: set
     * Ezsp Config: set transient key timeout to 0x0078:Success: set
     * Ezsp Endpoint 1 added, profile 0x0104, in clusters: 3, out clusters 10
     * Ezsp Endpoint 242 added, profile 0xA1E0, in clusters: 1, out clusters 1
     * ......................................................
     * ...........waiting for program do something...........
     * EMBER_NETWORK_UP 0x0000
     * Please press button on the device to join network
     * Ezsp Policy: set Trust Center Policy to "Allow preconfigured key joins":Success: set
     * pJoin for 120 sec: 0x00
     * NWK Creator Security: Open network: 0x00
     * FUNC_ID = 0x45, RSSI = 0xDA (-38 dBm)
     * FUNC_ID = 0x45, RSSI = 0xDA (-38 dBm)
     * Device Unsecured joined successfully, destAddr, 0x1D62 (7522), euiAddr: 0x7CE52400000C2667 (8999639012789200487)
     * ......................One device is found........................
     * ......................close network........................
     * Ezsp Policy: set Trust Center Policy to "Disallow all joins and rejoins":Success: set
     * pJoin for 0 sec: 0x00
     * -----return: 0x0000
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zigbee_tools %s", test_arg->zigbee_cmd_add);
	vediag_debug(VEDIAG_ZIGBEE, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			ret = sscanf(line, "Device Unsecured joined successfully, destAddr, 0x%x (%*[A-Z0-9]), euiAddr: 0x%*[A-Z0-9] (%*[A-Z0-9])", &nodeid);
			if(ret < 1) {
				ret = -1;
				//vediag_debug(VEDIAG_ZIGBEE, device, "%d: '%s' ret(%d)\n", bt_num, addr, ret);
				continue;
			}
			*nodeid_add = nodeid;
			vediag_debug(VEDIAG_ZIGBEE, device, "Added NodeID=%03u\n", nodeid);
			ret = 0;
			break;
		}
		pclose(devf);
	}
	return ret;
}

static int zigbee_tools_cmd_set(char *device, struct zigbee_test_info *test_arg, int nodeid, char *set_cmd)
{
	FILE *devf;
	int line_num = 0, ret = -1, funcid, rssi_hex, rssi_dBm, retry = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60];
	int rssi_min = test_arg->zigbee_rssi_min;
	int rssi_max = test_arg->zigbee_rssi_max;

	/*
	 * zigbee_tools -d 0xE3E9
	 * Reset info: 11 (SOFTWARE)
	 * ezsp ver 0x05 stack type 0x02 stack ver. [5.10.1 GA build 80]
	 * Ezsp Config: set source route table size to 0x00FA:Success: set
	 * Ezsp Config: set security level to 0x0005:Success: set
	 * Ezsp Config: set address table size to 0x0002:Success: set
	 * Ezsp Config: set TC addr cache to 0x0002:Success: set
	 * Ezsp Config: set stack profile to 0x0002:Success: set
	 * Ezsp Config: set MAC indirect TX timeout to 0x1E00:Success: set
	 * Ezsp Config: set max hops to 0x001E:Success: set
	 * Ezsp Config: set tx power mode to 0x8000:Success: set
	 * Ezsp Config: set supported networks to 0x0001:Success: set
	 * Ezsp Policy: set binding modify to "allow for valid endpoints & clusters only":Success: set
	 * Ezsp Policy: set message content in msgSent to "return":Success: set
	 * Ezsp Value : set maximum incoming transfer size to 0x00000052:Success: set
	 * Ezsp Value : set maximum outgoing transfer size to 0x00000052:Success: set
	 * Ezsp Config: set binding table size to 0x0010:Success: set
	 * Ezsp Config: set key table size to 0x0000:Success: set
	 * Ezsp Config: set max end device children to 0x0020:Success: set
	 * NCP supports maxing out packet buffers
	 * Ezsp Config: set packet buffers to 255
	 * Ezsp Config: set end device poll timeout to 0x0005:Success: set
	 * Ezsp Config: set end device poll timeout shift to 0x0006:Success: set
	 * Ezsp Config: set zll group addresses to 0x0000:Success: set
	 * Ezsp Config: set zll rssi threshold to 0xFF80:Success: set
	 * Ezsp Config: set transient key timeout to 0x0078:Success: set
	 * Ezsp Endpoint 1 added, profile 0x0104, in clusters: 3, out clusters 10
	 * Ezsp Endpoint 242 added, profile 0xA1E0, in clusters: 1, out clusters 1
	 * ......................................................
	 * ...........waiting for program do something...........
	 * EMBER_NETWORK_UP 0x0000
	 * Msg: clus 0x0006, cmd 0x00, len 3
	 * buffer: 01 00 00
	 * bytes send successfully.
	 * FUNC_ID = 0x45, RSSI = 0xDA (-38 dBm)
	 * T00000000:RX len 5, ep 01, clus 0x0006 (On/off) FC 08 seq 00 cmd 0B payload[00 00 ]
	 * ------return: 0x0000
	 */

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zigbee_tools %s 0x%X", set_cmd, nodeid);
	vediag_debug(VEDIAG_ZIGBEE, "command:", "%s\n", cmd);
cmd_retry:
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			ret = sscanf(line, "FUNC_ID = 0x%x, RSSI = 0x%x (%d dBm)", &funcid, &rssi_hex, &rssi_dBm);
			if(ret < 3) {
				ret = -1;
				continue;
			}
			if((rssi_dBm > rssi_min) && (rssi_dBm < rssi_max)) {
				vediag_info(VEDIAG_ZIGBEE, device, "RSSI: %d dBm pass in range [%d %d]\n", rssi_dBm, rssi_min, rssi_max);
				ret = 0;
			} else {
				vediag_err(VEDIAG_ZIGBEE, device, "RSSI: %d dBm fail out of range [%d %d]\n", rssi_dBm, rssi_min, rssi_max);
				ret = -1;
			}
			break;
		}
		pclose(devf);
	}

	/* Check if last command failure, device not respond */
	if ((line == NULL) && (test_arg->zigbee_cmd_retry > 0) && (retry < test_arg->zigbee_cmd_retry)) {
		retry++;
		vediag_warn(VEDIAG_ZIGBEE, device, "Command Failed, do retry %d (max %d)\n", retry, test_arg->zigbee_cmd_retry);
		goto cmd_retry;
	}

	return ret;
}

static int zigbee_tools_remove_device(char *device, struct zigbee_test_info *test_arg, int nodeid)
{
	FILE *devf;
	int line_num = 0, ret = -1, retval = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60];

	/*
     * zigbee_tools -a 0x1D62
     * Reset info: 11 (SOFTWARE)
     * ezsp ver 0x05 stack type 0x02 stack ver. [5.10.1 GA build 80]
     * Ezsp Config: set source route table size to 0x00FA:Success: set
     * Ezsp Config: set security level to 0x0005:Success: set
     * Ezsp Config: set address table size to 0x0002:Success: set
     * Ezsp Config: set TC addr cache to 0x0002:Success: set
     * Ezsp Config: set stack profile to 0x0002:Success: set
     * Ezsp Config: set MAC indirect TX timeout to 0x1E00:Success: set
     * Ezsp Config: set max hops to 0x001E:Success: set
     * Ezsp Config: set tx power mode to 0x8000:Success: set
     * Ezsp Config: set supported networks to 0x0001:Success: set
     * Ezsp Policy: set binding modify to "allow for valid endpoints & clusters only":Success: set
     * Ezsp Policy: set message content in msgSent to "return":Success: set
     * Ezsp Value : set maximum incoming transfer size to 0x00000052:Success: set
     * Ezsp Value : set maximum outgoing transfer size to 0x00000052:Success: set
     * Ezsp Config: set binding table size to 0x0010:Success: set
     * Ezsp Config: set key table size to 0x0000:Success: set
     * Ezsp Config: set max end device children to 0x0020:Success: set
     * NCP supports maxing out packet buffers
     * Ezsp Config: set packet buffers to 255
     * Ezsp Config: set end device poll timeout to 0x0005:Success: set
     * Ezsp Config: set end device poll timeout shift to 0x0006:Success: set
     * Ezsp Config: set zll group addresses to 0x0000:Success: set
     * Ezsp Config: set zll rssi threshold to 0xFF80:Success: set
     * Ezsp Config: set transient key timeout to 0x0078:Success: set
     * Ezsp Endpoint 1 added, profile 0x0104, in clusters: 3, out clusters 10
     * Ezsp Endpoint 242 added, profile 0xA1E0, in clusters: 1, out clusters 1
     * ......................................................
     * ...........waiting for program do something...........
     * EMBER_NETWORK_UP 0x0000
     * FUNC_ID = 0x45, RSSI = 0xDA (-38 dBm)
     * ------return: 0x0000
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zigbee_tools %s 0x%X", test_arg->zigbee_cmd_remove, nodeid);
	vediag_debug(VEDIAG_ZIGBEE, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			ret = sscanf(line, "------return: 0x%x", &retval);
			if(ret < 1) {
				ret = -1;
				//vediag_debug(VEDIAG_ZIGBEE, device, "%d: '%s' ret(%d)\n", bt_num, addr, ret);
				continue;
			}
			vediag_debug(VEDIAG_ZIGBEE, device, "Removed NodeID=0x%0X %s\n", nodeid, retval==0?"successful":"fail");
			ret = retval!=0?-1:0;
			break;
		}
		pclose(devf);
	}
	return ret;
}

static void *zigbee_test_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct zigbee_test_info *test_arg = (struct zigbee_test_info *)info->data;
	int test_done = 0, iteration = 0, nodeid = -1, retry = 0;

	while (!test_done) {
		if (test_ctrl->data_stop) {
			test_done = 1;
			continue;
		}

		if(info->test_app == ZIGBEE_TOOLS_TEST) {
			if(test_arg->zigbee_program != 0) {
				vediag_info(VEDIAG_ZIGBEE, info->dbg_info, "zigbee program firmware path '%s'\n", test_arg->zigbee_fwpath);
				test_ctrl->error |= zigbee_tools_fw_program(info->device, test_arg);
				/* Clear flag */
				test_arg->zigbee_program = 0;
				iteration++;
				retry = 0;
			} else if(test_arg->zigbee_add != 0) {
				vediag_info(VEDIAG_ZIGBEE, info->dbg_info, "zigbee add new device\n");
				sleep(1);
				if(zigbee_tools_add_device(info->device, test_arg, &nodeid) < 0) {
					retry++;
					if(retry >= ZIGBEE_ADD_DEVICE_RETRY) {
						test_ctrl->error |= 1;
					}
				} else {
					vediag_info(VEDIAG_ZIGBEE, info->dbg_info, "zigbee added nodeid[0x%X] successfully\n", nodeid);
					/* Clear flag */
					test_arg->zigbee_add = 0;
					iteration++;
					retry = 0;
				}
			} else if(test_arg->zigbee_cmd_loop > 0) {
				vediag_info(VEDIAG_ZIGBEE, info->dbg_info, "zigbee cmd set[%d]: [%s %ds %s], check RSSI[%d %d]\n",
						test_arg->zigbee_cmd_loop, test_arg->zigbee_cmd_set1, test_arg->zigbee_interval, test_arg->zigbee_cmd_set2,
						test_arg->zigbee_rssi_min, test_arg->zigbee_rssi_max);
				/* Delay interval sec */
				sleep(test_arg->zigbee_interval);
				test_ctrl->error |= zigbee_tools_cmd_set(info->device, test_arg, nodeid, test_arg->zigbee_cmd_set1);
				iteration++;
				/* Delay interval sec */
				sleep(test_arg->zigbee_interval);
				test_ctrl->error |= zigbee_tools_cmd_set(info->device, test_arg, nodeid, test_arg->zigbee_cmd_set2);
				iteration++;
				test_arg->zigbee_cmd_loop--;
			} else if(test_arg->zigbee_remove != 0) {
				vediag_info(VEDIAG_ZIGBEE, info->dbg_info, "zigbee remove nodeid[0x%x]\n", nodeid);
				sleep(1);
				if(zigbee_tools_remove_device(info->device, test_arg, nodeid) < 0) {
					retry++;
					if(retry >= ZIGBEE_ADD_DEVICE_RETRY) {
						test_ctrl->error |= 1;
					}
				} else {
					vediag_info(VEDIAG_ZIGBEE, info->dbg_info, "zigbee removed nodeid[0x%X] successfully\n", nodeid);
					/* Clear flag */
					test_arg->zigbee_remove = 0;
					iteration++;
					retry = 0;
				}
			}
		}

		/* Check to stop */
		if(info->test_app == ZIGBEE_TOOLS_TEST) {
			test_ctrl->tested_size = iteration;
			if(test_ctrl->tested_size >= info->total_size) {
				test_done = 1;
			}
		}

		if (test_ctrl->error) {
			vediag_err(VEDIAG_ZIGBEE, info->dbg_info, "ZIGBEE failed\n");
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		}

		sleep(0);
	}

	test_ctrl->data_running--;
	pthread_exit(NULL);
}

static int zigbee_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct zigbee_test_info *test_arg = (struct zigbee_test_info *) (info->data);
	struct zigbee_test_list *e;
	struct list_head *pos, *q;

	pthread_mutex_lock(&_mutex);
	test_count--;
	pthread_mutex_unlock(&_mutex);

	if (test_count == 0) {
		list_for_each_entry(e, &testlist, l) {

			if (e->devname) {
				free(e->devname);
			}
		}

		list_for_each_safe(pos, q, &testlist) {
			e = list_entry(pos, struct zigbee_test_list, l);
			list_del(pos);
			free(e);
		}
	}

	if (test_arg)
		free(test_arg);

	return 0;
}

struct vediag_test_device vediag_zigbee = {
    .type           = VEDIAG_ZIGBEE,
	.flags			= VEDIAG_TEST_MANUAL,
    .name           = "ZIGBEE",
    .cmd            = "zigbee",
    .desc           = "ZIGBEE TEST",
	.default_app	= "zigbee_tools",
	.get_test_app	= zigbee_get_app,
    .test_start     = zigbee_test_start,
	.create_argument= zigbee_create_arg,
    .check_dev      = zigbee_check_dev,
    .test_execute   = zigbee_test_execute,
    .test_complete  = zigbee_test_complete,
};

late_initcall(diag_zigbee_test_register)
{
    return vediag_test_register(&vediag_zigbee);
}
