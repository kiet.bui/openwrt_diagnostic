/*
 * net_iperf_test.c
 *
 *  Created on: Mar 29, 2017
 *      Author: nnguyen
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <ctype.h>
#include <linux/err.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <signal.h>
#include <linux/ioctl.h>
#include <sys/socket.h>
#include <linux/socket.h>
#include <linux/if.h>
#include <linux/sockios.h>
#include <pthread.h>
#include <cmd.h>
#include <vediag_common.h>
#include <vediag_net.h>

#include "net_subs.h"

#define CMD_LEN			1000
#define SUB_CMD_LEN		800
#define ARG_LEN			20
#define LINE_LEN		256
#define TIMEOUT			90
#define KILL_TIMEOUT	50
#define FILE_NAME_LEN	256
#define MBITS_PER_SEC	"m"

#define READ_FD  		0
#define WRITE_FD 		1

static pthread_mutex_t iperf_server_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t iperf_result_mutex = PTHREAD_MUTEX_INITIALIZER;

int iperf_test_execute(struct vediag_test_info *info)
{
	int ret = RETURN_NOERROR;
	int test_done = 0;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct vediag_net_test_info *net_info =
			(struct vediag_net_test_info *) info->data;
	u64 tx_error;
	u64 rx_error;
	char *tmp = NULL;
	static int port = DEFAULT_IPERF_PORT;
	/*clock_t t = clock();*/
	struct timeval start_time, current_time;
	long int t = 0;
	int display_time = 0;

	char cmd[CMD_LEN];
	char tmpcmd[CMD_LEN];
	char iperf_server_cmd[SUB_CMD_LEN];
	char iperf_client_cmd[SUB_CMD_LEN];
	char *token;
	char *saveptr1 = NULL;
	char *arg[ARG_LEN + 1];
	char serverlog[FILE_NAME_LEN];

	pid_t serverpid = 0;
	pid_t clientpid = 0;
	pid_t waitid = 0;
	int status = 0;
	int j = 0, k = 0;
	int reverse = 0;
	int runtime = 0;
	int timeout = 0;
	int sub_timeout = 0;
	int time_passed = 0;
	int killed = 0;

	char *end_line;
	int avg_speed = 0;
	int tmp_avg_speed = 0;
	int fds[2], pipe_created = 0;
	FILE *output;
	end_line = (char *) malloc(LINE_LEN);
	if (end_line == NULL) {
		vediag_err(VEDIAG_NET, net_info->hw_name, "Malloc failed\n");
		return -ENOMEM;
	}

	pthread_mutex_lock(&net_info->iperf_mutex);
	// If configure fail -> Skip other threads
	if (net_info->configured == CONFIGURE_FAILED) {
		vediag_err(VEDIAG_NET, net_info->hw_name, "Configure fail -> Abort\n");
		test_ctrl->error += 1;
		pthread_mutex_unlock(&net_info->iperf_mutex);
		return -EIO;
	}
	// If not configured -> Start ip config
	if (net_info->configured == NOT_CONFIGURED) {
		// Config loopback
		if (net_info->test_mode == LOOPBACK) {
			// Get Iperf Client's IP
			tmp = get_ip_for_hw_name(net_info->hw_name);
			if (likely(tmp != NULL)) {
				safe_strncpy(net_info->ip_addr, tmp, IFNAMSIZ);
			} else {
				test_ctrl->error += 1;
				net_info->configured = CONFIGURE_FAILED;
				pthread_mutex_unlock(&net_info->iperf_mutex);
				return -EIO;
			}

			// Get Iperf Server's IP
			tmp = get_ip_for_hw_name(net_info->server);
			if (likely(tmp != NULL)) {
				safe_strncpy(net_info->lb_ip_addr, tmp, IFNAMSIZ);
			} else {
				test_ctrl->error += 1;
				net_info->configured = CONFIGURE_FAILED;
				pthread_mutex_unlock(&net_info->iperf_mutex);
				return -EIO;
			}

			tmp = vediag_find_device(info->test_type, net_info->server);
			if (likely(tmp != NULL)) {
				net_info->lb_interface_name = strdup(tmp);
				free(tmp);		//According to anh HungNguyen
			} else {
				vediag_err(VEDIAG_NET, net_info->hw_name, "Loopback server %s not found in system\n", net_info->server);
				test_ctrl->error += 1;
				net_info->configured = CONFIGURE_FAILED;
				pthread_mutex_unlock(&net_info->iperf_mutex);
				return -EIO;
			}
			// Config loopback for Iperf Client and Server
			if ((ret = loopback_port_2_port_settings(net_info->interface_name, net_info->ip_addr,
					net_info->lb_interface_name, net_info->lb_ip_addr, net_info->iperf_server)) != 0) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "Config loopback to %s - return: %d\n", net_info->server, ret);
				test_ctrl->error += 1;
				net_info->configured = CONFIGURE_FAILED;
				pthread_mutex_unlock(&net_info->iperf_mutex);
				return -EIO;
			}
		} else {
			// Get Iperf Client's IP
			tmp = polling_address_ready(net_info->interface_name, IP_ADDRESS_POLLING_TIME);
			if (likely(tmp != NULL)) {
				safe_strncpy(net_info->ip_addr, tmp, IFNAMSIZ);
			} else {
				vediag_err(VEDIAG_NET, net_info->hw_name, "get ip address from %s\n", net_info->interface_name);
				test_ctrl->error += 1;
				net_info->configured = CONFIGURE_FAILED;
				pthread_mutex_unlock(&net_info->iperf_mutex);
				return -EIO;
			}

#ifdef CONFIG_USING_ROUTE_TABLE
			// Config remote
			memset(net_info->route_table_name, 0, sizeof(net_info->route_table_name));
			sprintf(net_info->route_table_name, ROUTE_TABLE_FORMAT, net_info->interface_name);
			ret = remote_port_settings(net_info->interface_name, net_info->ip_addr, net_info->default_gateway, net_info->route_table_name);
			if (ret) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "config remote fail");
				test_ctrl->error += 1;
				goto done;
			}
#endif

			// Remote Iperf Server
			sprintf(net_info->iperf_server, "%s%c", net_info->server, '\0');
			memset(cmd, 0, sizeof(cmd));
			sprintf(cmd, "ping -W 5 -c 5 -I %s %s", net_info->interface_name, net_info->iperf_server);
			ret = system(cmd);
			if (ret) {
			vediag_err(VEDIAG_NET, net_info->hw_name, "ping server %s - \"%s\" returns: %d\n", net_info->iperf_server, cmd, ret);
				test_ctrl->error += 1;
				pthread_mutex_unlock(&net_info->iperf_mutex);
				goto done;
			}
		}

		dump_net_test_info(net_info);
		net_info->configured = CONFIGURED;
	}
	pthread_mutex_unlock(&net_info->iperf_mutex);

	// Do Iperf test
    while (!test_done) {
		if (test_ctrl->data_stop != 0) {
			test_done = 1;
			continue;
		}

		// Calculate actual runtime
		runtime = (net_info->time / 2);

	while(1) {
		xinfo(CYAN_COLOR"[%s] IPERF transfer data from %s to %s\n"NONE_COLOR,
				net_info->hw_name, (reverse == 0 ? net_info->hw_name : net_info->server), (reverse == 0 ? net_info->server : net_info->hw_name));
		avg_speed = 0;
		tmp_avg_speed = 0;
		gettimeofday(&start_time, NULL);
		t = start_time.tv_usec;

		// Check ifconfig error
		tx_error = net_check_tx_error(net_info->interface_name);
		rx_error = net_check_rx_error(net_info->interface_name);

		if (net_info->test_mode == LOOPBACK) {
			pthread_mutex_lock(&iperf_server_mutex);
			// Check iperf port availability
			port = check_available_iperf_port(net_info->lb_ip_addr, port);

			// Create and run iperf server command
			memset(iperf_server_cmd, 0, sizeof(iperf_server_cmd));
			sprintf(iperf_server_cmd, "iperf -B %s -s -1 -i %d -p %d", net_info->lb_ip_addr, net_info->interval, port);
			memset(serverlog, 0, sizeof(serverlog));
			sprintf(serverlog, IPERF_SERVER_LOG_FILE_PREFIX"_%s_T%d_%ld.log", net_info->interface_name, port, t);
			memset(cmd, 0, sizeof(cmd));
			sprintf(cmd, "%s >> %s&", iperf_server_cmd, serverlog);
			nettestdebug("[%s] Server Cmd: %s\n", net_info->hw_name, cmd);

			ret = system(cmd);
			if (ret) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "FAIL command %s\n", cmd);
				test_ctrl->error += 1;
				pthread_mutex_unlock(&iperf_server_mutex);
				goto skip;
			}

			// Check if iperf server command is executed
			serverpid = pid_of_command(iperf_server_cmd);
			if (serverpid == 0) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "FAIL command %s\n", cmd);
				test_ctrl->error += 1;
				pthread_mutex_unlock(&iperf_server_mutex);
				goto skip;
			} else if (serverpid < 0) {
				nettestdebug("[%s] Server PID = %s\n", net_info->hw_name, "Unknown");
			} else {
				nettestdebug("[%s] Server PID = %d\n", net_info->hw_name, serverpid);
			}

			// Run Iperf Client
			memset(iperf_client_cmd, 0, sizeof(iperf_client_cmd));
			sprintf(iperf_client_cmd, "iperf -B %s -c %s -i %d -P %d -t %d -p %d -f %s %s -T T%d", net_info->ip_addr, net_info->iperf_server, net_info->interval,
					net_info->iperf_thread, runtime, port, MBITS_PER_SEC, ((reverse == 1) ? "-R" : ""), port);
			port++;
			pthread_mutex_unlock(&iperf_server_mutex);
		} else {
			// Run Iperf Client
			memset(cmd, 0, sizeof(cmd));
			// Run Iperf Client
			memset(iperf_client_cmd, 0, sizeof(iperf_client_cmd));
			sprintf(iperf_client_cmd, "iperf -B %s -c %s -P %d -t %d -f %s %s", net_info->ip_addr, net_info->iperf_server,
					net_info->iperf_thread, runtime, MBITS_PER_SEC, (reverse == 1) ? "-R" : "");
		}

		nettestdebug("[%s] Client Cmd: %s\n", net_info->hw_name, iperf_client_cmd);
		test_ctrl->runtime = net_info->time + (IPERF_SETUP_TIME_OFFSET * info->threads);

		// Clone cmd -> tmpcmd for strtok
		memset(tmpcmd, 0, CMD_LEN);
		strncpy(tmpcmd, iperf_client_cmd, CMD_LEN);
		// Malloc arg pointers
		for (j = 0; j < ARG_LEN; j++) {
			arg[j] = (char *) malloc(CMD_LEN);
			memset(arg[j], 0, CMD_LEN);
			if (arg[j] == NULL) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "Malloc failed\n");
				for (k = 0; k < j; k++) {
					free(arg[k]);
				}
				return -ENOMEM;
			}
		}
		// Seperate cmd to args
		for (j = 0, tmp = tmpcmd; ; j++, tmp = NULL) {
			token = strtok_r(tmp, " ", &saveptr1);
			if (token == NULL) {
				break;
			}
			strcpy(arg[j], token);
		}
		tmp = arg[j];
		arg[j] = NULL;

		// Create pipe to capture iperf output to log
		if (pipe(fds) < 0) {
			vediag_err(VEDIAG_NET, net_info->hw_name,
					"Create pipe failed: %s - Cannot capture iperf's output to log\n",
					strerror(errno));
			pipe_created = 0;
		} else {
			xdebug("[%s] Create pipe successful\n", net_info->hw_name);
			pipe_created = 1;
		}

		clientpid = fork();

		if (clientpid < 0) {
			vediag_err(VEDIAG_NET, net_info->hw_name, "Fork client command error\n");
			ret = RETURN_SYSTEM_ERROR;
			goto free_arg;
		}

		// Client cmd execution
		if (clientpid == 0) {
			if (pipe_created) {
				// redirect stdout & stderr to parent pipe
				dup2(fds[WRITE_FD], STDOUT_FILENO);
				dup2(fds[WRITE_FD], STDERR_FILENO);
				// close not-required pipes by child
				close(fds[READ_FD]);
				close(fds[WRITE_FD]);
			}
			// Execute command
			nettestdebug("[%s] Execute client command %s\n", net_info->hw_name, iperf_client_cmd);
			ret = execvp(arg[0], arg);
			nettestdebug("[%s] Fail to execute cmd: %s\n", net_info->hw_name, iperf_client_cmd);
			exit(ret);
		}

		arg[j] = tmp;

		// Polling client command's status and kill when timeout
		nettestdebug("[%s] Client PID = %d\n", net_info->hw_name, clientpid);
		if (clientpid > 0) {
			if (pipe_created) {
				// close not required pipe by parent
				close(fds[WRITE_FD]);
			}
			timeout = test_ctrl->runtime / 2 + TIMEOUT;
			time_passed = 0;
			killed = 0;
			nettestdebug("[%s] Timeout limit: %d seconds\n", net_info->hw_name, timeout);
			gettimeofday(&start_time, NULL);
			while (time_passed <= timeout) {
				if (info->stat->should_stop && !killed) {
					if (net_info->test_mode == LOOPBACK) {
						kill(serverpid, SIGINT);
					}
					kill(clientpid, SIGINT);
					// Because client command is child of diagwrt, need to clean up <defunct> by waitpid
					sub_timeout = KILL_TIMEOUT;
					while (sub_timeout >= 0 && waitpid(clientpid, &status, WNOHANG) != -1) {
						usleep(100000);
						sub_timeout--;
					}
					killed = 1;
				}
				waitid = waitpid(clientpid, &status, WNOHANG);
				if (status || (waitid == clientpid)) {
					nettestdebug("[%s] BREAK (Client[%d] waitid: %d, status: %d)\n", net_info->hw_name, clientpid, waitid, status);
					break;
				}
				gettimeofday(&current_time, NULL);
				if((current_time.tv_sec - start_time.tv_sec) >= 1) {
					time_passed += current_time.tv_sec - start_time.tv_sec;
					display_time = 1;
					gettimeofday(&start_time, NULL);
				}
				if ((time_passed % net_info->interval) == 0 && time_passed > 0 && display_time == 1){
					xinfo("[%s] Client[%d] IPERF transferred %d seconds\n", net_info->hw_name, clientpid, time_passed);
					display_time = 0;
				}
			}


			pthread_mutex_lock(&iperf_result_mutex);
			// Saving log file
			if (pipe_created) {
				// Open iperf output fd
				output = fdopen(fds[READ_FD], "r");
				if (!output) {
					vediag_err(VEDIAG_NET, net_info->hw_name, "[%d] Cannot open log stream\n", clientpid);
				} else {
					tmp_avg_speed = get_iperf_result(output, runtime, net_info->iperf_thread, (reverse==1) ? RECEIVE : TRANSMIT);
					if (tmp_avg_speed > 0) {
						xinfo("[%s] Average Speed: %s%d Mbits/s%s\n",
							net_info->hw_name, CYAN_COLOR, tmp_avg_speed, NONE_COLOR);
					}
					close(fds[READ_FD]);
				}
				test_ctrl->tested_size += ((u64)(tmp_avg_speed / 8)) * 1024 * 1024 * runtime;
			}
			pthread_mutex_unlock(&iperf_result_mutex);

			// Check timeout -> check child status -> check speed (if exist in .ini) -> reverse transfer if success
			if (time_passed > timeout) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "Timeout on client pid [%d] <-> server pid [%d]\n", clientpid, serverpid);
				if (!killed) {
					if (net_info->test_mode == LOOPBACK) {
						xinfo("[%s] Timeout interrupt server pid [%d]\n", net_info->hw_name, serverpid);
						kill(serverpid, SIGINT);
					}
					kill(clientpid, SIGINT);
					xinfo("[%s] Timeout kill client pid [%d]\n", net_info->hw_name, clientpid);
					// Because client command is child of diagwrt, need to clean up <defunct> by waitpid
					sub_timeout = KILL_TIMEOUT;
					while (sub_timeout >= 0 && waitpid(clientpid, &status, WNOHANG) != -1) {
						usleep(100000);
						sub_timeout--;
					}
					killed = 1;
				}
				ret = RETURN_TIMEOUT;
				pthread_mutex_lock(&net_info->iperf_mutex);
				net_info->threads--;
				if (net_info->threads == 0) {
					net_info->result += ret;
					net_info->all_done = 1;
				}
				pthread_mutex_unlock(&net_info->iperf_mutex);
			} else if (status != 0) {
				if (info->stat->should_stop) {
					ret = RETURN_USER_STOP_REQUEST;
				} else {
					if (WIFEXITED(status)) {
						vediag_err(VEDIAG_NET, net_info->hw_name,
								"Error occurred when execute client [%d] command %s -> status: %d (child exited)\n",
								clientpid, iperf_client_cmd, WEXITSTATUS(status));
					} else if (WIFSIGNALED(status)) {
						vediag_err(VEDIAG_NET, net_info->hw_name,
								"Error occurred when execute client [%d] command %s -> status: %d (child terminated by signal %d)\n",
								clientpid, iperf_client_cmd, status, WTERMSIG(status));
					} else if (WIFSTOPPED(status)) {
						vediag_err(VEDIAG_NET, net_info->hw_name,
								"Error occurred when execute client [%d] command %s -> status: %d (child stopped by signal %d)\n",
								clientpid, iperf_client_cmd, status, WTERMSIG(status));
					} else {
						vediag_err(VEDIAG_NET, net_info->hw_name,
								"Error occurred when execute client [%d] command %s -> status: %d (child terminated incorrectly)\n",
								clientpid, iperf_client_cmd, WTERMSIG(status));
					}
					if (!killed) {
						if (net_info->test_mode == LOOPBACK) {
							kill(serverpid, SIGINT);
							xinfo("[%s] Interrupt server pid [%d]\n", net_info->hw_name, serverpid);
						}
						killed = 1;
					}
					ret = RETURN_RUNTIME_ERROR;
				}
				pthread_mutex_lock(&net_info->iperf_mutex);
				net_info->threads--;
				if (net_info->threads == 0) {
					net_info->result += ret;
					net_info->all_done = 1;
				}
				pthread_mutex_unlock(&net_info->iperf_mutex);
			} else {
				pthread_mutex_lock(&net_info->iperf_mutex);
				net_info->threads--;
				// Calculate overall speed
				if (net_info->threads == 0) {
					if (reverse == 0) {
						avg_speed = (test_ctrl->tested_size * 8 / 1024 / 1024) / (net_info->time / 2);
					} else if (reverse == 1) {
						avg_speed = (test_ctrl->tested_size * 8 / 1024 / 1024) / (net_info->time);
					}
					xinfo(YELLOW_COLOR"[%s] Total Average Speed: %d Mbits/s\n"NONE_COLOR, net_info->hw_name, avg_speed);
					if (net_info->test_performance) {
						if (net_info->min_speed > 0 && avg_speed > 0 && avg_speed < net_info->min_speed) {
							vediag_err(VEDIAG_NET, net_info->hw_name,
									"Average transfer speed %d Mbits/s slower than minimum accepted speed (%d Mbits/s)\n",
									avg_speed, net_info->min_speed);
							ret = RETURN_SLOW_SPEED;
						} else {
							ret = RETURN_NOERROR;
						}
					} else {
						ret = RETURN_NOERROR;
					}
					net_info->result += ret;
					net_info->all_done = 1;
				}
				pthread_mutex_unlock(&net_info->iperf_mutex);
			}

			// Wait if not all done
			while (!net_info->all_done) {
				usleep(100000);
			}

			// Reset "all_done" -> 0
			pthread_mutex_lock(&net_info->iperf_mutex);
			net_info->threads++;
			if (net_info->threads == info->threads) {
				net_info->all_done = 0;
			}
			pthread_mutex_unlock(&net_info->iperf_mutex);

			while (net_info->all_done) {
				usleep(100000);
			}
		} else {
			vediag_err(VEDIAG_NET, net_info->hw_name, "Client cmd not executed\n");
			ret = RETURN_INVALID_ARGUMENTS;
		}

		// Remove iperf server log file
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "rm -f %s", serverlog);
		system(cmd);

		// Check result of all threads
		if (net_info->result) {
			switch (ret) {
				case RETURN_NOERROR:
					vediag_err(VEDIAG_NET, net_info->hw_name, "Abort\n");
					break;
				case RETURN_SLOW_SPEED:
					vediag_err(VEDIAG_NET, net_info->hw_name, "FAIL due to slow iperf avarage speed\n");
					break;
				case RETURN_USER_STOP_REQUEST:
					xinfo("[%s] Stopped by user request\n", net_info->hw_name);
					ret = 0;
					break;
				default:
					vediag_err(VEDIAG_NET, net_info->hw_name, "FAIL command %s -> return %d\n", iperf_client_cmd, ret);
					break;
			}
			test_ctrl->error += 1;
			break;
		} else {
			xinfo(GREEN_COLOR"[%s] DONE IPERF transfer data from %s to %s\n"NONE_COLOR, net_info->hw_name,
					(reverse == 0 ? net_info->hw_name : net_info->server), (reverse == 0 ? net_info->server : net_info->hw_name));
			if ((tx_error = (net_check_tx_error(net_info->interface_name) - tx_error)) > 0) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "tx %lld error(s)\n", tx_error);
			}
			if ((rx_error = (net_check_rx_error(net_info->interface_name) - rx_error)) > 0) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "rx %lld error(s)\n", rx_error);
			}
			if ((tx_error + rx_error) > 0) {
				ret = RETURN_TXRX_ERROR;
				test_ctrl->error += 1;
				break;
			}
			reverse++;
			if (reverse > 1) {
				break;
			}
		}
	}
free_arg:
		for (j = 0; j < ARG_LEN; j++) {
			if (arg[j]) {
				free(arg[j]);
			}
		}
		if (end_line)
			free(end_line);

		if (clientpid < 0 && pipe_created == 1) {
			close(fds[READ_FD]);
			close(fds[WRITE_FD]);
		}
skip:
		if (test_ctrl->error) {
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		} else {
			test_done = 1;
		}
    }

done:
#ifdef CONFIG_USING_ROUTE_TABLE
	if (net_info->test_mode == REMOTE) {
		delete_ip_rule(net_info->route_table_name);
	}
#endif
	return ret;
}
