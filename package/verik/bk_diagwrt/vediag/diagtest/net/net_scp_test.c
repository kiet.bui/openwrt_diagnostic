/*
 * net_scp_test.c
 *
 *  Created on: Mar 29, 2017
 *      Author: nnguyen
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <linux/err.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <linux/ioctl.h>
#include <sys/socket.h>
#include <linux/socket.h>
#include <linux/if.h>
#include <linux/sockios.h>
#include <cmd.h>
#include <vediag_common.h>
#include <vediag_net.h>

#include "net_subs.h"

#include <listbuff.h>

#define SCP_CONNECTION_TIMEOUT		60
#define SSH_CONNECTION_TIMEOUT		10
#define SCP_SERVER_ALIVE_INTERVAL	60
#define SCP_SERVER_ALIVE_COUNT_MAX	10
#define PUT_PREFIX		"diagwrt_put_"
#define GET_PREFIX		"diagwrt_get_"
#define CMD_LEN			1000
#define LEN				256
#define SSHPASS_PATH	"/etc/diagwrt"
#define DUMMY_SSHPASS_PATH_FILE "sshpass_path"

int scp_test_execute(struct vediag_test_info *info)
{
	int ret = RETURN_NOERROR;
	unsigned long int file_size;
	char put_file[LEN];
	char get_file[LEN];
	char return_file[LEN];
	char sshpass_path[LEN];
	char cmd[CMD_LEN];
	int test_done = 0;
	char *tmp_buf;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct vediag_net_test_info *net_info =
			(struct vediag_net_test_info *) info->data;
	u64 tx_error;
	u64 rx_error;
	char scp_server[IFNAMSIZ];

	clock_t t = clock();

	file_size = net_info->file_size;

	memset(sshpass_path, 0, sizeof(sshpass_path));
	if (!system("sshpass -V > "DUMMY_SSHPASS_PATH_FILE)) {
		sprintf(sshpass_path, "sshpass");
	} else if (!system("./sshpass -V > "DUMMY_SSHPASS_PATH_FILE)) {
		sprintf(sshpass_path, "./sshpass");
	} else if (!system(SSHPASS_PATH"/sshpass -V > "DUMMY_SSHPASS_PATH_FILE)) {
		sprintf(sshpass_path, "%s/sshpass", SSHPASS_PATH);
	} else {
		vediag_err(VEDIAG_NET, net_info->hw_name, "sshpass app is not installed\n");
		test_ctrl->error += 1;
		return RETURN_NO_APP;
	}

	memset(scp_server, 0, sizeof(scp_server));

	/**
	 * Setup loopback mode
	 */
	if (net_info->test_mode == LOOPBACK) {
		tmp_buf = get_ip_for_hw_name(net_info->hw_name);
		if (likely(tmp_buf != NULL)) {
			safe_strncpy(net_info->ip_addr, tmp_buf, IFNAMSIZ);
		} else {
			test_ctrl->error += 1;
			return -EIO;
		}
		tmp_buf = get_ip_for_hw_name(net_info->server);
		if (likely(tmp_buf != NULL)) {
			safe_strncpy(net_info->lb_ip_addr, tmp_buf, IFNAMSIZ);
		} else {
			test_ctrl->error += 1;
			return -EIO;
		}

		tmp_buf = vediag_find_device(info->test_type, net_info->server);
		if (likely(tmp_buf != NULL)) {
			net_info->lb_interface_name = strdup(tmp_buf);
			free(tmp_buf);
		} else {
			vediag_err(VEDIAG_NET, net_info->hw_name, "Loopback server %s not found in system\n", net_info->server);
			test_ctrl->error += 1;
			return -EIO;
		}
		if ((ret = loopback_port_2_port_settings(net_info->interface_name, net_info->ip_addr,
				net_info->lb_interface_name, net_info->lb_ip_addr, scp_server)) != 0) {
			vediag_err(VEDIAG_NET, net_info->hw_name, "Config loopback to %s - return: %d\n", net_info->server, ret);
			test_ctrl->error += 1;
			return -EIO;
		}
	} else {
		/* Check Ip Address of Device Name */
		tmp_buf = polling_address_ready(net_info->interface_name, IP_ADDRESS_POLLING_TIME);
		if (likely(tmp_buf != NULL)) {
			safe_strncpy(net_info->ip_addr, tmp_buf, IFNAMSIZ);
		} else {
			vediag_err(VEDIAG_NET, net_info->hw_name, "get ip address from %s failed\n", net_info->interface_name);
			memset(cmd, 0, sizeof(cmd));
			sprintf(cmd, "ifconfig %s", net_info->interface_name);
			xinfo("%s\n", cmd);
			system(cmd);
			test_ctrl->error += 1;
			return -EIO;
		}

#ifdef CONFIG_USING_ROUTE_TABLE
		// Config remote
		memset(net_info->route_table_name, 0, sizeof(net_info->route_table_name));
		sprintf(net_info->route_table_name, ROUTE_TABLE_FORMAT, net_info->interface_name);
		ret = remote_port_settings(net_info->interface_name, net_info->ip_addr, net_info->default_gateway, net_info->route_table_name);
		if (ret) {
			vediag_err(VEDIAG_NET, net_info->hw_name, "config remote fail");
			test_ctrl->error += 1;
			goto done;
		}
#endif

		// Ping check first
		sprintf(scp_server, "%s%c", net_info->server, '\0');
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "ping -W 60 -c 20 -I %s %s", net_info->interface_name, scp_server);
		ret = system(cmd);
		if (ret) {
			vediag_err(VEDIAG_NET, net_info->hw_name, "ping server %s - \"%s\" returns: %d\n", scp_server, cmd, ret);
			test_ctrl->error += 1;
			goto done;
		}
	}


	dump_net_test_info(net_info);

	// Format 'put' and 'get' file name
	memset(put_file, 0, sizeof(put_file));
	memset(get_file, 0, sizeof(get_file));
	int len = strlen(net_info->folder_name);
	if (net_info->folder_name[len - 1] == '/') {
		sprintf(put_file, "%s"PUT_PREFIX"%s_%s_%ld", net_info->folder_name, net_info->ip_addr, net_info->interface_name, t);
		sprintf(get_file, "%s"GET_PREFIX"%s_%s_%ld", net_info->folder_name, net_info->ip_addr, net_info->interface_name, t);
	} else {
		sprintf(put_file, "%s/"PUT_PREFIX"%s_%s_%ld", net_info->folder_name, net_info->ip_addr, net_info->interface_name, t);
		sprintf(get_file, "%s/"GET_PREFIX"%s_%s_%ld", net_info->folder_name, net_info->ip_addr, net_info->interface_name, t);
	}
	memset(return_file, 0, sizeof(return_file));
	sprintf(return_file, "%s%s", put_file, RETURN_FILE_SUFFIX);

	// Create new file to "put"
	ret = create_random_file(put_file, file_size);
	if (ret != 0) {
		vediag_err(VEDIAG_NET, net_info->hw_name, "Create file - return: %d\n", ret);
		test_ctrl->error += 1;
		goto done;
	}

    while (!test_done) {
		if (test_ctrl->data_stop != 0) {
			test_done = 1;
			continue;
		}

		// Check ifconfig error
		tx_error = net_check_tx_error(net_info->interface_name);
		rx_error = net_check_rx_error(net_info->interface_name);

		/**
		 * Put file
		 */
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd,
				"%s -p %s scp -o ServerAliveInterval=%d -o ServerAliveCountMax=%d -o BindAddress=%s -o ConnectTimeout=%d -o StrictHostKeyChecking=no %s %s@%s:%s",
				sshpass_path, net_info->pass, SCP_SERVER_ALIVE_INTERVAL, SCP_SERVER_ALIVE_COUNT_MAX, net_info->ip_addr,
				SCP_CONNECTION_TIMEOUT, put_file, net_info->user, scp_server,
				get_file);
		nettestdebug("[%s] Put file: %s\n", net_info->hw_name, cmd);
		ret = system(cmd);
		if (WIFEXITED(ret)) {
			ret = WEXITSTATUS(ret);
		}
		nettestdebug("[%s] Done put file = %d\n", net_info->hw_name, ret);

		/**
		 * Get file
		 */
		if (ret == 0) {
			memset(cmd, 0, sizeof(cmd));
			sprintf(cmd,
					"%s -p %s scp -o ServerAliveInterval=%d -o ServerAliveCountMax=%d -o BindAddress=%s -o ConnectTimeout=%d -o StrictHostKeyChecking=no %s@%s:%s %s",
					sshpass_path, net_info->pass, SCP_SERVER_ALIVE_INTERVAL, SCP_SERVER_ALIVE_COUNT_MAX, net_info->ip_addr,
					SCP_CONNECTION_TIMEOUT, net_info->user, scp_server,
					get_file, return_file);
			nettestdebug("[%s] Get file: %s\n", net_info->hw_name, cmd);
			ret = system(cmd);
			if (WIFEXITED(ret)) {
				ret = WEXITSTATUS(ret);
			}
			nettestdebug("[%s] Done get file = %d\n", net_info->hw_name, ret);

			/* Verify */
			if (ret == 0) {
				if ((ret = compare_test_files(put_file, &return_file[0], file_size)) != 0) {
					vediag_err(VEDIAG_NET, net_info->hw_name, "Compare files - return: %d\n", ret);
					ret = RETURN_COMPARE_FAIL;
					test_ctrl->error += 1;
				}
				if ((tx_error = (net_check_tx_error(net_info->interface_name) - tx_error)) > 0) {
					vediag_err(VEDIAG_NET, net_info->hw_name, "tx %lld error(s)\n", tx_error);
				}
				if ((rx_error = (net_check_rx_error(net_info->interface_name) - rx_error)) > 0) {
					vediag_err(VEDIAG_NET, net_info->hw_name, "rx %lld error(s)\n", rx_error);
				}
				if ((tx_error + rx_error) > 0) {
					ret = RETURN_TXRX_ERROR;
					test_ctrl->error += 1;
					break;
				}
			} else {		// FAIL get file
				vediag_err(VEDIAG_NET, net_info->hw_name, "Get file - return: %d\n", ret);
				test_ctrl->error += 1;
			}

			// Delete returned file
			delete_existing_file(return_file);
		} else {		// FAIL put file
			vediag_err(VEDIAG_NET, net_info->hw_name, "Put file - return: %d\n", ret);
			test_ctrl->error += 1;
		}

		// Delete file on server
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "%s -p %s ssh -o ConnectTimeout=%d %s@%s rm -f %s",
				sshpass_path, net_info->pass, SSH_CONNECTION_TIMEOUT, net_info->user, scp_server, get_file);
		nettestdebug("[%s] Delete %s on server %s\n", net_info->hw_name, get_file, scp_server);
		system(cmd);

		if (test_ctrl->error) {
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		} else {
			/* Update tested size, offset and decrease size test */
			test_ctrl->tested_size += 2 * file_size;
			xinfo("[%s] SCP Read/Write %s\n", net_info->hw_name, print_size_buf(test_ctrl->tested_size, " "));
			sleep(0);
		}
	}

	// Delete put file
	delete_existing_file(put_file);

done:
#ifdef CONFIG_USING_ROUTE_TABLE
	if (net_info->test_mode == REMOTE) {
		delete_ip_rule(net_info->route_table_name);
	}
#endif
	return ret;
}

