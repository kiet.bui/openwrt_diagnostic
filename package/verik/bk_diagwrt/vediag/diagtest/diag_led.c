
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmd.h>
#include <string.h>
#include <linux/err.h>
#include <common.h>
#include <vediag_common.h>
#include <pthread.h>
#include <time.h>
#include <vediag/led.h>
#include <time_util.h>
#include <log.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>
#include <string_plus.h>

static LIST_HEAD(testlist);
static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t test_serialize_mutex = PTHREAD_MUTEX_INITIALIZER;

static uint32_t test_count;

struct led_test_list {
	struct list_head l;
	char* devname;
};

struct led_test_info {
	int brightness;
	int max_brightness;
	char trigger[40];
	int cycle;
	u32 interval; /* ms */
};

#define LED_DEFAULT_RUNNING_TIME	10 /* sec */
#define LED_MIN_RUNNING_TIME		5 /* sec */

#define LED_MIN_RUNNING_CYCLE		2
#define LED_DEF_RUNNING_CYCLE		5
#define LED_MIN_RUNNING_INTERVAL	100 /* msec */
#define LED_DEF_RUNNING_INTERVAL	500 /* msec */

static int led_get_app(char *app)
{
	xinfo("app='%s'\n", app);

	if (app && !strcmp(app, "ring"))
		return LED_RING_TEST;
	else
		return LED_BLINK_TEST;
}

static int led_create_arg(struct list_head *arg_list)
{
	vediag_set_argument("brightness", "1", arg_list);
	vediag_set_argument("max_brightness", "255", arg_list);
	vediag_set_argument("trigger", "timer", arg_list);
	vediag_set_argument("time", "10", arg_list);
	vediag_set_argument("cycle", "3", arg_list);
	vediag_set_argument("interval", "500", arg_list);

	return 0;
}

static int led_check_dev(struct vediag_test_info *info,
		struct vediag_test_info *dev_info, char **error)
{
	char name[8];
	struct led_test_info *test_arg;
	int dev;
	char *tmp;

	if (IS_ERR_OR_NULL((void *) info)) {
		vediag_err(VEDIAG_LED, NULL, "%s(): info NULL\n", __func__);
		return 0;
	}

	if (IS_ERR_OR_NULL((void *) dev_info)) {
		vediag_err(VEDIAG_LED, NULL, "%s(): dev_info NULL\n", __func__);
		return 0;
	}

	/*xmanager will change 'dev_mask' to device index (count from 1)*/
	dev = dev_info->dev_mask;

	memset(name, 0, sizeof(name));

	test_arg = (struct led_test_info *) malloc(sizeof(struct led_test_info));
	if (IS_ERR_OR_NULL(test_arg)) {
		vediag_err(VEDIAG_LED, dev_info->dbg_info, "Allocate memory failed\n");
		*error = strdup("Out of memory");
		return 0;
	}
	memset(test_arg, 0, sizeof(struct led_test_info));

	if (!dev_info->physical_dev) {
		char section[64] = { 0 };
		snprintf(section, 63, "%s%d", get_section_type(dev_info->test_type),
				dev_info->dev_mask);
		*error = strdup("Parse physical failed");
		vediag_err(VEDIAG_LED, dev_info->dbg_info,
				"Parse physical failed, section '%s'\n", section);
		return 0;
	}

	tmp = vediag_get_argument(dev_info, "brightness");
	if (!tmp) {
		test_arg->brightness = 0;
	} else {
		if (strlen(tmp)) {
			test_arg->brightness = (uint32_t) strtoul(tmp, NULL, 0);
		} else {
			test_arg->brightness = 0;
		}
		/* Use 1 or 0 */
		test_arg->brightness = !!test_arg->brightness;

		free(tmp);
	}

	tmp = vediag_get_argument(dev_info, "max_brightness");
	if (!tmp) {
		test_arg->max_brightness = 0;
	} else {
		if (strlen(tmp)) {
			test_arg->max_brightness = (uint32_t) strtoul(tmp, NULL, 0);
		} else {
			test_arg->max_brightness = 0;
		}
		/* Limit max 255 */
		test_arg->max_brightness = (test_arg->max_brightness > 255) ? 255 : test_arg->max_brightness;

		free(tmp);
	}

	memset(test_arg->trigger, 0, sizeof(test_arg->trigger));
	tmp = vediag_get_argument(dev_info, "trigger");
	if (tmp != NULL) {
		strncpy(test_arg->trigger, tmp, min(sizeof(test_arg->trigger), strlen(tmp)));
		free(tmp);
	}

	test_arg->cycle = LED_DEF_RUNNING_CYCLE;
	tmp = vediag_get_argument(dev_info, "cycle");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->cycle = (uint32_t) strtoul(tmp, NULL, 0);
			if(test_arg->cycle < LED_MIN_RUNNING_CYCLE)
				test_arg->cycle = LED_MIN_RUNNING_CYCLE;
		}
		free(tmp);
	}

	test_arg->interval = LED_DEF_RUNNING_INTERVAL;
	tmp = vediag_get_argument(dev_info, "interval");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->interval = (uint32_t) strtoul(tmp, NULL, 0);
			if(test_arg->interval < LED_MIN_RUNNING_INTERVAL)
				test_arg->interval = LED_MIN_RUNNING_INTERVAL;
		}
		free(tmp);
	}

#if 0
	if (info->runtime > 0) {
		dev_info->runtime = info->runtime;
	} else {
		tmp = vediag_get_argument(dev_info, "time");
		if (!tmp) {
			dev_info->runtime = LED_DEFAULT_RUNNING_TIME;
		} else {
			if (strlen(tmp)) {
				dev_info->runtime = (int) strtoul(tmp, NULL, 10);
				if (dev_info->runtime == 0) {
					dev_info->runtime = LED_DEFAULT_RUNNING_TIME;
					vediag_info(VEDIAG_PCIE, dev_info->dbg_info,
							"runtime = 0, set to default %ds\n",
							dev_info->runtime);
				}
				if (dev_info->runtime < LED_MIN_RUNNING_TIME) {
					dev_info->runtime = LED_MIN_RUNNING_TIME;
					vediag_info(VEDIAG_PCIE, dev_info->dbg_info,
							"runtime < Min, set to min %ds\n",
							dev_info->runtime);
				}
			} else {
				dev_info->runtime = LED_DEFAULT_RUNNING_TIME;
			}
			free(tmp);
		}
	}
#endif

	dev_info->data = (void *) test_arg;
	dev_info->threads = info->threads;
	dev_info->total_size = test_arg->cycle;
	dev_info->dev_mask = 0;

	xinfo("[%s] Test arguments:\n", dev_info->device);
	xinfo("    test_app     	= %d\n", dev_info->test_app);
	xinfo("    brightness    	= %d\n", test_arg->brightness);
	xinfo("    max_brightness  	= %d\n", test_arg->max_brightness);
	xinfo("    trigger  		= '%s'\n", test_arg->trigger);
	xinfo("    time     		= %d sec\n", dev_info->runtime);
	xinfo("    cycle     		= %d\n", test_arg->cycle);
	xinfo("    interval     	= %d msec\n\n", test_arg->interval);

	struct led_test_list *new = malloc(sizeof(struct led_test_list));
	if (!new){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	new->devname = malloc(32);
	if (!new->devname){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	list_add_tail(&new->l, &testlist);

	pthread_mutex_lock(&_mutex);
	test_count++;
	pthread_mutex_unlock(&_mutex);

    return 1;
}

static int led_test_start(int argc, char *argv[], void * _data)
{
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;

	xdebug("%s Entry\n", __func__);

	//info->total_size = 0xffffffff;

	/* Select test device name */
	info->dev_mask = 0xffffffff;	//no input
	if ((arp = vediag_getopt(argc, argv, "-d")) > 0) {
		if (isdigit(argv[arp + 1][0]))
			info->dev_mask =  strtoul(argv[arp + 1], NULL, 0);
	}

	if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
		info->cfg_file = malloc(strlen(argv[arp + 1]));
		strcpy(info->cfg_file, argv[arp + 1]);
	}

	info->threads = 1;

	pthread_mutex_lock(&_mutex);
	test_count = 0;
	pthread_mutex_unlock(&_mutex);

	return sizeof(struct vediag_test_info);
}

static void *led_test_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct led_test_info *test_arg = (struct led_test_info *)info->data;
	int test_done = 0, ret = 0;
	u32 cycle_count = 0;
	struct led_test test = {0};
	extern int keyinput_yes;
	extern int keyinput_no;

	keyinput_yes = 0;
	keyinput_no = 0;
	
	test.name = info->device;
	test.brightness = test_arg->brightness;
	test.max_brightness= test_arg->max_brightness;
	test.trigger = &test_arg->trigger[0];
	test.cycle = test_arg->cycle;
	test.interval = test_arg->interval;

	xinfo("%s: wait\n", info->device);
	pthread_mutex_lock(&test_serialize_mutex);
	xinfo("%s: run\n", info->device);

	while (!test_done) {
		if (test_ctrl->data_stop) {
			test_done = 1;
			continue;
		}

		if(info->test_app == LED_RING_TEST) {
			vediag_info(VEDIAG_LED, info->dbg_info, "Ring Blinking...\n");
			test_ctrl->error = led_ring_test(&test, &cycle_count);
		} else {
			vediag_info(VEDIAG_LED, info->dbg_info, "Blinking...\n");
			test_ctrl->error = led_blink_test(&test, &cycle_count);
		}

		if(test_ctrl->error == 0) {
			test_ctrl->tested_size = cycle_count;

			ret = check_key_result(VEDIAG_LED, test.name, 1, 1);
			if(ret == 1) {
				info->total_size = test_ctrl->tested_size;
				test_done = 1;
				break;
			} else if(ret == 2) {
				test_done = 1;
				test_ctrl->error = 1;
				break;
			}
		}

		if (test_ctrl->error) {
			vediag_err(VEDIAG_LED, info->dbg_info, "LED failed\n");
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		}

		/* Check to complete test */
		if(test_ctrl->tested_size >= info->total_size) {
			test_done = 1;
			test_ctrl->error = 1;
		}

		sleep(0);
	}

	test_ctrl->data_running--;
	pthread_exit(NULL);
}

static int led_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct led_test_info *test_arg = (struct led_test_info *) (info->data);
	struct led_test_list *e;
	struct list_head *pos, *q;

	pthread_mutex_lock(&_mutex);
	test_count--;
	pthread_mutex_unlock(&_mutex);

	if (test_count == 0) {
		list_for_each_entry(e, &testlist, l) {
			if (e->devname) {
				free(e->devname);
			}
		}

		list_for_each_safe(pos, q, &testlist) {
			e = list_entry(pos, struct led_test_list, l);
			list_del(pos);
			free(e);
		}
	}

	if (test_arg)
		free(test_arg);

	xinfo("%s: done\n", info->device);
	pthread_mutex_unlock(&test_serialize_mutex);

	return 0;
}

struct vediag_test_device vediag_led = {
    .type           = VEDIAG_LED,
	.flags			= VEDIAG_TEST_MANUAL,
    .name           = "LED",
    .cmd            = "led",
    .desc           = "LED TEST",
	.default_app	= "blink",
	.get_test_app	= led_get_app,
    .test_start     = led_test_start,
	.create_argument= led_create_arg,
    .check_dev      = led_check_dev,
    .test_execute   = led_test_execute,
    .test_complete  = led_test_complete,
};

late_initcall(diag_led_test_register)
{
    return vediag_test_register(&vediag_led);
}
