
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmd.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include <time.h>
#include <vediag/i2c.h>
#include <time_util.h>
#include <log.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>
#include <string_plus.h>

static LIST_HEAD(testlist);
static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
static uint32_t test_count;

struct i2c_test_list {
	struct list_head l;
	char* devname;
};

struct i2c_test_info {
	unsigned int offset;
	unsigned int length;
	struct i2c_test i2ct;
};

static int i2c_get_app(char *app)
{
	xinfo("app='%s'\n", app);

	if (app && !strcmp(app, "readid"))
		return I2C_READID_TEST;
	else if (app && !strcmp(app, "eeprom"))
		return I2C_EEPROM_TEST;
	else if (app && !strcmp(app, "temperature"))
		return I2C_TEMPERATURE_TEST;
	else if (app && !strcmp(app, "rtc"))
		return I2C_RTC_TEST;
	else
		return I2C_PROBE_TEST;
}

static int i2c_create_arg(struct list_head *arg_list)
{
	vediag_set_argument("gpio_number", "100", arg_list);
	vediag_set_argument("gpio_value", "0", arg_list);
	vediag_set_argument("pagesize", "64", arg_list);
	vediag_set_argument("addrcycle", "2", arg_list);
	vediag_set_argument("backup", "1", arg_list);
	vediag_set_argument("offset", "0x400", arg_list);
	vediag_set_argument("length", "0x100", arg_list);

	return 0;
}

static int i2c_check_dev(struct vediag_test_info *info,
		struct vediag_test_info *dev_info, char **error)
{
	char name[8];
	struct i2c_test_info *test_arg;
	int dev;
	char *tmp;

	if (IS_ERR_OR_NULL((void *) info)) {
		vediag_err(VEDIAG_I2C, NULL, "%s(): info NULL\n", __func__);
		return 0;
	}

	if (IS_ERR_OR_NULL((void *) dev_info)) {
		vediag_err(VEDIAG_I2C, NULL, "%s(): dev_info NULL\n", __func__);
		return 0;
	}

	/*xmanager will change 'dev_mask' to device index (count from 1)*/
	dev = dev_info->dev_mask;

	memset(name, 0, sizeof(name));

	test_arg = (struct i2c_test_info *) malloc(sizeof(struct i2c_test_info));
	if (IS_ERR_OR_NULL(test_arg)) {
		vediag_err(VEDIAG_I2C, dev_info->dbg_info, "Allocate memory failed\n");
		*error = strdup("Out of memory");
		return 0;
	}
	memset(test_arg, 0, sizeof(struct i2c_test_info));

	if (!dev_info->physical_dev) {
		char section[64] = { 0 };
		snprintf(section, 63, "%s%d", get_section_type(dev_info->test_type),
				dev_info->dev_mask);
		*error = strdup("Parse physical failed");
		vediag_err(VEDIAG_I2C, dev_info->dbg_info,
				"Parse physical failed, section '%s'\n", section);
		return 0;
	}

	/* GPIO Param */
	/* GPIO Number */
	test_arg->i2ct.gpio_number = -1;
	tmp = vediag_get_argument(dev_info, "gpio_number");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->i2ct.gpio_number = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	/* GPIO Value */
	test_arg->i2ct.gpio_value = 0;
	tmp = vediag_get_argument(dev_info, "gpio_value");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->i2ct.gpio_value = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}

	/* EEPROM Param */
	/* EEPROM Pagesize */
	test_arg->i2ct.pagesize = 1;
	tmp = vediag_get_argument(dev_info, "pagesize");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->i2ct.pagesize = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	/* EEPROM Address cycle */
	test_arg->i2ct.addrcycle = 1;
	tmp = vediag_get_argument(dev_info, "addrcycle");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->i2ct.addrcycle = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	/* Test backup */
	test_arg->i2ct.backup = 1;
	tmp = vediag_get_argument(dev_info, "backup");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->i2ct.backup = (uint32_t) strtoul(tmp, NULL, 0);
			test_arg->i2ct.backup = !!test_arg->i2ct.backup;
		}
		free(tmp);
	}
	/* Test offset */
	test_arg->offset = 1;
	tmp = vediag_get_argument(dev_info, "offset");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->offset = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	/* Test length */
	test_arg->length = 1;
	tmp = vediag_get_argument(dev_info, "length");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->length = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}

	dev_info->data = (void *) test_arg;
	dev_info->threads = info->threads;
	dev_info->total_size = test_arg->length;
	if((info->test_app == I2C_PROBE_TEST) || \
			(info->test_app == I2C_READID_TEST)) {
		dev_info->total_size = 1;
	}
	dev_info->dev_mask = 0;

	xinfo("[%s] Test arguments:\n", dev_info->device);
	xinfo("    test_app     		= %d\n", dev_info->test_app);
	xinfo("    gpio_number     		= %d\n", test_arg->i2ct.gpio_number);
	xinfo("    gpio_value     		= %d\n", test_arg->i2ct.gpio_value);
	xinfo("    pagesize     		= %d\n", test_arg->i2ct.pagesize);
	xinfo("    addrcycle     		= %d\n", test_arg->i2ct.addrcycle);
	xinfo("    backup				= %d\n", test_arg->i2ct.backup);
	xinfo("    offset     			= %d\n", test_arg->offset);
	xinfo("    length     			= 0x%x\n", test_arg->length);

	struct i2c_test_list *new = malloc(sizeof(struct i2c_test_list));
	if (!new){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	new->devname = malloc(32);
	if (!new->devname){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	list_add_tail(&new->l, &testlist);

	pthread_mutex_lock(&_mutex);
	test_count++;
	pthread_mutex_unlock(&_mutex);

    return 1;
}

static int i2c_test_start(int argc, char *argv[], void * _data)
{
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;
	//struct i2c_test_info *test_arg = (struct i2c_test_info *)info->data;

	xdebug("%s Entry\n", __func__);

	//info->total_size = 0xffffffff;

	/* Select test device name */
	info->dev_mask = 0xffffffff;	//no input
	if ((arp = vediag_getopt(argc, argv, "-d")) > 0) {
		if (isdigit(argv[arp + 1][0]))
			info->dev_mask =  strtoul(argv[arp + 1], NULL, 0);
	}

	if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
		info->cfg_file = malloc(strlen(argv[arp + 1]));
		strcpy(info->cfg_file, argv[arp + 1]);
	}

	info->threads = 1;

	pthread_mutex_lock(&_mutex);
	test_count = 0;
	pthread_mutex_unlock(&_mutex);

	return sizeof(struct vediag_test_info);
}

static void *i2c_test_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct i2c_test_info *test_arg = (struct i2c_test_info *)info->data;
	int test_done = 0, chunk = 0;
	unsigned int size;
	char *ptr = NULL;
	struct i2c_test *i2ct = &test_arg->i2ct;

	/* i2c-0_24 */
	strcat(i2ct->i2cdev, "/dev/");
	strcat(i2ct->i2cdev, info->device);
	/* Remove  "_" */
	i2ct->i2cdev[strcspn(i2ct->i2cdev, "_")] = '\0';
	ptr = get_word_in_string(info->device, "_", 1);
	if(ptr == NULL) {
		test_ctrl->data_running--;
		pthread_exit(NULL);
		return NULL;
	}
	i2ct->i2caddr 		= (unsigned char)strtoul(ptr, NULL, 16);
	free(ptr);

	i2ct->pagesize 		= test_arg->i2ct.pagesize;
	i2ct->addrcycle 	= test_arg->i2ct.addrcycle;
	i2ct->backup 		= test_arg->i2ct.backup;
	i2ct->gpio_number 	= test_arg->i2ct.gpio_number;
	i2ct->gpio_value 	= test_arg->i2ct.gpio_value;
	i2ct->fd 			= -1;

	vediag_info(VEDIAG_I2C, info->dbg_info, "testapp=%d, backup=%d, i2cdev=%s, i2caddr=0x%x pagesize=%d, addrcycle=%d\n",
			info->test_app, i2ct->backup, i2ct->i2cdev, i2ct->i2caddr, i2ct->pagesize, i2ct->addrcycle);

	while (!test_done) {
		if (test_ctrl->data_stop) {
			test_done = 1;
			continue;
		}

		if(info->test_app == I2C_PROBE_TEST) {
			vediag_debug(VEDIAG_I2C, info->dbg_info, "I2C Probe offset=0x%x\n", test_arg->offset);
			test_ctrl->error = i2c_probe_test(i2ct, test_arg->offset);
		} else if(info->test_app == I2C_READID_TEST) {
			vediag_debug(VEDIAG_I2C, info->dbg_info, "I2C Read ID Test offset=0x%x, expect-ID=0x%x\n", test_arg->offset, test_arg->length);
			test_ctrl->error = i2c_readid_test(i2ct, test_arg->offset, test_arg->length);
		} else if(info->test_app == I2C_EEPROM_TEST) {
			/* Check chunk */
			size = test_arg->length - test_ctrl->tested_size;
			chunk = min_t(unsigned int, i2ct->pagesize, size);

			/* Init EEPROM */
			if(i2ct->fd <= 0) {
				vediag_debug(VEDIAG_I2C, info->dbg_info, "EEPROM Init\n");
				test_ctrl->error = eeprom_init(i2ct);
				if (test_ctrl->error < 0) {
					vediag_err(VEDIAG_I2C, info->dbg_info, "eeprom_init() failed\n");
				}
			}
			/* Execute EEPROM Test */
			if(chunk > 0) {
				vediag_debug(VEDIAG_I2C, info->dbg_info, "EEPROM Test offset=0x%08x, chunk=0x%x\n", test_arg->offset, chunk);
				test_ctrl->error = eeprom_test(i2ct, test_arg->offset, chunk);
			}
		}

		if (test_ctrl->error) {
			vediag_err(VEDIAG_I2C, info->dbg_info, "I2C failed\n");
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		} else {
			if(info->test_app == I2C_PROBE_TEST || \
					info->test_app == I2C_READID_TEST) {
				test_ctrl->tested_size += 1;
			} else if(info->test_app == I2C_EEPROM_TEST) {
				test_ctrl->tested_size += chunk;
				test_arg->offset += chunk;
			}
		}
		sleep(0);
	}

	test_ctrl->data_running--;
	pthread_exit(NULL);
}

static int i2c_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct i2c_test_info *test_arg = (struct i2c_test_info *) (info->data);
	struct i2c_test_list *e;
	struct list_head *pos, *q;
	struct i2c_test *i2ct = &test_arg->i2ct;

	pthread_mutex_lock(&_mutex);
	test_count--;
	pthread_mutex_unlock(&_mutex);

	if (test_count == 0) {
		list_for_each_entry(e, &testlist, l) {

			/* Exit EEPROM Test */
			if(info->test_app == I2C_EEPROM_TEST) {
				eeprom_exit(i2ct);
			}

			if (e->devname) {
				free(e->devname);
			}
		}

		list_for_each_safe(pos, q, &testlist) {
			e = list_entry(pos, struct i2c_test_list, l);
			list_del(pos);
			free(e);
		}
	}

	if (test_arg)
		free(test_arg);

	return 0;
}

struct vediag_test_device vediag_i2c = {
    .type           = VEDIAG_I2C,
    .name           = "I2C",
    .cmd            = "i2c",
    .desc           = "I2C TEST",
	.default_app	= "probe",
	.get_test_app	= i2c_get_app,
    .test_start     = i2c_test_start,
	.create_argument= i2c_create_arg,
    .check_dev      = i2c_check_dev,
    .test_execute   = i2c_test_execute,
    .test_complete  = i2c_test_complete,
};

late_initcall(diag_i2c_test_register)
{
    return vediag_test_register(&vediag_i2c);
}
