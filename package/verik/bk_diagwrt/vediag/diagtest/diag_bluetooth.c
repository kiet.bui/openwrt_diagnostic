
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmd.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include <time.h>
#include <vediag/bluetooth.h>
#include <time_util.h>
#include <log.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>
#include <string_plus.h>

static LIST_HEAD(testlist);
static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
static uint32_t test_count;

struct bluetooth_test_list {
	struct list_head l;
	char* devname;
};

struct bluetooth_test_info {
	char bluetooth_scanid[60];		/* BT partner device */
	int bluetooth_scaniter; 		/* BT number of scan and match */
};

static int bluetooth_get_app(char *app)
{
	xinfo("app='%s'\n", app);

	if (app && !strcmp(app, "hcitool"))
		return BLUETOOTH_HCITOOL_TEST;
	else
		return BLUETOOTH_HCITOOL_TEST;
}

static int bluetooth_create_arg(struct list_head *arg_list)
{
	vediag_set_argument("bluetooth_scanid", "00:01:02:03:04:05", arg_list);
	vediag_set_argument("bluetooth_scaniter", "1", arg_list);

	return 0;
}

static int bluetooth_check_dev(struct vediag_test_info *info,
		struct vediag_test_info *dev_info, char **error)
{
	char name[8];
	struct bluetooth_test_info *test_arg;
	int dev;
	char *tmp;

	if (IS_ERR_OR_NULL((void *) info)) {
		vediag_err(VEDIAG_BLUETOOTH, NULL, "%s(): info NULL\n", __func__);
		return 0;
	}

	if (IS_ERR_OR_NULL((void *) dev_info)) {
		vediag_err(VEDIAG_BLUETOOTH, NULL, "%s(): dev_info NULL\n", __func__);
		return 0;
	}

	/*xmanager will change 'dev_mask' to device index (count from 1)*/
	dev = dev_info->dev_mask;

	memset(name, 0, sizeof(name));

	test_arg = (struct bluetooth_test_info *) malloc(sizeof(struct bluetooth_test_info));
	if (IS_ERR_OR_NULL(test_arg)) {
		vediag_err(VEDIAG_BLUETOOTH, dev_info->dbg_info, "Allocate memory failed\n");
		*error = strdup("Out of memory");
		return 0;
	}
	memset(test_arg, 0, sizeof(struct bluetooth_test_info));

	if (!dev_info->physical_dev) {
		char section[64] = { 0 };
		snprintf(section, 63, "%s%d", get_section_type(dev_info->test_type),
				dev_info->dev_mask);
		*error = strdup("Parse physical failed");
		vediag_err(VEDIAG_BLUETOOTH, dev_info->dbg_info,
				"Parse physical failed, section '%s'\n", section);
		return 0;
	}

	dev_info->total_size = 0;
	/* Check file bluetooth.wav */
	tmp = vediag_get_argument(dev_info, "bluetooth_scanid");
	if (tmp != NULL) {
		strncpy(test_arg->bluetooth_scanid, tmp, min(sizeof(test_arg->bluetooth_scanid), strlen(tmp)));
		free(tmp);
	}

	test_arg->bluetooth_scaniter = 1;
	tmp = vediag_get_argument(dev_info, "bluetooth_scaniter");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->bluetooth_scaniter = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	dev_info->total_size = test_arg->bluetooth_scaniter;

	dev_info->data = (void *) test_arg;
	dev_info->threads = info->threads;
	dev_info->dev_mask = 0;

	xinfo("[%s] Test arguments:\n", dev_info->device);
	xinfo("    test_app     		= %d\n", dev_info->test_app);
	xinfo("    bluetooth_scanid		= %s\n", test_arg->bluetooth_scanid);
	xinfo("    bluetooth_scaniter	= %lld\n", dev_info->total_size);

	struct bluetooth_test_list *new = malloc(sizeof(struct bluetooth_test_list));
	if (!new){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	new->devname = malloc(32);
	if (!new->devname){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	list_add_tail(&new->l, &testlist);

	pthread_mutex_lock(&_mutex);
	test_count++;
	pthread_mutex_unlock(&_mutex);

    return 1;
}

static int bluetooth_test_start(int argc, char *argv[], void * _data)
{
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;
	//struct bluetooth_test_info *test_arg = (struct bluetooth_test_info *)info->data;

	xdebug("%s Entry\n", __func__);

	//info->total_size = 0xffffffff;

	/* Select test device name */
	info->dev_mask = 0xffffffff;	//no input
	if ((arp = vediag_getopt(argc, argv, "-d")) > 0) {
		if (isdigit(argv[arp + 1][0]))
			info->dev_mask =  strtoul(argv[arp + 1], NULL, 0);
	}

	if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
		info->cfg_file = malloc(strlen(argv[arp + 1]));
		strcpy(info->cfg_file, argv[arp + 1]);
	}

	info->threads = 1;

	pthread_mutex_lock(&_mutex);
	test_count = 0;
	pthread_mutex_unlock(&_mutex);

	return sizeof(struct vediag_test_info);
}

static int bluetooth_test_hcitool(char *device, struct bluetooth_test_info *test, u32 *size)
{
	int total_bt = 0;
	FILE *devf;
	int line_num = 0, ret = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60], scanname[60], addr[60];

	/* hcitool scan */
	if(test->bluetooth_scanid == NULL) {
		*size += 0;
		return -EINVAL;
	}

	/*
	 * hcitool scan
	 * Scanning ...
	 *         BC:02:4A:E3:8D:02       n/a
	 *         20:C3:8F:81:C8:8C       n/a
	 *         7C:E9:D3:DC:4A:80       HCMR9PRP5D
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "hcitool scan");
	//vediag_debug(VEDIAG_BLUETOOTH, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;
			
			/* Skip "Scanning ..." */
			if(line_num == 1) continue;

			//vediag_debug(VEDIAG_BLUETOOTH, "Line", "%d: '%s'\n", line_num, line);
			memset(addr, 0, sizeof(addr));
			memset(scanname, 0, sizeof(scanname));
			ret = sscanf(line, "    %s    %s", addr, scanname);
			if(ret < 2) {
				vediag_info(VEDIAG_BLUETOOTH, "", "%d: '%s' '%s' ret(%d)\n", total_bt, addr, scanname, ret);
				continue;
			}
			vediag_info(VEDIAG_BLUETOOTH, "Found", "%d: '%s' '%s'\n", total_bt, addr, scanname);
			/*
			 * 1. If '', accept any device ID
			 * 2. Else, must match device ID
			 */
			if((!strcmp("", test->bluetooth_scanid)) || (!strcmp(addr, test->bluetooth_scanid))) {
				*size += 1;
				total_bt++;
				break;
			}
		}
		pclose(devf);
		vediag_debug(VEDIAG_BLUETOOTH, "Total", "matched : %d\n", total_bt);
	}
	return 0;
}

static void *bluetooth_test_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct bluetooth_test_info *test_arg = (struct bluetooth_test_info *)info->data;
	int test_done = 0, iteration = 0;
	u32 size;

	while (!test_done) {
		if (test_ctrl->data_stop) {
			test_done = 1;
			continue;
		}

		if(info->test_app == BLUETOOTH_HCITOOL_TEST) {
			vediag_info(VEDIAG_BLUETOOTH, info->dbg_info, "hcitool scan --> '%s'\n", test_arg->bluetooth_scanid);
			test_ctrl->error = bluetooth_test_hcitool(info->device, test_arg, &size);
			iteration++;
		}

		/* Check to stop */
		if(info->test_app == BLUETOOTH_HCITOOL_TEST) {
			test_ctrl->tested_size = size;
			if(iteration >= test_arg->bluetooth_scaniter) {
				if(test_ctrl->tested_size < info->total_size)
					test_ctrl->error = 1;
				test_done = 1;
			}
		}

		if (test_ctrl->error) {
			vediag_err(VEDIAG_BLUETOOTH, info->dbg_info, "BLUETOOTH failed\n");
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		}

		sleep(0);
	}

	test_ctrl->data_running--;
	pthread_exit(NULL);
}

static int bluetooth_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct bluetooth_test_info *test_arg = (struct bluetooth_test_info *) (info->data);
	struct bluetooth_test_list *e;
	struct list_head *pos, *q;

	pthread_mutex_lock(&_mutex);
	test_count--;
	pthread_mutex_unlock(&_mutex);

	if (test_count == 0) {
		list_for_each_entry(e, &testlist, l) {

			if (e->devname) {
				free(e->devname);
			}
		}

		list_for_each_safe(pos, q, &testlist) {
			e = list_entry(pos, struct bluetooth_test_list, l);
			list_del(pos);
			free(e);
		}
	}

	if (test_arg)
		free(test_arg);

	return 0;
}

struct vediag_test_device vediag_bluetooth = {
    .type           = VEDIAG_BLUETOOTH,
    .name           = "BLUETOOTH",
    .cmd            = "bt",
    .desc           = "BLUETOOTH TEST",
	.default_app	= "hcitool",
	.get_test_app	= bluetooth_get_app,
    .test_start     = bluetooth_test_start,
	.create_argument= bluetooth_create_arg,
    .check_dev      = bluetooth_check_dev,
    .test_execute   = bluetooth_test_execute,
    .test_complete  = bluetooth_test_complete,
};

late_initcall(diag_bluetooth_test_register)
{
    return vediag_test_register(&vediag_bluetooth);
}
