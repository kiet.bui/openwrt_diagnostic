
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmd.h>
#include <string.h>
#include <vediag_common.h>
#include <pthread.h>
#include <sched.h>
#include <sys/time.h>
#include <linux/err.h>
#include <readline/readline.h>
#include <circbuf.h>
#include <log.h>
#include <ras.h>
#include <time_util.h>

extern char file_system[128];
extern char file_testconfig[128];
extern char *file_test_default;

//#define X_MANAGER_DEBUG
#if defined(X_MANAGER_DEBUG)
#define manager_debug(fmt,args...)	xdebug (fmt ,##args)
#else
#define manager_debug(fmt,args...)
#endif

static int diag_running = false;
static LIST_HEAD(vediag_test_list);
static pthread_mutex_t test_list_mutex = PTHREAD_MUTEX_INITIALIZER;

typedef enum vediag_connection {
	DISCONNECTED = 0,
	CONNECTED
} vediag_connection_t;

struct vediag_man_data {
	int mid;
	int board_number;
	int request_ctrl;
	int status_ctrl;
	int report_status;
	int updating;
	char *full_env;
	int env_size;
	vediag_connection_t gui;
	int vrm_num;
	int req_num;
	int status_num;
	u16 test_running;
	u16 test_finish;
	struct timeval start_time;
	struct timeval current_time;
	int init_done;
	int current_req;
	int req_in_sequence;
	pthread_mutex_t sensor_mutex;
	int sensor_cnt;
	int sensor_interval;
	char *sensor_name;
	pthread_t request_manager;
	pthread_cond_t has_request;
	pthread_mutex_t request_mutex;
	struct list_head request_list;
	pthread_t status_manager;
	pthread_cond_t update_status;
	pthread_mutex_t status_mutex;
	struct list_head status_list;
};

struct vediag_man_data *vediag_control;

struct vediag_test_thread_info {
	struct vediag_test_info *info;
	struct vediag_test_device *test;
};

struct vediag_request_header {
	u8	rqtype;
#define VEDIAG_CMD_REQUEST_START_TEST		0
#define VEDIAG_CMD_REQUEST_SUSP_TEST			1
#define VEDIAG_CMD_REQUEST_RESM_TEST			2
#define VEDIAG_CMD_REQUEST_STOP_TEST			3
#define VEDIAG_CMD_REQUEST_STAT_TEST			4
#define VEDIAG_CMD_REQUEST_DEL_TEST			5
#define VEDIAG_CMD_REQUEST_CONNECT_SERVER	6
#define VEDIAG_CMD_REQUEST_DISCONNECT_SERVER	7
	u8	cmd;
	u8	dtyp;
	u8	rptyp;
	u32	rptime;
	u32	loop;
	u16	mtid;
	u16	stid;
	u32	size;
};

enum {
	REQUEST_NOT_STARTED = 0,
	REQUEST_IN_PROGRESS,
	REQUEST_COMPLETED,
};

struct vediag_test_request {
	struct list_head	request;
	struct vediag_request_header header;
	struct vediag_test_info *info;
#define CONFIG_REQUEST_IN_PROCESS		0x1
#define CONFIG_REQUEST_COMPLETE			0x2
#define CONFIG_REQUEST_RESULT_FAIL		0x4
#define CONFIG_REQUEST_SEQUENCE			0x8		/* Request must done before another request */
#define CONFIG_REQUEST_STARTED			0x10
	int flags;
	int state;
	int	queue_number;
	int runtimes;
	struct timeval start_time;
};

#define VEDIAG_STATE_NOT_START			0
#define VEDIAG_STATE_RUNNING				1
#define VEDIAG_STATE_FINISH				2

/**
 * find_stat: find the status of parent test
 * @mid:	master ID of parent test
 * @sid:	slave ID of parent test
 */
static struct vediag_test_status *find_stat(u16 mtid, u16 stid)
{
	struct vediag_test_status *stat_entry;

	pthread_mutex_lock(&vediag_control->status_mutex);
	list_for_each_entry(stat_entry, &vediag_control->status_list, status){
		if((stat_entry->mtid == mtid) && (stat_entry->stid == stid)){
			pthread_mutex_unlock(&vediag_control->status_mutex);
			return stat_entry;
		}
	}
	pthread_mutex_unlock(&vediag_control->status_mutex);

	return NULL;
}

/**
 * find_request: find the request of test
 * @mid:	master ID of the test
 * @sid:	slave ID of the test
 */
static struct vediag_test_request *find_request(u16 mtid, u16 stid)
{
	struct vediag_test_request *request;

	list_for_each_entry(request, &vediag_control->request_list, request) {
		if(request && (request->header.mtid == mtid) && (request->header.stid == stid))
			return request;
	}
	return NULL;
}

/**
 * vediag_test_status_alloc: allocate status for the test
 * @mid:	master ID of the test
 * @sid:	slave ID of the test
 */
struct vediag_test_status *vediag_test_status_alloc(u16 mtid, u16 stid)
{
	struct vediag_test_status *new = NULL;
	struct vediag_test_status *stat_ptr = NULL;
	struct vediag_test_status *stat_entry;
	struct vediag_test_request *request;

	stat_ptr = find_stat(mtid, stid);
	request = find_request(mtid, stid);

	/* No status in list, so this is a new parent */
	if(!stat_ptr) {
		/* Malloc new status info */
		new = (struct vediag_test_status *) malloc(sizeof(struct vediag_test_status));
		if(IS_ERR_OR_NULL(new)) {
			xerror("Error alloc new test info\n");
			return NULL;
		}
		memset(new, 0, sizeof(struct vediag_test_status));

		new->mtid = mtid;
		new->stid = stid;
		new->child = NULL;
		new->next = NULL;
#ifdef VEDIAG_MANAGER_USING_MUTEX
		pthread_mutex_init(&new->mutex, NULL);
#else
		pthread_spin_init(&new->lock, PTHREAD_PROCESS_SHARED);
#endif
		new->stat = VEDIAG_STATE_NOT_START;

		pthread_mutex_lock(&vediag_control->status_mutex);
		list_add_tail(&new->status, &vediag_control->status_list);
		pthread_mutex_unlock(&vediag_control->status_mutex);
		new->finish = 1;
		new->index = 0;
		if(request) {
			new->start.tv_sec = request->start_time.tv_sec;
			new->start.tv_usec = request->start_time.tv_usec;
			new->remain = request->header.loop - request->runtimes;
			new->comp = 0;
			new->pass = 0;
		}
		else {
			gettimeofday(&new->start, NULL);
		}
		new->refcnt ++;
		vediag_control->status_num++;
		vediag_control->test_running++;
		return new;
	}
	/* Alreadey has parent status */
	else {
		/* Parent status isn't create new info */
		if(stat_ptr->refcnt == 0) {
			new = stat_ptr;
			stat_ptr->percent = 0;
			new->stat = VEDIAG_STATE_NOT_START;
			stat_ptr->flags &= ~ VEDIAG_TEST_STATE_COMP;
		}
		else {
			/* Malloc new status info */
			new = (struct vediag_test_status *) malloc(sizeof(struct vediag_test_status));
			if(IS_ERR_OR_NULL(new)) {
			    xerror("Error alloc new test info\n");
				return NULL;
			}
			memset(new, 0, sizeof(struct vediag_test_status));

			new->mtid = mtid;
			new->stid = stid;
			new->child = NULL;
			new->next = NULL;
#ifdef VEDIAG_MANAGER_USING_MUTEX
			pthread_mutex_init(&new->mutex, NULL);
#else
			pthread_spin_init(&new->lock, PTHREAD_PROCESS_SHARED);
#endif
			new->stat = VEDIAG_STATE_NOT_START;
			new->finish = 1;
			gettimeofday(&new->start, NULL);

#ifdef VEDIAG_MANAGER_USING_MUTEX
			pthread_mutex_lock(&stat_ptr->mutex);
#else
			pthread_spin_lock(&stat_ptr->lock);
#endif
			if(stat_ptr->child == NULL) {
				stat_ptr->child = new;
				stat_ptr->finish++;
				new->index = 1;
			}
			else {
				for ( stat_entry = stat_ptr->child ; stat_entry->next != NULL ; stat_entry = stat_entry->next ) ;
				stat_entry->next = new;
				new->index = stat_entry->index + 1;
				stat_ptr->finish++;
			}
#ifdef VEDIAG_MANAGER_USING_MUTEX
			pthread_mutex_unlock(&stat_ptr->mutex);
#else
			pthread_spin_unlock(&stat_ptr->lock);
#endif
			vediag_control->status_num++;
		}
		stat_ptr->refcnt ++;
	}

	return new;
}

static void vediag_update_status_detail(struct vediag_test_status *stat, char *app, int threads)
{
	int str_len;

#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_lock(&stat->mutex);
#else
	pthread_spin_lock(&stat->lock);
#endif

	if (app) {
		str_len = strlen(app);
		if(str_len > sizeof(stat->app))
			str_len = sizeof(stat->app);
		memset(stat->app, 0, sizeof(stat->app));
		strncpy(stat->app, app, str_len);
	}
	if (threads > 0)
		stat->threads = threads;

	stat->flags |= VEDIAG_TEST_STATUS_CREATED;
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_unlock(&stat->mutex);
#else
	pthread_spin_unlock(&stat->lock);
#endif
}

struct vediag_test_status *vediag_add_new_status(u16 mtid, u16 stid, char *name, char *desc)
{
	struct vediag_test_status *stat;
	struct vediag_test_request *request = NULL;
	char str[128];
	manager_debug("# %s IN\n", __func__);

	stat = vediag_test_status_alloc(mtid, stid);
	if(IS_ERR_OR_NULL(stat)) {
	    xerror("Can't create new status for %s test (%s) \n", name, desc);
		return NULL;
	}
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_lock(&stat->mutex);
#else
	pthread_spin_lock(&stat->lock);
#endif

	if (name) {
		strncpy(stat->name, name, min(strlen(name), sizeof(stat->name) - 1)); /* end of string '\0' */
	} else
		sprintf(stat->name, "Unknown");

	if(stat->index == 0) {
		request = find_request(mtid, stid);
	}

	if(request) {
		memset(str, 0, sizeof(str));
		sprintf(str, "%s", desc);
		strncpy(stat->desc, str, min(strlen(str), sizeof(stat->desc) - 1)); /* end of string '\0' */
	}
	else {
		if (desc) {
			strncpy(stat->desc, desc, min(strlen(desc), sizeof(stat->desc) - 1)); /* end of string '\0' */
		} else
			sprintf(stat->desc, "Unknown");
	}
	stat->flags |= VEDIAG_TEST_STATUS_CREATED;
#ifdef CONFIG_VEDIAG_TEST_GUI
	sprintf(stat->result_detail, "%s", vediag_report_get_result(stat->result));
#endif
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_unlock(&stat->mutex);
#else
	pthread_spin_unlock(&stat->lock);
#endif

	manager_debug("# %s: wakeup main thread (id %d flag 0x%08x)\n", __func__, stat->index, stat->flags);
//	schedule();

	return stat;
}

int vediag_get_parent_status(struct vediag_test_status *parent)
{
	struct vediag_test_status *child;
	int child_num = 0;
	int total_percent = 0;

	if(IS_ERR_OR_NULL(parent)) {
	    xerror("Error parent @%p\n", parent);
		return -1;
	}

#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_lock(&parent->mutex);
#else
	pthread_spin_lock(&parent->lock);
#endif
	if(parent->child != NULL) {
		for( child = parent->child ; child != NULL ; child = child->next) {
			child_num++;
			total_percent += child->percent;
		}
	}
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_unlock(&parent->mutex);
#else
	pthread_spin_unlock(&parent->lock);
#endif

	if (child_num == 0)
		return total_percent;
	else
		return total_percent / child_num;
}

int vediag_get_parent_result(struct vediag_test_status *parent)
{
	struct vediag_test_status *child;
	int ret = VEDIAG_NOERR;

	if(IS_ERR_OR_NULL(parent)) {
	    xerror("Error parent @%p\n", parent);
		return VEDIAG_NODEV;
	}

#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_lock(&parent->mutex);
#else
	pthread_spin_lock(&parent->lock);
#endif
	if(parent->child != NULL) {
		for( child = parent->child ; child != NULL ; child = child->next) {
			if (child->result != VEDIAG_NOERR) {
				ret = child->result;
				break;
			}
		}
	}
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_unlock(&parent->mutex);
#else
	pthread_spin_unlock(&parent->lock);
#endif

	return ret;
}

int vediag_test_running(struct vediag_test_status *parent)
{
	struct vediag_test_status *child;
	int running = 0;

	if(IS_ERR_OR_NULL(parent)) {
		xerror("Error parent @%p\n", parent);
		return -1;
	}

#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_lock(&parent->mutex);
#else
	pthread_spin_lock(&parent->lock);
#endif
	if(parent->child != NULL) {
		for( child = parent->child ; child != NULL ; child = child->next) {
			if((child->flags & VEDIAG_TEST_STATE_COMP) != VEDIAG_TEST_STATE_COMP)
				running++;
		}
	}
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_unlock(&parent->mutex);
#else
	pthread_spin_unlock(&parent->lock);
#endif

	return running;
}

void vediag_update_status(struct vediag_test_status *stat, int state, u64 size, int percent, int result)
{
#ifdef CONFIG_VEDIAG_TEST_GUI
	char str[128];
#endif

	manager_debug("# %s IN\n", __func__);
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_lock(&stat->mutex);
#else
	pthread_spin_lock(&stat->lock);
#endif

	if(state >= 0) {
		if(stat->stat != state ||
				(size > 0 && stat->size != size))
			stat->flags |= VEDIAG_TEST_STATE_CHANGED;

		if (((size == 0) && (state == VEDIAG_REPORT_SCAN))
			|| (size > 0)) {
			stat->size = size;
		}
		stat->stat = state;
	}
	stat->percent = percent;
	stat->result = result;
#ifdef CONFIG_VEDIAG_TEST_GUI
	memset(str, 0, 128);
	sprintf(str, "%s%s", vediag_report_get_result(stat->result), stat->message);
	strncpy(stat->result_detail, str, sizeof(stat->result_detail));
#endif
	gettimeofday(&stat->now, NULL);
	stat->runtimes = stat->now.tv_sec - stat->start.tv_sec;
	stat->flags |= VEDIAG_TEST_STATUS_UPDATED;
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_unlock(&stat->mutex);
#else
	pthread_spin_unlock(&stat->lock);
#endif

	manager_debug("# %s: wakeup main thread (id %d flag 0x%08x)\n", __func__, stat->index, stat->flags);
//	schedule();
}

static void vediag_update_result(struct vediag_test_status *stat, int result, int percent, char *result_detail)
{
	int len = 0;
	manager_debug("# %s IN\n", __func__);
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_lock(&stat->mutex);
#else
	pthread_spin_lock(&stat->lock);
#endif

	if(percent >= 0)
		stat->percent = percent;
	if (result_detail) {
		memset(stat->result_detail, 0, sizeof(stat->result_detail));
		len = strlen(result_detail);
		if (len > sizeof(stat->result_detail))
			len = sizeof(stat->result_detail);
		strncpy(stat->result_detail, result_detail, len);
	}
	stat->result_detail[sizeof(stat->result_detail) - 1] = '\0';
	stat->result = result;
	stat->flags |= VEDIAG_TEST_RESULT_UPDATED;
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_unlock(&stat->mutex);
#else
	pthread_spin_unlock(&stat->lock);
#endif

	manager_debug("# %s: wakeup main thread (id %d flag 0x%08x)\n", __func__, stat->index, stat->flags);
//	schedule();
}

int vediag_test_register(struct vediag_test_device *new)
{
	pthread_mutex_lock(&test_list_mutex);
	list_add_tail(&new->list, &vediag_test_list);
	pthread_mutex_unlock(&test_list_mutex);

	return 0;
}

int vediag_request_add(struct vediag_test_request *request, int sequence)
{
	if (!request)
		return 1;

	if(sequence) {
		request->flags |= CONFIG_REQUEST_SEQUENCE;
		request->queue_number = vediag_control->req_in_sequence;
		vediag_control->req_in_sequence++;
	}
	request->state = REQUEST_NOT_STARTED;

	pthread_mutex_lock(&vediag_control->request_mutex);
	list_add_tail(&request->request, &vediag_control->request_list);
	//pthread_cond_signal(&vediag_control->has_request);
	pthread_mutex_unlock(&vediag_control->request_mutex);
//	wake_up_process(vediag_control->thread);
	manager_debug("# %s: wakeup main thread\n", __func__);

	return 0;
}

static struct vediag_test_device *get_test_by_name(char *name)
{
	struct vediag_test_device *test;
	list_for_each_entry(test, &vediag_test_list, list){
		if(strcmp(test->cmd, name) == 0){
			return test;
		}
	}
	return NULL;
}

static struct vediag_test_device *get_test_by_type(int type)
{
	struct vediag_test_device *test;

	list_for_each_entry(test, &vediag_test_list, list){
		if(test->type == type){
			return test;
		}
	}
	return NULL;
}

static struct vediag_test_status *vediag_get_test_status_by_id(int id)
{
	struct vediag_test_status *stat_entry;

	pthread_mutex_lock(&vediag_control->status_mutex);
	list_for_each_entry(stat_entry, &vediag_control->status_list, status){
		if(stat_entry->stid == id){
			pthread_mutex_unlock(&vediag_control->status_mutex);
			return stat_entry;
		}
	}
	pthread_mutex_unlock(&vediag_control->status_mutex);
	return NULL;
}

static void vediag_test_suspend(int id)
{
	struct vediag_test_status *stat_entry;
	struct vediag_test_status *stat_ptr;

	if(id <0 ) {
		pthread_mutex_lock(&vediag_control->status_mutex);
		list_for_each_entry(stat_entry, &vediag_control->status_list, status){
#ifdef VEDIAG_MANAGER_USING_MUTEX
			pthread_mutex_lock(&stat_entry->mutex);
#else
			pthread_spin_lock(&stat_entry->lock);
#endif
			stat_entry->should_suspend = 1;
			if(stat_entry->child != NULL) {
				for( stat_ptr = stat_entry->child ; stat_ptr != NULL ; stat_ptr = stat_ptr->next) {
					stat_ptr->should_suspend = 1;
				}
			}
#ifdef VEDIAG_MANAGER_USING_MUTEX
			pthread_mutex_unlock(&stat_entry->mutex);
#else
			pthread_spin_unlock(&stat_entry->lock);
#endif
		}
		pthread_mutex_unlock(&vediag_control->status_mutex);
	}
	else {
		stat_entry = vediag_get_test_status_by_id(id);
		if(stat_entry == NULL) {
			xerror("No test with ID %d\n", id);
			return;
		}
#ifdef VEDIAG_MANAGER_USING_MUTEX
		pthread_mutex_lock(&stat_entry->mutex);
#else
		pthread_spin_lock(&stat_entry->lock);
#endif
		stat_entry->should_suspend = 1;
		if(stat_entry->child != NULL) {
			for( stat_ptr = stat_entry->child ; stat_ptr != NULL ; stat_ptr = stat_ptr->next) {
				stat_ptr->should_suspend = 1;
			}
		}
#ifdef VEDIAG_MANAGER_USING_MUTEX
		pthread_mutex_unlock(&stat_entry->mutex);
#else
		pthread_spin_unlock(&stat_entry->lock);
#endif
	}
}

static void vediag_resume_thread(struct vediag_test_status *stat_entry)
{
	//TODO: process resume test
}

static void vediag_test_resume(int id)
{
	struct vediag_test_status *stat_entry;

	if(id <0 ) {
		pthread_mutex_lock(&vediag_control->status_mutex);
		list_for_each_entry(stat_entry, &vediag_control->status_list, status){
			vediag_resume_thread(stat_entry);
		}
		pthread_mutex_unlock(&vediag_control->status_mutex);
	}
	else {
		stat_entry = vediag_get_test_status_by_id(id);
		if(stat_entry == NULL) {
			xerror("No test with ID %d\n", id);
			return;
		}
		vediag_resume_thread(stat_entry);
	}
}

static void vediag_stop_thread(struct vediag_test_status *stat_entry)
{
	struct vediag_test_status *stat_ptr;

#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_lock(&stat_entry->mutex);
#else
	pthread_spin_lock(&stat_entry->lock);
#endif
	stat_entry->should_stop = 1;
	if(stat_entry->child != NULL) {
		for( stat_ptr = stat_entry->child ; stat_ptr != NULL ; stat_ptr = stat_ptr->next) {
			stat_ptr->should_stop = 1;
		}
	}
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_unlock(&stat_entry->mutex);
#else
	pthread_spin_unlock(&stat_entry->lock);
#endif
}

static void vediag_test_stop(int id)
{
	struct vediag_test_status *stat_entry;

	if(id < 0 ) {
		pthread_mutex_lock(&vediag_control->status_mutex);
		list_for_each_entry(stat_entry, &vediag_control->status_list, status){
			vediag_stop_thread(stat_entry);
		}
		pthread_mutex_unlock(&vediag_control->status_mutex);
	}
	else {
		stat_entry = vediag_get_test_status_by_id(id);
		if(stat_entry == NULL) {
			xerror("No test with ID %d\n", id);
			return;
		}
		vediag_stop_thread(stat_entry);
	}
}

#if defined(CONFIG_VEDIAG_TEST_GUI)
static int vediag_full_report_to_gui(struct vediag_test_status *stat_entry)
{
	struct vediag_full_report *report;
	int len;

	report = (struct vediag_full_report *) malloc(sizeof(struct vediag_full_report));
	if(!report) {
		xerror("Fail to alloc full report\n");
		return 1;
	}
	memset(report, 0, sizeof(struct vediag_full_report));

//	mutex_lock(&stat_entry->mutex);
	report->mtid = stat_entry->mtid;
	report->stid = stat_entry->stid;
	report->index = stat_entry->index;
	report->percent = stat_entry->percent;
	report->result = stat_entry->result;
	report->state = stat_entry->stat;
	report->runtimes = stat_entry->runtimes;
	report->threads = stat_entry->threads;
	report->flags = stat_entry->flags & VEDIAG_TEST_STATE_COMP;
	report->size = (stat_entry->size >> 32) & 0xFFFFFFFF;
	report->size_l = stat_entry->size & 0xFFFFFFFF;

	len = strlen(stat_entry->name);
	if(len > sizeof(report->name))
		len = sizeof(report->name);
	strncpy(report->name, stat_entry->name, len);

	len = strlen(stat_entry->desc);
	if(len > sizeof(report->detail))
		len = sizeof(report->detail);
	strncpy(report->detail, stat_entry->desc, len);

	len = strlen(stat_entry->app);
	if(len > sizeof(report->app))
		len = sizeof(report->app);
	strncpy(report->app, stat_entry->app, len);

	len = strlen(stat_entry->result_detail);
	if(len > sizeof(report->status))
		len = sizeof(report->status);
	strncpy(report->status, stat_entry->result_detail, len);
//	mutex_unlock(&stat_entry->mutex);

	vediag_gui_add_report(VEDIAG_FULL_REPORT, (void*) report);

	return 0;
}
#endif

static void vediag_add_current_status(void)
{
	struct vediag_test_status *parent, *child;

	pthread_mutex_lock(&vediag_control->status_mutex);
	list_for_each_entry(parent, &vediag_control->status_list, status){
#ifdef VEDIAG_MANAGER_USING_MUTEX
		pthread_mutex_lock(&parent->mutex);
#else
		pthread_spin_lock(&parent->lock);
#endif
#if defined(CONFIG_VEDIAG_TEST_GUI)
		vediag_full_report_to_gui(parent);
#endif
		for(child = parent->child ; child != NULL; child = child->next) {
#ifdef VEDIAG_MANAGER_USING_MUTEX
			pthread_mutex_lock(&child->mutex);
#else
			pthread_spin_lock(&child->lock);
#endif
#if defined(CONFIG_VEDIAG_TEST_GUI)
			vediag_full_report_to_gui(child);
#endif
#ifdef VEDIAG_MANAGER_USING_MUTEX
			pthread_mutex_unlock(&child->mutex);
#else
			pthread_spin_unlock(&child->lock);
#endif
		}
#ifdef VEDIAG_MANAGER_USING_MUTEX
		pthread_mutex_unlock(&parent->mutex);
#else
		pthread_spin_unlock(&parent->lock);
#endif
	}
	pthread_mutex_unlock(&vediag_control->status_mutex);
}

void vediag_gui_connect(int connected)
{
	if(connected == 0) {
		vediag_control->gui = DISCONNECTED;
	}
	else {
		vediag_add_current_status();
		vediag_control->gui = CONNECTED;
	}
}

static void vediag_report_proc(struct vediag_test_status *stat_entry)
{
#if defined(CONFIG_VEDIAG_TEST_GUI)
	struct vediag_report *report;
#endif

#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_lock(&stat_entry->mutex);
#else
	pthread_spin_lock(&stat_entry->lock);
#endif
	manager_debug("## %s: id %d flags 0x%08x\n", __func__, stat_entry->index, stat_entry->flags);
	/* If there has a test created */
	if((stat_entry->flags & VEDIAG_TEST_STATUS_CREATED) == VEDIAG_TEST_STATUS_CREATED) {
#if defined(CONFIG_VEDIAG_TEST_GUI)
		if(vediag_control->gui == CONNECTED) {
			report = malloc(sizeof(struct vediag_report));
			if(IS_ERR_OR_NULL(report)) {
#ifdef VEDIAG_MANAGER_USING_MUTEX
				pthread_mutex_unlock(&stat_entry->mutex);
#else
				pthread_spin_unlock(&stat_entry->lock);
#endif
				return;
			}
			memset(report, 0, sizeof(struct vediag_report));

			report->rp_type = VEDIAG_REPORT_CREATE_NEW_ENTRY;
			if (stat_entry->name)
				strncpy(report->detail.new_entry.name, stat_entry->name, sizeof(report->detail.new_entry.name));
			if (stat_entry->desc)
				strncpy(report->detail.new_entry.detail, stat_entry->desc, sizeof(report->detail.new_entry.detail));
			if (stat_entry->app)
				strncpy(report->detail.new_entry.app, stat_entry->app, sizeof(report->detail.new_entry.app));
			/*report->detail.status.state = stat_entry->stat;*/
			if (stat_entry->threads > 0)
				report->detail.new_entry.threads = stat_entry->threads;
			report->mtid = stat_entry->mtid;
			report->stid = stat_entry->stid;
			report->index = stat_entry->index;
			manager_debug("## %s: create new id %d (flags 0x%08x)\n", __func__, stat_entry->index, stat_entry->flags);

			vediag_gui_add_report(VEDIAG_SIMPLE_REPORT, (void*) report);
		}
#endif
		stat_entry->flags &= ~VEDIAG_TEST_STATUS_CREATED;
	}
	if((stat_entry->flags & VEDIAG_TEST_STATUS_UPDATED) == VEDIAG_TEST_STATUS_UPDATED) {
#if defined(CONFIG_VEDIAG_TEST_GUI)
		if(vediag_control->gui == CONNECTED) {
			report = malloc(sizeof(struct vediag_report));
			if(IS_ERR_OR_NULL(report)){
#ifdef VEDIAG_MANAGER_USING_MUTEX
				pthread_mutex_unlock(&stat_entry->mutex);
#else
				pthread_spin_unlock(&stat_entry->lock);
#endif
				return;
			}
			memset(report, 0, sizeof(struct vediag_report));
			report->rp_type = VEDIAG_REPORT_UPDATE_STATUS;
			report->detail.status.percent = stat_entry->percent;
			report->detail.status.runtime = stat_entry->runtimes;
			if(stat_entry->flags & VEDIAG_TEST_STATE_CHANGED) {
				report->detail.status.flags |= REPORT_STATE_CHANGE;
			}
			report->detail.status.state = stat_entry->stat;
			report->detail.status.result = stat_entry->result;
			report->detail.status.size = (u32)(stat_entry->size >> 32);
			report->detail.status.size_l = (u32)(stat_entry->size);
			report->mtid = stat_entry->mtid;
			report->stid = stat_entry->stid;
			report->index = stat_entry->index;
			manager_debug("## %s: update status id %d (flags 0x%08x)\n", __func__, stat_entry->index, stat_entry->flags);

			vediag_gui_add_report(VEDIAG_SIMPLE_REPORT, (void*) report);
		}
#endif
		if(stat_entry->flags & VEDIAG_TEST_STATE_CHANGED) {
			stat_entry->flags &= ~VEDIAG_TEST_STATE_CHANGED;
		}
		stat_entry->flags &= ~VEDIAG_TEST_STATUS_UPDATED;
	}
	if((stat_entry->flags & VEDIAG_TEST_RESULT_UPDATED) == VEDIAG_TEST_RESULT_UPDATED) {
#if defined(CONFIG_VEDIAG_TEST_GUI)
		if(vediag_control->gui == CONNECTED) {
			report = malloc(sizeof(struct vediag_report));
			if(IS_ERR_OR_NULL(report)){
#ifdef VEDIAG_MANAGER_USING_MUTEX
				pthread_mutex_unlock(&stat_entry->mutex);
#else
				pthread_spin_unlock(&stat_entry->lock);
#endif
				return;
			}
			memset(report, 0, sizeof(struct vediag_report));
			report->rp_type = VEDIAG_REPORT_UPDATE_RESULT;
			report->mtid = stat_entry->mtid;
			report->stid = stat_entry->stid;
			report->index = stat_entry->index;
			report->detail.result.percent = stat_entry->percent;
			report->detail.result.result = stat_entry->result;
			strcpy(report->detail.result.result_detail, stat_entry->result_detail);
			manager_debug("## %s: update result id %d (flags 0x%08x)\n", __func__, stat_entry->index, stat_entry->flags);

			vediag_gui_add_report(VEDIAG_SIMPLE_REPORT, (void*) report);
		}
#endif
		stat_entry->flags &= ~VEDIAG_TEST_RESULT_UPDATED;
		stat_entry->flags |= VEDIAG_TEST_STATE_COMP;

#if 0
		/* Check if requet if finish */
		stat_ptr = find_stat(stat_entry->mtid, stat_entry->stid);
		if(stat_ptr) {
			stat_ptr->finish--;
			if(stat_entry->percent < 100) {
				stat_ptr->flags |= VEDIAG_TEST_STATE_FAIL;
				stat_entry->flags |= VEDIAG_TEST_STATE_FAIL;
			}
		}
#endif
	}
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_unlock(&stat_entry->mutex);
#else
	pthread_spin_unlock(&stat_entry->lock);
#endif
}

extern char* print_size_buf(unsigned long long size, const char *s);
static void __vediag_show_test_result(struct vediag_test_status *stat_entry)
{
	xinfo("%2d.%s (%s): Tested %son %d second(s) => %s (%d%%)\n",
			stat_entry->index,
			stat_entry->name,
			stat_entry->desc,
			print_size_buf(stat_entry->size, " "),
			stat_entry->runtimes,
			stat_entry->result_detail,
			stat_entry->percent);
}

static void vediag_show_test_result(struct vediag_test_status *stat_entry)
{
	struct vediag_test_status *child;

	if (stat_entry) {
		if (stat_entry->performance == 0) {
			xinfo("========= %s TEST RESULT ==========\n",
					vediag_get_IP_name(stat_entry->test_type));
		} else {
			xinfo("========= %s PERFORMANCE TEST RESULT ==========\n",
								vediag_get_IP_name(stat_entry->test_type));
		}
		if (stat_entry->child != NULL) {
			for (child = stat_entry->child; child != NULL; child =
					child->next) {
				__vediag_show_test_result(child);
			}
		}
		if (stat_entry->performance == 0) {
			xinfo("========= %s TEST %s ==========\n",
					vediag_get_IP_name(stat_entry->test_type),
					stat_entry->result == VEDIAG_NOERR ?
							"PASSED" :
							(stat_entry->result == VEDIAG_STOP ?
									"STOPPED" : "FAILED"));
		} else {
			xinfo("========= %s PERFORMANCE TEST %s ==========\n",
					vediag_get_IP_name(stat_entry->test_type),
					stat_entry->result == VEDIAG_NOERR ?
							"PASSED" :
							(stat_entry->result == VEDIAG_STOP ?
									"STOPPED" : "FAILED"));
		}
	}
}

static void vediag_process_test_done(struct vediag_test_request *request)
{
	vediag_control->test_running--;
	vediag_control->test_finish++;
	list_del(&request->request);
	free(request);

	if ((vediag_control->test_running == 0)
			&& (vediag_control->test_finish > 0)) {
#ifdef CONFIG_RAS
		/* Check RAS error */
		while (!cbIsEmpty(&ras_cb_system)) {
			ElemType *e;

			e = calloc(1, sizeof(ElemType));
			if (!e)
				goto exit;

			cbRead(&ras_cb_system, e);
			vediag_err(VEDIAG_SYSTEM, "RAS", "System Error: %s\n", e->buf);
			free(e);
		}
exit:
#endif
		xinfo("DIAG TEST DONE ALL\n");
		diag_running = false;
	}
}

static void vediag_complete_test(struct vediag_test_status *stat_entry)
{
	struct vediag_test_request *request;

	if (!stat_entry)
		return;

	vediag_show_test_result(stat_entry);
	stat_entry->flags |= VEDIAG_TEST_STATE_COMP;
	pthread_mutex_lock(&vediag_control->request_mutex);
	request = find_request(stat_entry->mtid, stat_entry->stid);
	if(request) {
		request->state = REQUEST_COMPLETED;
		if ((request->flags & CONFIG_REQUEST_SEQUENCE) == CONFIG_REQUEST_SEQUENCE) {
			vediag_control->current_req++;
			if(vediag_control->current_req > vediag_control->req_in_sequence) {
				vediag_control->current_req = 0;
			}
		}
		/* Make sure request manager is complete */
		stat_entry->refcnt = 0;
		stat_entry->finish = 1;
	}
	pthread_mutex_unlock(&vediag_control->request_mutex);
}

static void vediag_free_test_control(struct vediag_test_control *test_ctrl)
{
	if (!test_ctrl)
		return;

	if (test_ctrl->test_data)
		free(test_ctrl->test_data);
	if (test_ctrl->phy_data)
		free(test_ctrl->phy_data);
	free(test_ctrl);
}

void vediag_free_test_info(struct vediag_test_info *dev_info)
{
	struct vediag_test_argument *arg;

	if (!dev_info)
		return;

	while (!list_empty(&dev_info->arguments)) {
		arg = list_entry(dev_info->arguments.next,
				struct vediag_test_argument, entry);
		if(arg) {
			list_del_init(&arg->entry);
			if (arg->key)
				free(arg->key);
			if (arg->value)
				free(arg->value);
		}
	}

	if (dev_info->cfg_file)
		free(dev_info->cfg_file);
	if (dev_info->device)
		free(dev_info->device);
	if (dev_info->physical_dev)
		free(dev_info->physical_dev);
	if (dev_info->application)
		free(dev_info->application);
}
#define PTHREAD_STACK_MIN 16384
static void *vediag_test_detail_thread(void * __data)
{
	struct vediag_test_thread_info *data = (struct vediag_test_thread_info *) __data;
    struct vediag_test_info *info = data->info;
    struct vediag_test_info *test_info;
    struct vediag_test_control *test_ctrl;
    char str[24];
    u64 total_size = info->total_size;
    struct timeval start_time, current_time;
    pthread_t thread;
    int test_done = 0;
    int percent = 0, ret = 0;
    int i;

    test_ctrl = (struct vediag_test_control *) malloc(sizeof(struct vediag_test_control));
    if (test_ctrl == NULL) {
        sprintf(str, "No memory");
        vediag_update_result(info->stat, VEDIAG_NOMEM, 0, str);
        goto exit;
    }
    info->test_ctrl = test_ctrl;
    memset(test_ctrl, 0, sizeof(struct vediag_test_control));
    test_ctrl->test_type = info->test_type;
	test_ctrl->error = 0;
	test_ctrl->data_running = 0;
	test_ctrl->data_stop = 0;
	test_ctrl->phy_complete = 0;
	test_ctrl->test_restart = 0;
	test_ctrl->transfering = 0;
	test_ctrl->tested_size = 0;
	test_ctrl->runtime = info->runtime;

	/* Create data transfer thread */
    for (i = 0; i < info->threads; i++) {
    	test_info = malloc(sizeof(struct vediag_test_info));
        if (!test_info || IS_ERR(test_info)) {
            continue;
        }
        memset(test_info, 0, sizeof(struct vediag_test_info));
    	INIT_LIST_HEAD(&info->arguments);
        if (info->device)
        	test_info->device = strdup(info->device);
        if (info->physical_dev)
        	test_info->physical_dev = strdup(info->physical_dev);
        strcpy(test_info->dbg_info, info->dbg_info);
        test_info->test_ctrl = test_ctrl;
        test_info->test_type = info->test_type;
        test_info->data = info->data;
        test_info->runtime = info->runtime;
        test_info->core_nums = info->core_nums;
        test_info->total_size = info->total_size;
        test_info->chunk_size = info->chunk_size;
        test_info->dev_mask = info->dev_mask;
        test_info->offset += info->offset + i
                * (info->total_size / info->threads);
        test_info->test_app = info->test_app;
        test_info->test_mode = info->test_mode;
        test_info->threads = info->threads;
        test_info->stat = info->stat;
        test_info->application = info->application;
        if(data->test && data->test->test_execute) {
        	pthread_attr_t attr;
        	if (info->core_nums == 0) {
				pthread_attr_init(&attr);
				pthread_attr_setstacksize(&attr, 2 * PTHREAD_STACK_MIN);
				ret = pthread_create(&thread, &attr, data->test->test_execute, (void *)test_info);
				pthread_attr_destroy(&attr);
				pthread_detach(thread);
        	}
			else {
				cpu_set_t cpuset;
				CPU_ZERO(&cpuset);
				int total_cores = sysconf(_SC_NPROCESSORS_ONLN);
				if (info->core_nums > total_cores)
					info->core_nums = total_cores;
				int core_no = (i % (info->core_nums)) + (total_cores - info->core_nums);
				CPU_SET(core_no, &cpuset);
				pthread_attr_init(&attr);
				pthread_attr_setstacksize(&attr, 2 * PTHREAD_STACK_MIN);
				ret = pthread_create(&thread, &attr, data->test->test_execute, (void *)test_info);
				pthread_attr_destroy(&attr);
				pthread_detach(thread);
        		if (ret) {
        			vediag_err(test_info->test_type, test_info->dbg_info, "Create executing threads failed.\n");
        			vediag_free_test_info(test_info);
        			continue;
        		}
				ret = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset);
        		if (ret) {
        			vediag_err(test_info->test_type, test_info->dbg_info, "Assign core %d to threads failed.\n", core_no);
        			vediag_free_test_info(test_info);
        			continue;
        		}
			}
			
			if (ret) {
				vediag_err(test_info->test_type, test_info->dbg_info, "Create executing threads failed.\n");
				vediag_free_test_info(test_info);
				continue;
			}
			test_ctrl->data_running++;
        }
    }

    test_ctrl->phy_complete = 1;

    gettimeofday(&start_time, NULL);
    while (!test_done) {
        /* Check if has suspend or stop */

        if (info->stat->should_stop) {
        	test_ctrl->data_stop = 1;
        }

		/* Check complete */
        if (test_ctrl->error) {
        	test_ctrl->data_stop = 1;
        	test_ctrl->test_restart = 1;
        }

		/* Calculate percent task completed */
		if(test_ctrl->runtime > 0) {
			gettimeofday(&current_time, NULL);
			if((current_time.tv_sec - start_time.tv_sec) >= test_ctrl->runtime) {
				test_ctrl->data_stop = 1;
			}
			percent = (((current_time.tv_sec - start_time.tv_sec) * 100)/ test_ctrl->runtime);
			if(percent > 100) {
				percent = 100;
			}
		}
		else {
			if(test_ctrl->tested_size >= total_size) {
				percent = 100;
				test_ctrl->data_stop = 1;
			}
			else {
				percent = ((test_ctrl->tested_size * 100)/ total_size);
			}
			if (percent > 100) {
				percent = 100;
				test_ctrl->data_stop = 1;
			}
		}

		/*
		 * 	Update to GUI:
		 *	state: Read/Write
		 *	percent: current percent
		 *	result:	Running
		 */
		if ((data->test->flags & VEDIAG_TEST_CASE_TYPE) == VEDIAG_TEST_CASE_TYPE) {
			vediag_update_status(info->stat, VEDIAG_REPORT_READ_WRITE, 0,
					percent, VEDIAG_REPORT_EXCUTING);
		}
		else {
			vediag_update_status(info->stat, VEDIAG_REPORT_READ_WRITE, test_ctrl->tested_size,
					percent, VEDIAG_REPORT_EXCUTING);
		}

        if(test_ctrl->phy_complete && !test_ctrl->data_running) {
        	test_done = 1;
        }
        else {
        	 sleep(1);
        }
    }

    /* Complete test, free memory */
	if(data->test && data->test->test_complete) {
		data->test->test_complete(info);
	}

	/* Update result */
	if (info->stat->should_stop) {
		info->stat->should_stop = 0;
		vediag_update_result(info->stat, VEDIAG_STOP, percent, "Stop by request");
	} else if (test_ctrl->error){
		vediag_err(info->test_type, info->dbg_info, "Test failed (error %d)\n",
				test_ctrl->error);
		if (strlen(test_ctrl->err_msg) > 0)
			vediag_update_result(info->stat, VEDIAG_TEST_ERR, percent, test_ctrl->err_msg);
		else
			vediag_update_result(info->stat, VEDIAG_TEST_ERR, percent, "FAILED");
	} else {
		percent = 100;
		vediag_info(info->test_type, info->dbg_info, "Test passed.\n");
		vediag_update_result(info->stat, VEDIAG_NOERR, percent, "PASS");
	}

exit:
	vediag_free_test_control(test_ctrl);
	vediag_free_test_info(info);

    if (data) {
        free(data);
    }

    pthread_exit(NULL);
}

char *vediag_get_default_application(int test_type)
{
	struct vediag_test_device *test = get_test_by_type(test_type);

	if (test)
		return test->default_app;
	return NULL;
}

void vediag_get_default_argument(int test_type, struct list_head *arg_list)
{
	struct vediag_test_device *test = get_test_by_type(test_type);

	if (test && test->create_argument) {
		test->create_argument(arg_list);
	}
}

char *vediag_get_argument(struct vediag_test_info *dev_info, char *key)
{
	struct vediag_test_argument *arg;
	struct list_head *entry;

	for(entry = dev_info->arguments.next ; entry != &dev_info->arguments; ) {
    	arg = container_of(entry, struct vediag_test_argument, entry);
    	entry = entry->next;
    	if (arg && strcasecmp(arg->key, key) == 0) {
    		if (arg->value)
    			if (strlen(arg->value))
    				return strdup(arg->value);
    			else
    				return NULL;
    		else
    			return NULL;
    	}
    }

    return NULL;
}

static int vediag_parse_test_config(char *cfg_file, struct vediag_test_device *test, struct vediag_test_info *dev_info)
{
	char section[64];
	int dev;
	char *val;

	/* Parse test config */
	memset(section, 0, sizeof(section));
	dev = dev_info->dev_mask;
	sprintf(section, "%s%d", get_section_type(dev_info->test_type), dev);
	dev_info->device = vediag_parse_config(cfg_file, section, VEDIAG_DEVICE_KEY);
	dev_info->deviced = vediag_parse_config(cfg_file, section, VEDIAG_DEVICED_KEY);
	dev_info->physical_dev = vediag_parse_config(cfg_file, section, VEDIAG_PHYSICAL_KEY);
	/* No device name or physical name in test config file */
	if (!dev_info->device && !dev_info->physical_dev) {
    	vediag_err(test->type, NULL, "ERROR: No device name or physical name in test config file");
		return -1;
	}

    /* If device name not found in config file, get it from system */
    if (dev_info->physical_dev)
    	dev_info->device = vediag_find_device(dev_info->test_type, dev_info->physical_dev);

    if (!dev_info->physical_dev)
    	dev_info->physical_dev = vediag_find_physical_device(dev_info->test_type, dev_info->device);

    /* Parse argument */
    val = vediag_parse_config(cfg_file, section, VEDIAG_ARGUMENT_KEY);
    if (val) {
    	vediag_get_test_arguments(cfg_file, val, &dev_info->arguments);
    	free(val);
    } else {
    	vediag_err(test->type, dev_info->physical_dev, "ERROR: Parse argument section fail - section %s\n", section);
    }

    val = vediag_parse_config(cfg_file, section, VEDIAG_APPLICATION_KEY);
    if (val) {
		if (test->get_test_app)
			dev_info->test_app = test->get_test_app(val);
		dev_info->application = strdup(val);
		free(val);
	} else {
		vediag_err(test->type, dev_info->physical_dev, "ERROR: Parse test app fail - section %s\n",
				section);
		if (test->default_app) {
			dev_info->application = strdup(test->default_app);
			if (test->get_test_app)
				dev_info->test_app = test->get_test_app(dev_info->application);
		}
	}

	return 0;
}

static int vediag_get_test_number(struct vediag_test_info *info)
{
	char *val;
	int total_device = 0;

	val = vediag_parse_config(info->cfg_file, get_section_type(info->test_type), VEDIAG_DEVICE_NUM_KEY);
	if (val) {
		total_device = strtoul(val, NULL, 0);
		free(val);
	}

	return total_device;
}

static u32 vediag_get_dev_mask_from_name(struct vediag_test_info *info, char *input_physical_name, char *input_device_name)
{
	u32 dev_mask = 0x0;
	char section[64];
	int dev;
	int total_dev = 0;
	char *device = NULL;
	char *physical_dev = NULL;

	if (!input_physical_name && !input_device_name) {
		return info->dev_mask;
	} else {
		total_dev = vediag_get_test_number(info);
		for (dev = 0; dev < total_dev; dev++) {
			memset(section, 0, sizeof(section));
			sprintf(section, "%s%d", get_section_type(info->test_type), dev + 1);
			device = vediag_parse_config(info->cfg_file, section, VEDIAG_DEVICE_KEY);
			physical_dev = vediag_parse_config(info->cfg_file, section, VEDIAG_PHYSICAL_KEY);

			/* No device name or physical name in test config file */
			if (!device && !physical_dev) {
				continue;
			}
		    if (physical_dev) {
		    	if(device)
		    		free(device);
		    	device = vediag_find_device(info->test_type, physical_dev);
		    }
			if (!physical_dev) {
				physical_dev = vediag_find_physical_device(info->test_type, device);
			}

			/* Matching physical name*/
			if (input_physical_name) {
				if (physical_dev) {
					if (strcmp(physical_dev, input_physical_name) == 0) {
						dev_mask = 0x1 << dev;
						break;
					}
				}
			}

			/* Matching device name */
			if (input_device_name) {
				if (device) {
					if (strcmp(device, input_device_name) == 0) {
						dev_mask = 0x1 << dev;
						break;
					}
				}
			}
			
	    	if(device)
	    		free(device);
	    	if(physical_dev)
	    		free(physical_dev);
		}
	}

	return dev_mask;
}

static void *vediag_test_start(void *__data)
{
	struct vediag_test_thread_info *data = (struct vediag_test_thread_info *) __data;
	struct vediag_test_thread_info *test_data;
	struct vediag_test_device *test = data->test;
	struct vediag_test_info *info = data->info, *dev_info;
	int dev, dev_test_total = 0;
	struct vediag_test_status *stat;
	char *error = NULL;
	int percent = 0;
	pthread_t thread;
	int ret;

	dev_test_total = vediag_get_test_number(info);
	if (dev_test_total <= 0) {
		xinfo("ERROR: No selected devices\n");
		vediag_update_result(info->stat, VEDIAG_NOCFG, 100, "No selected devices");
		goto complete;
	}
	vediag_update_status(info->stat, VEDIAG_REPORT_SCAN, dev_test_total, 0, VEDIAG_REPORT_EXCUTING);

	// Update dev_mask in case: user input -pf -d (-phy, -dev)
	if (info->physical_dev || info->device) {
		info->dev_mask = vediag_get_dev_mask_from_name(info, info->physical_dev, info->device);
		if (info->dev_mask == 0) {
			xinfo("ERROR: No device selected from name %s \"%s\" - check test config file\n",
					(info->physical_dev ? "physical name" : "device name"), (info->physical_dev ? info->physical_dev : info->device));
			vediag_update_result(info->stat, VEDIAG_NODEV, 100, "No selected devices");
			goto complete;
		}
	}

	for(dev = 0; dev < dev_test_total; dev++) {
		if (((0x1 << dev) & info->dev_mask) == 0) {
			continue;
		}
		dev_info = malloc(sizeof(struct vediag_test_info));
		if(IS_ERR_OR_NULL(dev_info)) {
			vediag_err(test->type, NULL, "Out of memory to start test.\n");
			continue;
		}
		memset(dev_info, 0 , sizeof(struct vediag_test_info));
	    INIT_LIST_HEAD(&dev_info->arguments);
		dev_info->dev_mask = dev + 1; // Count from 1
		/* Copy information */
		dev_info->test_type = info->test_type;

		if (vediag_parse_test_config(info->cfg_file, test, dev_info) < 0) {
			xerror("No device or physical defined\n");
			continue;
		}

		/* Check enable test before jumping device check */
		char *str = vediag_get_argument(dev_info, "Enable");
		if (str && strlen(str) && (strcasecmp(str, "No") == 0))	{
			xinfo("%s %d %s - %s was Disable !!\n", __func__, __LINE__,
						dev_info->device ? dev_info->device : "Unknown",
						dev_info->physical_dev ? dev_info->physical_dev : "Unknown");
			stat = vediag_add_new_status(info->stat->mtid, info->stat->stid,
						dev_info->device ? dev_info->device : "Unknown",
						dev_info->physical_dev ? dev_info->physical_dev : "Unknown");
			vediag_update_result(stat, VEDIAG_NOERR, 0, "Disable");
			free(str);
			continue;
		}

		if (str)
			free(str);

		if (!dev_info->device) {
			stat = vediag_add_new_status(info->stat->mtid, info->stat->stid, "No device",
							dev_info->physical_dev ? dev_info->physical_dev : "Unknown");
			vediag_update_result(stat, VEDIAG_NODEV, 0, "Device not found");
			continue;
		}

		stat = vediag_add_new_status(info->stat->mtid, info->stat->stid,
				dev_info->deviced ? dev_info->deviced : dev_info->device ? dev_info->device : "Unknown",
						dev_info->physical_dev ? dev_info->physical_dev : "Unknown");
		if (!stat) {
			continue;
		}
		stat->test_type = test->type;
		dev_info->stat = stat;

		/*Setup Test device dbg info*/
		vediag_setup_device_dbg_info(dev_info);

		if(test->check_dev) {
			error = NULL;
			if(test->check_dev(info, dev_info, &error)) {
				if (dev_info->threads <= 0)
					dev_info->threads = 1;
				vediag_update_status_detail(dev_info->stat, dev_info->application, dev_info->threads);
				test_data = (struct vediag_test_thread_info *) malloc(sizeof(struct vediag_test_thread_info));
				if(IS_ERR_OR_NULL(test_data)) {
					continue;
				}
				test_data->info = dev_info;
				test_data->test = test;
				pthread_attr_t attr;
				
				pthread_attr_init(&attr);
				pthread_attr_setstacksize(&attr, 2 * PTHREAD_STACK_MIN);
				ret = pthread_create(&thread, &attr, vediag_test_detail_thread, (void *)test_data);
				pthread_attr_destroy(&attr);
				pthread_detach(thread);
				
				if (ret){
					vediag_err(test->type, dev_info->dbg_info, "Create execute thread failed.\n");
					vediag_update_result(dev_info->stat, VEDIAG_THREAD, 0, "Create thread failed");
					if(test_data->test && test_data->test->test_complete) {
						test_data->test->test_complete(dev_info->data);
					}
					continue;
				}
			}
			else {
				xerror("Check device failed '%s'\n", error);
				vediag_update_result(dev_info->stat, VEDIAG_NODEV, 0, error);
			}
			if (error)
				free(error);
		}
		else {
			vediag_update_result(dev_info->stat, VEDIAG_NODEV, 0, "Missing checking device");
			vediag_free_test_info(dev_info);
		}
	}

    /* Wait for all child thread complete */
    while(vediag_test_running(info->stat)) {
        /* Check if has suspend */
        if(info->stat->should_suspend) {
            info->stat->should_suspend = 0;
            //TODO: suspend thread
        }
        sleep(1);
        info->stat->flags |= VEDIAG_TEST_STATE_CHANGED;
        percent = vediag_get_parent_status(info->stat);
        vediag_update_status(info->stat, -1, -1, percent, VEDIAG_REPORT_EXCUTING);
    }

    /* Update final status */
    if(info->stat->should_stop) {
        vediag_update_result(info->stat, VEDIAG_STOP, percent, "Test stopped");
        vediag_info(info->stat->test_type, NULL, "Test stopped by request\n");
    }
    else if(vediag_get_parent_result(info->stat) != VEDIAG_NOERR) {
        vediag_update_result(info->stat, VEDIAG_TEST_ERR, -1, "Get failed");
        vediag_err(info->stat->test_type, NULL, "Got test failed\n");
    } else {
        percent = vediag_get_parent_status(info->stat);
        vediag_update_result(info->stat, VEDIAG_NOERR, percent, "All passed");
        vediag_info(info->stat->test_type, NULL, "All test passed\n");
    }

complete:
	ip_sysinfo(info->stat->test_type);
    vediag_info(info->stat->test_type, NULL, "ALL TEST DONE\n");
    info->stat->performance = info->performance;
    vediag_complete_test(info->stat);
    vediag_free_test_info(info);
    if (data)
    	free(data);
    pthread_exit(NULL);
}

static void vediag_process_request(struct vediag_test_request *request)
{
	struct vediag_test_thread_info *data;
	struct vediag_test_info *info;
	struct vediag_test_device *test;
	struct vediag_test_status *stat;
	int ret;
	pthread_t thread;

	if (!request)
		return;

	if(request->state == REQUEST_NOT_STARTED)
		gettimeofday(&request->start_time, NULL);

	info = request->info;
	switch(request->header.cmd) {
	/* Start new test */
	case VEDIAG_CMD_REQUEST_START_TEST:
		if (request->state == REQUEST_COMPLETED) {
			vediag_process_test_done(request);
			break;
		}
		test = get_test_by_type(request->header.dtyp);
		if (test != NULL) {
			if (request->state == REQUEST_NOT_STARTED){
				stat = vediag_add_new_status(request->header.mtid, request->header.stid, test->name, test->desc);
				if (!stat || IS_ERR(stat)) {
					xerror("Can't excute %s test \n", test->name);
					return;
				}
				stat->test_type = test->type;
				data = malloc(sizeof(struct vediag_test_thread_info));
				if(IS_ERR_OR_NULL(data)) {
					xerror("No memory to allocate test data\n");
					vediag_update_result(stat, VEDIAG_NOMEM, 0, "No memory");
					return;
				}
				data->test = test;
				data->info = info;
				if (!info->cfg_file) {
					info->cfg_file = vediag_parse_system("Test default");
					xinfo("VEDIAG_TEST_CFG: %s\n", info->cfg_file);
				}
				if (!info->cfg_file) {
					xerror("No test config found\n");
					vediag_update_result(stat, VEDIAG_NOCFG, 0, "No config");
					return;
				}
				if((strncmp(info->cfg_file, file_test_default, strlen(file_test_default)) == 0) && 
					(access(file_test_default, 0) != 0)) {
					info->cfg_file = strdup(file_testconfig);
				}

				info->stat = stat;

				/* Call test function */
				request->runtimes++;
				pthread_attr_t attr;
				
				pthread_attr_init(&attr);
				pthread_attr_setstacksize(&attr, 2 * PTHREAD_STACK_MIN);
				ret = pthread_create(&thread, &attr, vediag_test_start, (void *)data);
				pthread_attr_destroy(&attr);
				pthread_detach(thread);
				
				if (ret) {
					return;
				}
				request->flags |= CONFIG_REQUEST_IN_PROCESS;
				request->state = REQUEST_IN_PROGRESS;
			}
		}
		else {
			xerror("Don't support %s test\n", vediag_get_IP_name(request->header.dtyp));
		}
		break;
	case VEDIAG_CMD_REQUEST_SUSP_TEST:
		/* Suspend all test */
		if(request->header.stid == 0xFFFF) {
			vediag_test_suspend(-1);
		}
		else {
			vediag_test_suspend((int)request->header.stid);
		}

		list_del(&request->request);
		break;
	case VEDIAG_CMD_REQUEST_RESM_TEST:
		/* Suspend all test */
		if(request->header.stid == 0xFFFF) {
			vediag_test_resume(-1);
		}
		else {
			vediag_test_resume((int)request->header.stid);

		}
		list_del(&request->request);
		break;
	case VEDIAG_CMD_REQUEST_STOP_TEST:
		/* Suspend all test */
		if(request->header.stid == 0xFFFF) {
			vediag_test_stop(-1);
		}
		else {
			vediag_test_stop((int)request->header.stid);

		}
		list_del(&request->request);
		break;
	default:
		list_del(&request->request);
		break;
	}
}

static void *vediag_test_manager(void *__data)
{
	struct vediag_test_request *request;
	struct list_head *req_list;

	for(;;)
	{
		/* wait for a request coming */
		pthread_mutex_lock(&vediag_control->request_mutex);
		//pthread_cond_wait(&vediag_control->has_request, &vediag_control->request_mutex);

		for(req_list = vediag_control->request_list.next ; req_list != &vediag_control->request_list; ) {

			request = container_of(req_list, struct vediag_test_request, request);
			req_list = req_list->next;

			if(request && (request->state != REQUEST_IN_PROGRESS)) {
				if ((request->flags & CONFIG_REQUEST_SEQUENCE) == CONFIG_REQUEST_SEQUENCE) {
					if(request->queue_number != vediag_control->current_req) {
						continue;
					}
				}
				/* Process for request */
				vediag_process_request(request);
			}
		}
		pthread_mutex_unlock(&vediag_control->request_mutex);
//		schedule();
		sleep(0);
	}

	pthread_exit(NULL);
}
#ifdef CONFIG_VEDIAG_SENSORS
extern void vediag_sensor_print(const char *name, int print);
#endif

#define VEDIAG_PACKET_DUPLICATE_FOR_TEST		1
static void *vediag_status_manager(void *__data)
{
	struct vediag_test_status *stat_entry = NULL, *stat_ptr;
	struct list_head *stat_list;

	for(;;)
	{
		/* Monitor sensors */
		if ((is_diag_running() == true) && ((vediag_control->sensor_interval > 0) && (vediag_control->sensor_cnt >= vediag_control->sensor_interval))) {
			vediag_control->sensor_cnt = 0;
			pthread_mutex_lock(&vediag_control->sensor_mutex);
			if (vediag_control->sensor_name) {
#ifdef CONFIG_VEDIAG_SENSORS
				vediag_sensor_print(vediag_control->sensor_name, 0);
#endif
			}
			pthread_mutex_unlock(&vediag_control->sensor_mutex);
		}

		for(stat_list = vediag_control->status_list.next ; stat_list != &vediag_control->status_list; ) {
			stat_entry = container_of(stat_list, struct vediag_test_status, status);
			stat_list = stat_list->next;

			if(stat_entry && stat_entry->finish != 0) {
				/* Process for report */
				vediag_report_proc(stat_entry);
				/* Check for child stat */
				if(stat_entry->child != NULL) {
					for( stat_ptr = stat_entry->child ; stat_ptr != NULL ; stat_ptr = stat_ptr->next) {
						vediag_report_proc(stat_ptr);
					}
				}
			}
		}
		vediag_control->sensor_cnt++;
//		schedule();
		sleep(1);
	}

	pthread_exit(NULL);
}

int stop_test_func(int count, int key)
{
	struct vediag_test_request *request;

	request = malloc(sizeof(struct vediag_test_request));
	if(IS_ERR_OR_NULL(request)) {
		xerror("Alloc new request failed\n");
		return 1;
	}
	memset(request, 0, sizeof(struct vediag_test_request));

	request->header.cmd = VEDIAG_CMD_REQUEST_STOP_TEST;
	request->header.mtid = vediag_control->mid;
	request->header.stid = 0xFFFF;

	return vediag_request_add(request, 0);
}

int keyinput_yes = 0;
int yes_test_func(int count, int key)
{
	xdebug("yes_test_func: count=%d, key='%c'\n", count, key);
	keyinput_yes += count;
	return 0;
}

int keyinput_no = 0;
int no_test_func(int count, int key)
{
	xdebug("no_test_func: count=%d, key='%c'\n", count, key);
	keyinput_no += count;
	return 0;
}

/*
 * @device name
 * @remind number of message to remind user to press key
 * @interval delay between 2 remind (second)
 *
 * @return 0: no key press
 * @return 1: Yes key
 * @return 2: No key
 */
int check_key_result(int IP, char *device, int remind, int interval)
{
	while(remind--) {
		vediag_info(IP, device, "Pass/Fail ?\n  Press key: F8 or F9\n");
		sleep(interval);
		if(keyinput_yes > 0) {
			keyinput_yes = 0;
			keyinput_no = 0;
			vediag_info(IP, device, "PASS\n");
			return 1;
		} else if(keyinput_no > 0) {
			keyinput_no = 0;
			keyinput_yes = 0;
			vediag_info(IP, device, "FAIL\n");
			return 2;
		}
	}
	return 0;
}

/*
 * Init vediag main controller
 */
int vediag_test_manager_init(void)
{
	int ret;
	char *val;
	vediag_control = (struct vediag_man_data *)malloc(sizeof(struct vediag_man_data));
	if (IS_ERR_OR_NULL(vediag_control)) {
		xerror("Alloc VED main control data failed\n");
		goto done;
	}
	memset(vediag_control, 0, sizeof(struct vediag_man_data));

	gettimeofday(&vediag_control->start_time, NULL);
	vediag_control->mid = -1;

	/* Init request manager */
	xinfo("Start VED request manager ... ");
	vediag_control->current_req = 0;
	vediag_control->req_in_sequence = 0;
	INIT_LIST_HEAD(&vediag_control->request_list);
	pthread_cond_init (&vediag_control->has_request, NULL);
	pthread_mutex_init(&vediag_control->request_mutex, NULL);
	ret = pthread_create(&vediag_control->request_manager, NULL, vediag_test_manager, (void *)vediag_control);
	if (ret) {
		xinfo("failed\n");
	}
	else {
		xinfo("done\n");
	}

	/* Init status list */
	xinfo("Start VED status manager ... ");
	INIT_LIST_HEAD(&vediag_control->status_list);
	pthread_cond_init (&vediag_control->update_status, NULL);
	pthread_mutex_init(&vediag_control->status_mutex, NULL);
	vediag_control->status_num = 0;
	ret = pthread_create(&vediag_control->status_manager, NULL, vediag_status_manager, (void *)vediag_control);
	if (ret) {
		xinfo("failed\n");
	}
	else {
		xinfo("done\n");
	}
	vediag_gui_connect(0);

	/*
	 * Check INI file
	 */
	vediag_check_system_config();
	vediag_check_test_config();

	/* Get system config */
	vediag_control->sensor_interval = 0;
	val = vediag_parse_system(VEDIAG_SENSOR_INTERVAL_KEY);
	if (val) {
		vediag_control->sensor_interval = strtoul(val, NULL, 0);
		free (val);
	}
	vediag_control->sensor_name = vediag_parse_system(VEDIAG_SENSOR_NAME_KEY);
	xinfo("Sensor interval: %d \n", vediag_control->sensor_interval);
	rl_bind_keyseq(STOP_KEY_SEQ, stop_test_func);
	rl_bind_keyseq(YES_KEY_SEQ, yes_test_func);
	rl_bind_keyseq(NO_KEY_SEQ, no_test_func);
	vediag_control->init_done = 1;
done:
	return 0;
}

/*
 * Test request
 */

static void vediag_test_help(void)
{
	struct vediag_test_device *test;

	xinfo("\nAvaialble diag test:\n");
	pthread_mutex_lock(&test_list_mutex);
	list_for_each_entry(test, &vediag_test_list, list){
		xinfo ("  %-15s - %s \n", test->cmd, test->name);
	}
	pthread_mutex_unlock(&test_list_mutex);
}

static int _vediag_test_parse_option(struct vediag_test_info *info, int argc, char *argv[])
{
	int arp;

	/* Check config_file option */
	if ((arp = vediag_getopt(argc, argv, "-f")) > 0){
		if ((arp + 1) < argc) {
			info->cfg_file = strdup(argv[arp + 1]);
		}
	}

	/* Check threads option */
	info->threads = 0;
	if ((arp = vediag_getopt(argc, argv, "-t")) > 0){
		if ((arp + 1) < argc) {
			info->threads = strtoul(argv[arp + 1], NULL, 0);
		}
	}

	/* Check running time option */
	info->runtime = 0;
	if ((arp = vediag_getopt(argc, argv, "-time")) > 0){
		if ((arp + 1) < argc) {
			info->runtime = strtoul(argv[arp + 1], NULL, 0);
		}
	}

	/* Check performance option */
	info->performance = 0;
	if ((arp = vediag_getopt(argc, argv, "-pf")) > 0){
		info->performance = 1;
	}

	return 0;
}

static int _vediag_test_execute(struct vediag_test_device *test, int argc, char *argv[])
{
	struct vediag_test_request *request;
	int format_size = sizeof(struct vediag_test_info);
	int arp;
	int sequence = 0;
	struct vediag_test_info *info = NULL;

	info = malloc(sizeof(struct vediag_test_info));
	if(IS_ERR_OR_NULL(info)) {
		xerror("No memory to allocate test information\n");
		return 1;
	}
	memset(info, 0, sizeof(struct vediag_test_info));
	INIT_LIST_HEAD(&info->arguments);
	/* Parse default option */
	_vediag_test_parse_option(info, argc, argv);

	if(test->test_start){
		if(!test->test_start(argc, argv, info)) {
		    free(info);
			return 1;
		}
		info->test_type = test->type;
	}
	else {
	    xerror("No function to execute test %s\n", test->name);
	    vediag_free_test_info(info);
		return 1;
	}

	request = malloc(sizeof(struct vediag_test_request));
	if(IS_ERR_OR_NULL(request)) {
		xerror("Out of memory\n");
		vediag_free_test_info(info);
		return 1;
	}
	memset(request, 0, sizeof(struct vediag_test_request));
	request->info = info;

	request->header.loop = 1;

	if ((arp = vediag_getopt(argc, argv, "-q")) > 0){
		sequence = 1;
	}

	request->header.cmd = VEDIAG_CMD_REQUEST_START_TEST;
	request->header.size = format_size;
	request->header.dtyp = test->type & 0xFF;
	request->header.mtid = vediag_control->mid;
	request->header.stid = vediag_control->req_num++;

	xinfo("Request diag %s function \n", test->cmd);
	diag_running = true;
#ifdef CONFIG_RAS
	/* Clear RAS error in System queue */
	double start_time = get_time_sec();
	while (!cbIsEmpty(&ras_cb_system)) {
		ElemType *e;
		e = calloc(1, sizeof(ElemType));
		if (!e)
			break;

		cbRead(&ras_cb_system, e);
		free(e);

		/* break if error is bumped too fast*/
		if (get_time_sec() - start_time > 3)
			break;
	}
#endif
	return vediag_request_add(request, sequence);
}

static int _vediag_test_resume(int argc, char *argv[])
{
	struct vediag_test_request *request;

	if(argc < 3)
		return 1;

	request = malloc(sizeof(struct vediag_test_request));
	if(IS_ERR_OR_NULL(request)) {
		xerror("Alloc new request failed\n");
		return 1;
	}
	memset(request, 0, sizeof(struct vediag_test_request));

	request->header.cmd = VEDIAG_CMD_REQUEST_RESM_TEST;
	request->header.mtid = vediag_control->mid;

	if(strcmp(argv[2], "all") == 0) {
		request->header.stid = 0xFFFF;
	}
	else {
		request->header.stid = strtoul(argv[2], NULL, 0);
	}

	return vediag_request_add(request, 0);
}

static int _vediag_test_suspend(int argc, char *argv[])
{
	struct vediag_test_request *request;

	if(argc < 3)
		return 1;

	request = malloc(sizeof(struct vediag_test_request));
	if(IS_ERR_OR_NULL(request)) {
		xerror("Alloc new request failed\n");
		return 1;
	}
	memset(request, 0, sizeof(struct vediag_test_request));

	request->header.cmd = VEDIAG_CMD_REQUEST_SUSP_TEST;
	request->header.mtid = vediag_control->mid;

	if(strcmp(argv[2], "all") == 0) {
		request->header.stid = 0xFFFF;
	}
	else {
		request->header.stid = strtoul(argv[2], NULL, 0);
	}

	return vediag_request_add(request, 0);
}

static int _vediag_test_stop(int argc, char *argv[])
{
	struct vediag_test_request *request;

	if(argc < 3)
		return 1;

	request = malloc(sizeof(struct vediag_test_request));
	if(IS_ERR_OR_NULL(request)) {
		xerror("Alloc new request failed\n");
		return 1;
	}
	memset(request, 0, sizeof(struct vediag_test_request));

	request->header.cmd = VEDIAG_CMD_REQUEST_STOP_TEST;
	request->header.mtid = vediag_control->mid;

	if(strcmp(argv[2], "all") == 0) {
		request->header.stid = 0xFFFF;
	}
	else {
		request->header.stid = strtoul(argv[2], NULL, 0);
	}

	return vediag_request_add(request, 0);
}

int _vediag_print_log(void)
{
	int c;
	FILE *file;

#ifdef CONFIG_VEDIAG_TEST_GUI
	if (vediag_check_gui_mode()) {
		return 1;
	}
#endif
	file = fopen("diag.log", "r");
	xinfo("===== Diag Log ====\n");
	if (file) {
	    while ((c = getc(file)) != EOF)
	        xinfo("%c", c);
	    fclose(file);
	}
	xinfo("==================\n");
	return 0;
}

int vediag_test_execute (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	struct vediag_test_device *test;
	int ret = 0;

	if (argc <= 1) {
		goto help;
	}

	/* Suspend test running */
	else if(strcmp(argv[1], "sus") == 0) {
		if(_vediag_test_suspend(argc, argv) != 0)
			goto help;

		return 0;
	}
	/* Resume test susending */
	else if(strcmp(argv[1], "rsm") == 0) {
		if(_vediag_test_resume(argc, argv) != 0)
			goto help;

		return 0;
	}

	else if(strcmp(argv[1], "stop") == 0) {
		if(_vediag_test_stop(argc, argv) != 0)
			goto help;

		return 0;
	}

	else if(strcmp(argv[1], "log") == 0) {
		return _vediag_print_log();
	}

	else if(strcmp(argv[1], "all") == 0) {
/*		if ((arp = vediag_getopt(argc, argv, "-r")) > 0){
			vediag_control->report_status = 1;
		}*/
		pthread_mutex_lock(&test_list_mutex);
		list_for_each_entry(test, &vediag_test_list, list){
			/* Check to skip manual test */
			if(test->flags & VEDIAG_TEST_MANUAL)
				continue;
			ret += _vediag_test_execute(test, argc, argv);
		}
		pthread_mutex_unlock(&test_list_mutex);
		return ret;
	}
	else {
		test = get_test_by_name(argv[1]);
		if(test == NULL) {
			goto help;
		}
		ret += _vediag_test_execute(test, argc, argv);
		return ret;
	}

help:
	vediag_test_help();
	xprint ("Usage:\n%s\n", cmdtp->usage);
	return 1;
}

int is_diag_running(void)
{
	return diag_running;
}

void vediag_setup_device_dbg_info(struct vediag_test_info *dev_info)
{
	int mess_length = 0;
	int max_length = MAX_MESS_DBG_INFO - 1;

	memset(dev_info->dbg_info, 0x0, sizeof(dev_info->dbg_info));

	if (dev_info->physical_dev != NULL) {
		snprintf(&dev_info->dbg_info[mess_length], max_length, "%s", dev_info->physical_dev);
		mess_length += strlen(dev_info->physical_dev);
		max_length -= mess_length;
	}

	if (dev_info->device != NULL) {
		snprintf(&dev_info->dbg_info[mess_length], max_length, " %s", dev_info->device);
		mess_length += strlen(dev_info->device) + 1;
		max_length -= mess_length;
	}
}

VEDIAG_CMD(
	diag,	32,	0,	vediag_test_execute,
    "perform board diagnostics in GUI mode",
    "     => print list of available tests\n"
	"diag all [arg ...]                  - diag all function with arg\n"
	"diag test_name [test_arg ...]       - diag a function with arg\n"
	"diag test_name -h                   - print test function help\n"
    "diag sus [all | ID]                 - suspend all test or test with ID\n"
    "diag rsm [all | ID]                 - suspend all test or test with ID\n"
    "diag stop [all | ID]                - stop all test or test with ID\n"
	"diag log                            - show Logfile of printk history\n"
)

subsys_initcall(vediag_test_manager)
{
	return vediag_test_manager_init();
}
