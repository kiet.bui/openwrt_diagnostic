#!/bin/bash

ROOT_UID="0"
INI__SYSTEM__ENABLE_REMOVE_SPECIAL_CHAR=1

#Check if run as root
if [ "$UID" -ne "$ROOT_UID" ] ; then
	echo "You must be root to do that!"
	exit 1
fi
#Setup UART 
agrv1=$1
agrv2=$2
agrv3=$3
agrv4=$4
if [ $# -ne 4 ]
then
	echo "Help:"
	echo "  $1=CONSOLE $2=BOARDNAME $3=NPSScript $4=Cycle"
	echo "  EX:./diagwrt_pwrcycl_test.sh 0 Venus_012 ./s4powercycle.sh 1000"
	exit 1
fi
############################Check & install package###################
which expect
if [ $? -eq 1 ]  ; then
	sudo apt-get install expect
fi
which kermit
if [ $? -eq 1 ]  ; then
	sudo apt-get install ckermit
fi
############################Loop NPS 100 Time   ######################
localtime=`date "+$F-%Y-%m-%d-%T"`;
echo agrv $agrv1 $agrv2 $agrv3 $agrv4
for loop in $(seq 1 $agrv4)
do
	agrv2=$2
	UART_BASE=$agrv1
	Time=`date "+$F-%Y-%m-%d-%T"`;
	Screen_name=$agrv2
	BOARD=diagwrt-$agrv2-at-$loop${Time//:/}
	agrv2=$agrv2${localtime//:/}
	logdir=~/diagwrtlog/diagwrt-$agrv2/
	mkdir -p $logdir
	sumfile=$logdir/diagwrt-$agrv2.log
	sudo chmod -R 777 /dev/ttyUSB*

	#Gen tty for kermit
	CONSOLE=ttyUSB$UART_BASE

	echo "#!/usr/bin/expect                                                  " >  xconsole
	echo "set ttyUSB [lindex \$argv 0]                                       " >> xconsole
	echo "set logname [lindex \$argv 1]                                      " >> xconsole
	echo "system \"echo set line /dev/\$ttyUSB    > ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo set speed 115200         >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo set carrier-watch off    >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo set handshake none       >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo set flow-control none    >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo robust                   >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo set file type bin        >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo set file name lit        >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo set rec pack 1000        >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo set send pack 1000       >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "system \"echo set window 5             >> ./.kermit_\$ttyUSB\"     " >> xconsole
	echo "                                                                   " >> xconsole
	echo "set timeout 10                                                     " >> xconsole
	echo "log_file ~/diagwrtlog/\$logname                                    " >> xconsole
	echo "exp_spawn kermit -c -y ./.kermit_\$ttyUSB                          " >> xconsole
	echo "expect {                                                           " >> xconsole
	echo "  \"to see other options.\" {}                                     " >> xconsole
	echo "  -re \"Locked by process (\\\d+)\"  {                             " >> xconsole
	echo "      set pid \$expect_out(1,string);                              " >> xconsole
	echo "      send_user \"\$ttyUSB is locked by PID \$pid, killing...\\n\";" >> xconsole
	echo "      exec kill -9 \$pid;                                          " >> xconsole
	echo "      sleep 1;                                                     " >> xconsole
	echo "      exp_spawn kermit -c -y ./.kermit_\$ttyUSB                    " >> xconsole
	echo "  }                                                                " >> xconsole
	echo "  timeout {send_user \"\\nError: timeout\\n\" ; exit 2 }           " >> xconsole
	echo "}                                                                  " >> xconsole
	echo "interact                                                           " >> xconsole
	chmod -R 777 xconsole
	sudo chmod -R 777 /dev/ttyUSB*  > /dev/null 2>&1

	#Kill The xconsole for restart power cycle
	function kill_xconsole()
	{
	local i
	for i in $(sudo ps aux  |  grep -i "$1$" | grep -v "grep" | awk '{print $2}')
	do
		sudo kill -9 "$i" > /dev/null
	done
	}
	function display_info(){
		echo "$1"
		echo "$1" >> $sumfile
	}
	#Kill The xconsole for restart power cycle
#	echo "Kill0 input>"
#	sudo ps aux  |  grep -i "$CONSOLE"   | grep -v "grep"|  awk '{print $2}'
#	echo "Kill0 input<"
#	sudo ps aux  |  grep -i "$CONSOLE"   | grep -v "grep"|  awk '{print $2}'  |sudo xargs  kill -9 
#	sleep 1
	kill_xconsole "$CONSOLE";sleep 5
	if [ $loop -eq 1 ]  ; then
		#Kill screen
#		echo "Kill1 input>"
#		sudo screen -ls | grep $Screen_name | cut -d. -f1 | awk '{print $1}'
#		echo "Kill1 input<"
		sudo screen -ls | grep $Screen_name | cut -d. -f1 | awk '{print $1}' | sudo xargs kill  >>/dev/null
		sleep 0.5
		screen -wipe > /dev/null
		sleep 5
		echo create Screen Name $Screen_name
		screen -AdmS $Screen_name -t CONSOLE
		sleep 1
		screen -S $Screen_name -X screen -t SUM
		sleep 1
	fi	
	sleep 2
	screen -S $Screen_name  -p0 -X stuff "./xconsole $CONSOLE  diagwrt-$agrv2/$BOARD-CONSOLE.log\n"
	sleep 2
#	if [ ! -f "~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log" ] ; then
#		# open console and create log fail, retry
#		display_info " Warning: create CONSOLE log failed, retry...\n"
#		screen -S $Screen_name  -p0 -X stuff "./xconsole $CONSOLE  diagwrt-$agrv2/$BOARD-CONSOLE.log\n"
#		sleep 1
#	fi

	#NPS
	echo "sudo screen -xr $Screen_name #Login to console"
	echo NPS....$agrv3
	./$agrv3 > /dev/null
	#./$agrv3
	
	#Retry one-more time
	screen -S $Screen_name  -p0 -X stuff "./xconsole $CONSOLE  diagwrt-$agrv2/$BOARD-CONSOLE.log\n"

	echo Loop at $loop 
	echo "NPS at @ $loop $Time `date`" >>  $sumfile
	screen -S $Screen_name  -p1 -X stuff "watch tail -50 $sumfile\n";sleep 1
	echo "Logfile:"		
	ls -l ~/diagwrtlog/diagwrt-$agrv2/*$BOARD*
	
	################################function ##############################
	function grep_test_by_timeout() {
	#$1 Text seach
	#$2 File
	#$3 Timeout
	#$4 Note to logfile
	for i in $(seq 1 $3) 
	do	
	if [ `grep "$1" -R -a  $2 |wc -l` -eq 1 ]  ; then
		echo "$4 PASS"
		echo "$4 PASS" >> $sumfile
		return 1
	else
		sleep 1
	fi
	done
	if [ $i -eq $3 ]  ; then	
		echo "$4 FAIL"
		echo "$4 FAIL" >> $sumfile
		return 0
	fi
	}
	function draw_plot() {
		rm -rf ./.$2
		timebase=0
		Sensor_interval=`grep "Sensor interval:" -R -a $logdir/$BOARD-CONSOLE.log | awk '{print $3}'`
		for SoC_Temp in `grep "$1" -R -a $logdir/$BOARD-CONSOLE.log  | awk '{print $5}'`
		do
			echo "$timebase,  $SoC_Temp">> "./.$2"
			timebase=`expr $timebase + $Sensor_interval`
		done
		gnuplot -persist <<-EOFMarker
			set term dumb
			plot "./.$2" with lines
		EOFMarker
	}
	function grep_result(){
		###############Done Test & Grep Result ################################
		echo `grep "Error:"        -R -a  $logdir/$BOARD-* `		>>  $sumfile
		echo `grep "diagwrt_ERROR:"  -R -a  $logdir/$BOARD-* `		>>  $sumfile
		echo `grep "Test fail"     -R -a  $logdir/$BOARD-* `		>>  $sumfile

		##############Count power cycle        #################################
		display_info "Total NPS     :`grep "NPS at @ " -R -a $sumfile |wc -l`"
		display_info "##################################################################"
		#################DIAG TEST DONE ALL######################################
	}
	function remove_special_character()
	{
		if [ ${INI__SYSTEM__ENABLE_REMOVE_SPECIAL_CHAR} -eq 1 ] ; then
			if [ -f $1 ] ; then
				/bin/bash -c "sed $'s/[^[:print:]\t]//g' $1 > $1.tmp"
				/bin/bash -c "mv $1.tmp $1"
			fi
		fi

		return 0
	}
	
	#Flow by log
	grep_test_by_timeout "U-Boot 2015.04" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 20  "Uboot bootup"
	if [ $? -eq 0 ]  ; then	
		grep_result
		remove_special_character ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log
		remove_special_character $sumfile
		screen -S "$Screen_name" -p0 -X stuff "1:Continue at-"$loop$'\n'
		continue
#exit 1
	fi
#sleep 1;continue
	grep_test_by_timeout "Please press Enter " ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 20  "Openwrt bootup"
	if [ $? -eq 0 ]  ; then	
		grep_result
		remove_special_character ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log
		remove_special_character $sumfile
		screen -S "$Screen_name" -p0 -X stuff "2:Continue at-"$loop$'\n'
		continue
	fi
	
	sleep 20
	# Login OpenWRT
	screen -S $Screen_name  -p0 -X stuff $'\n'
	grep_test_by_timeout "OpenWrt login:" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 20  "Openwrt login prompt"
	if [ $? -eq 0 ]  ; then	
		grep_result
		remove_special_character ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log
		remove_special_character $sumfile
		screen -S "$Screen_name" -p0 -X stuff "3:Continue at-"$loop$'\n'
		continue
	fi
	screen -S $Screen_name -p0 -X stuff "root"$'\n';sleep 5
	screen -S $Screen_name -p0 -X stuff "VEriK"$'\n';sleep 2
#continue	
	# Launch test script
	screen -S "$Screen_name" -p0 -X stuff "cd /etc/diagwrt"$'\n'
	sleep 1
	screen -S "$Screen_name" -p0 -X stuff "./venus_setup.sh"$'\n'
	sleep 1
	
	grep_test_by_timeout "Auto command:" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 120  "diagwrt command started"
	if [ $? -eq 0 ]  ; then	
		grep_result
		remove_special_character ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log
		remove_special_character $sumfile
		screen -S "$Screen_name" -p0 -X stuff "4:Continue at-"$loop$'\n'
		continue
	fi
	
	grep_test_by_timeout "DIAG TEST DONE ALL" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 600  "Test diagwrt done"
	grep_test_by_timeout "I2C TEST PASSED" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 10  "I2C TEST"
	grep_test_by_timeout "BLUETOOTH TEST PASSED" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 10  "BLUETOOTH TEST"
	grep_test_by_timeout "STORAGE TEST PASSED" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 10  "STORAGE TEST"
	grep_test_by_timeout "MEMORY TEST PASSED" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 10  "MEMORY TEST"
	grep_test_by_timeout "========= NET TEST PASSED ==========" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 10  "NET TEST"

	# Performance test
	screen -S "$Screen_name" -p0 -X stuff "diag net -pf -dev wlan0"$'\n'
	grep_test_by_timeout "NET PERFORMANCE TEST PASSED" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 120  "NET IPERF"

	# Manual test
	screen -S "$Screen_name" -p0 -X stuff "diag led"$'\n'
	screen -S "$Screen_name" -p0 -X stuff "diag micro;diag audio"$'\n'
	grep_test_by_timeout "LED  ALL TEST DONE" ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log 180  "Manual LED"
	
	grep_result

	remove_special_character ~/diagwrtlog/diagwrt-$agrv2/$BOARD-CONSOLE.log
	remove_special_character $sumfile
done