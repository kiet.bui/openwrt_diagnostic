#
# Sed script to parse CPP macros and generate output usable by make
#
# It is expected that this script is fed the output of 'gpp -dM'
# which preprocesses the common.h header files and outputs the final
# list of CPP macros (and whitespace is sanitized)
#

# Only process values prefixed with #define CONFIG_
/^CONFIG_[A-Za-z0-9_][A-Za-z0-9_]*/ {
	# Strip the #define prefix
	#s/#define *//;
	# Change to form CONFIG_*=VALUE
	s/=[[:punct:][:alnum:][:blank:][:cntrl:][:graph:][:print:][:space:]]*/=/;	
	p
}