#!/bin/bash
if [ ! -f $1 ]
then
    echo "Input file [$1] not found"
    exit
fi

filename=$1
size=$2
filesize=`wc -c <"$filename"`

if [ $size -le $filesize ]
then
    echo "No Pad size: $size, $filename: $filesize"
    exit
fi

#echo "Pad $filename: $filesize to size: $size"

let "filesize = $size - $filesize"

echo "Pad size: $filesize"

dd if=/dev/zero of=tmp.bin bs=1 count=$filesize

if [ $3 == "top" ]
then
    cat $filename tmp.bin > temp.bin
    mv temp.bin $filename
else
    cat tmp.bin $filename > temp.bin
    mv temp.bin $filename
fi

rm -f tmp.bin
filesize=`wc -c <"$filename"`

echo "New $filename: $filesize"