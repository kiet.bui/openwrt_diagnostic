#!/bin/bash
#size=`wc -c <"$1"`
#echo -ne "$1 $size " >MAP
#echo $#
c=3
filename=$1
typeboot=$2
rm -rf $filename
if [ $typeboot == spi ]
then
	echo -n "slimpro  512 " >  $filename
fi
if [ $typeboot == sdio ]
then
	echo -n "slimpro  512 " >  $filename
fi
while [ $c -le $# ]
do
	if [ $filename == $(basename ${!c}) ]
	then
		echo -n "$(basename $filename) 512 " >> $filename
	else
		size=`wc -c <${!c}`
		size_cat=512
		let "sizek = $size / $size_cat"
		let "sizeb = $size % $size_cat"
		if [ $sizeb -eq 0 ]
		then
			echo " "
		else
			let "tmpsizefile = $size_cat - $sizeb"
			dd if=/dev/zero of=filetmp bs=1 count=$tmpsizefile
			cat ${!c} filetmp > ${!c}.tmp
			mv ${!c}.tmp ${!c}
			echo ${!c}
		fi

		size=`wc -c <${!c}`
		echo -n "$(basename ${!c}) $size " >> $filename
	fi
		 ((c++))
done
	echo -ne '\0' >> $filename

./tools/scripts/cat_file_size.sh  $filename 512 top
