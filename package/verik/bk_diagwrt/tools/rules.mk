#
# Makefile for udiag.
#
#########################################################################

CC	= $(CROSS_COMPILE)gcc
gccincdir := $(shell $(CC) -print-file-name=include)
PLATFORM_CPPFLAGS += -DCONFIG_POWERPC -D__powerpc__ -DCONFIG_BOOT1
CPPFLAGS += -I$(TOPDIR)/include -I$(TOPDIR)/arch/powerpc/include
CPPFLAGS += -fno-builtin -ffreestanding -nostdinc	\
	-isystem $(shell $(CC) -print-file-name=include) -pipe $(PLATFORM_CPPFLAGS)	
CFLAGS := $(CPPFLAGS) -Wall -Wstrict-prototypes  -fno-stack-protector
_depend:	.depend

.depend:	$(src)Makefile $(TOPDIR)/tools/config.mk $(SRCS)
		@rm -f $@
		@for f in $(SRCS); do \
			g=`basename $$f | sed -e 's/\(.*\)\.\w/\1.o/'`; \
			$(CC) -M  -g  -Os   -D__KERNEL__  $(CPPFLAGS)  $g $f  >> $@ ; \
		done

#########################################################################
