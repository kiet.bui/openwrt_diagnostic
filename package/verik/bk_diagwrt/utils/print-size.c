/*
 * print-size.c
 */
#include <common.h>
#include <string.h>
#include <ctype.h>
#include <linux/math64.h>
#include <inttypes.h>

/**
 * chrtostr - Convert char to string
 * @c: The character
 */
char the_char[2];
char * chartostr(uchar c)
{
	char *s;
	s = the_char;

	memset(s, 0, 2);
	s[0] = c;
	s[1] = '\0';
	return s;
}

#define MAX_LINE_LENGTH_BYTES (64)
#define DEFAULT_LINE_LENGTH_BYTES (16)
int print_buffer(ulong addr, void* data, uint width, uint count, uint linelen)
{
	uint8_t linebuf[MAX_LINE_LENGTH_BYTES];
	uint32_t *uip = (void*) linebuf;
	uint16_t *usp = (void*) linebuf;
	uint8_t *ucp = (void*) linebuf;
	int i;
	int count_bk = count;

	if (linelen * width > MAX_LINE_LENGTH_BYTES)
		linelen = MAX_LINE_LENGTH_BYTES / width;
	if (linelen < 1)
		linelen = DEFAULT_LINE_LENGTH_BYTES / width;

	while (count_bk) {
		printf("%08lx:", addr);

		/* check for overflow condition */
		if (count_bk < linelen)
			linelen = count_bk;

		/* Copy from memory into linebuf and print hex values */
		for (i = 0; i < linelen; i++) {
			if (width == 4) {
				uip[i] = *(volatile uint32_t *) data;
				printf(" %08x", uip[i]);
			} else if (width == 2) {
				usp[i] = *(volatile uint16_t *) data;
				printf(" %04x", usp[i]);
			} else {
				ucp[i] = *(volatile uint8_t *) data;
				printf(" %02x", ucp[i]);
			}
			data += width;
		}

		/* Print data in ASCII characters */
		printf("    ");
		for (i = 0; i < linelen * width; i++) {
			printf(isprint(ucp[i]) && (ucp[i] < 0x80) ?
							chartostr(ucp[i]) : ".");
		}
		printf("\n");

		/* update references */
		addr += linelen * width;
		count_bk -= linelen;
	}

	return 0;
}

void print_size(unsigned long long size, const char *s)
{
	unsigned long m = 0, n;
	unsigned long long f;
	static const char names[] = { 'E', 'P', 'T', 'G', 'M', 'K' };
	unsigned long d = 10 * ARRAY_SIZE(names);
	char c = 0;
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(names); i++, d -= 10) {
		if (size >> d) {
			c = names[i];
			break;
		}
	}

	if (!c) {
		printf("%lldBytes%s", size, s);
		return;
	}

	n = size >> d;
	f = size & ((1ULL << d) - 1);

	/* If there's a remainder, deal with it */
	if (f) {
		m = (10ULL * f + (1ULL << (d - 1))) >> d;

		if (m >= 10) {
			m -= 10;
			n += 1;
		}
	}

	printf("%lu", n);
	if (m) {
		printf(".%ld", m);
	}
	printf(" %ciB%s", c, s);
}

char printf_buffer[64];
void print_speed(unsigned long long size, unsigned long long utime,
		const char *s) {
	sprintf(printf_buffer, "/s%s", s);
	print_size(div64_u64(size, div64_u64(utime, 1000)) * 1000, printf_buffer);
}

/*
 * print sizes as "xxx KHz", "xxx.y KHz", "xxx MHz", "xxx.y MHz",
 * xxx GHz, xxx.y GHz, etc as needed; allow for optional trailing string
 * (like "\n")
 */
char *print_speed_buf(unsigned long long speed, const char *s)
{
	ulong m = 0, n, i, off = 0;
	phys_size_t d = 1000000000; /* 1 GB */
	char c = 'G';

	memset(printf_buffer, 0, 64);

	if (speed < d) { /* try MB */
		c = 'M';
		d = 1000000;
		if (speed < d) { /* print in kB */
			c = 'K';
			d = 1000;
		}
	}

	n = div64_u64(speed, d);

	/* If there's a remainder, deal with it */
	if (speed != (d * n)) {
		m = div64_u64((10 * (speed - (n * d)) + div64_u64(d, 2)), d);

		if (m >= 10) {
			m -= 10;
			n += 1;
		}
	}

	i = sprintf(printf_buffer, "%ld", n);
	off += i;
	if (m) {
		i = sprintf(printf_buffer + off, ".%ld", m);
		off += i;
	}
	sprintf(printf_buffer + off, " %c%s", c, s);

	return (printf_buffer);
}

char* print_size_buf(unsigned long long size, const char *s)
{
	unsigned long m = 0, n;
	unsigned long long f;
	static const char names[] = { 'E', 'P', 'T', 'G', 'M', 'K' };
	unsigned long d = 10 * ARRAY_SIZE(names);
	char c = 0;
	unsigned int i;

	memset(printf_buffer, 0, 64);

	for (i = 0; i < ARRAY_SIZE(names); i++, d -= 10) {
		if (size >> d) {
			c = names[i];
			break;
		}
	}

	if (!c) {
		i = sprintf(printf_buffer, "%lldBytes%s", size, s);
		return (printf_buffer);
	}

	n = size >> d;
	f = size & ((1ULL << d) - 1);

	/* If there's a remainder, deal with it */
	if (f) {
		m = (10ULL * f + (1ULL << (d - 1))) >> d;

		if (m >= 10) {
			m -= 10;
			n += 1;
		}
	}
	i = sprintf(printf_buffer, "%lu", n);
	if (m) {
		i += sprintf(printf_buffer + i, ".%ld", m);
	}
	i += sprintf(printf_buffer + i, " %ciB%s", c, s);

	return (printf_buffer);
}
