/*
 * main.c
 */

#include <common.h>
#ifdef __WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <setjmp.h>
#include <signal.h>
#include <errno.h>
#include <linux/types.h>

/*#define CONFIG_VEDIAG_BACKTRACE*/
#ifdef CONFIG_VEDIAG_BACKTRACE
#include <execinfo.h>
#include <stdlib.h>
#endif

#ifdef	__USE_READLINE__
#include <readline/readline.h>
#include <readline/history.h>
#endif

#include <time_util.h>
#include <cmd.h>
#include <log.h>
#include <vediag_common.h>
#include <configs/config.h>
#include <ras.h>

int	rl_inhibit;	/* disable readline support */
int fromatty; /* input is from a terminal */
int stdin_cmd_scan; /* Flag to stop get key from main() */
int verbose; /* print messages coming back from server */
char *altarg; /* argv[1] with no shell-like preprocessing  */

char line[200]; /* input line buffer */
char *stringbase; /* current scan point in line buffer */
char argbuf[200]; /* argument storage buffer */
char *argbase; /* current storage point in arg buffer */

#ifndef CONFIG_VEDIAG_VERSION
#define CONFIG_VEDIAG_VERSION	0
#define CONFIG_VEDIAG_PATCHLEVEL	0
#define CONFIG_VEDIAG_SUBLEVEL	0
#endif

#ifndef CONFIG_VEDIAG_USER
#define CONFIG_VEDIAG_USER	"Unknown"
#endif

#ifndef CONFIG_SYS_BOARD
#define CONFIG_SYS_BOARD	"Unknown"
#endif

#define CONFIG_CBSIZE			256
#define CONFIG_PBSIZE			(CONFIG_CBSIZE + sizeof(CONFIG_PROMPT)+16)
#define CONFIG_BARGSIZE         CONFIG_CBSIZE
#define CONFIG_MAXARGS 	16

extern int rtctest_check(void);
extern int vediag_call_kernel_cmd(int argc, char *argv[]);
int vediag_exit = 0;
int run_command(const char *cmd);
static void cmdscanner(int top);
#ifdef CONFIG_AUTO_BOOTCMD
static int abortcmd(int cmddelay, char *s);
#endif
#ifdef CONFIG_VEDIAG_BACKTRACE
void handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}
#endif

#include <termios.h>
#include <sys/ioctl.h>

static const int STDIN = 0;
int kbhit(void)
{
   int nbbytes;
   ioctl(STDIN, FIONREAD, &nbbytes);  // 0 is STDIN
   return nbbytes;
}

/*
 * Check STDIN Key
 * Compare with input key
 * Return 0: key matching,
 * Retrun -EINVAL: not matching,
 * Return -ETIME: timeout no key
 */
int check_input_key(int timeout, char input_key)
{
	int c = 0;
	ulong us_timeout = timeout;

	while(--us_timeout) {
		/* we got a key press	*/
		if (kbhit()) {
			xinfo("Key hit!\n");
			c = getchar();
			xinfo("c='%c'\n", c);
			break;
		} else {
			usleep(1000);
		}
	}

	if(us_timeout <= 0) {
		xinfo("check_input_key: No key!\n");
		return -ETIME;
	}
	c = tolower(c);
	xinfo("input_key='%c', stdin_key='%c'\n", input_key, c);
	if(c == input_key)
		return 0;
	else
		return -EINVAL;
}

/**
 * This is entry point for any program
 */
int main(int argc, char *argv[])
{
	int top = 0;
	char *s;
	int cmd_delay;

	xprint("\n\n");
	xprint("Board: %s@%s\n\t(%s %s)\n", CONFIG_VEDIAG_USER, CONFIG_SYS_BOARD,
			__DATE__, __TIME__);
	xprint("       Copyright (c) 2018 VErik Systems.                                               \n");
	xprint("                   DIAGWRT  %d.%d.%d                                               \n\n\n",
			CONFIG_VEDIAG_VERSION,
			CONFIG_VEDIAG_PATCHLEVEL,
			CONFIG_VEDIAG_SUBLEVEL);

	/* Init flag */
	stdin_cmd_scan = 0;
	rl_inhibit = 0;
	fromatty = isatty(fileno(stdin));
	if (fromatty)
		verbose++;
#ifdef CONFIG_VEDIAG_BACKTRACE
	signal(SIGSEGV, handler);   // install our handler
#endif

#ifdef CONFIG_RAS
	ras_init();
#endif

#ifdef CONFIG_COMMAND_RTCTEST
	/* Check Real Time Clock testing from previous time */
	rtctest_check();
#endif

	env_init();
	env_relocate();
	s = vediag_parse_system("autodelay");
	cmd_delay = s ? (int) strtoul(s, NULL, 10) : CONFIG_BOOTDELAY;
	if (s){
		free(s);
	}

	s = vediag_parse_system("autocmd");
	printf("\n\n");

	if (cmd_delay >= 0 && s && !abortcmd(cmd_delay, s)) {
		run_command(s);
	}
	if (s){
		free(s);
	}

	for (;;) {
		if ((fromatty) && (!(stdin_cmd_scan & STDIN_KEY_STOP)))
			cmdscanner(top);
		top = 1;
        if (vediag_exit)
        	break;
	}

	/* TODO: vediag clean up */

	return vediag_exit - 1;
}

pure_initcall(screen_clear)
{
	int i, delay = 2;
	int nbbytes;

	/* reset device */
	printf("\033c");

	/* with Teraterm COM port, need some delay before sending next char after
	 * reset device*/
	for(i = 0; i < delay; i++){
		printf(".");
		fflush(stdout);
		sleep(1);
	}

	/* all atributes are OFF*/
	printf("\033[0m");

	/* clear screen */
	printf("\033[2J");

	/* set position to 0,0*/
	printf("\33[0;0H");
	fflush(stdout);

	/* Setup stdin */
	// Use termios to turn off line buffering
	struct termios term;
	tcgetattr(STDIN, &term);
	term.c_lflag &= ~ICANON;
	tcsetattr(STDIN, TCSANOW, &term);
	setbuf(stdin, NULL);
	/* Flush stdin */
	ioctl(STDIN, FIONREAD, &nbbytes);  // 0 is STDIN
	//printf("nbbytes=%d\n", nbbytes);
	while(nbbytes-- > 0) {
		i = getchar();
	}

	return 0;
}

#ifdef CONFIG_AUTO_BOOTCMD
/*
 * Watch for 'delay' seconds for autoboot stop or autoboot delay string.
 * returns: 0 -  no key string, allow autoboot
 *          1 - got key string, abort
 */
static int abortcmd(int cmddelay, char *s)
{
	int abort = 0, i, c;

	if ((s != NULL) && (strcmp(s, ""))) {
		xinfo("Auto command: %2d ", cmddelay);
	} else {
		return 1;
	}

#define SECOND_TO_10MSEC(x)    ((x) * 100)

	while ((cmddelay > 0) && (!abort)) {
		--cmddelay;
		for (i = 0; i < SECOND_TO_10MSEC(1); i++) {
			if (kbhit()) { /* we got a key press	*/
				c = getchar();
				abort = 1; /* don't auto boot	*/
				cmddelay = 0; /* no more delay	*/
				goto exit;
			}
			usleep(10000);
		}
		xinfo("\b\b\b%2d ", cmddelay);
		fflush(stdout);
	}
exit:
	xinfo("\n");
	return abort;
}
#endif

static char *get_input_line(char *buf, int buflen)
{
	char *lineread = readline(CONFIG_PROMPT);

	if (!lineread)
		return NULL;

	strncpy(buf, lineread, buflen);
	buf[buflen - 1] = 0;
	if (lineread[0])
		add_history(lineread);
	free(lineread);
	return buf;

}

/*static*/ void process_macros (const char *input, char *output)
{
	char c, prev;
	const char *varname_start = NULL;
	int inputcnt = strlen (input);
	int outputcnt = CONFIG_CBSIZE;
	int state = 0;		/* 0 = waiting for '$'  */

	/* 1 = waiting for '(' or '{' */
	/* 2 = waiting for ')' or '}' */
	/* 3 = waiting for '''  */
#ifdef DEBUG_PARSER
	char *output_start = output;

	dbg ("[PROCESS_MACROS] INPUT len %d: \"%s\"\n", strlen (input),
		input);
#endif

	prev = '\0';		/* previous character   */

	while (inputcnt && outputcnt) {
		c = *input++;
		inputcnt--;

		if (state != 3) {
			/* remove one level of escape characters */
			if ((c == '\\') && (prev != '\\')) {
				if (inputcnt-- == 0)
					break;
				prev = c;
				c = *input++;
			}
		}

		switch (state) {
		case 0:	/* Waiting for (unescaped) $    */
			if ((c == '\'') && (prev != '\\')) {
				state = 3;
				break;
			}
			if ((c == '$') && (prev != '\\')) {
				state++;
			} else {
				*(output++) = c;
				outputcnt--;
			}
			break;
		case 1:	/* Waiting for (        */
			if (c == '(' || c == '{') {
				state++;
				varname_start = input;
			} else {
				state = 0;
				*(output++) = '$';
				outputcnt--;

				if (outputcnt) {
					*(output++) = c;
					outputcnt--;
				}
			}
			break;
		case 2:	/* Waiting for )        */
			if (c == ')' || c == '}') {
				int i;
				char envname[CONFIG_CBSIZE], *envval;
				int envcnt = input - varname_start - 1;	/* Varname # of chars */

				/* Get the varname */
				for (i = 0; i < envcnt; i++) {
					envname[i] = varname_start[i];
				}
				envname[i] = 0;

				/* Get its value */
				envval = xgetenv (envname);

				/* Copy into the line if it exists */
				if (envval != NULL)
					while ((*envval) && outputcnt) {
						*(output++) = *(envval++);
						outputcnt--;
					}
				/* Look for another '$' */
				state = 0;
			}
			break;
		case 3:	/* Waiting for '        */
			if ((c == '\'') && (prev != '\\')) {
				state = 0;
			} else {
				*(output++) = c;
				outputcnt--;
			}
			break;
		}
		prev = c;
	}

	if (outputcnt)
		*output = 0;
	else
		*(output - 1) = 0;
#ifdef DEBUG_PARSER
	dbg ("[PROCESS_MACROS] OUTPUT len %d: \"%s\"\n",
		strlen (output_start), output_start);
#endif
}

static int parse_line (char *line, char *argv[])
{
	int nargs = 0;
#ifdef DEBUG_PARSER
	dbg ("parse_line: \"%s\"\n", line);
#endif
	while (nargs < CONFIG_MAXARGS) {

		/* skip any white space */
		while ((*line == ' ') || (*line == '\t')) {
			++line;
		}

		if (*line == '\0') {	/* end of line, no more args	*/
			argv[nargs] = NULL;
#ifdef DEBUG_PARSER
			dbg("parse_line: nargs=%d\n", nargs);
#endif
			return (nargs);
		}

		argv[nargs++] = line;	/* begin of argument string	*/

		/* find end of string */
		while (*line && (*line != ' ') && (*line != '\t')) {
			++line;
		}

		if (*line == '\0') {	/* end of line, no more args	*/
			argv[nargs] = NULL;
#ifdef DEBUG_PARSER
			dbg("parse_line: nargs=%d, argv[%d]=%s\n", nargs, nargs-1, argv[nargs-1]);
#endif
			return (nargs);
		}

		*line++ = '\0';		/* terminate current arg	 */
	}

	printf ("** Too many args (max. %d) **\n", CONFIG_MAXARGS);
#ifdef DEBUG_PARSER
	dbg("parse_line: nargs=%d\n", nargs);
#endif
	return (nargs);
}

/* @return 0 on success, or != 0 on error.*/
int run_command(const char *cmd)
{
	cmd_tbl_t *cmdtp = NULL;
	char cmdbuf[CONFIG_CBSIZE];	/* working copy of cmd		*/
	char *token = NULL;			/* start of token in cmdbuf	*/
	char *sep = NULL;			/* end of token (separator) in cmdbuf */
	char finaltoken[CONFIG_CBSIZE];
	char *str = cmdbuf;
	char *argv[CONFIG_MAXARGS + 1];	/* NULL terminated	*/
	int argc, inquotes;
	int rc = 0;
	int i;

	memset(cmdbuf, 0, sizeof(cmdbuf));
	memset(finaltoken, 0, sizeof(finaltoken));
	for(i = 0; i < ARRAY_SIZE(argv); i++){
		argv[i] = NULL;
	}

#ifdef DEBUG_PARSER
	dbg ("[RUN_COMMAND] cmd[%p]=\"", cmd);
	xinfo (cmd ? cmd : "NULL");	/* use puts - string may be loooong */
	xinfo ("\"\r\n");
#endif

	xdebug("[RUN_COMMAND] %s\n", cmd);

	/*clear_ctrlc();*/		/* forget any previous Control C */

	if (!cmd || !*cmd) {
		return -1;	/* empty command */
	}

	if (strlen(cmd) >= CONFIG_CBSIZE) {
		xerror ("## Command too long!\n");
		return -1;
	}


	strncpy(cmdbuf, cmd, strlen(cmd));

	/* Process separators and check for invalid
	 * repeatable commands
	 */
#ifdef DEBUG_PARSER
	dbg ("[PROCESS_SEPARATORS] %s\n", cmd);
#endif
	while (*str) {

		/*
		 * Find separator, or string end
		 * Allow simple escape of ';' by writing "\;"
		 */
		for (inquotes = 0, sep = str; *sep; sep++) {
			if ((*sep=='\'') &&
			    (*(sep-1) != '\\'))
				inquotes=!inquotes;

			if (!inquotes &&
			    ((*sep == ';') || (*sep == '&')) &&	/* separator		*/
			    ( sep != str) &&	/* past string start	*/
			    (*(sep-1) != '\\'))	/* and NOT escaped	*/{
				break;
			}
		}

		/*
		 * Limit the token to data between separators
		 */
		token = str;
#ifdef DEBUG_PARSER
		xinfo("\n");
		{
			int i;
			for(i = 0; i < strlen(str);i++){
			    xinfo("%02x ", str[i]);
			}
			xinfo("\n");
		}
#endif

		if (*sep) {
			str = sep + 1;	/* start of command for next pass */
			*sep = '\0';
		}
		else
			str = sep;	/* no more commands for next pass */
#ifdef DEBUG_PARSER
		xinfo("\n");
		{
			int i;
			for(i = 0; i < strlen(token);i++){
			    xinfo("%02x ", token[i]);
			}
			xinfo("\n");
		}
		dbg ("token[%p]: \"%s\"\n", token, token);
#endif

		/* find macros in this token and replace them */
		process_macros (token, finaltoken);

		/* Extract arguments */
		if ((argc = parse_line (finaltoken, argv)) == 0) {
			rc = -1;	/* no command at all */
			continue;
		}

		/* Look up command in command table */
		if ((cmdtp = find_cmd(argv[0])) == NULL) {
			int ret = vediag_call_kernel_cmd(argc, argv);
			if (ret == -1) {
				xinfo("?Invalid command\n");
				rc = -1; /* give up after bad command */
				continue;
			}
			continue;
		}

		/* OK - call function to do the command */
		if ((cmdtp->cmd) (cmdtp, 0, argc, argv) != 0) {
			rc = -1;
		}

		/* Did the user stop this? */
		if (ctrlc())
			return -1; /* if stopped then not repeatable */
	}

	return rc ;
}

/*
 * Command parser.
 */
static void cmdscanner(int top)
{
	if (!top)
		(void) xinfo("\n");

	for (;;) {
		if (!get_input_line(line, sizeof(line))) {
			xerror("CLI: Fatal error %s\n", strerror(errno));
            exit(0);
		}
        run_command(line);
        if (vediag_exit)
        	break;
	}
}
