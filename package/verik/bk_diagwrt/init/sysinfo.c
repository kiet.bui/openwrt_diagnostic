/*
 * sysinfo.c
 *
 *  Created on: Apr 13, 2017
 *      Author: ddh
 */

#include <linux/init.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <ucontext.h>
#include <execinfo.h>
#include <sys/wait.h>

#include <log.h>
#include <vediag_common.h>

/*#define CONFIG_VEDIAG_BACKTRACE*/

#define READ_FD  0
#define WRITE_FD 1

struct sysinfo_cmd {
    char** cmdargs;
};

static const struct sysinfo_cmd SYSINFO_ETH_CMD = {
    .cmdargs = (char* []) { "ifconfig", "-a", NULL },
};

static const struct sysinfo_cmd SYSINFO_PCI_CMD = {
    .cmdargs = (char* []) { "lspci", "-vvv", NULL },
};

static const struct sysinfo_cmd SYSINFO_SCSI_CMD = {
    .cmdargs = (char* []) { "lsblk", NULL }, /* lsscsi */
};

static const struct sysinfo_cmd SYSINFO_CMD[] = {
    {
        .cmdargs = (char* []) { "dmesg", NULL, },
    },
#ifdef CONFIG_COMMAND_STORAGE
    {
        .cmdargs = (char* []) { "lsblk", NULL }, /* lsscsi */
    },
#endif
#ifdef CONFIG_COMMAND_PCI
	{
	    .cmdargs = (char* []) { "lspci", "-vvv", NULL },
	},
#endif
    {
        .cmdargs = (char* []) { "ifconfig", "-a", NULL },
    },
    {
        .cmdargs = (char* []) { "/bin/sh", "-c", "cat " VEDIAG_SYSTEM_CONFIG_FILE " || cat " VEDIAG_SYSTEM_CONFIG_FILE_PATH, NULL },
    },
#if 0
	{
        .cmdargs = (char* []) { "/bin/sh", "-c", "cat " VEDIAG_TEST_CONFIG_FILE " || cat " VEDIAG_TEST_CONFIG_FILE_PATH, NULL },
    },
#endif
    {
        .cmdargs = NULL,
    },
};

static void sigsegv_handler ( int sig, siginfo_t *info, void *data )
{
#ifdef CONFIG_VEDIAG_BACKTRACE
    int c, i;
    void *addresses[10];
    char **strings;
#endif

    // dump some information from siginfo when catched sigsegv
    xdebug("\n=== Siginfo ===\n");
    xdebug("\tsi_signo:  %d\n"
           "\tsi_code:   %s\n"
           "\tsi_errno:  %d\n"
           "\tsi_pid:    %d\n"
           "\tsi_uid:    %d\n"
           "\tsi_addr:   %p\n"
           "\tsi_status: %d\n"
           "\tsi_band:   %ld\n",
           info->si_signo,
           (info->si_code == SEGV_MAPERR) ? "SEGV_MAPERR" : "SEGV_ACCERR",
           info->si_errno,
           info->si_pid,
           info->si_uid,
           info->si_addr,
           info->si_status,
           info->si_band);

#ifdef CONFIG_VEDIAG_BACKTRACE
    // dump last 10 called functions from backtrace
    c = backtrace(addresses, 10);
    strings = backtrace_symbols(addresses, c);
    xdebug("\nBacktrace returned: %d\n", c);
    for(i = 0; i < c; i++) {
        xdebug("\t%d: %p  %s\n", i, addresses[i], strings[i]);
    }
#endif
    exit(-1);
}

static int get_sysinfo ( const struct sysinfo_cmd* sysinfo_cmd )
{
    int rc = 0, fds[2];
    pid_t child_pid = 0;

    // create one-way pipe communication
    if (pipe(fds) < 0) {
        xdebug("[SYSINFO] create pipe failed: %s", strerror(errno));
        rc = -1;
        return rc;
    }

    // create child process to execute commands
    child_pid = fork();
    if (child_pid > 0) { // parent process
        FILE* fp = NULL;
        char buff[512] = { 0 };

        // close not required pipe by parent
        close(fds[WRITE_FD]);
        // read output from child's buffer
        fp = fdopen(fds[READ_FD], "r");
        if (fp) {
            while (fgets(buff, sizeof(buff) - 1, fp)) {
                xdebug("%s", buff);
            }
            fclose(fp);	//fclose will close fd also
        }else {
        	xdebug("[SYSINFO:C] Cannot read child process --> FAIL !!\n");
        	close(fds[READ_FD]);
        }

        wait(&rc);
    }
    else if (child_pid == 0) { // child process
        // redirect stdout & stderr to parent pipe
        dup2(fds[WRITE_FD], STDOUT_FILENO);
        dup2(fds[WRITE_FD], STDERR_FILENO);
        // close not-required pipes by child
        close(fds[READ_FD]);
        close(fds[WRITE_FD]);
        // start executing commands
        rc = execvp(*sysinfo_cmd->cmdargs, sysinfo_cmd->cmdargs);
        // execute child failed
        xdebug("[SYSINFO:C] Run '%s' Error: %s\n", *sysinfo_cmd->cmdargs, strerror(errno));
        exit(rc);
    } else { // fork error
        xdebug("sysinfo failed: %s\n", strerror(errno));
        rc = -2;
    }

    return rc;
}

int ip_sysinfo(int test_type)
{
	int rc = 0;
	const struct sysinfo_cmd* sysinfo_cmd = NULL;

	switch(test_type) {

	case VEDIAG_PCIE :
		sysinfo_cmd = &SYSINFO_PCI_CMD;
		break;

	case VEDIAG_NET :
		sysinfo_cmd = &SYSINFO_ETH_CMD;
		break;

	case VEDIAG_STORAGE:
		sysinfo_cmd = &SYSINFO_SCSI_CMD;
		break;
	default :
		break;
	}

	if (sysinfo_cmd != NULL)
		rc = get_sysinfo(sysinfo_cmd);

	return rc;
}

core_initcall(sysinfo)
{
    int rc = 0, i = 0;
    const struct sysinfo_cmd* sysinfo_cmd = NULL;
    struct sigaction sa;

    xinfo("Start getting system info ... ");

    // register segmentation fault handler
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigsegv_handler;

    if (sigaction(SIGSEGV, &sa, NULL)) {
        xdebug("Register SIGSEGV handler failed: %s\n", strerror(errno));
    }

    // getting system info
    for(sysinfo_cmd = SYSINFO_CMD; sysinfo_cmd->cmdargs != NULL; ++sysinfo_cmd) {
        xdebug("\n=== [SYSINFO] '");
        for (i = 0; sysinfo_cmd->cmdargs[i] != NULL; ++i)
            xdebug("%s ", sysinfo_cmd->cmdargs[i]);
        xdebug("' ===\n");

        rc |= get_sysinfo(sysinfo_cmd);

        xdebug("======\n");
    }

    if (!rc)
        xinfo("done\n");
    else
        xinfo("failed\n");

    return rc;
}
