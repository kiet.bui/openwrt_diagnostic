/*
 * init.c
 *
 *  Created on: Mar 13, 2017
 *      Author: nnguyen
 */

#include <common.h>
#include <string.h>

#ifdef CONFIG_VEDIAG_MOD_NAME
core_initcall(vediag_module_init)
{
#define INT_BUFF_SIZE 1024
	const char *vediag_name = CONFIG_VEDIAG_OUTPUT_NAME;
	const char *mod_name = CONFIG_VEDIAG_MOD_NAME;

	FILE *input_file = NULL;
	FILE *output_file = NULL;
	FILE *p_file = NULL;

	char *line;
	char *buff = NULL;
	int count;

	/* open input file for reading */
	input_file = fopen(vediag_name, "rb");
	if (unlikely(!input_file)) {
		logerr("Could not open file %s for reading\n", vediag_name);
		goto done;
	}

	/* open output file for writting,
	 * file name is same with module name that input from make command line */
	output_file = fopen(mod_name, "wb+");
	if (unlikely(!output_file)) {
		logerr("Could not open file %s for writting\n", mod_name);
		goto done;
	}


	/* set stream position to the starting of module name */
	if (unlikely(fseek(input_file, -CONFIG_VEDIAG_MOD_SIZE, SEEK_END) != 0)) {
		logerr("Not found %s, fuckkkk.......\n", mod_name);
		goto done;
	}

	buff = (char *) malloc(INT_BUFF_SIZE);
	if (unlikely(!buff)) {
		goto done;
	}

	while (1) {
		count = fread(buff, 1, INT_BUFF_SIZE, input_file);
		if (count > 0) {
			fwrite(buff, 1, count, output_file);
		} else {
			break;
		}
	}

	/* install vediag module here using "insmod" */
	fflush(output_file);
	fclose(output_file);
	output_file = NULL;
	memset(buff, 0, INT_BUFF_SIZE);
	sprintf(buff, "insmod %s", mod_name);
	p_file = popen(buff, "r");
	if (likely(p_file != NULL)) {
		while(1) {
			line = fgets(buff, INT_BUFF_SIZE, p_file);
			if (line == NULL)
				break;
			else
				printf("%s", line);
		}
	}

done:
	if (input_file != NULL) {
		fclose(input_file);
	}

	if (output_file != NULL) {
		fclose(output_file);
	}

	if (p_file != NULL) {
		pclose(p_file);
	}

	if (buff != NULL) {
		free(buff);
	}

	return 0;
}

static void __destructor(CFG_PRIORITY_INIT_3)
vediag_module_remove(void)
{
	char *line;
	char buff[INT_BUFF_SIZE];
	FILE *p_file = NULL;

	memset(buff, 0, sizeof(buff));
	sprintf(buff, "rmmod %s", CONFIG_VEDIAG_MOD_NAME);
	p_file = popen(buff, "r");
	if (likely(p_file != NULL)) {
		while(1) {
			line = fgets(buff, sizeof(buff), p_file);
			if (line == NULL)
				break;
			else
				printf("%s", buff);
		}

		pclose(p_file);
	}
}
#endif
