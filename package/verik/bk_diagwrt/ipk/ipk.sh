#!/bin/bash

version=0.2

#control.tar.gz  data.tar.gz  debian-binary
#control  control.tar.gz  data.tar.gz  debian-binary  etc  usr

#Pack data.tar.gz
#cp ../venus_system.ini etc/diagwrt/system.ini
#cp ../venus_testall.ini etc/diagwrt/
cp ../../zwave_tools*/zwave_tools bin/
cp ../../zigbee_tools*/zigbee_tools bin/
cp ../../../libs/libzigbee-gecko/*.so* usr/lib/
cp ../diagwrt  etc/diagwrt/

rm -rf data.tar.gz
tar -czf data.tar.gz bin etc usr

#Pack control.tar.gz
cd control/
#ls: control  postinst  prerm
tar -czf control.tar.gz control postinst prerm 
mv control.tar.gz ../
cd ..

#Pack diagwrt-1.0.ipk
tar -czf diagwrt-${version}.ipk data.tar.gz control.tar.gz debian-binary
rm -rf data.tar.gz control.tar.gz

