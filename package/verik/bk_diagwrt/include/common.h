/*
 * common.h
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <asm/byteorder.h>
#include <linux/compiler.h>
#include <linux/bug.h>
#include <kernel.h>
#include <linux/init.h>
#include <log.h>
#include <hexdump.h>
#include <utils.h>
#include <malloc.h>

/* DEBUG */
#ifdef	DEBUG
#define debug(fmt,args...)	xinfo (fmt ,##args)
#define debugX(level,fmt,args...) if (DEBUG>=level) xinfo(fmt,##args);
#else
#define debug(fmt,args...) do {} while (0)
#define debugX(level,fmt,args...) do {} while (0)
#endif

/* DEBUG_FUNC */
#ifdef DEBUG_FUNC
#define tracefunc(fmt,args...)	xinfo("%s()" fmt, __func__, ##args)
#else
#define tracefunc(fmt,args...) do {} while (0)
#endif
#define TRACEFUNC	tracefunc("\n");

/** Low level debug */
#define LOWDEBUG
#ifdef LOWDEBUG
#define lowdbg(fmt,args...) xinfo(fmt, ##args)
#else
#define lowdbg(fmt, args...) do {} while (0)
#endif

/*#define LOG*/
#ifdef LOG
#define log(fmt,args...) log_file(fmt, ##args)
#else
#define log(fmt,args...) do {} while(0)
#endif

/*#define VLOG*/
#ifdef VLOG
#define vlog(fmt, args...) log_file("%s::%s(): "fmt, __FILE__, __func__, ##args); xerror("%s::%s(): "fmt, __FILE__, __func__, ##args)
#else
#define vlog(fmt,args...) do {} while(0)
#endif

#if 1
#define logerr(fmt,args...) log_file("ERROR:" fmt, ##args); xerror("ERROR: " fmt, ##args)
#else
#define logerr(fmt,args...) do {} while(0)
#endif

#if 1
#define logwarn(fmt,args...) log_file("WARN:" fmt, ##args); xinfo("WARN:" fmt, ##args)
#else
#define logwarn(fmt,args...) do {} while(0)
#endif


extern int ctrlc (void);

struct listbuff;
extern struct listbuff *get_outputstream_of_shell_command(const char *cmd);
int print_buffer(ulong addr, void* data, uint width, uint count, uint linelen);
extern int kbhit(void);
#define TESTER_INTERACT_TIMEOUT		1000
extern int check_input_key(int timeout, char input_key);
extern int stdin_cmd_scan; /* Flag to stop get key from main() */
#define STDIN_KEY_STOP	0x01

#endif /* COMMON_H_ */
