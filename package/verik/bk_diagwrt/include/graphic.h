/*
 * graphic.h
 */

#ifndef GRAPHIC_H_
#define GRAPHIC_H_

/*
 * ANSI CODE
 */
#define save_cursor_position()				gprintf("\033[s")
#define restore_cursor_position()			gprintf("\033[u")
#define erase_display()						gprintf("\033[2J")
#define erase_line()						gprintf("\033[K")
#define set_cursor_position(line,column)	gprintf("\033[%d;%dH",line,column)

/* Define Graphic Mode */
#define GRAPH_ALL_ATTRIBUTE_OFF				0
#define GRAPH_BOLD_ON						1
#define GRAPH_UNDERSCORE					4
#define GRAPH_BLINK_ON						5
#define GRAPH_REVERSE_VIDEO_ON				7
#define GRAPH_CONCEALED_ON					8
#define GRAPH_FOREGROUND_COLOR_BLACK		30
#define GRAPH_FOREGROUND_COLOR_RED			31
#define GRAPH_FOREGROUND_COLOR_GREEN		32
#define GRAPH_FOREGROUND_COLOR_YELLOW		33
#define GRAPH_FOREGROUND_COLOR_BLUE			34
#define GRAPH_FOREGROUND_COLOR_MAGENTA		35
#define GRAPH_FOREGROUND_COLOR_CYAN			36
#define GRAPH_FOREGROUND_COLOR_WHITE		37
#define GRAPH_BACKGROUND_COLOR_BLACK		40
#define GRAPH_BACKGROUND_COLOR_RED			41
#define GRAPH_BACKGROUND_COLOR_GREEN		42
#define GRAPH_BACKGROUND_COLOR_YELLOW		43
#define GRAPH_BACKGROUND_COLOR_BLUE			44
#define GRAPH_BACKGROUND_COLOR_MAGENTA		45
#define GRAPH_BACKGROUND_COLOR_CYAN			46
#define GRAPH_BACKGROUND_COLOR_WHITE		47
#define set_graphic_mode(mode)				gprintf("\033[%dm",mode)

#define set_foreground_blue					puts ("\033[34m")
#define set_foreground_default				puts ("\033[0m")

#endif /* GRAPHIC_H_ */
