#ifndef _LINUX_UNALIGNED_PACKED_STRUCT_H
#define _LINUX_UNALIGNED_PACKED_STRUCT_H

#include <kernel.h>

struct __una_u16 { u16 x; } __packed;
struct __una_u32 { u32 x; } __packed;
struct __una_u64 { u64 x; } __packed;

#if defined(CONFIG_BOOT0) || \
	defined(CONFIG_BOOT1)
/* Fix for STORM */
static inline u16 __get_unaligned_cpu16(const u8 *p)
{
//@Fixme
//	const struct __una_u16 *ptr = (const struct __una_u16 *)p;
//	return ptr->x;
	return p[0] | p[1] << 8;
}

/* Fix for STORM */
static inline u32 __get_unaligned_cpu32(const u8 *p)
{
//	const struct __una_u32 *ptr = (const struct __una_u32 *)p;
//	return ptr->x;
	return p[0] | p[1] << 8 | p[2] << 16 | p[3] << 24;
}
#else
static inline u16 __get_unaligned_cpu16(const void *p)
{
	const struct __una_u16 *ptr = (const struct __una_u16 *)p;
	return ptr->x;
}

static inline u32 __get_unaligned_cpu32(const void *p)
{
	const struct __una_u32 *ptr = (const struct __una_u32 *)p;
	return ptr->x;
}
#endif

static inline u64 __get_unaligned_cpu64(const void *p)
{
	const struct __una_u64 *ptr = (const struct __una_u64 *)p;
	return ptr->x;
}

static inline void __put_unaligned_cpu16(u16 val, void *p)
{
	struct __una_u16 *ptr = (struct __una_u16 *)p;
	ptr->x = val;
}

static inline void __put_unaligned_cpu32(u32 val, void *p)
{
	struct __una_u32 *ptr = (struct __una_u32 *)p;
	ptr->x = val;
}

static inline void __put_unaligned_cpu64(u64 val, void *p)
{
	struct __una_u64 *ptr = (struct __una_u64 *)p;
	ptr->x = val;
}

#endif /* _LINUX_UNALIGNED_PACKED_STRUCT_H */
