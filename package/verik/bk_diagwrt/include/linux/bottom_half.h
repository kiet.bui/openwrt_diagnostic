#ifndef _LINUX_BH_H
#define _LINUX_BH_H

#if 1
static inline void local_bh_disable(void)
{
}
#else
extern void local_bh_disable(void);
#endif
#if 1
static inline void _local_bh_enable(void)
{
}
#else
extern void _local_bh_enable(void);
#endif
#if 1
static inline void local_bh_enable(void)
{
}
#else
extern void local_bh_enable(void);
#endif
#if 1
static inline void local_bh_enable_ip(unsigned long ip)
{
}
#else
extern void local_bh_enable_ip(unsigned long ip);
#endif

#endif /* _LINUX_BH_H */
