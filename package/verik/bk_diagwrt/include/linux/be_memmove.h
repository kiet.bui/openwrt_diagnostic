#ifndef _LINUX_UNALIGNED_BE_MEMMOVE_H
#define _LINUX_UNALIGNED_BE_MEMMOVE_H

#include <linux/memmove.h>

static inline ushort get_unaligned_be16(const void *p)
{
	return __get_unaligned_memmove16((const uchar *)p);
}

static inline uint get_unaligned_be32(const void *p)
{
	return __get_unaligned_memmove32((const uchar *)p);
}

static inline ulong get_unaligned_be64(const void *p)
{
	return __get_unaligned_memmove64((const uchar *)p);
}

static inline void put_unaligned_be16(ushort val, void *p)
{
	__put_unaligned_memmove16(val, p);
}

static inline void put_unaligned_be32(uint val, void *p)
{
	__put_unaligned_memmove32(val, p);
}

static inline void put_unaligned_be64(ulong val, void *p)
{
	__put_unaligned_memmove64(val, p);
}

#endif /* _LINUX_UNALIGNED_LE_MEMMOVE_H */
