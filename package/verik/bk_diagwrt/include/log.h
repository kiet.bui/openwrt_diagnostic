/*
 * log.h
 *
 */

#ifndef LOG_H_
#define LOG_H_

#include <linux/types.h>

#ifdef CONFIG_XLOG

#include <vediag_common.h>
#include <zlog/zlog.h>

#define  VEDIAG_ZLOG_CATEGORY      "diagwrt"
#define  VEDIAG_ZLOG_MODULE(name)  VEDIAG_ZLOG_CATEGORY"_"name
#define  VEDIAG_ZLOG_CONFFILE      "zlog.conf"

#define xlog_fatal(...) \
    dzlog_fatal(__VA_ARGS__)
#define xlog_error(...) \
    dzlog_error( __VA_ARGS__)
#define xlog_warn(...)  \
    dzlog_warn(__VA_ARGS__)
#define xlog_print(...) \
    dzlog_notice(__VA_ARGS__)
#define xlog_info(...)  \
    dzlog_info(__VA_ARGS__)
#define xlog_debug(...) \
    dzlog_debug(__VA_ARGS__)

#define hlog_fatal(buf, buf_len) \
    hdzlog_fatal(buf, buf_len)
#define hlog_error(buf, buf_len) \
    hdzlog_error(buf, buf_len)
#define hlog_warn(buf, buf_len) \
    hdzlog_warn(buf, buf_len)
#define hlog_print(buf, buf_len) \
    hdzlog_notice(buf, buf_len)
#define hlog_info(buf, buf_len) \
    hdzlog_info(buf, buf_len)
#define hlog_debug(buf, buf_len) \
    hdzlog_debug(buf, buf_len)

#define vlog_fatal(format, args) \
    vdzlog_fatal(format, args)
#define vlog_error(format, args) \
    vdzlog_error(format, args)
#define vlog_warn(format, args) \
    vdzlog_warn(format, args)
#define vlog_print(format, args) \
    vdzlog_notice(format, args)
#define vlog_info(format, args) \
    vdzlog_info(format, args)
#define vlog_debug(format, args) \
    vdzlog_debug(format, args)

#else

#define  VEDIAG_ZLOG_CATEGORY
#define  VEDIAG_ZLOG_MODULE
#define  VEDIAG_ZLOG_CONFFILE

#define xlog_fatal(...)      fprintf(stdout, __VA_ARGS__)
#define xlog_error(...)      fprintf(stderr, __VA_ARGS__)
#define xlog_warn(...)       fprintf(stdout, __VA_ARGS__)
#define xlog_print(...)      fprintf(stdout, __VA_ARGS__)
#define xlog_info(...)       fprintf(stdout, __VA_ARGS__)
#define xlog_debug(...)      fprintf(stdout, __VA_ARGS__)

#define hlog_fatal(buf, buf_len)
#define hlog_error(buf, buf_len)
#define hlog_warn(buf, buf_len)
#define hlog_print(buf, buf_len)
#define hlog_info(buf, buf_len)
#define hlog_debug(buf, buf_len)

#define vlog_fatal(format, args)    vfprintf(stdout, format, args)
#define vlog_error(format, args)    vfprintf(stderr, format, args)
#define vlog_warn(format, args)     vfprintf(stdout, format, args)
#define vlog_print(format, args)    vfprintf(stdout, format, args)
#define vlog_info(format, args)     vfprintf(stdout, format, args)
#define vlog_debug(format, args)    vfprintf(stdout, format, args)

#endif /* CONFIG_XLOG */

/*
 *          +--------+----------+
 *          | stdout | Log file |
 * ---------+--------+----------+
 *   xerror |   x    |    x     |
 *   xprint |   x    |          |
 *   xinfo  |   x    |    x     |
 *   xdebug |        |    x     |
 */

/*
 * Printf buffer as normal
 */
#define xerror(x, fmt ...)   xlog_error(x, ##fmt)
#define xprint(x, fmt ...)   xlog_print(x, ##fmt)
#define xinfo(x, fmt ...)    xlog_info(x, ##fmt)
#define xdebug(x, fmt ...)   xlog_debug(x, ##fmt)

/*
 * Print buffer as hexadecimal
 */
#define hxerror(buf, buf_len)   hlog_error(buf, buf_len)
#define hxprint(buf, buf_len)   hlog_print(buf, buf_len)
#define hxinfo(buf, buf_len)    hlog_info(buf, buf_len)
#define hxdebug(buf, buf_len)   hlog_debug(buf, buf_len)

/*
 * Print buffer as hexadecimal
 */
#define vxerror(format, args)   vlog_error(format, args)
#define vxprint(format, args)   vlog_print(format, args)
#define vxinfo(format, args)    vlog_info(format, args)
#define vxdebug(format, args)   vlog_debug(format, args)

int ip_sysinfo(int test_type);

#endif /* LOG_H_ */
