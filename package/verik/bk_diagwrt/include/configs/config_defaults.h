
#ifndef _CONFIG_DEFAULTS_H_
#define _CONFIG_DEFAULTS_H_

#define AUTHOR 			"VErik Diag"
#define CONFIG_PROMPT	"diagwrt > "
#ifndef CONFIG_VEDIAG_OUTPUT_NAME
#define CONFIG_VEDIAG_OUTPUT_NAME	"diagwrt"
#endif

#define CONFIG_LOG
#define CONFIG_AVL_TREE
#define CONFIG_BINARY_TREE
#define CONFIG_TREE
#define CONFIG_TIME_UTILS

/*#define CONFIG_CHECKSUM			1*/
#define CONFIG_GENERIC_HWEIGHT	1
#define CONFIG_XLOG             1
/*#define CONFIG_RAS              1*/
#define CONFIG_SYSINFO          1

/**
 * The GNU Readline library provides a set of functions for use by applications
 * that allow users to edit command lines as they are typed in.
 * Both Emacs and vi editing modes are available.
 * The Readline library includes additional functions to maintain a list of
 * previously-entered command lines, to recall and perhaps reedit those lines,
 * and perform csh-like history expansion on previous commands.
 */
#define __USE_READLINE__

/**
 * iniParser: stand-alone ini parser library in ANSI C
 * iniparser is distributed under an MIT license.
 * http://ndevilla.free.fr/iniparser/
 */
#define CONFIG_INIPARSER	1

#endif
