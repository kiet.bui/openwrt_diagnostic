/**
 * config.h
 */
#ifndef __CONFIG__H__
#define __CONFIG__H__

#if defined(__x86_64__) \
	|| defined(__x86_64) \
	|| defined(__aarch64__) \
	|| defined(__amd64__) \
	|| defined(__amd64) \
	|| defined(__ia64__) \
	|| defined(__ia64)
#define CONFIG_64BIT
#define CONFIG_GENERIC_ATOMIC64
#else
/*#error "Compiler Identify Issue."*/
/* @see https://sourceforge.net/p/predef/wiki/Architectures/ */
#endif

#include "config_cmd_defaults.h"
#include "config_defaults.h"
#include "config_fallbacks.h"
#include "config_uncmd_spl.h"

/**
 * USB
 */
/*#define CONFIG_USB*/
#ifdef CONFIG_USB
/*#define CONFIG_USB_UTILS*/
#define CONFIG_USB_CMD
#endif
#ifdef CONFIG_USB_CMD
#define CONFIG_COMMAND_USB_DPFP
#define CONFIG_COMMAND_USB_HOTPLUGTEST
#define CONFIG_COMMAND_USB_LISTDEVS
#define CONFIG_COMMAND_XUSB
#endif

/*
 * TEST MANAGER & GUI
 */
#define CONFIG_VEDIAG_TEST_MANAGER
#define VEDIAG_MANAGER_USING_MUTEX
#define CONFIG_VEDIAG_TEST_GUI

/**
 * Net
 */
#define CONFIG_NET
#define CONFIG_COMMAND_NET
/*#define CONFIG_USING_IPERF3_1_7*/
/*#define CONFIG_USING_SSHPASS1_06*/

#define CONFIG_CMD_ENV
#define CONFIG_CMD_SAVEENV
#define CONFIG_CMD_CLEANENV
#define CONFIG_ENV_SIZE										0x2000		/* 8 KiB */
#define CONFIG_AUTO_BOOTCMD												/* Enable get bootcmd/bootdelay */
#define CONFIG_BOOTDELAY 5

#endif
