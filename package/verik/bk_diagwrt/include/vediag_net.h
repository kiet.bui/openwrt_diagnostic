/*
 * vediagnet.h
 *
 */

#ifndef VEDIAG_NET_H_
#define VEDIAG_NET_H_

#include <stdio.h>
#include <log.h>

/*
#define WIFSIGNALED(a)	0
#define WIFEXITED(a)	0
#define WTERMSIG(a)		0
#define WEXITSTATUS(a)	0
#define WNOHANG			1
*/

#define IPERF_APP_ENV		"iperftest"
#define FORCE_REMOTE_ENV	"remotetest"

struct vediag_net_test_info;
#ifndef IFNAMSIZ
#define IFNAMSIZ	16
#endif

#define NET_TEST_DEBUG
#if defined(NET_TEST_DEBUG)
#define nettestdebug(fmt,args...)	xdebug(fmt ,##args)
#else
#define nettestdebug(fmt,args...)
#endif
#define RED_COLOR	"\033[91m"
#define GREEN_COLOR	"\033[92m"
#define YELLOW_COLOR	"\033[93m"
#define MAGENTA_COLOR	"\033[95m"
#define CYAN_COLOR	"\033[96m"
#define NONE_COLOR	"\033[0m"
#define VERSION_COLOR			"\033[1;92m"
#define ERROR_COLOR				"\033[1;91m"

#define DEFAULT_THREADS			1
#define DEFAULT_INTERFACE_NAME	"eth0"
#define DEFAULT_APP				"scp"

#define DEFAULT_SERVER_IP		"10.110.1.1"
#define DEFAULT_USER_NAME		"verik"
#define DEFAULT_PASSWORD		"123456"

#define DEFAULT_TEST_TIME		"600"
#define DEFAULT_FILE_NAME		"/tmp"
#define DEFAULT_FILE_SIZE		"0x1000000"
#define DEFAULT_TEST_SIZE		"0x6400000"
#define DEFAULT_LOOPBACK_MODE	"no"

#define HARDWARE_ADDRESS_LENGTH 18
#define RETURN_FILE_SUFFIX		"-returns"

#define NET_ERROR_MSG			"ERROR:"
#define ROUTE_TABLE_FORMAT		"diag_%s_table"
#define GET_IP_FILE				"get_ip_temp_file"
#define GET_HW_ADDR_FILE		"get_hw_addr_file"

#define ROUTE_TABLE_MAX_ENTRY	253
#define DHCLIENT_TIME_OUT		10

enum vediag_net_test_return_code {
	RETURN_SYSTEM_ERROR = -4,
	RETURN_NO_APP = -3,
	RETURN_INSUFFICIENT_RESOURCES = -2,
	RETURN_FILE_ERROR = -1,
	RETURN_NOERROR = 0,
	RETURN_INVALID_ARGUMENTS = 1,
	RETURN_CONFLICTING_ARGUMENTS = 2,
	RETURN_RUNTIME_ERROR = 3,
	RETURN_PARSE_ERRROR = 4,
	RETURN_INCORRECT_PASSWORD = 5,
	RETURN_HOST_KEY_UNKNOWN = 6,
	RETURN_HOST_KEY_CHANGED = 7,
	RETURN_IOCTL_ERROR = 8,
	RETURN_COMPARE_FAIL = 9,
	RETURN_TXRX_ERROR = 10,
	RETURN_TIMEOUT = 11,
	RETURN_SLOW_SPEED = 12,
	RETURN_USER_STOP_REQUEST = 13,
};

enum vediag_net_test_app {
	NET_INVALID_TEST = 0,
	NET_SCP_TEST,
	NET_IPERF_TEST,
};

enum vediag_net_configured {
	NOT_CONFIGURED = 0,
	CONFIGURED,
	CONFIGURE_FAILED,
};

enum vediag_net_test_mode {
	REMOTE = 0,
	LOOPBACK = 1,
};

struct netstat_info {
	char destination[IFNAMSIZ];
	char gateway[IFNAMSIZ];
	char genmask[IFNAMSIZ];
	char flags[5];
	int MSS;
	int window;
	int irtt;
	char iface[20];
};

struct interface_address_info {
	char ip_addr[IFNAMSIZ];
	char netmask[IFNAMSIZ];
	char brd_addr[IFNAMSIZ];
};

struct interface_hw_address_info {
	char hw_addr[HARDWARE_ADDRESS_LENGTH];
	char brd_hw_addr[HARDWARE_ADDRESS_LENGTH];
};

struct iperf_result_info {
	char transfer_size[10];
	char transfer_size_unit[10];
	char transfer_speed[10];
	char transfer_speed_unit[10];
	int retransmit;
};

enum iperf_direction {
	RECEIVE = 0,
	TRANSMIT = 1,
};

enum interface_address_type {
	IP_ADDR = 1,
	NETMASK = 2,
	BC_ADDR = 3,
};

enum interface_hw_address_type {
	HW_ADDR = 1,
	BC_HW_ADDR = 3,
};

struct vediag_net_test_info {
	char					app_test[IFNAMSIZ];
	char					*interface_name;			//interface name - same as in ifconfig
	char 					*hw_name;
	char					ip_addr[IFNAMSIZ];			//[IFNAMSIZ];
	/* value=1 is for loopback, otherwise 0 */
	int 					test_mode;
	int 					app;						//enum net_test_mode { scp, iperf }
	char 					*server;					//ip4 address format
	char 					*user;						//user name for server
	char 					*pass;						//password for server
	int 					time;						//time to run test
	char 					*folder_name;				//file name of the file to scp with server (including folder path)
	u64						file_size;					//file size of the file to scp with server (hex format)
	int						interval;					//Iperf interval
	int 					iperf_thread;				//Iperf number of threads
	char					*window_size;				//Iperf window size
	char					*lb_interface_name;			//for loopback
	char					lb_ip_addr[IFNAMSIZ];		//for loopback
	int						port;
	int						min_speed;					//iperf minimum speed accepted
	char					hw_addr[HARDWARE_ADDRESS_LENGTH];
	char					configured;
	pthread_mutex_t			iperf_mutex;
	char					iperf_server[IFNAMSIZ];
	int						threads;
	int						all_done;
	int						result;
	int						test_performance;
	char					route_table_name[30];
	char					default_gateway[IFNAMSIZ];
	/*int						(*execute)(struct vediag_net_test_info *detail_net_test_info);					//pointer to execute function*/
};

extern void free_net_test_info(struct vediag_net_test_info **info);
extern void dump_net_test_info(struct vediag_net_test_info *info);

#define _PATH_PROCNET_DEV				"/proc/net/dev"
#define NETWORK_SECTION_TOKEN 			"Network"
#define NETWORK_TOTAL_TOKEN				"Total"
#define NETWORK_DEVICE_TOKEN			"Device"
#define NETWORK_PHYSICAL_TOKEN			"Physical"
#define NETWORK_ARGUMENT_TOKEN			"Argument"
#define NETWORK_APP_TOKEN				"App"
#define NETWORK_SERVER_TOKEN			"Server"
#define NETWORK_USER_TOKEN				"User"
#define NETWORK_PASSWORD_TOKEN			"Password"
#define NETWORK_FILE_TOKEN				"Folder"
#define NETWORK_CHUNK_SIZE_TOKEN		"Chunk_Size"
#define NETWORK_THREAD_TOKEN			"Thread"
#define NETWORK_TIME_TOKEN				"Iperf_Time"
#define NETWORK_PORT_TOKEN				"Port"
#define NETWORK_INTERVAL_TOKEN			"Interval"
#define NETWORK_IPERF_THREAD_TOKEN		"Iperf_Thread"
#define NETWORK_WINDOWSIZE_TOKEN		"Windowsize"
#define NETWORK_IPERF_MIN_SPEED_TOKEN	"Iperf_Min_Speed"
#define NETWORK_LOOPBACK_TOKEN			"Loopback"
#define NETWORK_LB_SERVER_TOKEN			"Loopback_Server"
#define NETWORK_SCP_APP_NAME			"scp"
#define NETWORK_IPERF_APP_NAME			"iperf"
#define NETWORK_MODE_TOKEN				"Mode"
#define NETWORK_TEST_SIZE_TOKEN			"Test_Size"
#define NETWORK_TEST_TIME_TOKEN			"Test_Time"
#define NETWORK_PERFORMANCE_TIME_TOKEN	"Performance_Time"

#define DEFAULT_NETWORK_TEST_SIZE	(100 << 20)
#define DEFAULT_NETWORK_TEST_TIME	60
#define IP_ADDRESS_POLLING_TIME		5000	// msec
#define IP_ADDRESS_POLLING_MS_INTERVAL	1000
#define SCP_TOTAL_ARG_NUMBER		12
#define IPERF_SETUP_TIME_OFFSET		10
#define DEFAULT_IPERF_PORT			5002

#define DEFAULT_IPERF_INTERVAL		"20"
#define DEFAULT_IPERF_THREAED		"8"

#define IPERF_SERVER_LOG_FILE_PREFIX	"iperfserver"
#define IPERF_PROCESS_CHECK_FILE		IPERF_SERVER_LOG_FILE_PREFIX"_process.log"

/*#define DEFAULT_LOOPBACK_APP_IPERF	1*/

#define MELLANOX_100G_DRIVER_NAME 	"mlx5_core"
#define MELLANOX_100G_NAME 			"100GMLX"
#define MELLANOX_100G_MIN_SPEED		"55000"
#define MELLANOX_100G_PERF_TIME		"1200"

#define MELLANOX_10G_DRIVER_NAME 	"mlx4_core"
#define MELLANOX_10G_NAME 			"10GMLX"
#define MELLANOX_10G_MIN_SPEED		"9000"
#define MELLANOX_10G_PERF_TIME		"600"

extern int scp_test_execute(struct vediag_test_info *info);
extern int iperf_test_execute(struct vediag_test_info *info);
extern int loopback_port_2_port_settings(char *src_interface,
		char *src_addr, char *dst_interface, char *dst_addr, char *dummy_server);
extern int net_test_initial_configuration(struct vediag_net_test_info *net_info);
extern void clear_initial_config_flag(void);
extern int check_available_iperf_port(char *ip, int port);
extern int pid_of_command(char *command);
extern int get_iperf_result(FILE* stream, int runtime, int thread_num, int direction);


extern char* print_size_buf(unsigned long long size, const char *s);
#endif
