/*
 * utils.h
 *
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <linux/types.h>

extern int print_unknown_data(const u_char *cp, const char *ident, int len);
extern void ascii_print(const u_char *, u_int);
extern void hex_and_ascii_print_with_offset(const char *, const u_char *,
	u_int, u_int);
extern void hex_and_ascii_print(const char *, const u_char *, u_int);
extern void hex_print_with_offset(const char *, const u_char *, u_int, u_int);
extern void hex_print(const char *, const u_char *, u_int);

struct tok {
	int v;			/* value */
	const char *s;		/* string */
};

#define TOKBUFSIZE 128
extern const char *tok2strbuf(const struct tok *, const char *, int,
			      char *buf, size_t bufsize);

/* tok2str is deprecated */
extern const char *tok2str(const struct tok *, const char *, int);
extern const int tok2index(register const struct tok *lp, register const char *buf);
extern char *bittok2str(const struct tok *, const char *, int);
extern char *bittok2str_nosep(const struct tok *, const char *, int);
extern void safeputchar(int);
extern void safeputs(const char *, int);

extern void print_error(const char *fmt, ...);
extern void print_warning(const char *fmt, ...);

enum {
    UNIT_LEN = 32
};

double unit_atof( const char *s );
double unit_atof_rate( const char *s );
uint64_t unit_atoi( const char *s );
void unit_snprintf( char *s, int inLen, double inNum, char inFormat );

#endif /* UTILS_H_ */
