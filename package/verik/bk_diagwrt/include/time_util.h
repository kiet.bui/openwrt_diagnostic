/*
 * time_util.h
 *
 */

#ifndef TIME_UTIL_H_
#define TIME_UTIL_H_
#include <sys/time.h>

#define TIME_T_MAX	(time_t)((1UL << ((sizeof(time_t) << 3) - 1)) - 1)

extern int timeval_subtract(struct timeval *result, struct timeval *x,
		struct timeval *y);
double get_time_sec(void);

#endif /* TIME_UTIL_H_ */
