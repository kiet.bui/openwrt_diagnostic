
#ifndef _VEDIAG_EVENT_H_
#define _VEDIAG_EVENT_H_

#include <log.h>

struct event_test {
	char *name;
	//char *mode;
	int cycle;
	//int interval; /* ms */
	int timeout; /* sec */
	u32 count1, count2;
};

int event_test(struct event_test *test);
int event_count(struct event_test *test);

#endif /* _VEDIAG_EVENT_H_ */
