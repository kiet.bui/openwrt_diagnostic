
#ifndef _VEDIAG_AUDIO_H
#define _VEDIAG_AUDIO_H

#include <log.h>

enum {
	AUDIO_PROBE_TEST,
	AUDIO_MPG123_TEST,
	AUDIO_APLAY_TEST,
};


#endif /* _VEDIAG_AUDIO_H */
