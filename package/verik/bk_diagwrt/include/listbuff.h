/*
 * listbuff.h
 * @author Sy Nguyen
 */

#ifndef LISTBUFF_H_
#define LISTBUFF_H_

#include <malloc.h>
#include <string.h>

struct listbuff {
	struct listbuff *next, *prev;
	void *buff;
	int len;
	int allocated;
};

#define LISTBUFF_HEAD_INIT(name) { &(name), &(name) }

#define LISTBUFF_HEAD(name) \
		struct listbuff name = LISTBUFF_HEAD_INIT(name)

static inline void INIT_LISTBUFF_HEAD(struct listbuff *list)
{
	memset(list, 0, sizeof(struct listbuff));
	list->next = list;
	list->prev = list;
}

/**
 * @return the latest <code>listbuff</code> that was added into the list
 */
static inline struct listbuff* listbuff_get_last(struct listbuff *head)
{
	return head->prev;
}

/**
 * listbuff_for_each	-	iterate over a list
 * @pos:	the &struct listbuff to use as a loop cursor.
 * @head:	the head for your list.
 */
#define listbuff_for_each(pos, head) \
	for (pos = (head)->next; pos != (head); pos = pos->next)

/**
 * listbuff_for_each_prev	-	iterate over a list backwards
 * @pos:	the &struct listbuff to use as a loop cursor.
 * @head:	the head for your list.
 */
#define listbuff_for_each_prev(pos, head) \
	for (pos = (head)->prev; pos != (head); pos = pos->prev)

static inline void __listbuff_add(struct listbuff *new,
		struct listbuff *prev,
		struct listbuff *next)
{
	next->prev = new;
	new->next = next;
	new->prev = prev;
	prev->next = new;
}

static inline void listbuff_add(struct listbuff *new, struct listbuff *head)
{
	__listbuff_add(new, head, head->next);
}

static inline void listbuff_add_tail(struct listbuff *new,
		struct listbuff *head)
{
	__listbuff_add(new, head->prev, head);
}

static inline void __listbuff_del(struct listbuff *prev, struct listbuff *next)
{
	next->prev = prev;
	prev->next = next;
}

static inline void __listbuff_del_entry(struct listbuff *entry)
{
	__listbuff_del(entry->prev, entry->next);
}

static inline void listbuff_del(struct listbuff *entry)
{
	__listbuff_del(entry->prev, entry->next);
	entry->next = NULL;
	entry->prev = NULL;
}

static inline void listbuff_del_head(struct listbuff *head)
{
	struct listbuff *lst, *temp;
	for (lst = (head)->next; lst != (head);) {
		if (lst != head) {
			__listbuff_del_entry(lst);
			temp = lst;
			lst = lst->next;
			free(temp);
		}
	}
}

static inline struct listbuff *allocate_listbuff(int len)
{
	struct listbuff *buff = (struct listbuff *) malloc(sizeof(struct listbuff));
	INIT_LISTBUFF_HEAD(buff);
	buff->buff = malloc(len);
	memset(buff->buff, 0, len);
	buff->len = len;
	buff->allocated = 1;
	return buff;
}

static inline int size_of_listbuff_list(struct listbuff *lb)
{
	int size = 0;
	struct listbuff *tmp = lb;
	do {
		size += tmp->len;
		tmp = tmp->next;
	} while (tmp != lb);

	return size;
}

static inline struct listbuff *listbuff_all_in_one(struct listbuff *lb)
{
	int idx = 0;
	int size = size_of_listbuff_list(lb);
	struct listbuff *new = allocate_listbuff(size);
	struct listbuff *tmp = lb;
	do {
		memcpy(new->buff + idx, tmp->buff, tmp->len);
		idx += tmp->len;
		tmp = tmp->next;
	} while (tmp != lb);

	return new;
}

static inline void free_listbuff(struct listbuff **lb)
{
	struct listbuff *tmp = *lb;
	if ((tmp != NULL) && (tmp->allocated == 1)) {
		if (tmp->buff) {
			free(tmp->buff);
			tmp->buff = NULL;
		}
		tmp->allocated =  0;
		free(tmp);
		tmp = NULL;
		*lb = tmp;
	}
}

static inline void free_listbuff_list(struct listbuff **lb)
{
	struct listbuff *head = *lb;
	if (head == NULL) {
		return;
	}

	struct listbuff *tmp2;
	struct listbuff *tmp = head;
	do {
		tmp2 = tmp;
		tmp = tmp->next;
		free_listbuff(&tmp2);
	} while (tmp != head);
}

static inline struct listbuff *import_listbuff(const void *data, int len)
{
	struct listbuff *buff = allocate_listbuff(len);
	memcpy(buff->buff, data, len);
	return buff;
}

static inline int listbuff_length(struct listbuff *lb)
{
	int length = 0;
	struct listbuff *tmp = lb;
	do {
		length++;
		tmp = tmp->next;
	} while (tmp != lb);

	return length;
}

static inline char** listbuff_to_array(struct listbuff *head, int *arraysize)
{
	struct listbuff *tmp = head;
	int length = listbuff_length(head);
	char **argv = malloc(length * sizeof(char *));
	if (argv == NULL) {
		return argv;
	}

	*arraysize = 0;
	do {
		argv[*arraysize] = (char *) malloc(tmp->len);
		if (argv[*arraysize] == NULL) {
			break;
		}
		memcpy(argv[*arraysize], tmp->buff, tmp->len);
		tmp = tmp->next;
		*arraysize += 1;
	} while (tmp != head);

	return argv;
}

#endif /* LISTBUFF_H_ */
