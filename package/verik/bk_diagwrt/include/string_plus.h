/*
 * string_plus.h
 */

#ifndef STRING_PLUS_H_
#define STRING_PLUS_H_

#include <pthread.h> /*for mutex*/

char *get_word_in_string(char *input, char *delimiter, int windex);
int check_string_isdigit(char *input, int length);
int urandom(unsigned char *input, unsigned int length);
void strlwr(char *p);

#endif /* STRING_PLUS_H_ */
